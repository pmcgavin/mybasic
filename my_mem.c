#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* #include "my_mem.h" */

#ifdef DEBUG

#ifndef __SIZEOF_POINTER__
#define __SIZEOF_POINTER__ sizeof(char *)
#endif

void *my_malloc (size_t size)
{
  char *ptr;
  size_t fullsize = __SIZEOF_POINTER__ + 8  + size + 8;
  if ((ptr = malloc(fullsize)) != NULL) {
    memset (ptr, 0, fullsize);
    *((size_t *)ptr) = size;
    ptr += __SIZEOF_POINTER__ + 8;
    *(ptr - 8) = (char)0xab;
    *(ptr - 7) = (char)0x10;
    *(ptr - 6) = (char)0xe5;
    *(ptr - 5) = (char)0x23;
    *(ptr - 4) = (char)0x9a;
    *(ptr - 3) = (char)0x18;
    *(ptr - 2) = (char)0xaa;
    *(ptr - 1) = (char)0xab;
    *(ptr + size + 0) = (char)0xde;
    *(ptr + size + 1) = (char)0xad;
    *(ptr + size + 2) = (char)0xbe;
    *(ptr + size + 3) = (char)0xef;
    *(ptr + size + 4) = (char)0x12;
    *(ptr + size + 5) = (char)0x34;
    *(ptr + size + 6) = (char)0x56;
    *(ptr + size + 7) = (char)0x79;
    /*printf ("        Allocated %zu from %p .. %p\n", size, ptr, ptr + size - 1);*/
  }
  return (ptr);
}

void *my_realloc (void *ptr, size_t size)
{
  char *p = (char *)ptr;
  size_t fullsize, oldsize = *((size_t *)(p - (__SIZEOF_POINTER__ + 8)));
  /*printf ("        Reallocating %zu from %p .. %p\n", oldsize, p, p + size - 1);*/
  if (*(p - 8) != (char)0xab ||
      *(p - 7) != (char)0x10 ||
      *(p - 6) != (char)0xe5 ||
      *(p - 5) != (char)0x23 ||
      *(p - 4) != (char)0x9a ||
      *(p - 3) != (char)0x18 ||
      *(p - 2) != (char)0xaa ||
      *(p - 1) != (char)0xab)
    fprintf (stderr, "\nWRITE BEFORE START OF ALLOCATED MEMORY DETECTED!!!!!\n\n");
  if (*(p + oldsize + 0) != (char)0xde ||
      *(p + oldsize + 1) != (char)0xad ||
      *(p + oldsize + 2) != (char)0xbe ||
      *(p + oldsize + 3) != (char)0xef ||
      *(p + oldsize + 4) != (char)0x12 ||
      *(p + oldsize + 5) != (char)0x34 ||
      *(p + oldsize + 6) != (char)0x56 ||
      *(p + oldsize + 7) != (char)0x79)
    fprintf (stderr, "\nWRITE PAST END OF ALLOCATED MEMORY DETECTED!!!!!\n\n");
  p -= (__SIZEOF_POINTER__ + 8);
  fullsize = __SIZEOF_POINTER__ + 8  + size + 8;
  if ((p = realloc(p, fullsize)) != NULL) {
    *((size_t *)p) = size;
    p += __SIZEOF_POINTER__ + 8;
    *(p - 8) = (char)0xab;
    *(p - 7) = (char)0x10;
    *(p - 6) = (char)0xe5;
    *(p - 5) = (char)0x23;
    *(p - 4) = (char)0x9a;
    *(p - 3) = (char)0x18;
    *(p - 2) = (char)0xaa;
    *(p - 1) = (char)0xab;
    if (size > oldsize)
      memset (p + oldsize, 0, size - oldsize);
    *(p + size + 0) = (char)0xde;
    *(p + size + 1) = (char)0xad;
    *(p + size + 2) = (char)0xbe;
    *(p + size + 3) = (char)0xef;
    *(p + size + 4) = (char)0x12;
    *(p + size + 5) = (char)0x34;
    *(p + size + 6) = (char)0x56;
    *(p + size + 7) = (char)0x79;
    /*printf ("        Reallocated %zu from %p .. %p\n", size, (char *)ptr,
      ((char *)ptr) + size - 1);*/
  }
  return (p);
}

void *my_free (void *ptr)
{
  char *p = (char *)ptr;
  size_t size = *((size_t *)(p - (__SIZEOF_POINTER__ + 8)));
  /*printf ("        Freeing %zu from %p .. %p\n", size, p, p + size - 1);*/
  if (*(p - 8) != (char)0xab ||
      *(p - 7) != (char)0x10 ||
      *(p - 6) != (char)0xe5 ||
      *(p - 5) != (char)0x23 ||
      *(p - 4) != (char)0x9a ||
      *(p - 3) != (char)0x18 ||
      *(p - 2) != (char)0xaa ||
      *(p - 1) != (char)0xab)
    fprintf (stderr, "\nWRITE BEFORE START OF ALLOCATED MEMORY DETECTED!!!!!\n\n");
  if (*(p + size + 0) != (char)0xde ||
      *(p + size + 1) != (char)0xad ||
      *(p + size + 2) != (char)0xbe ||
      *(p + size + 3) != (char)0xef ||
      *(p + size + 4) != (char)0x12 ||
      *(p + size + 5) != (char)0x34 ||
      *(p + size + 6) != (char)0x56 ||
      *(p + size + 7) != (char)0x79)
    fprintf (stderr, "\nWRITE PAST END OF ALLOCATED MEMORY DETECTED!!!!!\n\n");
  p -= (__SIZEOF_POINTER__ + 8);
  free (p);
  return NULL;
}

#endif /* DEBUG */
