# mybasic

mybasic is a BASIC compiler/interpreter that I wrote in about 1988 as an exercise in ANSI C.

It implements many/most non-graphical features of early 8-bit Basic interpreters such as gwbasic and early Microsoft Basic.

My experience at the time was with DEC Focal, HP2100 Time-Shared BASIC, Sinclair ZX81 BASIC, Hitachi Level 3 BASIC and AmigaBASIC.  mybasic is probably closest to Hitachi Level 3 BASIC.

Originally developed on a Commodore Amiga, mybasic has low memory usage, typically less than 2 MB, and its only dependencies are libc and libm.  Mybasic could be a useful scripting language for embedded devices, especially for programmers already familiar with early BASIC implementations.

Internally, mybasic compiles (in milliseconds or less) to a bytecode representation that it then interprets.  That makes mybasic an order of magnitude faster than an ordinary Basic interpreter, while retaining portability.  And because it compiles BASIC programs so quickly, without writing anything to disk, the development debug/test cycle typically is faster than using a BASIC to C++ to machine code compiler.

Peter McGavin.

------------------------------------------------------------------------
#### Building mybasic from source code

To build under Linux, under Windows with cygwin or under Windows WSL Ubuntu, you'll need the "clang" or "gcc" (or "gcc-core"), "binutils", "make" and optionally "libreadline-dev" packages pre-installed.  For example:

    sudo apt-get install git clang gcc binutils make libreadline-dev

then:

    git clone https://bitbucket.org/pmcgavin/mybasic.git
    cd mybasic
    make

To run a mybasic program, at the command prompt, type ./mybasic program.bas

Some example mybasic BASIC programs are in the basic_examples folder.  For example:

    ./mybasic basic_examples/pi8.bas

------------------------------------------------------------------------
#### Pre-compiled binaries

Instead of building mybasic from source code, you might be able to use a pre-built one.

There are some pre-built optimized mybasic executables for various platforms and architectures under the binaries folder.  For example, to run the pi8.bas example with the optimized 64-bit Linux mybasic:

    ./binaries/linux/x86_64/mybasic basic_examples/pi8.bas

To install a pre-compiled binary, simply copy an appropriate one to a directory in your OS's PATH.

    cp binaries/linux/x86_64/mybasic ~/bin/

or (for example):

    sudo cp binaries/linux/x86_64/mybasic /usr/local/bin/

------------------------------------------------------------------------
#### Linux or Windows/Cygwin or Windows/WSL Command-Line Usage:

mybasic is not interactive.

You need to prepare a file of BASIC commands in a file using your favourite text editor.

It's best to give the file name extension ".bas".

Then run your BASIC program in a Linux or Cygwin terminal window as follows:

    /full/path/to/mybasic mybasicprogram.bas

If the mybasic executable file is in the current directory, then you only need:

    ./mybasic mybasicprogram.bas

If you copy the mybasic executable to a directory that is in your operating system's PATH, such as /usr/local/bin or ~/bin, then you only need:

    mybasic mybasicprogram.bas

It's possible to set things up so that you only need to type:

    ./mybasicprogram.bas

To do the above, first copy the mybasic executable to a directory that is in your operating system's PATH, such as /usr/local/bin/, ~/bin/, or ~/.local/bin/

    cp -p mybasic ~/.local/bin/

or:

    sudo cp -p mybasic /usr/local/bin/

Second, using your text editor, ensure the first (top) line in your BASIC program file is:

    #!/usr/bin/env mybasic

Finally, set the executable file attribute on your BASIC program file, i.e.,

    chmod +x mybasicprogram.bas

If you follow the above steps and put your BASIC program file in a directory that is in your operating system's PATH, then you only need to type:

    mybasicprogram.bas

The .bas file name extension is optional, so you could rename mybasicprogram.bas to mybasicprogram and type, simply:

    mybasicprogram

------------------------------------------------------------------------
#### Windows

The Cygwin version of mybasic can be run from a Windows Command Prompt (outside of Cygwin) provided the full path to the mybasic executable is given, e.g:

    c:\cygwin64\home\myusername\mybasic\mybasic.exe mybasicprogram.bas

If you get an error about cygwin1.dll not found, then

either: Add the directory containing cygwin1.dll to the Windows PATH environment variable (recommended), i.e,

    Control Panel -> System -> Advanced system settings -> Advanced tab -> Environment Variables -> "PATH" -> Edit -> append ";c:\cygwin64\bin" (without quotes)

or: Copy cygwin1.dll to the same directory as mybasic.exe, i.e,

    copy c:\cygwin64\bin\cygwin1.dll c:\cygwin64\home\myusername\mybasic\

In the latter case, pleas note that if/when you wish to rebuild mybasic, you might have to delete the copy of cygwin1.dll again.  I have had issues with mybasic failing to compile when cygwin1.dll resides in the same directory.

To set up Windows to run a BASIC program simply by double-clicking a BASIC program's icon, set the "Open with" property of the .bas extension to the full path to mybasic.exe

    Right-click a BASIC program's icon and select Properties
        Next to "Open with" click Change...
        Click "More apps"
        Scroll to the bottom of the list and "Look for another app on this PC"
        Navigate to the directory containing mybasic.exe, e.g, This PC -> C: -> cygwin64 -> home -> myusername -> mybasic
        Select mybasic.exe
        Click Open
        Click OK

Note that this changes the "Open with" property for all files with the .bas extension.

As before, if you get an error about cygwin1.dll not found, copy cygwin1.dll to the same directory as mybasic.exe, e.g,

    copy c:\cygwin64\bin\cygwin1.dll c:\cygwin64\home\myusername\mybasic\

------------------------------------------------------------------------
#### Native Windows

Using MinGW gcc or MSVC CL, it is now possible to build a native Windows version of mybasic, i.e, one that doesn't require Cygwin nor WSL.

To build a native version of mybasic.exe with MinGW gcc at a Windows Command Prompt, use the following command:

    gcc -ansi -DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=short -Wall -o mybasic.exe mybasic.c runtime.c mfile.c windows.c -Os -s -lm

To build a native version of mybasic.exe with MSVC CL at a Windows Command Prompt, install Microsoft Developer Command Prompt for VS and use the following command:

    cl -DNOALIGN -Wall -o mybasic.exe mybasic.c runtime.c mfile.c windows.c -Os

If you are experienced with Windows Visual Studio, you could create a project, add the appropriate source files, set defines in project settings, and build.  It's possible to compile a native version of mybasic with cl, gcc, icx or clang this way.

Please note that, performance-wise, I recommend the Cygwin or WSL versions.

------------------------------------------------------------------------
#### Cross-compile on Linux for MS-DOS

I tested with djgpp cross-compiler on Linux downloaded from https://github.com/andrewwutw/build-djgpp/releases

    /usr/local/djgpp/bin/i586-pc-msdosdjgpp-gcc -DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=short -o mybasic.exe -Os mybasic.c runtime.c mfile.c msdos.c -s -lm

The resulting mybasic.exe runs in protected mode, which requires at least an 80386.  Tested with MS-DOS 6.22 in VirtualBox and in dosbox.

Programs compiled with djgpp require a DPMI server in MS-DOS.  I use the freely available CWSDPMI.EXE placed in the same directory as MYBASIC.EXE

Any programs compiled with djgpp don't seem to work in dosemu, at least not in dosemu 1.4 that I tried.

------------------------------------------------------------------------
#### Compile natively in MS-DOS

Compile with OpenWatcom for DOS on an MSDOS host:

    wcl -l=dos -cc -0 -bc -d0 -mm -fpi -k8192 -dNOALIGN mybasic.c runtime.c mfile.c msdos.c

In this case, the resulting mybasic.exe runs in real mode on an 8086 and up, but BASIC programs with more than 64 kB of BASIC variables and arrays may crash.

Or compile with Borland Turbo C 3.0 on an MSDOS host:

    tcc -ml -O -G- -w- -d -DNOALIGN -emybasic mybasic.c runtime.c mfile.c my_mem.c msdos.c

12 Feb 2024 Update: The latest version of mybasic seems to be too big for Turbo C to compile.  There is a OpenWatcom executable for mybasic under binaries/msdos/8086/

------------------------------------------------------------------------
#### Amiga

To build using pre-installed SAS/C on an Amiga, type smake.  Or use the SAS/C command:

    sc optimize opttime optsched nodebug nostackcheck ansi parms=registers cpu=any math=standard verbose stringmerge smalldata noicons code=far data=near define=_STRICT_ANSI memorysize=huge link programname mybasic mybasic.c runtime.c mfile.c amiga.c

Optionally replace cpu=any with cpu=68020, cpu=68030, cpu=68040 or cpu=68060.

Optionally replace math=standard with math=ffp, math=ieee or math=68881.

You'll need lots of Amiga RAM, say 12 MB or more, to compile mybasic on an Amiga, but mybasic itself doesn't need much RAM to run.

------------------------------------------------------------------------
#### RISCOS

Cross-compile on Linux with RISCOS gccsdk:

    ~/gccsdk/env/ro-path gcc -ansi -Wall -o mybasic -Os -DNDEBUG mybasic.c runtime.c mfile.c unix.c -s -lm

Copy the resulting mybasic executable to a RISCOS system and change its file-type to ELF.  Use it in RISCOS from a command-line.

The -DNOALIGN option would cause a crash.

Experimentally, threaded code appears to be slower than not threaded on RISCOS.

------------------------------------------------------------------------
#### Advanced build options

The base command for building mybasic on Unix or Linux target platforms is:

    clang -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lm

An alternative C compiler may be substituted for clang.  For examples: gcc, icx, icc, djgpp, cl, tcc, sc

Replace unix.c with windows.c, msdos.c or amiga.c for a Windows, MSDOS or Amiga target platform respectively.

The following optional C compiler options usually result in a faster BASIC compiler/interpreter:

    -DNOALIGN
    -DTHREADED -DTHREADED_BCODE_TYPE=short
    -DNDEBUG
    -mcpu=native -march=native -mtune=native

If you have libreadline-dev, add the following C compiler options to enable full line editing and TAB filename completion in mybasic INPUT and LINE INPUT statements:

    -DHAVE_READLINE=1 -lreadline

For example:

    clang -DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=short -DNDEBUG -DHAVE_READLINE=1 -mcpu=native -march=native -mtune=native -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

------------------------------------------------------------------------
#### Threaded code

Depending on the compiler and platform, performance of mybasic programs may (or may not) be increased by adding the option -DTHREADED to the compilation line.

This exploits a clang/gcc extension called "Labels as values" to shortcut the amount of processing required to run a BASIC program.

Several other C compilers implement the "Labels as values" extension.  These include Intel icc and Intel icx.

As far as I can tell, neither Microsoft MSVC nor SAS/C implement this extension.

The option -DTHREADED_BCODE_TYPE=short produces a further speedup, but it only works if the label offsets all fit in 16 bits.  If they don't, mybasic will tell you at run-time.

For examples:

    clang -DTHREADED -DTHREADED_BCODE_TYPE=short -DHAVE_READLINE=1 -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

    gcc -DTHREADED -DTHREADED_BCODE_TYPE=short -DHAVE_READLINE=1 -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

    icc -DTHREADED -DTHREADED_BCODE_TYPE=short -DHAVE_READLINE=1 -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

    icx -DTHREADED -DTHREADED_BCODE_TYPE=short -DHAVE_READLINE=1 -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

In my experience, icc with -DTHREADED generates the fastest code of all on x86_64 systems I tested.  Sadly, icc is now deprecated in favour of icx which generates slower code than gcc.

------------------------------------------------------------------------
#### Data alignment

By default, mybasic aligns variables to byte boundaries equivalent to data size.  For example, all double-precision float variables are aligned to 8-byte boundaries.  Some older processors require this, but most recent ones do not.  The compile option -DNOALIGN can save some memory and generate faster code, but could cause an exception on older processors.  For example:

    gcc -DNOALIGN -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

------------------------------------------------------------------------
#### Optimisation

Some experiments with various C compilers and options suggest the following compiler/options produce about the fastest code:

Linux x86_64: Intel OneAPI 2021.2:

    icc -DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=short -DHAVE_READLINE=1 -mcpu=native -march=native -mtune=native -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

Windows x86_64: Cygwin clang 5.0.1:

    clang -DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=short -DHAVE_READLINE=1 -mcpu=native -march=native -mtune=native -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

Debian 11 aarch64 (Raspberry Pi 400): clang 11.0.1:

    clang -DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=short -DHAVE_READLINE=1 -mcpu=native -mtune=native -ansi -Wall -o mybasic mybasic.c runtime.c mfile.c unix.c -Os -s -lreadline -lm

Note that, in the case of mybasic, -Os generally produces faster code than -O2 and -O3.

------------------------------------------------------------------------
#### Some idiosyncracies:

BASIC line numbers are optional.  Labels may be used instead of line numbers.  For example:

      x = 1
    mylabel:
      x = x * x + 1
      IF x < 1000 THEN mylabel
      PRINT x

There are 5 data types:

    SNG  32-bit float     -10^38 .. 10^38       approx 7 significant digits
    DBL  64-bit float     -10^308 .. 10^308     approx 15 significant digits
    INT  16-bit integer   -32768..32768 (-2147483648 to 2147483647 on a 64-bit system)
    LNG  32-bit integer   -2147483648..2147483647 (-9223372036854775808..9223372036854775807 on a 64-bit system)
    STR  string           "...characters..."

By default, variables are of type SNG, i.e, DEFSNG A-Z

The type of a constant number is determined by the number of digits and whether or not it contains a decimal point.

These can be overridden by appending one of the following characters to a variable name or to a number:

    "!"    SNG  32-bit float
    "#"    DBL  64-bit float
    "%"    INT  16-bit integer
    "&"    LNG  32-bit integer
    "$"    STR  string

or by using the DEFxxx commands.

Note that A%, A&, A!, A# and A$ are separate, independent variables.

The variable A is the same as A! unless the default type for variables starting with A is changed with DEFINT, DEFLNG, DEFSNG, DEFDBL or DEFSTR.

A simple variable and an array variable can have the same name, but they are different.

In the interest of speed, array dimensions must be constant, arrays can only be dimensioned once and this must be done before the array is used.

Lines starting with "#" or "'" (single quote) are treated as comments.

"?" (question mark) is a synonym for PRINT.

Use / for floating divide and \ for integer divide.

The file number in OPEN, CLOSE, PRINT, INPUT, LINEINPUT, POS, LOF, EOF and INPUT$ no longer needs to be prefixed with "#".

CLS, LOCATE and COLOR are implemented by PRINTing ANSI escape sequences to the standard output.  Linux terminals and consoles generally interpret these correctly by default.  The native Windows version of mybasic enables virtual terminal processing and sets code page 65001 (UTF-8) for compatibility, then restores original settings on exit.

Extended Unicode UTF-8 characters, such as foreign language characters, box drawing characters, Braille dots, chess piece characters, card suit characters and so on, generally work when PRINTed to Linux and Cygwin terminal windows.  But Windows Command Prompt, Windows PowerShell and the default Windows WSL Ubuntu Terminal do not appear to implement the full set of fonts.

Colours are standard ANSI: Colours 0-7 are black, red, green, yellow, blue, maganta, cyan and white.  Colours 8-15 are bright versions of these on systems that support tham.

------------------------------------------------------------------------
#### Quick reference of BASIC commands, functions and operators:

        ABS(number)
        AND
        ASC(string)
        ATN(number)
        BEEP
        BIN$(number)
        CDBL(number)
        CHAIN basic-filename-string
        CHDIR string
        CHR$(number)
        CINT(number)
        CLEAR
        CLNG(number)
        CLOSE(#file-number)
        CLS
        COLOR [number][,number]
        COMMAND$[(number)]
        _COMMANDCOUNT
        COS(number)
        CSNG(number)
        CVD(string)
        CVI(string)
        CVL(string)
        CVS(string)
        DATA constant[,constant]...
        DATE$
        DECR(variable)
        DEF FNname[%|&|!|#|$][(arg,...)] = expression
        DEF[INT|LNG|SNG|DBL|STR] letter[-letter](,letter[-letter])...
        DIM variable[(elements...)][,variable[(elements...)]]...
        DO WHILE|UNTIL expression : statements... : LOOP
        DO : statements... : LOOP WHILE|UNIL expression
        ELSE
        ELSEIF
        END
        END IF
        EOF(#file-number)
        ERL
        ERR
        ERROR number
        EXIT FOR|DO
        EXP(number)
        FIX(number)
        FOR counter = start TO finish [STEP increment]
        GETENV$(string)
        GOSUB line-number|label
        GOTO line-number|label
        HEX$(number)
        IF expression GOTO [line-number|label] [ELSE line-number|label|statement]
        IF expression THEN [line-number|label|statement] [ELSE line-number|label|statement]
        INCR(variable)
        INKEY$
        INPUT [#file-number;]["prompt string";]list of variables
        INPUT$(number,#file-number)
        INSTR([start-position,]string-searched,string-pattern)
        INT(number)
        KILL string
        LBOUND(array)
        LCASE$(string)
        LEFT$(string,number-of-spaces)
        LEN(string)
        [LET] variable=expression
        [LET] MID$(string-variable,start-position-in-string[,number-of-chars])=string-expression
        LINEINPUT [#file-number,]|["prompt string";]string-variable
        LOCATE row-number,column-number
        LOF(#file-number)
        LOG(number)
        LOG10(number)
        LOG2(number)
        LTRIM$(string)
        MID$(string,start-position-in-string[,number-of-chars])
        MKD$(number)
        MKDIR string
        MKI$(number)
        MKL$(number)
        MKS$(number)
        MOD
        NAME string,string
        NEXT [counter][,counter]...
        NOT
        OCT$(number)
        ON expression GOTO|GOSUB line-number|label[,line-number|label]...
        ON ERROR GOTO 0|line-number|label
        OPEN string FOR INPUT|OUTPUT|APPEND AS #file-number
        OPTION BASE 0|1
        OR
        POS(#file-number)
        PRINT [#file-number,][USING format-string;]expressions...
        RANDOMIZE [number|TIMER]
        READ variable[,variable]...
        REM comment
        RESTORE [line-nymber|label]
        RESUME [NEXT|0|line-number|label]
        RETURN [line-number|label]
        RIGHT$(string,number-of-chars)
        RINSTR([start-position,]string-searched,string-pattern)
        RMDIR string
        RND[(number)]
        RTRIM$(string)
        SGN(number)
        SHELL string
        SIN(number)
        SPACE$(number)
        SPC(number)
        SQR(number)
        STOP
        STR$(number)
        SWAP variable,variable
        SYSTEM
        TAB(number)
        TAN(number)
        TIME$
        TIMER
        TRIM$(string)
        UBOUND(array)
        UCASE$(string)
        VAL(string)
        WEND
        WHILE expression
        XOR
