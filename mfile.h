#include <sys/types.h>

struct mfile {
  char *data;
  long int pos;
  size_t size;
  size_t allocated_size;
};

typedef struct mfile MFILE;

extern MFILE *mopen (void);

extern size_t mread (void *ptr, size_t size, size_t n, MFILE *stream);

extern size_t mwrite (void *ptr, size_t size, size_t n, MFILE *stream);

extern int mseek (MFILE *stream, long int off, int whence);

extern long int mtell (MFILE *stream);

extern void mrewind (MFILE *stream);

extern void *mclose (MFILE *stream);
