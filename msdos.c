#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <conio.h>

#include "mybasic.h"
#include "my_mem.h"

/****************************************************************************/

static char tmp_string[MAX_FNAME_LENGTH];

/****************************************************************************/

int change_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return chdir (tmp_string);
}

/****************************************************************************/

int make_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return mkdir (tmp_string, 0755);
}

/****************************************************************************/

int remove_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return rmdir (tmp_string);
}

/****************************************************************************/

static int tty_state = 0; /* 0 = normal, 1 = buffersize=1byte & echo-off */

static void reset_keypress (void)
{
  if (tty_state == 1) {

    tty_state = 0;
  }
}

void init_tty (void)
{
  static BOOL first = TRUE;

  if (isatty (fileno (stdin)) && tty_state == 0) {
    if (first) {
      atexit (reset_keypress);
      first = FALSE;
    }
    tty_state = 1;
  }
}

/****************************************************************************/

int tty_getc (FILE *f)
{
  int c;

  fflush (stdout);
  if (f == stdin && isatty (fileno (f)))
    c = getch ();
  else
    c = fgetc (f);
  if (c != EOF && isatty (fileno (f))) {
    if (c == '\r')
      c = '\n';
  }
  return c;
}

/****************************************************************************/

size_t tty_getline (struct string_descr *prompt, char **buf, size_t *bufsiz,
                    FILE *f, BOOL *error)
/* getline(buf,bufsiz,f) with prompt, echo & editing if(isatty) */
{
  BOOL is_a_tty;
  char *ptr, *eptr;
  FILE *g;    /* file to write prompt and echo to */
  int c;

  *error = FALSE;
  
  is_a_tty = isatty (fileno (f));
  g = (f == stdin) ? stdout : f;
  if (is_a_tty) {
    if (prompt != NULL && prompt->length > 0) {
      if (fwrite (prompt->body, 1, prompt->length, g) != prompt->length)
        fprintf (stderr, "Error writing input prompt");
      fflush (g);
    }
  }
  if (*buf == NULL || *bufsiz == 0) {
    *bufsiz = BUFSIZ;
    if ((*buf = malloc (*bufsiz)) == NULL) {
      *error = TRUE;
      return 0;
    }
  }
  for (ptr = *buf, eptr = *buf + *bufsiz;;) {
    if (is_a_tty)
      c = tty_getc(f);
    else
      c = fgetc (f);
    if (c == -1) {
      if (feof (f))
        return ptr == *buf ? -1 : ptr - *buf;
      else {
        *error = TRUE;
        return 0;
      }
    }
    if (is_a_tty) {
      fputc (c, g);  /* echo */
      if (c == '\b' || c == '\x7f') { /* backspace or rubout */
        if (ptr > *buf) {
          if (c == '\x7f')
            fputc ('\b', g);
          fputc (' ', g);
          fputc ('\b', g);
          --ptr;
        }
        fflush (g);
        continue;
      }
      fflush (g);
    }
    *ptr++ = c;
    if (c == '\n') {
      *ptr = '\0';
      return ptr - *buf;
    }
    if (ptr + 2 >= eptr) {
      char *nbuf;
      size_t nbufsiz = *bufsiz * 2;
      size_t d = ptr - *buf;
      if ((nbuf = realloc (*buf, nbufsiz)) == NULL) {
        *error = TRUE;
        return 0;
      }
      *buf = nbuf;
      *bufsiz = nbufsiz;
      eptr = nbuf + nbufsiz;
      ptr = nbuf + d;
    }
  }
}

/****************************************************************************/

size_t tty_fread (void *buf, size_t bsize, size_t n, FILE *f)
{
  size_t ret, i;
  char *b;

  if (isatty (fileno (f))) {
    b = buf;
    for (ret = 0; ret < n; ret++)
      for (i = 0; i < bsize; i++)
        *b++ = getch();
  } else
    ret = fread (buf, bsize, n, f);
  return ret;
}

/****************************************************************************/

#define NOBLOCK

int inkey (struct string_descr *str)
{

  fflush (stdout);
#ifdef NOBLOCK
  if (isatty (fileno (stdin))) {
    if (kbhit()) {
      str->body[0] = getch();
      str->length = 1;
    } else
      str->length = 0;
  } else {
    str->body[0] = getchar();
    str->length = 1;
  }
#else
  str->body[0] = getchar();
  str->length = 1;
#endif
  return 1;
}

/****************************************************************************/

double get_timer (void)
{
  return ((double)(clock())) / CLOCKS_PER_SEC;
}

/****************************************************************************/

BOOL shell_cmd (struct string_descr *str)
{
  char *tmp_string = NULL;
  int result;

  fflush (stdout);
  if ((tmp_string = my_malloc (str->length + 1)) == NULL)
    return FALSE;
  memcpy (tmp_string, str->body, str->length);
  tmp_string[str->length] = '\0';
  result = system (tmp_string);
  tmp_string = my_free (tmp_string);
  return result == 0;
}

/****************************************************************************/

void sleep_delay (double seconds)
{
  clock_t endclock = clock() + (clock_t)(CLOCKS_PER_SEC * seconds);
  fflush (stdout);
  while (clock() < endclock && !kbhit()) {
#ifdef __TURBOC__
    asm hlt; /* halt cpu until the next external interrupt */
#endif
#ifdef __GCC__
    asm ("hlt"); /* halt cpu until the next external interrupt */
#endif
    /* do nothing */;
  }
}

/****************************************************************************/
