# Detect whether clang is available, if not, fall back to gcc
ifeq "$(shell clang --version > /dev/null 2>&1 && echo $$? )" "0"
   CC = clang -ansi -Wall
else
   CC = gcc -ansi -Wall
endif
#CC = icc -ansi
#CC = icx -ansi

# Detect platform and test whether libreadline is available
PLATFORM=unix
IREADLINE=without readline
UNAME=$(shell uname)
ifeq '$(OS)' 'Windows_NT'
   ifneq '$(findstring CYGWIN,$(UNAME))' 'CYGWIN'
      PLATFORM=windows
   endif
endif
ifeq '$(PLATFORM)' 'unix'
   $(shell rm -f rtmp.c)
   $(shell echo '#include <stdio.h>' > rtmp.c)
   $(shell echo '#include <readline/readline.h>' >> rtmp.c)
   $(shell echo '#include <readline/history.h>' >> rtmp.c)
   $(shell echo 'int main(){char *p=NULL;readline(p);return 0;}' >> rtmp.c)
   ifeq "$(shell $(CC) -x c -O0 -o rtmp rtmp.c -lreadline > /dev/null 2>&1 && echo $$? )" "0"
      IREADLINE=with readline
      DREADLINE=-DHAVE_READLINE=1
      LREADLINE=-lreadline
      SREADLINE=-l:libreadline.a -l:libtinfo.a
   endif
   $(shell rm -f rtmp.c rtmp rtmp.exe)
endif

# Default to unaligned, threaded code
DEFS = -DNOALIGN $(DREADLINE)
#DEFS = $(DREADLINE)
THREADED = -DTHREADED -DTHREADED_BCODE_TYPE=short

all: mybasic
#all: mybasic mybasic_debug mybasic_opt mybasic_static

#-----------------------------------------------------------------------

mybasic: mybasic.c runtime.c mfile.c $(PLATFORM).c my_mem.c mybasic.h runtime.h my_mem.h Makefile
	# Compiling mybasic release version for $(PLATFORM) $(IREADLINE)
	$(CC) -o mybasic -Os $(DEFS) $(THREADED) -DNDEBUG mybasic.c runtime.c mfile.c $(PLATFORM).c -s $(LREADLINE) -lm

#-----------------------------------------------------------------------

mybasic_debug: mybasic.c runtime.c mfile.c $(PLATFORM).c my_mem.c mybasic.h runtime.h my_mem.h Makefile
	# Compiling mybasic_debug version for $(PLATFORM) $(IREADLINE)
	$(CC) -o mybasic_debug -O0 -g -fbounds-check -fstack-check -DDEBUG $(DEFS) mybasic.c runtime.c mfile.c $(PLATFORM).c my_mem.c $(LREADLINE) -lm

#-----------------------------------------------------------------------

OPTIONSGEN=-DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=int -DNDEBUG
OPTIONSUSE=-DNOALIGN -DTHREADED -DTHREADED_BCODE_TYPE=short -DNDEBUG
#CCOPT=clang-9 -ansi -Wall
#LLVM_PROFDATA=/usr/bin/llvm-profdata-9
CCOPT=icx -ansi -Wall
LLVM_PROFDATA=llvm-profdata
mybasic_opt: mybasic.c runtime.c mfile.c $(PLATFORM).c my_mem.c mybasic.h runtime.h my_mem.h Makefile
	# Compiling mybasic_opt native optimised version for $(PLATFORM) $(IREADLINE)
	rm -rf profdir
	mkdir profdir
	$(CCOPT) -fprofile-generate=profdir -o mybasic_opt -Os $(OPTIONSGEN) $(DEFS) $(THREADED) mybasic.c runtime.c mfile.c $(PLATFORM).c my_mem.c $(LREADLINE) -lm
	echo 1000 | ./mybasic_opt ./basic_examples/pi8.bas
	echo 1000 | ./mybasic_opt ./basic_examples/pi8cse.bas
	echo 1000 | ./mybasic_opt ./basic_examples/pi8csei.bas
	./mybasic_opt ./basic_examples/mandelbrot.bas
	./mybasic_opt ./basic_examples/battle_royale.bas
	echo 1000000 | ./mybasic_opt ./basic_examples/correlation.bas
	#$(LLVM_PROFDATA) merge -output=profdir/default.profdata profdir/*.profraw
	$(CC) -fprofile-use=profdir -o mybasic_opt -Os $(OPTIONSUSE) $(DEFS) $(THREADED) mybasic.c runtime.c mfile.c $(PLATFORM).c my_mem.c $(LREADLINE) -lm -s
	#rm -rf profdir

#-----------------------------------------------------------------------

mybasic_static: mybasic.c runtime.c mfile.c $(PLATFORM).c my_mem.c mybasic.h runtime.h my_mem.h Makefile
	# Compiling mybasic static release version for $(PLATFORM) $(IREADLINE)
	$(CC) -o mybasic_static -Os $(DEFS) $(THREADED) -DNDEBUG mybasic.c runtime.c mfile.c $(PLATFORM).c $(SREADLINE) -s -lm
