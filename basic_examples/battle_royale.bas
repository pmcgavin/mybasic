#!/usr/bin/env mybasic

print "Matt Heffernan's 'Battle Royale' Mandelbrot Basic benchmark"
print "https://www.youtube.com/watch?v=DC5wi6iv9io"
print
print " Sinclair ZX-Spectrum               4:33 min"
print " Commodore 64                       3:23 min"
print " Apple II                           2:45 min"
print " Atari 800                          2:16 min"
print " Amstrad CPC 464                    1:39 min"
print " BBC Micro                          1:37 min"
print " ZX Spectrum Next at 8x turbo mode    34 sec"
print " Commander X16                        23 sec"
print

e$=chr$(27)
dim c$(15)
c$(0)="[41m"
c$(1)="[42m"
c$(2)="[43m"
c$(3)="[44m"
c$(4)="[45m"
c$(5)="[46m"
c$(6)="[47m"
c$(7)="[100m"
c$(8)="[101m"
c$(9)="[102m"
c$(10)="[103m"
c$(11)="[104m"
c$(12)="[105m"
c$(13)="[106m"
c$(14)="[107m"
c$(15)="[40m"

t0 = timer
for py = 0 to 21
  for px = 0 to 31
    xz = px * 3.5 / 32 - 2.5
    yz = py * 2 / 22 - 1
    x = 0
    y = 0
    for i = 0 to 14
      if x * x + y * y > 4 then goto done
      xt = x * x - y * y + xz
      y = 2 * x * y + yz
      x = xt
    next i
done:
     ' vpoke bla, i
     print e$; c$(i); "  ";
  next px
  print
next py

t1 = timer
print e$; "[0mTime taken ="; t1 - t0; "sec"

end
