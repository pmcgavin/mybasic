1 REM SAVAGE benchmark
5 REM RADIANS //the calculation is performed with the angle mode in radians.
6 DEFDBL A:DEFINT I:DEFSNG T
8 T0=TIMER
10 A=1
20 FOR I=1 TO 2499
30 A=TAN(ATN(EXP(LOG(SQR(A*A)))))+1
40 NEXT I
45 T1=TIMER
50 PRINT A,T1-T0
