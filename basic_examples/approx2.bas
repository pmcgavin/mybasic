defdbl a-h,o-z
deflng i-n

input "Enter number to be approximated: ", x
b = x
for j = 1 to 100000
  i = clng(j * x)
  y = cdbl(i) / cdbl(j)
  r = abs(y - x)
  if r < b then
    print i; "/"; j; "="; y
    b = r
  end if
next j
end
