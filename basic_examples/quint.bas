5 REM https://www.youtube.com/watch?v=WYPNjSoDrqw 28:09
10 DEF FNF(X) = X^5 + 2*X^3 - 1
20 READ N
30 LET A = 0
40 LET B = 1
50 LET X = .5
60 FOR I = 1 TO N
70 IF FNF(X) > 0 THEN GOTO 100
80 LET A = X
90 GOTO 150
100 LET B = X
150 LET X = (A+B)/2
200 NEXT I
210 PRINT X, FNF(X)
300 DATA 20
999 END
