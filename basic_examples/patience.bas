
  REM CLEAR 300,&H6FFF: SCREEN 1: WIDTH 80
  REM IF PEEK(&H7002)<>&H88 THEN LOADM "udc"
  DEFINT A-Z: RANDOMIZE TIMER
  DIM BD(18,6),BF(6),BN(6),PI(3),CD(51),CU(23)
  GOSUB InitPics
  DEF FNV$(V)=MID$("A23456789TJQK",V,1)
  DEF FNS$(S)=MID$("CSDH",S+1,1)
  DEF FNC$(S)=CHR$(27)+"[3"+MID$("51",S\2+1,1)+"m"
  DEF FNA$(V,S)=FNC$(S)+FNV$(V)+FNS$(S)
  C3$=CHR$(27)+"[33m"
  C6$=CHR$(27)+"[36m"
  C7$=CHR$(27)+"[37m"

NewGame:
  ' Start new game
  GOSUB Shuffle
  FOR S=0 TO 3: PI(S)=0: NEXT
  COLOR 1: FOR CO=0 TO 6: LOCATE 1,9*CO+3: PRINT CO+1;: BF(CO)=6-CO: BN(CO)=7-CO: NEXT
  LOCATE 1,67: COLOR 4: PRINT "PATIENCE"
  FOR RO=0 TO 6
   FOR CO=0 TO 5-RO
    KD=KD-1: CD=CD(KD): BD(RO,CO)=CD
    R=2*RO+2: C=9*CO+1: GOSUB DrawCardFaceDown
   NEXT
   KD=KD-1: CD=CD(KD): BD(RO,6-RO)=CD
   GOSUB DrawCardFaceUp
  NEXT
  KU=0
  GOSUB DrawDeckThickness

MainLoop:
  P$="1..7,D,P,Q? ": GOSUB PromptIn
  IF O$>="1" AND O$<="7" THEN MoveFromCol
  IF O$="D" OR O$="0" OR O$=" " THEN Deal
  IF O$="P" OR O$="." THEN MoveTop
  IF O$="Q" THEN Quit
  BEEP
  GOTO MainLoop

MoveFromCol:
  ' Move from column
  CF=ASC(O$)-49
  IF BN(CF)=0 THEN E$="Column "+CHR$(CF+49)+"'s empty": GOTO Perror
  P$=" To (1..7,P)? ": GOSUB PromptIn
  IF O$>="1" AND O$<="7" THEN MoveColToCol
  IF O$="P" OR O$="." THEN MoveColToPile
  BEEP
  GOTO MainLoop

MoveColToPile:
  ' Move from column CF to pile
  CD=BD(BN(CF)-1,CF)
  GOSUB CheckForPile
  NC=1
  GOSUB TakeFromCol
  GOTO DrawCheckWin

Perror:
  ' Print error message in E$
  LOCATE 25,62: PRINT "                ";
  'LOCATE 25,62: A$=USR2(E$): PRINT CHR$(5);: BEEP
  LOCATE 25,62: PRINT E$;: BEEP
  'WHILE INKEY$<>""
  'WEND
  PS!=3: GOSUB Pause
  GOTO MainLoop

MoveColToCol:
  ' Move one entire column to another
  CT=ASC(O$)-49
  IF CT=CF THEN E$="Same column": GOTO Perror
  CDF=BD(BF(CF),CF)
  GOSUB CheckForCol
  NC=BN(CF)-BF(CF)
  FOR I=1 TO NC
   BD(BN(CT)+I-1,CT)=BD(BF(CF)+I-1,CF)
  NEXT
  BN(CT)=BN(CT)+NC
  GOSUB TakeFromCol
  CO=CT
  FOR RO=BN(CT)-NC TO BN(CT)-1
   CD=BD(RO,CO): GOSUB DrawCardFaceUp
  NEXT
  GOTO MainLoop

Deal:
  ' Deal next triple
  IF KD<=0 THEN
    IF KU=0 THEN E$="No cards to deal": GOTO Perror
    LOCATE 25,62: COLOR 7: PRINT " Turning deck ";
    PS!=2: GOSUB Pause
    FOR I=0 TO KU-1: CD(KU-I-1)=CU(I): NEXT
    KD=KU: KU=0
    GOSUB DrawDeckThickness
  END IF
  FOR I=1 TO 3
   KD=KD-1: CD=CD(KD)
   CU(KU)=CD: KU=KU+1
   'LINE (504,168-KD)-(535,168-KD),PRESET
   'LINE (608,169-KU)-(639,169-KU),PSET,1
   IF KD=0 THEN L1
  NEXT
L1:
  R=17: C=67: GOSUB DrawCardFaceUp2
  GOTO MainLoop

MoveTop:
  ' Move top card to pile
  IF KU=0 THEN E$="No card face up": GOTO Perror
  P$=" To (1..7,P)? ": GOSUB PromptIn
  IF O$>="1" AND O$<="7" THEN MoveTopToCol
  IF O$="P" OR O$="." THEN MoveTopToPile
  BEEP
  GOTO MainLoop

MoveTopToCol:
  ' Move top card to column
  CT=ASC(O$)-49
  CDF=CU(KU-1)
  GOSUB CheckForCol
  BD(BN(CT),CT)=CDF
  BN(CT)=BN(CT)+1
  GOSUB TakeFromTop
  CO=CT: RO=BN(CT)-1: CD=BD(RO,CO): GOSUB DrawCardFaceUp
  GOTO MainLoop

MoveTopToPile:
  ' Move top card to pile
  CD=CU(KU-1): S=CD\13
  GOSUB CheckForPile
  GOSUB TakeFromTop
  GOTO DrawCheckWin

Quit:
  ' Quit
  P$="Quit (Y or N)? ": GOSUB PromptIn
  IF O$<>"Y" THEN MainLoop

PlayAgain:
  P$=" Play again? ": GOSUB PromptIn
  IF O$="Y" THEN NewGame
  IF O$<>"N" THEN PlayAgain
  'COLOR 7,0: SCREEN 0
  COLOR 7: PRINT
  'RUN ""
  END

DrawCheckWin:
  ' Display card SS,SV on deck and check for win
  S=SS: V=SV: PI(S)=PI(S)+1: R=3+7*(S\2): C=63+8*(S MOD 2): GOSUB DrawCardFaceUp3
  IF PI(0)<13 OR PI(1)<13 OR PI(2)<13 OR PI(3)<13 THEN MainLoop
  FOR I=1 TO 7
    LOCATE 3*I+1,21: COLOR I: PRINT "C O N G R A T U L A T I O N S";
  NEXT
  GOTO PlayAgain

TakeFromTop:
  ' Take top card from deck
  KU=KU-1
  R=17: C=67
  IF KU=0 THEN
    FOR I=0 TO 6: LOCATE R+I,C: PRINT "       ";: NEXT
  ELSE
    CD=CU(KU-1): GOSUB DrawCardFaceUp2
  END IF
  'LINE (608,168-KU)-(639,168-KU),PRESET
  RETURN

CheckForCol:
  ' Check card CDF will go on column CT
  SF=CDF\13: VF=CDF MOD 13+1
  IF BN(CT)<>0 THEN
    CDT=BD(BN(CT)-1,CT): ST=CDT\13: VT=CDT MOD 13+1
    IF VF+1<>VT OR (SF-1.5)*(ST-1.5)>0 THEN E$=FNA$(VF,SF)+C7$+" won't go on "+FNA$(VT,ST): RETURN Perror
  ELSE
    IF VF<>13 THEN E$=FNA$(VF,SF)+C7$+" won't go to "+CHR$(CT+49): RETURN Perror
  END IF
  RETURN

TakeFromCol:
  ' Take top NC cards from column CF
  BN(CF)=BN(CF)-NC
  IF BF(CF)=BN(CF) THEN IF BF(CF)>0 THEN BF(CF)=BF(CF)-1
  RO=BN(CF)-1: CO=CF: C=9*CO+1
  IF RO<0 THEN FOR R=2 TO 2*NC+6: LOCATE R,C: PRINT "       ";: NEXT: RETURN
  CD=BD(RO,CO): GOSUB DrawCardFaceUp
  FOR I=7 TO 2*NC+6: LOCATE R+I,C: PRINT "       ";: NEXT
  RETURN

CheckForPile:
  ' Check card CD goes on pile
  S=CD\13: V=CD MOD 13+1: SS=S: SV=V
  IF V<>PI(S)+1 THEN E$=FNA$(V,S)+" doesn't go": RETURN Perror
  RETURN

PromptIn:
  ' Print prompt P$ and get response in O$
  LOCATE 25,62: PRINT "                  ";
  'LOCATE 25,62: A$=USR2(P$): PRINT CHR$(5);
  LOCATE 25,62: COLOR 7: PRINT P$;
L3:
  'O$=INKEY$: IF O$="" THEN L3
  O$=INPUT$(1)
  'IF ASC(O$)>=32 THEN PRINT O$;
  IF O$=CHR$(13) THEN L3
  O$=UCASE$(O$)
  RETURN

DrawDeckThickness:
  ' Display deck thickness when all cards down
  'LINE (608,168)-(639,145),PRESET,BF
  'IF KD>0 THEN LINE (504,168)-(535,169-KD),PSET,1,BF
  RETURN

Pause:
  ' Pause for PS! seconds or until a key is pressed
  T0!=TIMER
  WHILE INKEY$="" AND TIMER-T0!<PS!
  WEND
  RETURN

Shuffle:
  ' Shuffle cards in CD(51)
  CLS: LOCATE 13,31: COLOR 6: PRINT "Shuffling cards...";
  FOR I=0 TO 51: CD(I)=I: NEXT
  FOR I=51 TO 1 STEP -1: SWAP CD(I),CD(INT(RND*(I+1))): NEXT
  KD=52
  CLS
  RETURN

InitPics:
  ' Initialise card picture chars
  DEFUSR1=&H7000: DEFUSR2=&H7003: DIM PA$(3),PA(13,4)
  'A$=USR1("")
  PA$(0)="    C  C    C CC C   CCCC CC CC CCCCC C"
  PA$(1)="    S  S    S SS S   SSSS SS SS SSSSS S"
  PA$(2)="    D  D    D DD D   DDDD DD DD DDDDD D"
  PA$(3)="    H  H    H HH H   HHHH HH HH HHHHH H"
  FOR V=1 TO 10: FOR I=0 TO 4: READ PA(V,I): NEXT: NEXT
  RETURN

DrawCardFaceUp:
  ' Display card CD at RO,CO
  R=2*RO+2: C=9*CO+1

DrawCardFaceUp2:
  S=CD\13: V=CD MOD 13+1

DrawCardFaceUp3:
  ' Display card value V, suit S (0=C, 1=S, 2=D, 3=H), at R,C
  CO$=FNC$(S)
  S1$=CO$+FNS$(S)+C6$: V$=CO$+FNV$(V)+C6$
  LOCATE R,C: PRINT C6$+"┌─────┐";
  IF V<11 THEN A$=CO$+" "+MID$(PA$(S),3*PA(V,0)+1,3)+" "+C6$ ELSE A$=CO$+"┌───┐"+C6$
  LOCATE R+1,C: PRINT V$+A$+"│";
  IF V<11 THEN A$=CO$+" "+MID$(PA$(S),3*PA(V,1)+1,3)+" "+C6$ ELSE A$=CO$+"│"+S1$+CO$+"  │"+C6$
  LOCATE R+2,C: PRINT S1$+A$+"│";
  IF V<11 THEN A$=CO$+" "+MID$(PA$(S),3*PA(V,2)+1,3)+" "+C6$ ELSE A$=CO$+"│   │"+C6$
  LOCATE R+3,C: PRINT "│"+CO$+A$+"│";
  IF V<11 THEN A$=CO$+" "+MID$(PA$(S),3*PA(V,3)+1,3)+" "+C6$ ELSE A$=CO$+"│  "+S1$+CO$+"│"+C6$
  LOCATE R+4,C: PRINT "│"+CO$+A$+S1$;
  IF V<11 THEN A$=CO$+" "+MID$(PA$(S),3*PA(V,4)+1,3)+" "+C6$ ELSE A$=CO$+"└───┘"+C6$
  LOCATE R+5,C: PRINT "│"+CO$+A$+V$;
  LOCATE R+6,C: PRINT "└─────┘";
  RETURN

DrawCardFaceDown:
  ' Display card face down at R,C
  'LOCATE R,C: A$=USR2("       ")
  LOCATE R,C: PRINT C6$+"┌─────┐";
  FOR I=1 TO 5
   LOCATE R+I,C: PRINT "│"+C3$+"┼┼┼┼┼"+C6$+"│";
  NEXT
  LOCATE R+6,C: PRINT "└─────┘";
  RETURN
  DATA 0,0,1,0,0, 2,3,0,2,3, 2,3,1,2,3, 5,6,0,5,6, 5,6,1,5,6
  DATA 5,6,4,5,6, 5,6,7,8,6, 5,9,10,8,6, 5,12,11,12,6, 5,11,12,11,6
