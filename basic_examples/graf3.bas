  10 REM GRAFXY.BAS by John G. Kemeny and Thomas E. Kurtz
  20 REM Modified by Peter McGavin
  30 LINEINPUT "Function of X to plot? Example SIN(X): ",F$
  40 INPUT "X range? Example 0,6.4:  ",X0,X1
  50 INPUT "Y range? Example -1,1:   ",Y0,Y1
  60 DX=(X1-X0)/32.0:    REM X subdivision size
  70 N=50:               REM number of Y subdivisions
  80 DY=(Y1-Y0)/N:       REM Y subdivision size
  90 Q$=CHR$(34):        REM double-quote
 100 OPEN "graftmp.bas" FOR OUTPUT AS #1
 110 PRINT #1,"OPEN ";Q$;"graftmp.dat";Q$;" FOR OUTPUT AS #1"
 120 PRINT #1,"FOR X=";X0;" TO";X1;" STEP";DX
 130 PRINT #1,"  PRINT #1,X;";Q$;",";Q$;";";F$
 140 PRINT #1,"NEXT X"
 150 PRINT #1,"CLOSE #1"
 160 CLOSE #1
 170 SHELL "mybasic graftmp.bas"
 180 KILL "graftmp.bas"
 190 PRINT "Y-AXIS FROM ";Y0;"TO ";Y1;"IN INCREMENTS OF ";DY
 200 PRINT
 210 PRINT TAB(8);"'";STRING$(N-1,"-");"'"
 220 OPEN "graftmp.dat" FOR INPUT AS #1
 230 FOR X=X0 TO X1 STEP DX
 240   INPUT #1,X,Y
 250   PRINT INT(100*X+0.5)/100; TAB(8+INT((Y-Y0)/DY+0.5)); "*"
 260 NEXT X
 270 CLOSE #1
 280 KILL "graftmp.dat"
 290 END
