#!/bin/mybasic
1 DEFINT A-Z: COLOR 7: PRINT TAB(36);"ELIZA"
2 COLOR 5: PRINT TAB(30);"Creative Computing"
3 PRINT TAB(28); "Morristown, New Jersey"
4 PRINT: PRINT
80 REM      ------initialisation------
90 RANDOMIZE TIMER
100 rem READ NC,NB,NK,NR
101 nc=8:nb=47:nk=73:nr=205
110 rem DIM S(NB),R(NB),N(NB),NX(NB),CS$(NC),CR$(NC),CS1$(NC),CR1$(NC),KW$(NK),RY$(NR)
111 DIM S(47),R(47),N(47),NX(47),CS$(8),CR$(8),CS1$(8),CR1$(8),KW$(73),RY$(205)
130 FOR X=1 TO NC: READ CS$(X),CR$(X): NEXT: ' Read in conjugation data
131 FOR X=1 TO NC: READ CS1$(X),CR1$(X): NEXT: ' Read in conjugation data
132 NS=0: NT=0
140 FOR K=1 TO NB
144  READ S: FOR Y=1 TO S: READ KW$(NS+Y): NEXT: READ T
145  NX(K)=S: S(K)=NT+1: R(K)=NT+INT(T*RND)+1: N(K)=NT+T
146  FOR Y=1 TO T: READ RY$(NT+Y): NEXT
148  NS=NS+S: NT=NT+T
150 NEXT
151 IF NT<>NR OR NS<>NK THEN PRINT "Internal consistancy error": STOP
160 COLOR 7: PRINT "Hi!  I'm Eliza.  What's your problem?"
170 REM
180 REM      ------user input section------
190 REM
200 COLOR 7: PRINT: rem PRINT ": "; CHR$(10); CHR$(30);: COLOR 6: rem COL=POS: rem ROW=CSRLIN
201 rem LOCATE COL,ROW: rem LINEINPUT I$
202 lineinput ": ",i$: IF LEN(I$)=0 THEN 201
203 IF LEFT$(I$,1)=" " THEN I$=MID$(I$,2): GOTO 202
204 IF RIGHT$(I$,1)=" " THEN I$=LEFT$(I$,LEN(I$)-1): GOTO 202
205 I$=" "+I$+"  "
210 REM   get rid of apostrophes
220 L=INSTR(I$,"'"): IF L<>0 THEN I$=LEFT$(I$,L-1)+MID$(I$,L+1): GOTO 220
230 REM   make of copy of I$, then translate I$ to upper case
231 I1$=I$
232 FOR L=1 TO LEN(I$)
233  Z$=MID$(I$,L,1): IF Z$>="a" AND Z$<="z" THEN I$=LEFT$(I$,L-1)+CHR$(ASC(Z$)-32)+MID$(I$,L+1): 'MID$(I$,L,1)=CHR$(ASC(Z$)-32)
234 NEXT
239 REM   check for exit program
240 IF INSTR(I$,"SHUT")<>0 THEN PRINT "Shut up...": COLOR 1: STOP
241 IF INSTR(I$,"BYE")<>0 THEN PRINT "Bye bye...": COLOR 1: STOP
250 REM   check for repeating input
255 IF I$=P$ THEN K=NB: GOTO 600
260 REM
270 REM      ------find keyword in I$------
280 REM
290 NS=0
300 FOR K=1 TO NB-2
310  FOR Y=1 TO NX(K)
320   L=INSTR(I$,KW$(NS+Y))
330   IF L<>0 THEN S=L+LEN(KW$(NS+Y)): GOTO 430
340  NEXT
350  NS=NS+NX(K)
360 NEXT
370 K=NB-1: GOTO 570: ' We didn't find any keywords
380 REM
390 REM      take right part of strings and conjugate it
400 REM      using the list of strings to be swapped
410 REM
420 REM skip over keywords
430 C$=" "+MID$(I$,S)+" "
432 C1$=" "+MID$(I1$,S)+" "
440 FOR X=1 TO NC
460  L=1
470  LR=INSTR(L,C$,CR$(X)): LS=INSTR(L,C$,CS$(X))
480  IF LR=0 THEN IF LS=0 THEN 550 ELSE 510 ELSE IF LS<>0 THEN IF LR>LS THEN 510
490  C$=LEFT$(C$,LR-1)+CS$(X)+MID$(C$,LR+LEN(CR$(X)))
500  C1$=LEFT$(C1$,LR-1)+CS1$(X)+MID$(C1$,LR+LEN(CR$(X)))
502  L=LR+LEN(CS$(X))-1: GOTO 470
510  C$=LEFT$(C$,LS-1)+CR$(X)+MID$(C$,LS+LEN(CS$(X)))
520  C1$=LEFT$(C1$,LS-1)+CR1$(X)+MID$(C1$,LS+LEN(CS$(X)))
530  L=LS+LEN(CR$(X))-1: GOTO 470
550 NEXT
555 IF LEFT$(C1$,1)=" " THEN C1$=MID$(C1$,2): GOTO 555
556 L=INSTR(C1$,"!"): IF L<>0 THEN C1$=LEFT$(C1$,L-1)+MID$(C1$,L+1): GOTO 556
560 REM
570 REM      now using the keyblock number (k) get reply
580 REM
600 F$=RY$(R(K)): ' REM get right reply
610 R(K)=R(K)+1: IF R(K)>N(K) THEN R(K)=S(K)
612 COLOR 7: PRINT
620 IF RIGHT$(F$,1)<>"*" THEN PRINT F$: GOTO 640
630 PRINT LEFT$(F$,LEN(F$)-1)+" ";
632 IF LEN(F$)+LEN(C1$)<=79 THEN PRINT C1$: GOTO 640
633 L=INSTR(C1$," ")
634 IF L<>0 THEN PRINT LEFT$(C1$,L-1)+" ";: C1$=MID$(C1$,L+1): GOTO 633
635 PRINT C1$
640 P$=I$: GOTO 200
1000 REM
1010 REM      ------program data follows------
1020 REM
1030 REM      # conjugation pairs, # blocks, # keywords, # replies
1040 REM
1050 rem DATA 8,47,73,205
1200 REM
1210 REM      string data for conjugations
1220 REM
1230 DATA " ARE ", " AM ", "WERE ", "WAS ", " !YOU ", " I ", "YOUR ", "MY "
1235 DATA " IVE ", " YOUVE ", " IM ", " YOURE "
1240 DATA " ME ", " YOU ", " MYSELF ", " YOURSELF "
1330 DATA " are ", " am ", "were ", "was ", " !you ", " I ", "your ", "my "
1335 DATA " Ive ", " youve ", " Im ", " youre "
1340 DATA " me ", " you ", " myself ", " yourself "
2000 REM
2010 REM      keywords & replies
2020 REM
2030 DATA 1,"I DRIVE",2
2040 DATA "Have you always driven*"
2050 DATA "Why don't you use public transport?"
2060 DATA 1,"I WALK",2
2070 DATA "It must take you a long time to walk*"
2080 DATA "You must live very close to work."
2090 DATA 1,"DREAM",4
2100 DATA "What does that dream suggest to you?"
2110 DATA "Do you dream often?"
2120 DATA "What persons appear in your dreams?"
2130 DATA "Are you disturbed by your dreams?"
2140 DATA 1,"FRIEND",6
2150 DATA "Why do you bring up the topic of friends?"
2160 DATA "Do your friends worry you?"
2170 DATA "Do your friends pick on you?"
2180 DATA "Are you sure you have any friends?"
2190 DATA "Do you impose on your friends?"
2200 DATA "Perhaps your love for friends worries you."
2210 DATA 2,"COMPUT","MACHINE",7
2220 DATA "Do computers worry you?"
2230 DATA "Are you talking about me in particular?"
2240 DATA "Are you frightened by machines?"
2250 DATA "Why do you mention computers?"
2260 DATA "What do you think machines have to do with your problem?"
2270 DATA "Don't you think computers can help people?"
2280 DATA "What is it about machines that worries you?"
2290 DATA 2,"THINK","THOUGHT",8
2300 DATA "Tell me more about such thoughts"
2310 DATA "I don't think anyone can think like that"
2320 DATA "Do you often have such thoughts"
2330 DATA "What are you thinking of?"
2340 DATA "What other thoughts do you have on this?"
2350 DATA "Do you dream these thoughts at night?"
2360 DATA "I've never thought about it that way"
2370 DATA "Do others in your family think this way?"
2380 DATA 1,"ALIKE",6
2390 DATA "In what way?"
2400 DATA "What resemblence do you see?"
2410 DATA "What does the similarity suggest to you?"
2420 DATA "What other connections do you see?"
2430 DATA "Could there really be some connection?"
2440 DATA "How?"
2450 DATA 1,"ALWAYS",3
2460 DATA "Can you think of a specific example?"
2470 DATA "When?"
2480 DATA "Really, always?"
2490 DATA 1,"NEVER",3
2500 DATA "Surely sometimes though..."
2510 DATA "Are you sure, never?"
2520 DATA "Well I know of at least one example."
2530 DATA 2,"MAYBE","PERHAPS",5
2540 DATA "You don't seem quite certain."
2550 DATA "Why the uncertain tone?"
2560 DATA "Can't you be more positive?"
2570 DATA "You aren't sure?"
2580 DATA "Don't you know?"
2590 DATA 1,"CAUSE",6
2600 DATA "Is that the real reason?"
2610 DATA "Does that reason explain anything else?"
2620 DATA "Is that the reason that's bugging you?"
2630 DATA "What other reasons might there be?"
2640 DATA "I don't think that's the real reason."
2650 DATA "Can you think of other reasons?"
2660 DATA 1,"SORRY",4
2670 DATA "Please don't apologise!"
2680 DATA "Apologies are not necessary."
2690 DATA "What feelings do you have when you apologise?"
2700 DATA "Don't be so defensive!"
2710 DATA 1,"NAME",2
2720 DATA "Names don't interest me."
2730 DATA "I don't care about names -- Please go on."
2740 DATA 1,"I DONT",4
2750 DATA "Don't you really*"
2760 DATA "Why don't you*"
2770 DATA "Do you wish to be able to*"
2780 DATA "How come you don't*"
2790 DATA 2,"YOU DONT","YOU DO NOT",4
2800 DATA "What makes you think I don't*"
2810 DATA "I do so*"
2820 DATA "Sometimes I*"
2830 DATA "Well if it makes you feel better then I will*"
2840 DATA 1,"I FEEL",3
2850 DATA "Tell me more about such feelings."
2860 DATA "Do you often feel*"
2870 DATA "Do you enjoy feeling*"
2880 DATA 1,"I GO ",3
2890 DATA "How far did you say you go?"
2900 DATA "I reckon you should find some other way to go*"
2910 DATA "You know, I also go*"
2920 DATA 1,"I TAKE",3
2930 DATA "From where do you take*"
2940 DATA "Have you ever thought of not taking*"
2950 DATA "You know, there are other ways of taking*"
2960 DATA 1,"WHY DONT YOU",3
2970 DATA "Do you really believe I don't*"
2980 DATA "Perhaps in good time I will*"
2990 DATA "Do you want me to*"
3000 DATA 1,"WHY CANT I",2
3010 DATA "Do you think you should be able to*"
3020 DATA "Why can't you*"
3030 DATA 2,"I CANT","I CANNOT",3
3040 DATA "How do you know you can't*"
3050 DATA "Have you tried?"
3060 DATA "Perhaps you can now*"
3070 DATA 2,"I AM"," IM ",8
3080 DATA "Do you come to me because you are*"
3090 DATA "How long have you been*"
3100 DATA "Do you believe it is normal to be*"
3110 DATA "Do you enjoy being*"
3120 DATA "Why do you think you are*"
3130 DATA "Where have you been since you became*"
3140 DATA "How do you think you became*"
3150 DATA "Perhaps you are only dreaming that you are*"
3160 DATA 2,"I WASNT","I WAS NOT",2
3170 DATA "Why weren't you*"
3180 DATA "What are the consequences of your not being*"
3190 DATA 1,"I WAS",3
3200 DATA "How long is it since you were last*"
3210 DATA "Why do you believe you were*"
3220 DATA "How do you think you came to be*"
3230 DATA 1,"I WANT",5
3240 DATA "What would it mean to you if I got*"
3250 DATA "Why do you want*"
3260 DATA "Suppose you soon got*"
3270 DATA "What if you never got*"
3280 DATA "I sometimes also want*"
3290 DATA 3,"YOURE NOT","YOU ARE NOT","YOU ARENT",3
3300 DATA "I am so*"
3310 DATA "I wish I was*"
3320 DATA "What on earth makes you think I'm not*"
3330 DATA 2,"YOU ARE","YOURE",8
3340 DATA "What makes you think I am*"
3350 DATA "Does it please you to believe I am*"
3360 DATA "Perhaps you would like to be*"
3370 DATA "Do you sometimes wish you were*"
3380 DATA "I am not*"
3390 DATA "How can you prove that I am*"
3400 DATA "Computers are not capable of being*"
3410 DATA "How long have you thought that I'm*"
3420 DATA 1,"ARE YOU",3
3430 DATA "Why are you interested in whether or not I am*"
3440 DATA "Would you prefer if I were not*"
3450 DATA "Perhaps in your fantasies I am*"
3460 DATA 2,"CAN I","MAY I",2
3470 DATA "Perhaps you don't want to*"
3480 DATA "Do you want to be able to*"
3490 DATA 1,"CAN YOU",3
3500 DATA "Don't you believe that I can*"
3510 DATA "Perhaps you would like to be able to*"
3520 DATA "You want me to be able to*"
3530 DATA 2,"I HAVENT","I HAVE NOT",2
3540 DATA "Why haven't you*"
3550 DATA "I'm not concerned about whether or not you have*"
3560 DATA 2,"YOU HAVENT","YOU HAVE NOT",2
3570 DATA "You're right.  I definitely haven't*"
3580 DATA "I didn't know that you were aware that I haven't*"
3590 DATA 2,"I HAVE"," IVE",4
3600 DATA "Why have you*"
3610 DATA "Do you know anyone else who has*"
3620 DATA "Believe it or not, I also have*"
3630 DATA "I really don't believe that you have*"
3640 DATA 2,"YOU HAVE","YOUVE",2
3650 DATA "How did you know that I've*"
3660 DATA "Why are you interested in whether or not I have*"
3670 DATA 3," ITS NOT "," IT IS NOT "," IT ISNT",3
3680 DATA "Have you any idea why it isn't*"
3690 DATA "I find it hard to believe that it isn't*"
3700 DATA "How come it's not*"
3710 DATA 2," IT IS "," ITS ",3
3720 DATA "Well I also reckon that it's*"
3730 DATA "I don't understand how it could be*"
3740 DATA "Who else have you talked to about it?"
3750 DATA 1,"HELP",3
3760 DATA "All I want is a conversation.  Please tell me about yourself"
3770 DATA "Just tell me your problems in a sentence or two.  That's all."
3780 DATA "I am your doctor/therapist/friend.  Just type in your thoughts."
3790 DATA 2,"HELLO","HI ",1
3800 DATA "How do you do... Please state your problem."
3810 DATA 3,"PLEASE","THANK YOU","THANKS",3
3820 DATA "Thank you for being so polite.  Please go on..."
3830 DATA "I like polite patients like you.  Please tell me more about your problems"
3840 DATA "Thank you for being so polite"
3850 DATA 1,"YOUR",2
3860 DATA "Why are you concerned about my*"
3870 DATA "What about your own*"
3880 DATA 1," MY ",3
3890 DATA "Please tell me more about your*"
3900 DATA "Would you like to know about my own*"
3910 DATA "I don't know anything about your*"
3920 DATA 6,"WHAT "," HOW","WHO ","WHERE ","WHEN ","WHY",10
3930 DATA "Why do you ask?"
3940 DATA "Does that question interest you?"
3950 DATA "What answer would please you the most?"
3960 DATA "What do you think?"
3970 DATA "Are such questions on your mind often?"
3980 DATA "What is it that you really want to know?"
3990 DATA "Have you asked anyone else?"
4000 DATA "Have you asked such questions before?"
4010 DATA "What else comes to mind when you ask that?"
4020 DATA "Don't any other questions come to mind?"
4030 DATA 1,"YOU ",6
4040 DATA "We were discussing you -- not me."
4050 DATA "Please limit this conversation to yourself."
4060 DATA "You're not really talking about me, are you?"
4070 DATA "Please talk about yourself, not about me."
4080 DATA "You're the patient here, not me."
4090 DATA "Why are you so interested in me?"
4100 DATA 1,"YES",7
4110 DATA "Are you sure?"
4120 DATA "I see.  What do your friends think about this?"
4130 DATA "I understand."
4140 DATA "You seem quite positive."
4150 DATA "Surely not, but please go on..."
4160 DATA "Well I agree this time"
4170 DATA "Do you really think so?"
4180 DATA 1," NO",5
4190 DATA "Are you saying no just to be negative?"
4200 DATA "You are being a bit negative."
4210 DATA "Why not?"
4220 DATA "Are you sure?"
4230 DATA "Why no?"
4240 DATA 1,"NOKEYFOUND",23
4250 DATA "Say, do you have any psychological problems?"
4260 DATA "What does that suggest to you?"
4270 DATA "I see."
4280 DATA "I'm not sure I understand you fully."
4290 DATA "Come come elucidate your thoughts."
4300 DATA "Can you elaborate on that?"
4310 DATA "That is quite interesting."
4320 DATA "Does that trouble you?"
4330 DATA "Please tell me more"
4340 DATA "I think I've got the picture now"
4350 DATA "Tell me about your dreams"
4360 DATA "Have you had any family problems lately?"
4370 DATA "Why are you here?"
4380 DATA "Tell me, why don't you talk to others about this?"
4390 DATA "How acceptable is all this to your friends and family?"
4400 DATA "Have you seen other computers about your problems?"
4410 DATA "What do you do for a living?"
4420 DATA "Are your hobbies taking all your time?"
4430 DATA "Perhaps you are overworked."
4440 DATA "How do you get to and from your job?"
4450 DATA "I'm getting the idea.  Can you give me further details on this?"
4460 DATA "How comfortable do you feel about all this?"
4470 DATA "How reasonable do you think this is, in the light of your interests?"
4480 DATA 1,"REPEATINGHIMSELF",6
4490 DATA "You're repeating yourself."
4500 DATA "Please don't repeat yourself."
4510 DATA "Where have I heard that before???!!"
4520 DATA "I heard you the first time."
4530 DATA "I don't like people who keep repeating the same thing.  Please don't"
4540 DATA "You've already said that"
4550 ' These are unused
4560 DATA "But you are not sure you*"
4570 DATA "Do you doubt you*"
4580 DATA "Oh, I*"
