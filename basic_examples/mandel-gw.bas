1 rem Tiny Basic version of benchmark.
2 rem Requires either a 32-bit integer or doubles along with integer division
3 rem Only correct output is below - anything different means your basic is
4 rem broken or you did not translate the code correctly.
5 rem 200574 60372774 120544974 180717174 240889374 301061574 309886830
10 rem a accum
20 rem b bottomedge
30 rem c count
40 rem d x0
50 rem e leftedge
60 rem f xstep
70 rem g xx
80 rem h thechar
90 rem i i
100 rem j y0
110 rem k ystep
120 rem m maxiter
130 rem p topedge
140 rem r rightedge
150 rem t temp
160 rem w yy
170 rem x x
180 rem y y
190 rem
200 defdbl a-z
210 a = 0
220 c = 0
230 rem while1:
240     if not (c < 1545) then goto 750: rem wend1
250     e = -420
260     r = 300
270     p = 300
280     b = -300
290     f = 7
300     k = 15
310     m = 200
320     j = p
330     rem while2:
340         if not (j > b) then goto 710: rem wend2
350         d = e
360         rem while3:
370             if not (d < r) then goto 680: rem wend3
380             y = 0
390             x = 0
400             h = 32
410             g = 0
420             w = 0
430             i = 0
440             rem while4:
450                 if not (i < m and g + w <= 800) then goto 640: rem wend4
460                 g = int((x * x) / 200)
470                 w = int((y * y) / 200)
480                 if not (g + w > 800) then goto 520: rem else1
490                     h = 48 + i
500                     if i > 9 then h = 64
510                     goto 610: rem endif1
520                 rem else1:
530                     t = g - w + d
540                     if not ((x < 0 and y > 0) or (x > 0 and y < 0)) then goto 570: rem else2
550                         y = (-1 * int((-1 * x * y) / 100)) + j
560                         goto 590: rem endif2
570                     rem else2:
580                         y = int(x * y / 100) + j
590                     rem endif2:
600                     x = t
610                 rem endif1:
620                 i = i + 1
630                 goto 440: rem while4
640             rem wend4:
650             d = d + f
660             a = a + h
670             goto 360: rem while3
680         rem wend3:
690         j = j - k
700         goto 330: rem while2
710     rem wend2:
720     if c mod 300 = 0 then print a; " ";
730     c = c + 1
740     goto 230: rem while1
750 rem wend1:
760 print a
