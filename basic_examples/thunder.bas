#!/bin/mybasic
5 pi = 4 * atn(1)
10 print "Enter height of thunderclap in metres";
20 input h
30 print "Enter temperature at ground level (degrees celcius)";
40 input t0
50 t0 = t0 + 273
60 print "Enter temperature at thunderclap";
70 input t1
80 t1 = t1 + 273
100 x = sqr((t0 - t1) * t1) / t0
105 y = 1 - t1 / t0
110 d = t0 * h * (x - atn((1 - 2 * y) / (2 * x)) / 2 + pi / 4) / (t0 - t1)
120 print
130 print "The thunder cannot be heard on the ground more than"; d; "metres away."
140 print
150 goto 10
160 end
