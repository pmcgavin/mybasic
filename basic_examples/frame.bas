deflng a-z
dim k$(3000), s$(3000)

t$ = chr$(9)
q$ = chr$(34)
n = 0
open "frame.strings.new" for input as #1
while not eof(#1)
  line input #1,l$
  if left$(l$,2) = "M_" then
    l$ = mid$(l$, 3)
    i1 = instr(l$, ",")
    k$(n) = left$(l$, i1-1)
    l$ = mid$(l$, i1+1)
    i2 = instr(l$, q$)
    i3 = rinstr(l$, q$)
    s$(n) = mid$(l$, i2+1, i3-i2-1)
    rem print k$(n), s$(n)
    n = n + 1
  end if
wend
close #1
'print n

'print "Sorting..."
inc = n \ 2
while inc > 0
  for i = inc to n - 1
    j = i - inc
    while j >= 0
      k = j + inc
      if k$(j) < k$(k) then goto skip1
      swap k$(j),k$(k)
      swap s$(j),s$(k)
      j = j - inc
    wend
skip1:
  next i
  inc = inc \ 2
wend

'for i = 0 to n-1
'  print k$(i), s$(i)
'next

open "frame.strings.orig" for input as #1
while not eof(#1)
  line input #1,l$
  if left$(l$,2) = "M_" then
    i1 = instr(l$, ",")
    k$ = mid$(l$, 3, i1-3)
    for i = 0 to n - 1
      if k$ = k$(i) then
        print "M_"; k$; ","; string$(instr(l$, q$)-i1-1, t$); q$; s$(i); q$; ";"
        goto skip
      end if
    next i
  end if
  print l$
skip:
wend
close #1

end
