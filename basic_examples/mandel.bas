' Generic Basic version of benchmark.
' Requires either a 32-bit integer or doubles along with integer division
' Only correct output is below - anything different means your basic is
' broken or you did not translate the code correctly.
' 200574 60372774 120544974 180717174 240889374 301061574 309886830
'
deflng a-z
accum = 0
count = 0
while count < 1545
    leftedge   = -420
    rightedge  =  300
    topedge    =  300
    bottomedge = -300
    xstep      =  7
    ystep      =  15

    maxiter    =  200

    y0 = topedge
    while y0 > bottomedge
        x0 = leftedge
        while x0 < rightedge
            y = 0
            x = 0
            thechar = 32
            xx = 0
            yy = 0
            i = 0
            while i < maxiter and xx + yy <= 800
                xx = (x * x) \ 200
                yy = (y * y) \ 200
                if xx + yy > 800 then
                    thechar = 48 + i
                    if i > 9 then thechar = 64
                else
                    temp = xx - yy + x0
                    if (x < 0 and y > 0) or (x > 0 and y < 0) then
                        y = (-1 * ((-1 * x * y) \ 100)) + y0
                    else
                        y = (x * y \ 100) + y0
                    end if
                    x = temp
                end if

                i = i + 1
            wend
            accum = accum + thechar

            x0 = x0 + xstep
        wend

        y0 = y0 - ystep
    wend

    if count mod 300 = 0 then print accum; " ";

    count = count + 1
wend

print accum
