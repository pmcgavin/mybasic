L.10:
REM $NOWINDOW
  ' Monopoly simulation - by Peter McGavin
  'SCREEN0:WIDTH80
  'WINDOW 1,"Monopoly"
  DEFINT A-Z:RANDOMIZE TIMER
  DEF FND$(D)="$"+MID$(STR$(D),2)
  DEF FNC$(N)=MID$(STR$(N),2)+MID$("stndrd",N+N-1,2)
  DEF FNS$(N,A$)=STR$(N)+" "+A$+LEFT$("s",-(N<>1))
  DEF FNH$(N)=MID$(" a hotel"+STR$(N)+" houses",9+8*(N=5),9+(N=1)+(N=5))
  DIM PD$(27),GC$(9),PN$(10),PD(27,9),GC(9,6),SI(39),P(10,16),PL(10)
  DIM CC$(15),CH$(15),CC(15,2),CH(15,2),CCP(15),CHP(15),B(10,3),HN(3)
  CLS:LOCATE 2,29:PRINT"M O N O P O L Y":LOCATE 12,30:PRINT"Initialising..."
  FOR I=0 TO 27:READ PD$(I):NEXT
  FOR I=0 TO 9:READ GC$(I):NEXT
  FOR I=0 TO 27:FOR J=2 TO 9:READ PD(I,J):NEXT J:NEXT I
  FOR I=0 TO 9:FOR J=1 TO 5:READ GC(I,J):NEXT J:NEXT I
  FOR I=0 TO 39:READ SI(I):NEXT
  FOR I=0 TO 15:READ CC$(I):NEXT
  FOR I=0 TO 15:READ CH$(I):NEXT
  FOR I=0 TO 15:READ CC(I,0),CC(I,1),CC(I,2):NEXT
  FOR I=0 TO 15:READ CH(I,0),CH(I,1),CH(I,2):NEXT
  P(0,4)=28:PG$="You passed GO and collected $200"
  GOSUB L.7200:GOSUB L.7250

L.400:
  LOCATE 16,24:INPUT"How many players (2-10): ",NP
  CLS:IF NP<2 OR NP>10 THEN L.400
  FOR I=1 TO NP

L.430:
   LOCATE (3+(NP>8))*I-1,20
   PRINT"Enter name of player #"MID$(STR$(I),2);
   LINE INPUT": ",PN$(I)
   IF PN$(I)="" THEN L.430
   P(I,1)=1500
  NEXT I
  PN$(0)="Bank"
  CLS:PRINT:PRINT"Throwing dice to see who starts..."

L.500:
  PRINT:MT=0:NPL=0
  FOR I=1 TO NP
   IF PL(I)<>-1 THEN
    GOSUB L.7500
    PRINT PN$(I)" threw"D1"+"D2"="DT
    PL(I)=DT
    IF DT>MT THEN NPL=0:PT=I:MT=DT
    IF DT>=MT THEN NPL=NPL+1
   END IF
  NEXT
  PRINT
  IF NPL=1 THEN PRINT PN$(PT)" starts":GOTO L.910
  PRINT"It's a tie between"NPL"players.  Those players throw again."
  FOR I=1 TO NP
   IF PL(I)<MT THEN PL(I)=-1
  NEXT
  GOTO L.500

L.900:
  ' Next player
  PT=PT MOD NP+1
  IF P(PT,1)<0 THEN L.900
  NT=0:XT=0

L.910:
  ' Extra turn
  ER=0:PRINT:PRINT PN$(PT)".  It's your turn."
  PRINT"You are on ";
  S=P(PT,0):BR=1:GOSUB L.7600
  PRINT:PRINT"You have "FND$(P(PT,1))" in hand"
  PRINT:LINE INPUT"Enter number or hit <RETURN> for menu: ",OP$
  CLS:IF OP$<>""THEN L.1110
  PRINT
  PRINT PN$(PT)", do you want to:":PRINT
  PRINT TAB(18)"1:  Throw dice"
  PRINT TAB(18)"2:  Display a player's assets"
  PRINT TAB(18)"3:  See where players are"
  PRINT TAB(18)"4:  Inspect a title deed"
  PRINT TAB(18)"5:  Mortgage property"
  PRINT TAB(18)"6:  Unmortgage property"
  PRINT TAB(18)"7:  Buy houses and hotels"
  PRINT TAB(18)"8:  Sell houses and hotels"
  PRINT TAB(18)"9:  Bargain with other players"
  IF P(PT,0)<0 THEN
   PRINT TAB(17)"10:  Pay $50 to get out of jail"
   PRINT TAB(17)"11:  Use a Get Out of Jail Free Card"
  END IF
  IF NT=0 THEN PRINT:PRINT"(Options 2-9 are open to any player)"
  GOTO L.910

L.1110:
  OP=VAL(OP$)
  IF OP<1 OR OP>11 OR OP>9 AND P(PT,0)>=0 THEN L.910
  ON OP GOTO L.2000,L.1200,L.1250,L.1300,L.1400,L.1500,L.1600,L.1700,L.1800,L.1900,L.1950

L.1200:
  ' Disp assets
  BI=0:GOSUB L.7800
  PRINT:PRINT PN$(PT)", ";
  LINE INPUT"Which player (enter number): ",R$
  CLS:IF R$=""THEN PY=PT ELSE PY=VAL(R$)
  IF PY<0 OR PY>NP THEN L.910
  GOSUB L.7400:GOTO L.910

L.1250:
  ' See players
  FOR PY=1 TO NP
   IF P(PY,1)>=0 THEN
    PRINT:PRINT PN$(PY)" has "FND$(P(PY,1))" and is on ";
    S=P(PY,0):BR=1:GOSUB L.7600
   END IF
  NEXT
  PRINT:GOTO L.910

L.1300:
  ' Disp deed
  PRINT"Title deeds are as follows:":PRINT
  FOR I=0 TO 6
   PRINT USING" ## \             \ ";I;PD$(I);I+7;PD$(I+7);I+14;PD$(I+14);I+21;PD$(I+21)
  NEXT
  PRINT:INPUT"Which deed do you wish to inspect (enter number): ",P
  CLS:IF P>=0 AND P<=27 THEN GOSUB L.7300
  GOTO L.910

L.1400:
  ' Mortgage
  GOSUB L.8100:IF PM THEN GOSUB L.8400
  GOTO L.910

L.1500:
  ' Unmortgage
  GOSUB L.8100:IF PM=0 THEN L.910
  PRINT PN$(PM)", you ";:IF P(PM,4)=0 THEN PRINT"don't own any property deeds":GOTO L.910
  PRINT"own the following properties:"
  PY=PM:WN=1:GOSUB L.7900

L.1550:
  PH=PM:GOSUB L.8000
  LINE INPUT"Which do you wish to unmortgage (enter number or hit <RETURN>): ",R$
  IF R$="" THEN GOTO L.910
  P=VAL(R$)
  IF P<0 OR P>27 THEN GOTO L.910
  IF PD(P,0)<>PM THEN PRINT"You don't own "PD$(P):GOTO L.910
  IF PD(P,1)>=0 THEN PRINT PD$(P)" is not mortgaged":GOTO L.910
  IF P(PM,1)<INT(PD(P,2)*0.55) THEN PRINT"You don't have enough cash to unmortgage "PD$(P):GOTO L.910
  GOSUB L.5700:PRINT PN$(PM)", that cost you "FND$(INT(PD(P,2)*0.55))
  GOTO L.1550

L.1600:
  ' Buy houses
  GOSUB L.8100
  R$="build on":GOSUB L.8300:IF G<0 THEN L.910
  MH=5*GC(G,1)-GC(G,6)
  IF MH<=0 THEN PRINT"The "GC$(G)" group is loaded with Hotels":GOTO L.910
  PRINT"Houses on the "GC$(G)" group cost "FND$(50*(G\2+1))" each, and you have "FND$(P(PM,1))" on hand"
  PRINT"Limit of house buying is"MH"as this would be"GC(G,1)"Hotels"
  PRINT:HT=0
  FOR H=1 TO GC(G,1)
   P=GC(G,H+1)
   IF PD(P,1)<5 THEN PRINT"How many houses do you wish to buy on "PD$(P)" (has"PD(P,1)"now): ";ELSE HN(H)=5:GOTO L.1655
   INPUT"",HO:HN(H)=PD(P,1)+HO:HT=HT+HO
   IF HO<0 OR HN(H)>5 THEN PRINT"Too many houses":GOTO L.910
   PRINT"  ie new total of"FNH$(HN(H))" on "PD$(P)

L.1655:
   FOR I=1 TO H-1
    IF ABS(HN(H)-HN(I))>1 THEN PRINT"Not allowed uneven building":GOTO L.910
   NEXT
  NEXT
  HC=HT*50*(G\2+1)
  PRINT PN$(PM)", that costs you "FND$(HC)" for"FNS$(HT,"new house")
  FR=PM:DE=0:AM=HC:CO=1:GOSUB L.6000
  IF CO=1 THEN PRINT"You didn't buy any houses":GOTO L.910
  IF GC(G,0)<>PM OR GC(G,6)<0 THEN PRINT PN$(PM)", you can't buy houses on the "GC$(G)" group now":GOTO L.910
  GC(G,6)=GC(G,6)+HT:PD(GC(G,2),1)=HN(1):PD(GC(G,3),1)=HN(2)
  IF GC(G,1)=3THEN PD(GC(G,4),1)=HN(3)
  GOTO L.910

L.1700:
  ' Sell houses
  GOSUB L.8100:IF PM THEN GOSUB L.8500
  GOTO L.910

L.1800:
  ' Bargain
  GOSUB L.4100:GOTO L.910

L.1900:
  ' Get out of jail ($50)
  PRINT PN$(PT)", that costs you $50"
  FR=PT:DE=0:AM=50:CO=1:GOSUB L.6000
  IF CO THEN PRINT"You didn't pay after all, so you're still in jail":GOTO L.910 ELSE L.1990

L.1950:
  ' Get out of jail free
  IF P(PT,2)=0 THEN PRINT"But "PN$(PT)", you don't have any Get Out of Jail Free cards":GOTO L.910
  P(PT,2)=P(PT,2)-1
  PRINT PN$(PT)", you used a Get Out of Jail Free card and now have"P(PT,2)"left"

L.1990:
  P(PT,0)=10

L.2000:
  ' Throw dice
  GOSUB L.7500:NT=NT+1
  PRINT PN$(PT)", you threw"D1"+"D2"="DT
  IF P(PT,0)<0 THEN L.2130:' In jail
  IF D1=D2 THEN PRINT"That is your "FNC$(NT)" double":IF NT>=3THEN GOSUB L.5400:GOTO L.900

L.2026:
  P(PT,0)=P(PT,0)+DT:' come here from 2140 & 2230
  IF P(PT,0)>=40 THEN PRINT PG$:P(PT,0)=P(PT,0)-40:P(PT,1)=P(PT,1)+200
  PRINT"That takes you to ";:S=P(PT,0):BR=0:GOSUB L.7600:PRINT
  GOSUB L.3000:' handle square s

L.2070:
  ' check doubles
  IF D1<>D2 THEN L.900
  IF XT THEN PRINT PN$(PT)", your turn ends even though you threw doubles":GOTO L.900
  PRINT:PRINT PN$(PT)" - You threw doubles and may have another turn.":GOTO L.910

L.2130:
  ' in jail
  IF D1=D2 THEN PRINT"You threw a double and got out of jail":P(PT,0)=10:XT=1:GOTO L.2026
  PRINT"You failed to throw a double and remain in jail"
  P(PT,0)=P(PT,0)-1
  PRINT"You have now had"FNS$(-P(PT,0)-1,"attempt")" to get out of jail"
  IF P(PT,0)>-4THEN L.2070
  PRINT:PRINT"You must pay $50 and then move"
  FR=PT:DE=0:AM=50:CO=0:GOSUB L.6000
  P(PT,0)=10:GOTO L.2026

L.3000:
  ' Player PT on square S
  IF SI(S)>27 THEN ON SI(S)-27 GOTO L.3330,L.3400,L.3450,L.3800,L.5400,L.3850,L.3970,L.4010
  ' Property
  P=SI(S)
  IF PD(P,0)=PT THEN PRINT"You don't do anything on your own property":RETURN
  IF PD(P,0)=0 THEN L.3230:' For sale
  IF PD(P,1)<0 THEN PRINT"The property is mortgaged":RETURN
  DE=PD(P,0)
  ' Rent
  IF PD(P,3)=9 THEN PRINT"Rent is ";:IF GC(9,0)=0 AND ER=0 THEN PRINT"4 * dice":AM=4*DT:GOTO L.3190 ELSE PRINT"10 * dice":AM=10*DT:GOTO L.3190
  IF PD(P,3)=8 THEN PRINT PN$(DE)" has"FNS$(P(DE,14),"station"):AM=12.5*2^P(DE,14):IF ER=1 THEN AM=2*AM:GOTO L.3190 ELSE L.3190
  AM=PD(P,PD(P,1)+4)
  IF GC(PD(P,3),0) AND PD(P,1)=0 THEN PRINT"Rent is doubled":AM=2*AM

L.3190:
  PRINT"You pay "FND$(AM)" rent to "PN$(DE)
  FR=PT:CO=0:GOSUB L.6000
  PH=PT:GOTO L.8000

L.3230:
  PRINT PD$(P)" is for sale for "FND$(PD(P,2))
  PH=PT:GOSUB L.8000
  LINE INPUT"Do you want to buy or auction (B or A): ",R$
  R$=LEFT$(R$,1)
  IF R$="A" OR R$="a" THEN L.5500
  IF R$<>"B" AND R$<>"b" THEN L.3230
  ' Buy
  FR=PT:DE=0:AM=PD(P,2):CO=1:GOSUB L.6000
  IF CO=1THEN PRINT PN$(FR)" didn't buy "PD$(P)" after all":GOTO L.3230
  DE=PT:GOSUB L.5900
  PRINT PN$(PT)" just bought "PD$(P)" for "FND$(AM)" and has "FND$(P(PT,1))" left"

L.3330:
  RETURN

L.3400:
  ' CC card
  CC=(CC+1) MOD 16
  C=CCP(CC):C$=CC$(C):P0=CC(C,0):P1=CC(C,1):P2=CC(C,2)
  GOTO L.3500

L.3450:
  ' CH card
  CH=(CH+1) MOD 16
  C=CHP(CH):C$=CH$(C):P0=CH(C,0):P1=CH(C,1):P2=CH(C,2)

L.3500:
  ' Card C
  PRINT:PRINT"The card reads:":PRINT:PRINT C$:PRINT
  ON P0+1 GOTO L.3540,L.3590,L.5400,L.3660,L.3700,L.3730,L.3740,L.3750,L.3760,L.3770

L.3540:
  ' Advance to P1
  IF P1<=S THEN PRINT PG$:P(PT,1)=P(PT,1)+200
  S=P1:P(PT,0)=S:PRINT"You are now on ";:BR=0:GOSUB L.7600:PRINT
  GOTO L.3000:' recursive

L.3590:
  ' Pay money
  AM=P1:GOTO L.3995

L.3660:
  ' Collect money
  P(PT,1)=P(PT,1)+P1
  PRINT"You collected "FND$(P1)" and now have "FND$(P(PT,1))" on hand"
  RETURN

L.3700:
  ' Street repairs
  NHS=0:NHT=0
  FOR P=0 TO 27
   IF PD(P,3)<8 THEN IF PD(P,0)=PT THEN IF PD(P,1)>0 THEN IF PD(P,1)<5 THEN NHS=NHS+PD(P,1) ELSE NHT=NHT+1
  NEXT
  PRINT"You have"FNS$(NHS,"house")" and"FNS$(NHT,"hotel")
  AM=P1*NHS+P2*NHT
  PRINT"You must pay "FND$(AM):GOTO L.3995

L.3730:
  ' Collect P1 from all players
  FOR PL=1 TO NP
   IF PL<>PT THEN IF P(PL,1)>=0 THEN FR=PL:DE=PT:AM=P1:CO=0:GOSUB L.6000
  NEXT
  PH=PT:GOTO L.8000

L.3740:
  ' Get Out of Jail Free
  P(PT,2)=P(PT,2)+1
  PRINT"You now have"FNS$(P(PT,2),"Get Out of Jail Free card")
  RETURN

L.3750:
  ' Go back P1 spaces
  S=S-P1:IF S<0 THEN S=S+40
  P(PT,0)=S
  PRINT"That takes you back to ";:BR=0:GOSUB L.7600:PRINT
  GOTO L.3000:' recursive

L.3760:
  ' Pay each player P1
  FOR PL=1 TO NP
   IF PL<>PT THEN IF P(PL,1)>=0 THEN FR=PT:DE=PL:AM=P1:CO=0:GOSUB L.6000
  NEXT
  PH=PT:GOTO L.8000

L.3770:
  ' Advance to station/utility
  IF P1=0 THEN S=(((S+5)\10)*10+5) MOD 40 ELSE IF S>=12 AND S<28 THEN S=28 ELSE S=12
  IF S<P(PT,0) THEN PRINT PG$:P(PT,1)=P(PT,1)+200
  P(PT,0)=S
  PRINT"You are now on ";:BR=0:GOSUB L.7600:PRINT:ER=1:GOTO L.3000

L.3800:
  ' Free Parking
  PRINT"You don't do anything on Free Parking":RETURN

L.3850:
  ' Income tax

L.3860:
  PH=PT:GOSUB L.8000
  LINE INPUT"Do you want to pay 10% or $200, (% or $): ",R$
  R$=LEFT$(R$,1)
  IF R$="$"THEN AM=200:GOTO L.3995
  IF R$<>"%"THEN L.3860
  GOSUB L.5100:' Compute wealth
  PRINT"Your total wealth is "FND$(W)", so you must pay "FND$(INT(W/10))
  AM=INT(W/10):GOTO L.3995

L.3970:
  ' Super tax
  PRINT"You must pay $100":AM=100

L.3995:
  ' Pay to bank
  FR=PT:DE=0:CO=0:GOSUB L.6000:PH=PT:GOTO L.8000

L.4010:
  ' Visit jail
  PRINT"You don't do anything while just visiting jail"
  J=0
  FOR I=1 TO NP
   IF P(I,1)>=0 AND P(I,0)<0 THEN J=I
  NEXT
  IF J THEN PRINT"but you can say hello to "PN$(J)" through the bars!!!"
  RETURN

L.4100:
  ' Bargain
  B=0

L.4112:
  B=B+1:IF B<=NB THEN GOSUB L.4900:GOTO L.4112
  GOSUB L.5000
  PRINT:PRINT"Do you want to:"
  PRINT TAB(12)"1:  Add a money transaction to bargain"
  PRINT TAB(12)"2:  Add a property transaction to bargain"
  PRINT TAB(12)"3:  Add a Get Out of Jail Free card transaction to bargain"
  PRINT TAB(12)"4:  Delete a bargain entry"
  PRINT TAB(12)"5:  Clear bargain"
  PRINT TAB(12)"6:  Carry out bargain and return to game"
  PRINT TAB(12)"7:  Return to game (retaining current bargain)"
  PRINT:INPUT"Enter number: ",OP
  CLS:IF OP<=0 OR OP>7 THEN L.4100
  IF OP<4 AND NB=10 THEN PRINT"Bargain cannot have more than 10 entries":GOTO L.4100
  ON OP GOTO L.4230,L.4280,L.4390,L.4460,L.4840,L.4500,L.4750

L.4222:
  NB=NB+1:B(NB,0)=P3:B(NB,1)=P2:B(NB,2)=P0:B(NB,3)=P1:GOTO L.4100

L.4230:
  ' Add money trans
  BI=1:GOSUB L.7800
  PRINT:INPUT"Enter player from, player to and amount: ",P0,P1,P2
  CLS:IF P0<0 OR P0>NP OR P1<0 OR P1>NP OR P0=P1 OR P2<=0 THEN PRINT"Invalid":GOTO L.4100
  IF P(P0,1)<0 OR P(P1,1)<0 THEN PRINT"One of those players is out":GOTO L.4100
  P3=0:GOTO L.4222

L.4280:
  ' Add prop trans
  BI=1:GOSUB L.7800
  PRINT:INPUT"Enter player from and player to (enter numbers): ",P0,P1
  CLS:IF P0<0 OR P0>NP OR P1<0 OR P1>NP OR P0=P1 THEN PRINT"Invalid":GOTO L.4100
  IF P(P0,1)<0 OR P(P1,1)<0 THEN PRINT"One of those players is out":GOTO L.4100
  IF P(P0,4)=0 THEN PRINT PN$(P0)" owns no title deeds":GOTO L.4100
  PRINT PN$(P0)" owns the following title deeds:"
  PY=P0:WN=1:GOSUB L.7900
  PRINT:PRINT"Which does "PN$(P0)" give to "PN$(P1);
  INPUT" (enter number): ",P2
  CLS:IF P2<0 OR P2>27 THEN L.4100
  IF PD(P2,0)<>P0 THEN PRINT PN$(P0)" doesn't own "PD$(P2):GOTO L.4100
  IF GC(PD(P2,3),6)>0 THEN PRINT"The "GC$(PD(P2,3))" group has houses on it":GOTO L.4100
  FOR B=1 TO NB
   IF B(B,0)=1 THEN IF P2=B(B,1) THEN PRINT PD$(P2)" is already involved in a transaction":GOTO L.4100
  NEXT
  P3=1:GOTO L.4222

L.4390:
  ' Get Out of Jail Free card trans
  BI=1:GOSUB L.7800
  PRINT:INPUT"Enter player from, player to and number of cards (enter numbers): ",P0,P1,P2
  CLS:IF P0<0 OR P0>NP OR P1<0 OR P1>NP OR P0=P1 OR P2<=0 THEN PRINT"Invalid":GOTO L.4100
  IF P(P0,1)<0 OR P(P1,1)<0 THEN PRINT"One of those players is out":GOTO L.4100
  IF P(P0,2)<P2 THEN PRINT PN$(P0)" doesn't have that many Get Out of Jail Free cards":GOTO L.4100
  P3=2:GOTO L.4222

L.4460:
  ' Delete
  IF NB=0 THEN L.4100 ELSE GOSUB L.5000
  PRINT:INPUT"Which entry do you want to delete (enter number): ",B
  CLS:IF B>=0 AND B<=NB THEN GOSUB L.4950
  GOTO L.4100

L.4500:
  ' Carry out bargain
  IF NB=0 THEN L.4100
  FOR I=1 TO NP:PL(I)=P(I,1):P(I,16)=0:NEXT
  BM=0
  FOR B=1 TO NB
   BM=BM+B(B,0):P0=B(B,1):P1=B(B,2):P2=B(B,3)
   P(P1,16)=(P(P1,16) OR 1):P(P2,16)=(P(P2,16) OR 2)
   IF B(B,0)=0 THEN PL(P1)=PL(P1)-P0:PL(P2)=PL(P2)+P0
   IF B(B,0)=1 THEN IF PD(P0,1)<0 THEN PL(P2)=PL(P2)-INT(PD(P0,2)*.05)
  NEXT
  IF BM=0 THEN PRINT"Bargains must include property not just money":GOTO L.4100
  FOR I=1 TO NP
   IF P(I,16)=1 THEN PRINT PN$(I)" gives, but doesn't receive":GOTO L.4100
   IF P(I,16)=2 THEN PRINT PN$(I)" receives, but doesn't give":GOTO L.4100
   IF P(I,16)=3 THEN IF PL(I)<0THEN PRINT PN$(I)" can't afford the bargain (has only "FND$(P(I,1))")":GOTO L.4100
  NEXT
  GOSUB L.5000
  PRINT
  FOR I=1 TO NP
   IF P(I,16)<>0 THEN

L.4600:
    PRINT PN$(I);:LINE INPUT", are you satisfied with the above bargain (Y or N): ",R$
    R$=LEFT$(R$,1)
    IF R$="N" OR R$="n"THEN L.4100
    IF R$<>"Y" AND R$<>"y"THEN L.4600
   END IF
  NEXT
  FOR B=1 TO NB
   P0=B(B,1):P1=B(B,2):P2=B(B,3):IF B(B,0)=0 THEN L.4700
   IF B(B,0)=2 THEN P(P1,2)=P(P1,2)-B(B,1):P(P2,2)=P(P2,2)+P0:GOTO L.4700
   P=P0:FR=P1:GOSUB L.5800:DE=P2:GOSUB L.5900
   IF PD(P,1)<0 THEN PRINT PN$(DE)" paid "FND$(INT(PD(P,2)*.05))" mortgage interest"

L.4700:
  NEXT
  FOR I=1 TO NP:P(I,1)=PL(I):NEXT
  PRINT"Bargain carried out":NB=0

L.4750:
  RETURN

L.4840:
  ' Clear bargain
  NB=0:GOTO L.4100

L.4900:
  ' Check bargain entry B and delete if necessary
  IF P(B(B,2),1)<0 OR P(B(B,3),1)<0 THEN L.4950
  IF B(B,0)=1 THEN IF PD(B(B,1),0)<>B(B,2) OR GC(PD(B(B,1),3),6)>0 THEN L.4950
  IF B(B,0)=2 THEN IF P(B(B,2),2)<B(B,1) THEN L.4950
  RETURN

L.4950:
  ' Delete bargain entry B
  NB=NB-1
  FOR I=B TO NB
   B(I,0)=B(I+1,0):B(I,1)=B(I+1,1):B(I,2)=B(I+1,2):B(I,3)=B(I+1,3)
  NEXT
  B=B-1:RETURN

L.5000:
  ' Display bargain
  PRINT
  IF NB=0 THEN PRINT"There is currently no bargain set up":RETURN
  PRINT"The current bargain is as follows:"
  FOR I=1 TO NB
   PRINT I;TAB(6)PN$(B(I,2))" gives";
   IF B(I,0)=0 THEN PRINT" "FND$(B(I,1));
   IF B(I,0)=1 THEN PRINT" "PD$(B(I,1));
   IF B(I,0)=2 THEN PRINT FNS$(B(I,1),"Get Out of Jail Free card");
   PRINT" to "PN$(B(I,3))
  NEXT
  RETURN

L.5100:
  ' Compute wealth of PT, in W
  W=P(PT,1)
  FOR P=0 TO 27
   IF PD(P,0)=PT THEN W=W+PD(P,2): IF PD(P,1)>0 THEN W=W+50*(PD(P,3)\2+1)*PD(P,1)
  NEXT P
  RETURN

L.5400:
  ' PT goes to jail
  PRINT "You are in jail": P(PT,0)=-1: XT=1: RETURN

L.5500:
  ' Auction property P to highest bidder
  PRINT: PRINT PD$(P)" is up for auction"
  BI=1: GOSUB L.7800
  PRINT: INPUT "Which player bid the most (enter number): ",FR
  IF FR<0 OR FR>NP THEN L.5500
  IF P(FR,1)<0 THEN PRINT PN$(FR)" is bankrupt and out of the game": GOTO L.5500
  PRINT "How much money did "PN$(FR)" bid: ";
  INPUT "",AM
  IF AM<=0 THEN L.5500
  DE=0: CO=1: GOSUB L.6000
  IF CO=1 THEN PRINT PN$(FR)" didn't buy "PD$(P)" after all": GOTO L.5500
  DE=FR: GOSUB L.5900
  PRINT PN$(FR)" just bought "PD$(P)" for "FND$(AM)" and has "FND$(P(FR,1))" left"
  RETURN

L.5600:
  ' Mortgage property P
  PD(P,1)=-1: OW=PD(P,0): P(OW,1)=P(OW,1)+PD(P,2)\2
  GC(PD(P,3),6)=GC(PD(P,3),6)-1: RETURN

L.5700:
  ' Unmortgage property P
  PD(P,1)=0: OW=PD(P,0)
  IF OW THEN P(OW,1)=P(OW,1)-INT(PD(P,2)*0.55)
  GC(PD(P,3),6)=GC(PD(P,3),6)+1: RETURN

L.5800:
  ' Player FR loses property P
  IF P(FR,PD(P,3)+6)<GC(PD(P,3),1) THEN L.5865
  PRINT: PRINT PN$(FR)", you no longer own the "GC$(PD(P,3))" group": PRINT
  GC(PD(P,3),0)=0: P(FR,5)=P(FR,5)-1

L.5865:
  PD(P,0)=0: P(FR,4)=P(FR,4)-1: P(0,4)=P(0,4)+1
  P(FR,PD(P,3)+6)=P(FR,PD(P,3)+6)-1: RETURN

L.5900:
  ' Player DE receives property P
  P(DE,4)=P(DE,4)+1: PD(P,0)=DE: P(0,4)=P(0,4)-1
  P(DE,PD(P,3)+6)=P(DE,PD(P,3)+6)+1
  IF P(DE,PD(P,3)+6)<GC(PD(P,3),1) THEN RETURN
  PRINT: PRINT PN$(DE)", you now own the complete "GC$(PD(P,3))" group": PRINT: BEEP
  GC(PD(P,3),0)=DE: P(DE,5)=P(DE,5)+1: RETURN

L.6000:
  ' Transaction of AM dollars from FR to DE, compulsary if CO=0
  ' Return with CO=0 if paid, CO=1 if not paid
  SP=P: SPM=PM: ' save current property number and player

L.6005:
  IF FR=0 THEN L.6020
  IF P(FR,1)<AM THEN L.6100
  P(FR,1)=P(FR,1)-AM

L.6020:
  IF DE<>0 THEN P(DE,1)=P(DE,1)+AM

L.6030:
  CO=0
  PRINT PN$(FR)" paid "FND$(AM)" to "PN$(DE)

L.6040:
  P=SP: PM=SPM: ' restore P and PM
  RETURN

L.6100:
  ' FR can't pay
  PRINT: PRINT PN$(FR)", you owe "FND$(AM)" to "PN$(DE)" but you have only "FND$(P(FR,1))" on hand"
  PRINT: PRINT "Do you want to: "
  PRINT "  1:  Sell houses"
  PRINT "  2:  Mortgage property"
  PRINT "  3:  Bargain with other players"
  PRINT "  4:  ";: IF CO THEN PRINT "Retract the transaction" ELSE PRINT "Declare bankruptcy"
  PRINT: INPUT "Enter number: ", OP
  CLS: IF OP<1 OR OP>4 THEN L.6100
  ON OP GOTO L.6250,L.6300,L.6350,L.6400

L.6250:
  ' Sell houses
  PM=FR: GOSUB L.8500: GOTO L.6005

L.6300:
  ' Mortgage property
  PM=FR: GOSUB L.8400: GOTO L.6005

L.6350:
  ' Bargain with other players
  GOSUB L.4100: GOTO L.6005

L.6400:
  ' Retract the transaction or go bankrupt
  IF CO THEN L.6040: ' Transaction not done
  PB=FR: PW=DE
  ' Player PB is bankrupt to PW
  PRINT: PRINT PN$(PB)" is bankrupt to "PN$(PW)" and is out of the game!!!": PRINT
  BEEP: BEEP: BEEP
  ' Remove all PB's houses
  FOR P=0 TO 27
   IF PD(P,0)<>PB OR PD(P,1)<=0 THEN L.6572
   I=PD(P,1)*25*(PD(P,3)\2+1): PRINT FNH$(PD(P,1))" from "PD$(P)" gives "FND$(I)
   P(PB,1)=P(PB,1)+I: PD(P,1)=0: GC(PD(P,3),6)=0

L.6572:
  NEXT P
  ' Turn over cash on hand to PW
  PRINT: PRINT PN$(PB)" hands over "FND$(P(PB,1))" to "PN$(PW)
  IF PW THEN P(PW,1)=P(PW,1)+P(PB,1)
  P(PB,1)=-1: ' flag PB as bankrupt
  IF PW=0 THEN L.6800
  ' Turn all PB's properties over to PW
  FOR P=0 TO 27
   IF PD(P,0)<>PB THEN L.6770
   FR=PB: GOSUB L.5800
   PRINT: PRINT PN$(PW)" now owns "PD$(P)
   DE=PW: GOSUB L.5900
   IF PD(P,1)>=0 THEN L.6770

L.6708:
   IF PD(P,2)>P(PW,1) THEN PRINT PN$(PW)", you don't have enough money to unmortgage "PD$(P)" right now": GOTO L.6750

L.6710:
   PH=PW: GOSUB L.8000
   PRINT "Do you wish to unmortgage "PD$(P)" (Y or N): ";
   LINE INPUT R$: IF R$="" THEN L.6710
   R$=LEFT$(R$,1): IF R$="Y" OR R$="y" THEN L.6760
   IF R$<>"N" AND R$<>"n" THEN L.6710

L.6750:
   AM=INT(PD(P,2)*.05)
   PRINT "That costs "FND$(AM)" to keep it mortgaged"
   FR=PW: DE=0: CO=1: GOSUB L.6000
   IF CO=1 THEN L.6708
   GOTO L.6770

L.6760:
   PRINT "That costs "FND$(INT(PD(P,2)*.55))" to unmortgage it"
   GOSUB L.5700

L.6770:
  NEXT P
  GOTO L.6880

L.6800:
  ' Auction off properties
  FOR P=0 TO 27
   IF PD(P,0)<>PB THEN L.6870
   FR=PB: GOSUB L.5800: ' return it to bank
   IF PD(P,1)<0 THEN GOSUB L.5700: ' unmortgage it
   GOSUB L.5500: ' auction it

L.6870:
  NEXT P

L.6880:
  PRINT
  ' Check to see if there is only one player left
  J=0
  FOR I=1 TO NP
   IF P(I,1)>=0 THEN J=J+1: WN=I
  NEXT I
  IF J<=1 THEN L.6960
  IF PB=PT THEN XT=1: RETURN L.2070: ' Abort PT's turn
  GOTO L.6030: ' continue PT's turn 'cos another player is out

L.6960:
  ' We have a winner
  IF J=0 THEN WN=PW: P(PW,1)=0: ' Tidy up case where everyone's bankrupt
  PRINT: PRINT "Congratulations "PN$(WN)", you won."
  PRINT: PRINT "You finished with "FND$(P(WN,1))","P(WN,4)"properties and"P(WN,5)"complete groups"
  PT=WN: GOSUB L.5100
  PRINT: PRINT "You finished worth "FND$(W)

L.6990:
  PRINT: LINE INPUT "Want to play again: ", R$
  IF R$="" THEN L.6990
  R$=LEFT$(R$,1): IF R$="Y" OR R$="y" THEN L.10
  PRINT: PRINT "Thanks for playing"
  STOP

L.7200:
  ' Shuffle Community Chest cards
  LOCATE 13,20: PRINT "Shuffling Community Chest cards..."
  FOR I=0 TO 15: CCP(I)=I: NEXT
  FOR I=0 TO 14: SWAP CCP(I),CCP(I+INT(RND*(16-I))): NEXT
  CC=0: RETURN

L.7250:
  ' Shuffle Chance Cards
  LOCATE 14,24: PRINT "Shuffling Chance cards...";
  FOR I=0 TO 15: CHP(I)=I: NEXT
  FOR I=0 TO 14: SWAP CHP(I),CHP(I+INT(RND*(16-I))): NEXT
  CH=0: RETURN

L.7300:
  ' Display title deed for property P
  LOCATE 2,10: PRINT PD$(P)TAB(34)GC$(PD(P,3))TAB(54)FND$(PD(P,2))
  PRINT: PRINT TAB(27)"Owned by "PN$(PD(P,0));
  IF PD(P,0)=0 OR PD(P,1)=0 THEN PRINT: GOTO L.7327
  IF PD(P,1)>0 THEN PRINT " with"FNH$(PD(P,1))
  IF PD(P,1)<0 THEN PRINT " mortgaged"

L.7327:
  PRINT
  IF PD(P,3)=8 THEN L.7350
  IF PD(P,3)=9 THEN L.7360
  PRINT TAB(20)"Rent  -  Site only    "FND$(PD(P,4))
  PRINT TAB(29)"1 house     "; PD(P,5)
  PRINT TAB(29)"2 houses    "; PD(P,6)
  PRINT TAB(29)"3 houses    "; PD(P,7)
  PRINT TAB(29)"4 houses    "; PD(P,8)
  PRINT TAB(29)"Hotel       "; PD(P,9)
  PRINT TAB(20)"Cost of houses,       "FND$(50*(PD(P,3)\2+1))
  GOTO L.7370

L.7350:
  PRINT TAB(20)"Rent                     $25"
  PRINT TAB(20)"If 2 Railways are owned   50"
  PRINT TAB(20)"If 3 Railways are owned  100"
  PRINT TAB(20)"If 4 Railways are owned  200"
  GOTO L.7370

L.7360:
  PRINT TAB(20)"If one 'Utility' is owned rent is"
  PRINT TAB(20)"4 times amount shown on dice."
  PRINT
  PRINT TAB(20)"If both 'Utilities' are owned rent"
  PRINT TAB(20)"is 10 times amount shown on dice."

L.7370:
  PRINT: PRINT TAB(20)"Mortgage value of site, "FND$(PD(P,2)\2)
  PRINT
  RETURN

L.7400:
  ' Display player PY's assets
  PRINT: IF PY=0 THEN PRINT "The bank ";: GOTO L.7410
  PRINT PN$(PY);: IF P(PY,1)<0 THEN PRINT " is bankrupt and out of the game": RETURN
  PRINT " has "FND$(P(PY,1));
  IF P(PY,2)>0 THEN PRINT " and"FNS$(P(PY,2),"Get Out of Jail Free card");
  PRINT " on hand and ";

L.7410:
  IF P(PY,4)=0 THEN PRINT "has no title deeds" ELSE PRINT "owns the following title deeds:"
  WN=0: GOSUB L.7900
  IF PY=0 OR P(PY,5)=0 THEN RETURN
  PRINT: PRINT PN$(PY)" has the following complete groups:"
  FOR I=0 TO 9
   IF GC(I,0)=PY THEN PRINT "   "GC$(I);: IF GC(I,6)<0 THEN PRINT " (mortgaged)";
  NEXT I
  PRINT: RETURN

L.7500:
  ' Throw dice.  Return in D1 and D2, total in DT
  D1=INT(RND*6)+1: D2=INT(RND*6)+1: DT=D1+D2: RETURN

L.7600:
  ' Print out short description of square S.  (Brief if BR=1)
  IF S<0 THEN PRINT "Jail (behind bars)  -  had"FNS$(-P(PT,0)-1,"attempt")" to get out": RETURN
  IF SI(S)>27 THEN ON SI(S)-27 GOTO L.7675,L.7680,L.7720,L.7730,L.7732,L.7740,L.7750,L.7760
  P=SI(S)
  PRINT PD$(P);
  IF BR=1 THEN BR=0: RETURN
  PRINT " - value "FND$(PD(P,2))"  -  "GC$(PD(P,3))" group"
  PRINT "   - owned by "PN$(PD(P,0));
  IF PD(P,0)=0 THEN L.7670
  IF PD(P,1)<0 THEN PRINT " (mortgaged)";
  IF PD(P,3)<8 THEN IF PD(P,1)>0 THEN PRINT " with"FNH$(PD(P,1));

L.7670:
  RETURN

L.7675:
  PRINT "Go (collect $200 salary as you pass)";: RETURN

L.7680:
  PRINT "Community Chest between ";

L.7690:
  S=S-1: BR=1: GOSUB L.7600
  PRINT " and ";
  S=S+2: BR=1: GOSUB L.7600: S=S-1: BR=0: RETURN

L.7720:
  PRINT "Chance between ";: GOTO L.7690

L.7730:
  PRINT "Free Parking";: RETURN

L.7732:
  PRINT "GO TO JAIL";: RETURN

L.7740:
  PRINT "Income Tax (pay 10% or $200)";: RETURN

L.7750:
  PRINT "Super Tax (pay $100)";: RETURN

L.7760:
  PRINT "Jail (just visiting)";: RETURN

L.7800:
  ' List players names & numbers (include bank if BI=0)
  PRINT "Players are as follows:": PRINT
  FOR I=BI TO NP
   IF P(I,1)>=0 THEN
    IF I=PT THEN PRINT TAB(15)"*";
    PRINT TAB(18)I;PN$(I)
   END IF
  NEXT I
  RETURN

L.7900:
  ' List deeds owned by PY.  Number if WN>1. Mortgage value if WN=1
  FOR P=0 TO 27
   IF PD(P,0)=PY THEN
    IF WN>0 THEN PRINT P;
    PRINT TAB(6)PD$(P)TAB(30)GC$(PD(P,3))TAB(42)FND$(PD(P,2));
    IF PD(P,1)>0 THEN PRINT TAB(48)"with"FNH$(PD(P,1));
    IF PD(P,1)<0 THEN PRINT TAB(48)"but it is mortgaged for "FND$(PD(P,2)\2);
    IF PD(P,1)=0 AND WN=1 THEN PRINT TAB(48)"mortgage value "FND$(PD(P,2)\2);
    PRINT
   END IF
  NEXT
  RETURN

L.8000:
  ' print player PH's cash on hand
  PRINT PN$(PH)", you now have "FND$(P(PH,1))" on hand": RETURN

L.8100:
  ' Get player id in PM
  IF NT>0 THEN PM=PT: RETURN
  BI=1: GOSUB L.7800
  PRINT: LINE INPUT "Please identify yourself (enter number): ",R$
  CLS: IF R$="" THEN PM=PT ELSE PM=VAL(R$)
  IF PM<=0 OR PM>NP THEN L.8180
  IF P(PM,1)<0 THEN PRINT PN$(PM)", you are out of the game": GOTO L.8180
  RETURN

L.8180:
  PM=0: ' invalid flag
  RETURN

L.8200:
  ' List properties on group G
  PRINT:PRINT"You now have"FNH$(PD(GC(G,2),1))" on "PD$(GC(G,2))
  PRINT TAB(12)FNH$(PD(GC(G,3),1))" on "PD$(GC(G,3))
  IF GC(G,1)=3 THEN PRINT TAB(12)FNH$(PD(GC(G,4),1))" on "PD$(GC(G,4))
  PRINT:RETURN

L.8300:
  ' Get group
  NG=0
  FOR I=0 TO 7
   IF GC(I,0)=PM THEN NG=NG+1:G=I
  NEXT
  IF NG=0 THEN PRINT PN$(PM)", you have no complete colour groups":GOTO L.8390
  IF NG=1 THEN L.8346
  PG=PM:GOSUB L.8600
  PRINT:PRINT"Which group do you wish to "R$;:INPUT" (enter number): ",G
  CLS:IF G<0 OR G>7 THEN L.8390
  IF GC(G,0)<>PM THEN PRINT"You don't own the "GC$(G)" group":GOTO L.8390

L.8346:
  IF GC(G,6)<0 THEN PRINT-GC(G,6)"of the "GC$(G)" properties are mortgaged":GOTO L.8390
  GOTO L.8200

L.8390:
  G=-1: RETURN

L.8400:
  ' Player PM mortgages property
  IF P(PM,4)=0 THEN PRINT PN$(PM)", you don't own any property deeds": RETURN
  PRINT PN$(PM)", you own the following properties:"
  PY=PM: WN=1: GOSUB L.7900

L.8425:
  PH=PM: GOSUB L.8000
  LINE INPUT "Which do you wish to mortgage (enter number or hit <RETURN>): "; R$
  IF R$="" THEN RETURN
  P=VAL(R$)
  IF P<0 OR P>27 THEN RETURN
  IF PD(P,0)<>PM THEN PRINT "You don't own "PD$(P): RETURN
  IF PD(P,1)<0 THEN PRINT PD$(P)" is already mortgaged": RETURN
  IF GC(PD(P,3),6)>0 THEN PRINT "The "GC$(PD(P,3))" group has houses on it": RETURN
  GOSUB L.5600: GOTO L.8425

L.8500:
  ' Player PM sells houses
  R$="sell houses from":GOSUB L.8300:IF G<0 THEN RETURN
  IF GC(G,6)=0 THEN PRINT "The "GC$(G)" group has no houses on it": RETURN
  PRINT "You get "FND$(25*(G\2+1))" for each house, and you have "FND$(P(PM,1))" on hand"
  PRINT: HT=0
  FOR H=1 TO GC(G,1)
   P=GC(G,H+1)
   IF PD(P,1)>0 THEN PRINT "How many houses do you sell from "PD$(P)" (has"PD(P,1)"now): "; ELSE HN(H)=0: GOTO L.8557
   INPUT "",HO: HN(H)=PD(P,1)-HO: HT=HT+HO
   IF HO<0 OR HN(H)<0 THEN PRINT "Too many houses": RETURN
   PRINT "  ie total of"FNH$(HN(H))" on "PD$(P)

L.8557:
   FOR I=1 TO H-1
    IF ABS(HN(H)-HN(I))>1 THEN PRINT "Not allowed uneven building": RETURN
   NEXT I
  NEXT H
  HC=HT*25*(G\2+1)
  PRINT PN$(PM)", you get "FND$(HC)" for selling"FNS$(HT,"house")
  P(PM,1)=P(PM,1)+HC: GC(G,6)=GC(G,6)-HT
  PD(GC(G,2),1)=HN(1): PD(GC(G,3),1)=HN(2)
  IF GC(G,1)=3 THEN PD(GC(G,4),1)=HN(3)
  RETURN

L.8600:
  ' List colour groups owned by PG
  PRINT: PRINT PN$(PG)", your complete colour groups are:"
  FOR G=0 TO 7
   IF GC(G,0)=PG THEN
    PRINT G;TAB(6)GC$(G)TAB(26);
    IF GC(G,6)<0 THEN PRINT "(mortgaged)" ELSE PRINT "with"FNS$(GC(G,6),"house")
   END IF
  NEXT
  RETURN
  DATA "Old Kent Road","Whitechapel Road","King's Cross Station","The Angel Islington","Euston Road","Pentonville Road","Pall Mall","Electric Company","Whitehall","Northumberland Avenue","Marylebone Station","Bow Street","Marlborough Street","Vine Street","Strand","Fleet Street"
  DATA "Trafalgar Square","Fenchurch St. Station","Leicester Square","Coventry Street","Water Works","Piccadilly","Regent Street","Oxford Street","Bond Street","Liverpool St. Station","Park Lane","Mayfair","Purple","Light Blue","Crimson","Orange","Red","Yellow","Green","Dark Blue","Station"
  DATA "Utility",60,0,2,10,30,90,160,250,60,0,4,20,60,180,320,450,200,8,0,0,0,0,0,0,100,1,6,30,90,270,400,550,100,1,6,30,90,270,400,550,120,1,8,40,100,300,450,600,140,2,10,50,150,450,625,750,150,9,0,0,0,0,0,0,140,2,10,50,150,450,625,750,160,2,12,60,180
  DATA 500,700,900,200,8,0,0,0,0,0,0,180,3,14,70,200,550,750,950,180,3,14,70,200,550,750,950,200,3,16,80,220,600,800,1000,220,4,18,90,250,700,875,1050,220,4,18,90,250,700,875,1050,240,4,20,100,300,750,925,1100,200,8,0,0,0,0,0,0,260,5,22,110,330,800,975
  DATA 1150,260,5,22,110,330,800,975,1150,150,9,0,0,0,0,0,0,280,5,22,120,360,850,1025,1200,300,6,26,130,390,900,1100,1275,300,6,26,130,390,900,1100,1275,320,6,28,150,450,1000,1200,1400,200,8,0,0,0,0,0,0,350,7,35,175,500,1100,1300,1500,400,7,50,200,600
  DATA 1400,1700,2000,2,0,1,0,0,3,3,4,5,0,3,6,8,9,0,3,11,12,13,0,3,14,15,16,0,3,18,19,21,0,3,22,23,24,0,2,26,27,0,0,4,2,10,17,25,2,7,20,0,0,28,0,29,1,33,2,3,30,4,5,35,6,7,8,9,10,11,29,12,13,31,14,30,15,16,17,18,19,20,21,32,22,23,29,24,25,30,26,34,27
  DATA "Advance to Go (Collect $200)","Pay Hospital $100","Go to Jail.  Go Directly to Jail.  Do Not Pass Go.  Do Not Collect $200","Bank Error in Your Favour.  Collect $200","Receive for Services $25"
  DATA "You are Assessed for Street Repairs.  $40 per House.  $115 per Hotel","You Inherit $100","Life Insurance Matures.  Collect $100","From Sale of Stock You Get $45","Pay School Tax of $150"
  DATA "Grand Opera Opening.  Collect $50 from Every Player for Opening Night Seats","Income Tax Refund.  Collect $20","You have Won Second Prize in a Beauty Contest.  Collect $10","Xmas Fund Matures.  Collect $100","Doctor's Fee.  Pay $50"
  DATA "Get Out of Jail Free.  This card may be kept until needed or sold","Take a Ride to King's Cross Station.  If You Pass Go Collect $200","Go Back Three Spaces","Bank Pays You Dividend of $50"
  DATA "Get Out of Jail Free.  This card may be kept until needed or sold","Advance to Trafalgar Square","Pay Poor Tax of $15","You Have Been Elected Chairman of the Board.  Pay Each Player $50","Your Building and Loan Matures.  Collect $150"
  DATA "Advance to the nearest Railroad and pay the owner Twice the Rental to which he  is otherwise entitled.  If Railroad is unowned you may buy it from the Bank","Advance to Pall Mall.  If You Pass Go Collect $200"
  DATA "Advance Token to Nearest Utility.  If Unowned you may buy it from bank.  If     Owned throw dice and pay owner ten times the amount thrown","Make General Repairs On ALL Your Property.  For Each House Pay $25.  For Each   Hotel $100"
  DATA "Take a Walk on the Board Walk.  Advance Token to Mayfair","Go Directly to Jail.  Do Not Pass Go. Do Not Collect $200","Advance to Go (Collect $200)"
  DATA "Advance to the nearest Railroad and pay the owner Twice the Rental to which he  is otherwise entitled.  If Railroad is unowned you may buy it from the Bank",0,0,0,1,100,0,2,0,0,3,200,0,3,25,0,4,40,115,3,100,0,3,100,0,3,45,0,1,150,0,5,50,0,3,20,0
  DATA 3,10,0,3,100,0,1,50,0,6,0,0,0,5,0,7,3,0,3,50,0,6,0,0,0,24,0,1,15,0,8,50,0,3,150,0,9,0,0,0,11,0,9,1,0,4,25,100,0,39,0,2,0,0,0,0,0,9,0,0
