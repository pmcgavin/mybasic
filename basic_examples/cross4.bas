  DEFINT A-Z
  DIM ALL$(25)
  A0=ASC("a")
  DO
    READ WORD$
    IF WORD$="zzz" THEN EXIT DO
    A=ASC(LEFT$(WORD$,1))-A0
    FOR I=LEN(ALL$(A))-2 TO 2 STEP -4
      IF WORD$>MID$(ALL$(A),I,3) THEN EXIT FOR
    NEXT I
    ALL$(A)=LEFT$(ALL$(A),I+2)+" "+WORD$+MID$(ALL$(A),I+3)
  LOOP
  REM FOR A=0 TO 25
  REM   PRINT A; LEN(A$)/4, ALL$(A)
  REM NEXT A
  REM STOP
  OPEN "PERFECT4" FOR OUTPUT AS #1
  FOR TL=0 TO 25                         ' for top-left char a-z
    FOR I=2 TO LEN(ALL$(TL))-2 STEP 4    ' for words starting with top-left char
      TOP$=MID$(ALL$(TL),I,3)            ' top row word
      TM$=MID$(TOP$,2,1)                 ' top-middle char
      TR$=RIGHT$(TOP$,1)                 ' top-right char
      TM=ASC(TM$)-A0
      TR=ASC(TR$)-A0
      FOR J=2 TO LEN(ALL$(TL))-2 STEP 4  ' for words starting with top-left char
        DOWNLEFT$=MID$(ALL$(TL),J,3)     ' left column word
        ML=ASC(MID$(DOWNLEFT$,2,1))-A0   ' middle-left char
        BL=ASC(RIGHT$(DOWNLEFT$,1))-A0   ' bottom-left char
        FOR K=2 TO LEN(ALL$(ML))-2 STEP 4' for words starting with mid-left char
          MIDDLE$=MID$(ALL$(ML),K,3)     ' middle row word
          TM2$=TM$+MID$(MIDDLE$,2,1)     ' top-middle char + middle-middle char
          IF INSTR(ALL$(TM)," "+TM2$) THEN
            TR2$=TR$+RIGHT$(MIDDLE$,1)   ' top-right char + middle-right char
            IF INSTR(ALL$(TR)," "+TR2$) THEN
              FOR L=2 TO LEN(ALL$(BL))-2 STEP 4
                BOTTOM$=MID$(ALL$(BL),L,3)  ' bottom row word
                IF INSTR(ALL$(TM),TM2$+MID$(BOTTOM$,2,1)) THEN
                  IF INSTR(ALL$(TR),TR2$+RIGHT$(BOTTOM$,1)) THEN
                    PRINT TOP$;" ";MIDDLE$;" ";BOTTOM$
                    PRINT #1,TOP$;" ";MIDDLE$;" ";BOTTOM$
                  END IF
                END IF
              NEXT L
            END IF
          END IF
        NEXT K
      NEXT J
    NEXT I
  NEXT TL
  CLOSE #1
  END
   REM WORD LIST
   REM A
  DATA ace,act,add,ado,aft,age,ago,aid,ail,aim,air,all,any,ant,ape,arc
  DATA are,arm,ark,ash,asp,ass,ate,auk,awe,awn,aye,axe
  REM B
  DATA bad,bag,ban,bar,bat,bay,bed,bee,beg,bet,bib,bid,big,bin,bis,bit,boa,bog
  DATA boo,bop,box,bow,bra,bud,bum,bun,bur,bus,but,buy,bye
  REM C
   DATA cab,cad,cam,cap,car,cat,cay,caw,cod,cog,con,coo,cop,cot,cow,cox
   DATA coy,cry,cub,cud,cue,cum,cup,cur,cut
  REM D
  DATA dab,dam,day,deb,den,dew,did,die,dig,dim,din,dip,doc,doe,dog,don,dot
  DATA dry,dub,dud,due,dun,duo,dye
  REM E
  DATA ear,eat,ebb,eel,eft,egg,ego,elf,ell,elm,end,eon,era,ere,erg,err,eve
  DATA ewe,eye
  REM F
  DATA fad,fag,fan,far,fat,feb,fed,fen,few,fey,fez,fib,fie,fig,fin,fir,fit
  DATA fix,flu,fly,fob,foe,fog,fop,for,fox,fro,fry,fur
  REM G
  DATA gab,gad,gag,gal,gap,gar,gas,gay,gel,gem,get,gig,gin,gob,god,goo,got
  DATA gum,gun,gut,guy,gym,gyp
  REM H
  DATA had,hag,ham,hap,hat,has,haw,hay,her,hew,hex,him,hip,his,hit,hob,hod
  DATA hoe,hog,hop,hot,how,hub,hue,hug,hum,hun,hut
  REM I
  DATA ice,ill,imp,ink,inn,ion,ire,irk,its,ivy
  REM J
  DATA jab,jam,jar,jaw,jay,jet,jew,jib,jig,job,jog,jot,joy,jug,jut
  REM K
  DATA key,kid,kin,kip,kit
  REM L
  DATA lab,lac,lag,lam,lap,law,lax,lay,lea,led,lee,leg,lei,leo,let,lid,lie
  DATA lip,lob,log,lop,lot,low,lox,lug,lye
  REM M
  DATA mad,man,map,mar,mat,maw,may,men,mew,mid,mix,mob,mod,moo,mop,mot,mud
  DATA mug,mum
  REM N
  DATA nab,nag,nap,net,new,nil,nip,nit,nix,nod,nor,not,now,nub,nun,nut
  REM O
  DATA oaf,oak,oar,oat,obi,odd,ode,off,oft,ohm,oil,old,one,opt,orb,ore,ort
  DATA our,out,ova,owe,owl,own
  REM P
  DATA pad,pal,pan,pap,par,pas,pat,paw,pay,pea,peg,pen,pep,per,pet,pew,pie
  DATA pig,pin,pip,pit,pix,ply,pod,poi,pop,pot,pox,pro,pry,pub,pug,pun,pup
  DATA pus,put
  REM Q
  DATA qua
  REM R
  DATA rag,ram,rap,rat,raw,ray,red,rem,rev,rib,rid,rig,rim,rip,rob,roc,rod
  DATA roe,rot,row,rub,rue,rug,rum,run,rut,rye
  REM S
  DATA sac,sad,sag,sap,saw,say,sea,see,set,sew,sex,she,shy,sic,sin,sip,sir
  DATA sit,six,ski,sky,sly,sob,sod,son,sop,sow,sox,soy,spa,spy,sty,sub,sum
  DATA sun,sup
  REM T
  DATA tab,tad,tag,tan,tap,tar,tat,tax,tea,tee,ten,the,thy,tie,tin,tip,tit
  DATA toe,tog,tom,ton,too,top,tot,tow,toy,tug,tun,two
  REM U
  DATA ugh,urn,use
  REM V
  DATA van,vat,vet,vex,via,vim,vow
  REM W
  DATA wad,wag,wan,war,was,wax,way,web,wet,who,why,wig,win,wit,wok,woe,won
  DATA woo,wow,wry
  REM X
  DATA xia
  REM Y
  DATA yak,yam,yap,yaw,yea,yen,yes,yet,yew,yip,yon,you
  REM Z
  DATA zap,zen,zip,zoo
  DATA zzz
