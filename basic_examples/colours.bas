defint a-z

for j = 0 to 8 step 8
  for k = 0 to 15 step 15
    for i = j to j + 7
      color k,i
      print using "##",i;
      print "    ";
      color 7,0
      print "  ";
    next i
    print
  next k
  print
next j

for i = 0 to 15
  for j = 0 to 7
    color i,j
    print using "##",i;
    print ",";
    print using "##",j;
    color 7,0
    print "    ";
  next j
  print
  print
next i
