dim a$(10)
n = 5
i = 2
j = 3

a$ = "123456789"
mid$(a$,2,3) = "abcd"
print a$

a$(5) = "123456789"
mid$(a$(5),2,3) = "abcd"
print a$(5)

a$(n) = "123456789"
mid$(a$(n),2,3) = "abcd"
print a$(n)

a$ = "123456789"
mid$(a$,i,3) = "abcd"
print a$

a$(5) = "123456789"
mid$(a$(5),i,3) = "abcd"
print a$(5)

a$(n) = "123456789"
mid$(a$(n),i,3) = "abcd"
print a$(n)

a$ = "123456789"
mid$(a$,i,j) = "abcd"
print a$

a$(5) = "123456789"
mid$(a$(5),i,j) = "abcd"
print a$(5)

a$(n) = "123456789"
mid$(a$(n),i,j) = "abcd"
print a$(n)
