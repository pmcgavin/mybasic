#!/bin/mybasic
  defdbl a-z

  x = 1

  c4 = 1: c3 = -388: c2 = 51736: c1 = -2610400: c0 = 32890000

iterate:
  x2 = x * x
  y = (((c4 * x2 + c3) * x2 + c2) * x2 + c1) * x2 + c0
  yp = (((8 * c4 * x2 + 6 * c3) * x2 + 4 * c2) * x2 + 2 * c1) * x
  x = x - y / yp
  i = i + 1
  print i, x, y
  if (abs(y) > 1.0e-6) then goto iterate

  end
