print using "&"; "abcd", "abcd"
print using "!"; "abcd"
print using "\\"; "abcd"
print using "\ \"; "abcd"
print using "\  \"; "abcd"
print using "\   \"; "abcd"
for x = 0 to 1300 step 111
  print using "###"; x
next x
print using "!";"FRED"
print using "\ \";"JOHN"
print using "&";"Today is"
print using "###.#";25.4
print using "#.#";2.54
print using "##.##";12.34,5.678,1.2
print using "+#.# ";-1.1,2.2
print using "#.#- ";-3.3,4.4
print using "**#.# ";56.78,-91.2,-3
print using "$$##.#";123.45
print using "**$$#.##";2.54
print using "####,.##";4567.89
print using "+####^^^^";-0.4444
print using "#.####^^^^";-0.4444
print using "_�##.##";4.99
print using "#.#";33.25
print using ".##";.999
print using ".##";.99
print using "#.####^^^^";0.4444
end
