#!/bin/mybasic
open "test9.bas" for input as #1
size = lof(#1)
print "test9.bas is"; size; "bytes long"
open "ram:copy.bas" for output as #2
while not eof(#1)
  line input #1, l$
  print #2, l$
wend
close #1
close #2
print "Copied test9.bas to ram:copy.bas"
end
