#!/bin/mybasic
REM Program to un-number a Basic program

DEFLNG a-z
DIM l(1000)
l(0)=100000000&
ml=0

REM Pass 1.  Locate all GOTOs, GOSUBs and THENs and build table of line numbers
LINE INPUT "Input AmigaBASIC program:  "; n1$
OPEN n1$ for input as #1
LINE INPUT "Output AmigaBASIC program: "; n2$
WHILE NOT EOF(#1)
  LINE INPUT #1,l$
  IF RIGHT$(l$,1) = CHR$(13) THEN l$ = LEFT$(l$,LEN(l$)-1)
  PRINT l$
  w$ = "GOTO": GOSUB search
  w$ = "GOSUB": GOSUB search
  w$ = "THEN": GOSUB search
  w$ = "ELSE": GOSUB search
WEND
CLOSE #1

PRINT ml; "labels recorded, as follows:"
i=0
WHILE l(i)<>100000000&
  PRINT l(i);
  i=i+1
  IF i MOD 10 = 0 THEN PRINT
WEND
PRINT
PRINT

REM Pass 2
OPEN n1$ for input as #1
OPEN n2$ for output as #2
WHILE NOT EOF(#1)
  LINE INPUT #1,l$
  IF RIGHT$(l$,1) = CHR$(13) THEN l$ = LEFT$(l$,LEN(l$)-1)
  w$ = "GOTO": GOSUB search2
  w$ = "GOSUB": GOSUB search2
  w$ = "THEN": GOSUB search2
  w$ = "ELSE": GOSUB search2
  IF LEFT$(l$,1) >= "0" AND LEFT$(l$,1) <= "9" THEN
    p = 2
    WHILE MID$(l$,p,1) >= "0" AND MID$(l$,p,1) <= "9"
      p = p + 1
    WEND
    n = VAL(LEFT$(l$,p-1))
    li = 0
    lj = ml - 1
    lk = (li + lj) \ 2
    IF n > l(lk) THEN li = lk + 1 ELSE lj = lk - 1
    WHILE n <> l(lk) AND (li <= lj)
      lk = (li + lj) \ 2
      IF n > l(lk) THEN li = lk + 1 ELSE lj = lk - 1
    WEND
    IF n = l(lk) THEN
      PRINT
      PRINT #2,""
      PRINT "L."; MID$(STR$(n),2); ":"
      PRINT #2,"L."; MID$(STR$(n),2); ":"
    END IF
    l$=" "+MID$(l$,p)
  END IF
  PRINT l$
  PRINT #2,l$
WEND
CLOSE #2
CLOSE #1
END

search:
p = INSTR(UCASE$(l$),UCASE$(w$))
WHILE p <> 0
  p = p + LEN(w$)
  c$ = ","
  WHILE c$ = ","
    WHILE MID$(l$,p,1) = " "
      p = p + 1
    WEND
    ps = p
    WHILE MID$(l$,p,1) >= "0" AND MID$(l$,p,1) <= "9"
      p = p + 1
    WEND
    IF p > ps THEN PRINT "-----"; MID$(l$,ps,p-ps); "-----"
    n = VAL(MID$(l$,ps,p-ps))
    IF n <> 0 THEN
      i = 0
      WHILE n > l(i)
        i = i + 1
      WEND
      IF n <> l(i) THEN
        FOR j = ml TO i STEP -1
          l(j+1) = l(j)
        NEXT
        l(i) = n
        ml = ml + 1
      END IF
    END IF
    WHILE MID$(l$,p,1) = " "
      p = p + 1
    WEND
    c$ = MID$(l$,p,1)
    p = p + 1
  WEND
  p = INSTR(p,UCASE$(l$),UCASE$(w$))
WEND
RETURN

search2:
p = INSTR(UCASE$(l$),UCASE$(w$))
WHILE p <> 0
  p = p + LEN(w$)
  c$ = ","
  WHILE c$ = ","
    WHILE MID$(l$,p,1)=" "
      p = p + 1
    WEND
    ps = p
    WHILE MID$(l$,p,1) >= "0" AND MID$(l$,p,1) <= "9"
      p = p + 1
    WEND
    IF p > ps THEN l$ = LEFT$(l$,ps-1) + "L." + MID$(l$,ps)
    p = p + 2
    WHILE MID$(l$,p,1) = " "
      p = p + 1
    WEND
    c$ = MID$(l$,p,1)
    p = p + 1
  WEND
  p = INSTR(p,UCASE$(l$),UCASE$(w$))
WEND
RETURN
