#!/bin/mybasic
  DEFINT A-Z
  DIM R(4), S(7,7)
  DEF FNN$(N)=CHR$(N+48)
  RANDOMIZE TIMER
  PRINT TAB(32);"CHECKERS"
  PRINT TAB(15);"Creative Computing  Morristown, New Jersey"
  PRINT:PRINT:PRINT
  PRINT TAB(22)"This is the game of ";: COLOR 6: PRINT"checkers": COLOR 1: PRINT
  PRINT"The computer is X, and you are O.": PRINT
  PRINT"The computer will move first.": PRINT
  PRINT"Squares are referred to by a coordinate system."
  PRINT"(0,0) is the lower left corner"
  PRINT"(0,7) is the upper left corner"
  PRINT"(7,0) is the lower right corner"
  PRINT"(7,7) is the upper right corner": PRINT
  PRINT"The computer will type '+to' when you have another jump."
  COLOR 4: PRINT"Enter two negative numbers if you cannot jump.": COLOR 1: PRINT
  'PRINT"Please wait..."
  G=-1
  R(0)=-99
  FOR X=0 TO 7
   FOR Y=0 TO 7
    READ J
    IF J<>15 THEN
     S(X,Y)=J
    ELSE
     RESTORE
     READ S(X,Y)
    END IF
   NEXT Y
  NEXT X
  DATA 1,0,1,0,0,0,-1,0,0,1,0,0,0,-1,0,-1,15

L.230:
  FOR X=0 TO 7
   FOR Y=0 TO 7
    IF S(X,Y)<=-1 THEN
     IF S(X,Y)=-1 THEN
      FOR A=-1 TO 1 STEP 2
       B=G: GOSUB S.650
      NEXT A
     END IF
     IF S(X,Y)=-2 THEN
      FOR A=-1 TO 1 STEP 2
       FOR B=-1 TO 1 STEP 2
        GOSUB S.650
       NEXT B
      NEXT A
     END IF
    END IF
   NEXT Y
  NEXT X
  IF R(0)=-99 THEN L.1880
  PRINT "I move from "FNN$(R(1))","FNN$(R(2))" to "FNN$(R(3))","FNN$(R(4));
  R(0)=-99

L.1240:
  IF R(4)=0 THEN
   S(R(3),R(4))=-2
  ELSE
   S(R(3),R(4))=S(R(1),R(2))
  END IF
  S(R(1),R(2))=0
  IF ABS(R(1)-R(3))=2 THEN
   S((R(1)+R(3))\2,(R(2)+R(4))\2)=0
   X=R(3): Y=R(4)
   IF S(X,Y)=-1 THEN
    B=-2
    FOR A=-2 TO 2 STEP 4
     GOSUB S.1370
    NEXT A
   END IF
   IF S(X,Y)=-2 THEN
    FOR A=-2 TO 2 STEP 4
     FOR B=-2 TO 2 STEP 4
      GOSUB S.1370
     NEXT B
    NEXT A
   END IF
   IF R(0)<>-99 THEN
    PRINT" to "FNN$(R(3))","FNN$(R(4));
    R(0)=-99
    GOTO L.1240
   END IF
  END IF
  'CLS
  PRINT: PRINT: COLOR 3: PRINT "       0    1    2    3    4    5    6    7"
  PRINT"      --------------------------------------"
  FOR Y=7 TO 0 STEP -1
   COLOR 3: PRINT Y;"  |";
   FOR X=0 TO 7
    PRINT TAB(5*X+7);
    ON S(X,Y)+3 GOSUB S.1440,S.1450,S.1460,S.1470,S.1480
   NEXT X
   COLOR 3: PRINT TAB(44)"|  "Y: IF Y<>0 THEN PRINT TAB(6)"|"TAB(44)"|"
  NEXT Y
  PRINT"      --------------------------------------"
  COLOR 3: PRINT "       0    1    2    3    4    5    6    7": COLOR 1: PRINT

L.1590:
  'LOCATE 22,1
  INPUT "From";E,H
  IF E<0 OR E>7 OR H<0 OR H>7 THEN L.1590
  X=E: Y=H: IF S(X,Y)<=0 THEN BEEP: GOTO L.1590

L.1670:
  'LOCATE 22,12
  INPUT "to";A,B
  X=A: Y=B
  IF S(X,Y)<>0 OR ABS(A-E)>2 OR ABS(A-E)<>ABS(B-H) THEN BEEP: GOTO L.1670
  I=22

L.1750:
  S(A,B)=S(E,H): S(E,H)=0
  IF ABS(E-A)<>2 THEN L.1810
  S((E+A)\2,(H+B)\2)=0

L.1802:
  'LOCATE 22,I
  INPUT"+to";A1,B1
  IF A1<0 OR A1>7 OR B1<0 OR B1>7 THEN L.1810
  IF S(A1,B1)<>0 OR ABS(A1-A)<>2 OR ABS(B1-B)<>2 THEN L.1802
  E=A: H=B: A=A1: B=B1: I=I+11
  GOTO L.1750

L.1810:
  IF B=7 THEN S(A,B)=2
  GOTO L.230

S.650:
  U=X+A
  V=Y+B
  IF U<0 OR U>7 OR V<0 OR V>7 THEN RETURN
  IF S(U,V)=0 THEN GOSUB S.910: RETURN
  IF S(U,V)<0 THEN RETURN
  U=U+A
  V=V+B
  IF U<0 OR U>7 OR V<0 OR V>7 THEN RETURN
  IF S(U,V)=0 THEN GOSUB S.910
  RETURN

S.910:
  Q=RND
  IF V=0 AND S(X,Y)=-1 THEN Q=Q+2
  IF ABS(Y-V)=2 THEN Q=Q+5
  IF Y=7 THEN Q=Q-2
  IF Y=0 OR U=7 THEN Q=Q+1
  FOR C=-1 TO 1 STEP 2
   IF U+C<0 OR U+C>7 OR V+G<0 THEN L.1080
   IF S(U+C,V+G)<0 THEN Q=Q+1: GOTO L.1080
   IF U-C<0 OR U-C>7 OR V-G>7 THEN L.1080
   IF S(U+C,V+G)>0 AND (S(U-C,V-G)=0 OR (U-C=X AND V-G=Y)) THEN Q=Q-2

L.1080:
  NEXT C
  IF Q>R(0) THEN
   R(0)=Q: R(1)=X: R(2)=Y: R(3)=U: R(4)=V
  END IF
  RETURN

S.1370:
  U=X+A: V=Y+B
  IF U<0 OR U>7 OR V<0 OR V>7 THEN RETURN
  IF S(U,V)=0 AND S(X+A\2,Y+B\2)>0 THEN GOSUB S.910
  RETURN

S.1440:
  COLOR 4: PRINT"X*";: RETURN

S.1450:
  COLOR 4: PRINT"X";: RETURN

S.1460:
  COLOR (X+Y) MOD 2+1: PRINT".";: RETURN

S.1470:
  COLOR 1: PRINT"O";: RETURN

S.1480:
  COLOR 1: PRINT"O*";: RETURN

L.1880:
  PRINT"You win"
  LINE INPUT "Hit <RETURN> to end: ",X$
  END
