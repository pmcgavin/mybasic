#!/bin/mybasic
defint a-z
dim w(100,100), d(3)
randomize timer

input "What are your width and length"; h, v
if h < 1 or h > 100 or v < 1 or v > 100 then
  print "Invalid range"
  stop
end if

for r = 1 to v
  for s = 1 to h
    p = r: q = s
    n = 0
    if w(p,q) mod 2 = 0 then
      n = 0
      if q < h then if w(p,q+1) <> 0 then d(n) = 1: n = n + 1
      if p < v then if w(p+1,q) <> 0 then d(n) = 2: n = n + 1
      if q > 1 then if w(p,q-1) <> 0 then d(n) = 3: n = n + 1
      if p > 1 then if w(p-1,q) <> 0 then d(n) = 4: n = n + 1
      if n > 0 then
        x = d(int(rnd(1) * n))
        if x = 1 then w(p,q) = w(p,q) + 2
        if x = 2 then w(p,q) = w(p,q) + 4
        if x = 3 then w(p,q-1) = w(p,q-1) + 2
        if x = 4 then w(p-1,q) = w(p-1,q) + 4
      end if
    end if
    while (w(p,q) mod 2 = 0)
      n = 0
      if q < h then if w(p,q+1) = 0 then d(n) = 1: n = n + 1
      if p < v then if w(p+1,q) = 0 then d(n) = 2: n = n + 1
      if q > 1 then if w(p,q-1) = 0 then d(n) = 3: n = n + 1
      if p > 1 then if w(p-1,q) = 0 then d(n) = 4: n = n + 1
      w(p,q) = w(p,q) + 1
      if n > 0 then
        x = d(int(rnd(1) * n))
        if x = 1 then w(p,q) = w(p,q) + 2: q = q + 1
        if x = 2 then w(p,q) = w(p,q) + 4: p = p + 1
        if x = 3 then q = q - 1: w(p,q) = w(p,q) + 2
        if x = 4 then p = p - 1: w(p,q) = w(p,q) + 4
      end if
    wend
  next s
next r

x = int(rnd(1) * h) + 1
print ".";
for s = 1 to h
  if s = x then print " ."; else print "_.";
next s
print
x = int(rnd(1) * h) + 1
w(v,x) = w(v,x) + 4
for r = 1 to v
  print "|";
  for s = 1 to h
    x = w(r,s)
    if x = 1 then print "_|";
    if x = 3 then print "_.";
    if x = 5 then print " |";
    if x = 7 then print " .";
  next s
  print
next r

end
