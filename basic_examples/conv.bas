10 defint i-n
20 deflng j
30 a = 58.8
40 b = 63.2
50 c = -58.8
60 d = -63.2
70 print a;b;c;d
80 ia = a
90 ib = b
100 ic = c
110 id = d
120 print "int:  ";ia;ib;ic;id
130 ja = a
140 jb = b
150 jc = c
160 jd = d
170 print "lng:  ";ja;jb;jc;jd
180 ia = int(a)
190 ib = int(b)
200 ic = int(c)
210 id = int(d)
220 print "INT:  ";ia;ib;ic;id
230 ia = fix(a)
240 ib = fix(b)
250 ic = fix(c)
260 id = fix(d)
270 print "FIX:  ";ia;ib;ic;id
280 ia = cint(a)
290 ib = cint(b)
300 ic = cint(c)
310 id = cint(d)
320 print "CINT: ";ia;ib;ic;id
330 ja = clng(a)
340 jb = clng(b)
350 jc = clng(c)
360 jd = clng(d)
370 print "CLNG: ";ja;jb;jc;jd
