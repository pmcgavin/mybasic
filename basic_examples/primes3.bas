10 CLS
20 DEFDBL A: INPUT "Enter Prime count cutoff point" ;X2:REM Double precision of 16 digits for (A)
30 Dim AA(7400),A(10)
40 FOR I= 1 to 8
50          READ A(I)
60 NEXT I
70 A1=7:B=1:X=4:X1=1:Q=1:X5=1: REM  The 7 is forced here to start the sieve. (A1) :Prime count X also is forced where X=4.
80 PRINT A1;:AA(X1)=A1
90 A2=A(B):A3=A1+A2:Q=1:X3=X5
100 FOR I = 1 to X3
110     A7=AA(I)*AA(I):IF A7= A3 THEN X9= I+1:X5=X9:I=X3:REM Cuts the loop short here if A7=A3. 
120         A4=A3/AA(I):A5=INT(A4):A6=A4-A5
130                IF A6 = 0 THEN P=1: I=X3: REM Cuts the loop short here also if  A6=0 
140 NEXT I
150 IF P=0 THEN PRINT A3;:X=X+1:X1=X1+1:IF X1<7401 THEN AA(X1)=A3:REM Allows for primes > Array
160 B=B+1:IF B=9 THEN B=1:A1=A3:P=0:GOTO 180:REM  Falls through here if above test fails.(Composite)
170 A1=A3:P=0
180 IF X=X2 THEN 190  ELSE GOTO 90
190 PRINT "       ": PRINT "pi(";A3;")= ";X;"     Largest prime factor used  =  ";AA(X9); " Index number used out of a possible (7,400) = ";X9:END
200 DATA 4,2,4,2,4,6,2,6:REM  The Prime # footprint.
