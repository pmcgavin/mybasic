1 rem BYTE Magazine January 1983 Page 286
5 defint a-z
10 dim flags(8191)
20 print "10 iterations"
25 s# = timer
30 for m = 1 to 10
40   count = 0
50   for i = 0 to 8190
60     flags(i) = 1
70   next i
80   for i = 0 to 8190
90     if flags(i) = 0 goto 170
100       prime = i + i + 3
105 rem   print prime
110       k = i + prime
120       while k <= 8190
130         flags(k) = 0
140         k = k + prime
150       wend
160       count = count + 1
170   next i
180 next m
185 e# = timer
190 print count,"primes in"; csng(e# - s#); "seconds"
200 end
