rem Game of Katego for 2 human players
rem
rem https://boardgamegeek.com/boardgame/11508/katego

defint a-z
dim board(1,10), dice(1), total(1)
randomize timer
for turn = 0 to 10
  for player = 0 to 1
    t1 = 0: gosub printboard
    dice(0) = int(rnd(1) * 6) + 1
    dice(1) = int(rnd(1) * 6) + 1
    print  "Player"; player+1; "throws:"; dice(0); dice(1)
    print

    retry:
    input "Which column (2..12)"; c
    c = c - 2
    if c < 0 or c > 10 then print "Invalid column !!": goto retry
    if board(player,c) <> 0 then print "Column is already used !!": goto retry
    board(player,c) = dice(0) + dice(1)
  next player
next turn
t1 = 1: gosub printboard
if total(0) > total(1) then print "Player 1 wins !!"
if total(0) = total(1) then print "It's a draw !!"
if total(0) < total(1) then print "Player 2 wins !!"
end

printboard:
rem if t1=0 then print only raw board
rem if t1=1 then print both raw board and score board
print
for t = 0 to t1
  if t = 0 then print "Board";: else print "Score";
  for c = 0 to 10
    print using " ##"; c + 2;
  next c
  if t = 1 then print " total" else print
  for p = 0 to 1
    total(p) = 0
    print p + 1; ": ";
    for c = 0 to 10
      b0 = board(p,c)
      b1 = board(1-p,c)
      wincolumn = (t = 1 and b0 <> 0 and b1 <> 0 and b0 > b1)
      if wincolumn then total(p) = total(p) + b0
      if (t = 0 and b0 <> 0) or wincolumn then print using " ##"; b0;: else print space$(3);
    next c
    if t = 1 then print space$(3);: print using "###"; total(p) else print
  next p
  print
next t
return
