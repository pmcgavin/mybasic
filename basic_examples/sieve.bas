Rem This is an old Sieve benchmark useful for determining
Rem the computer and the metacompiler speed.

rem defint a-z
rem hidecursor

cls
print "Testing......"
print
10 rem sieve 6/1/92 test
20 loops = 50
30 t = timer
40 dim f(8194)
50 for j = 1 to loops
60 c = 0
70 s = 8191
80 for i = 0 to s : f(i) = 1 : next i
90 for i = 0 to s
100   if f(i) = 0 then goto 140
110   p = i+i+3
115   if i+p > s then 130
rem 120   for k = i+p to s step p : f(k) = 0 : next k
120   k = i+p: while k <= s:  f(k) = 0: k = k+p: wend
130   c = c+1
140 next i
150 next j
160 print c;" primes found ";
170 t = timer-t
180 print loops;" times in ";
190 print t;" seconds"

' -- print out the statistics --

print:print "**************************************************************"
print:print "Your result:                    ";t;" seconds"
  print "PowerMac G4/400 (METAL v1.7a1): 1.064516 seconds"

relative = 1.064516*10/t
rel$ = string$(relative, "*")

print:print "Your computer:                  ";rel$;" (";int(relative*10);"%)"
  print "PowerMac G4/400 (METAL v1.7.2): ";string$(12,"*");" (124%)"
  print "PowerMac G4/400 (METAL v1.7a1): ";string$(10,"*");" (100%)"

rem showcursor
