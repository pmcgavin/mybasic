DEFDBL a-z

pi = 3.1415926535897932384626

f = 38400

OPEN "ram:x" FOR OUTPUT AS #1

FOR c = 0.000000010 TO 0.000001000 STEP 0.000000010
  l = 1 / (4 * pi * pi * f * f * c)
  xl = 2 * pi * f * l
  xc = 1 / (2 * pi * f * c)
  PRINT #1,USING "f = #####.### Hz    C = #.### uF    L = ####.### uH     XC = ###.## Ohms",f,c*1000000,l*1000000,xc
  rem PRINT #1,f,c*1000000,l*1000000,xc
NEXT c

CLOSE #1

END
