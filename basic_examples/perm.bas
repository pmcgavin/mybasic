#!/bin/mybasic
defint a-z

dim a(6)

for i=1 to 6
  a(i)=i
next i

for h=1 to 6
  swap a(1),a(h)
  for i=2 to 6
    swap a(2),a(i)
    for j=3 to 6
      swap a(3),a(j)
      for k=4 to 6
        swap a(4),a(k)
        for l=5 to 6
          swap a(5),a(l)
          print a(1); a(2); a(3); a(4); a(5); a(6)
          swap a(5),a(l)
        next l
        swap a(4),a(k)
      next k
      swap a(3),a(j)
    next j
    swap a(2),a(i)
  next i
  swap a(1),a(h)
next h

end
