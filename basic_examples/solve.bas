DEFINT a-z

DIM a(11), b(11)

PRINT "Starting at... "; TIME$
t0! = timer

FOR i = 0 TO 11
  a(i) = i + 1
NEXT i

b(0) = 10
b(1) = 12
b(2) = 11
b(3) = 7
b(4) = 2
b(5) = 5
b(6) = 1
b(7) = 4
b(8) = 3
b(9) = 9
b(10) = 6
b(11) = 8

OPEN "ram:solutions.dat" FOR OUTPUT AS #1

FOR i1 = 0 TO 11
  SWAP a(0), a(i1)
  FOR i2 = 1 TO 11
    SWAP a(1), a(i2)
    FOR i3 = 2 TO 11
      SWAP a(2), a(i3)
      FOR i4 = 3 TO 11
        SWAP a(3), a(i4)
        FOR i5 = 4 TO 11
          SWAP a(4), a(i5)
          FOR i6 = 5 TO 11
            SWAP a(5), a(i6)
            FOR i7 = 6 TO 11
              SWAP a(6), a(i7)
              FOR i8 = 7 TO 11
                SWAP a(7), a(i8)
                IF a(0)+a(6)+a(7)+a(2)=26 THEN
                  FOR i9 = 8 TO 11
                    SWAP a(8), a(i9)
                    IF a(1)+a(7)+a(8)+a(3)=26 THEN
                      FOR i10 = 9 TO 11
                        SWAP a(9), a(i10)
                        IF a(2)+a(8)+a(9)+a(4)=26 THEN
                          FOR i11 = 10 TO 11
                            SWAP a(10), a(i11)
                            IF a(3)+a(9)+a(10)+a(5)=26 AND a(4)+a(10)+a(11)+a(0)=26 AND a(5)+a(11)+a(6)+a(1)=26 THEN
                              d = 0
                              n = 0
                              FOR j = 0 TO 11
                                PRINT #1,a(j);
                                d = d + (a(j) - b(j)) * (a(j) - b(j))
                                IF a(j) = b(j) THEN
                                  n = n + 1
                                END IF
                              NEXT j
                              PRINT #1, "   "; d, n
                            END IF
                            SWAP a(10), a(i11)
                          NEXT i11
                        END IF
                        SWAP a(9), a(i10)
                      NEXT i10
                    END IF
                    SWAP a(8), a(i9)
                  NEXT i9
                END IF
                SWAP a(7), a(i8)
              NEXT i8
              SWAP a(6), a(i7)
            NEXT i7
            SWAP a(5), a(i6)
          NEXT i6
          SWAP a(4), a(i5)
        NEXT i5
        SWAP a(3), a(i4)
      NEXT i4
      SWAP a(2), a(i3)
    NEXT i3
    SWAP a(1), a(i2)
  NEXT i2
  SWAP a(0), a(i1)
NEXT i1

CLOSE #1

t1! = timer
PRINT "Finished at... "; TIME$, "("; t1! - t0!; "seconds)."

END
