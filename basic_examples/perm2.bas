#!/bin/mybasic
defint a-z

dim a(6)

for i=1 to 6
  a(i)=i
next i

p = 0
for h=1 to 6
  swap a(1),a(h)
  if h<>1 then p=1-p
  for i=2 to 6
    swap a(2),a(i)
    if i<>2 then p=1-p
    for j=3 to 6
      swap a(3),a(j)
      if j<>3 then p=1-p
      for k=4 to 6
        swap a(4),a(k)
        if k<>4 then p=1-p
        for l=5 to 6
          swap a(5),a(l)
          if l<>5 then p=1-p
          print p; a(1); a(2); a(3); a(4); a(5); a(6)
          swap a(5),a(l)
          if l<>5 then p=1-p
        next l
        swap a(4),a(k)
        if k<>4 then p=1-p
      next k
      swap a(3),a(j)
      if j<>3 then p=1-p
    next j
    swap a(2),a(i)
    if i<>2 then p=1-p
  next i
  swap a(1),a(h)
  if h<>1 then p=1-p
next h

end
