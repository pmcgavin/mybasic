  10 REM GRAFXY.BAS by John G. Kemeny and Thomas E. Kurtz
  20 REM Modified by Peter McGavin
  30 LINE INPUT "Function of X to plot? Example SIN(X): ",F$
  40 INPUT "X range? Example 0,10:   ",X0,X1
  50 INPUT "Y range? Example -1,1:   ",Y0,Y1
  60 Q$=CHR$(34):        REM double-quote
  70 OPEN "graftmp.bas" FOR OUTPUT AS #1
  80 PRINT #1,"  10 X0=";X0
  90 PRINT #1,"  20 X1=";X1
 100 PRINT #1,"  30 Y0=";Y0
 110 PRINT #1,"  40 Y1=";Y1
 120 PRINT #1,"  50 DX=(X1-X0)/32.0:    REM X subdivision size"
 130 PRINT #1,"  60 N=50:               REM number of Y subdivisions"
 140 PRINT #1,"  70 DY=(Y1-Y0)/N:       REM Y subdivision size"
 150 PRINT #1,"  80 PRINT "+Q$+"Y-AXIS FROM "+Q$+";Y0;"+Q$+" TO "+Q$+";Y1;"+Q$+" IN INCREMENTS OF "+Q$+";DY"
 160 PRINT #1,"  90 PRINT"
 170 PRINT #1," 100 PRINT TAB(8);"+Q$+"'"+Q$+";STRING$(N-1,"+Q$+"-"+Q$+");"+Q$+"'"+Q$
 180 PRINT #1," 110 FOR X=X0 TO X1 STEP DX"
 190 PRINT #1," 120   PRINT INT(100*X+0.5)/100; TAB(8+INT(((";F$;")-Y0)/DY+0.5));"+Q$+"*"+Q$
 200 PRINT #1," 130 NEXT X"
 210 PRINT #1," 140 KILL "+Q$+"graftmp.bas"+Q$
 220 PRINT #1," 150 CHAIN "+Q$+"graf4.bas"+Q$
 230 CLOSE #1
 240 CHAIN "graftmp.bas"
