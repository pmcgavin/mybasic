deflng i-n
defdbl a-h,o-z

dim a(100000)

print time$
t0!=timer
n=100000
for i=0 to n
  a=i
  a(i)=a/n
next i
sum=0
sum2=0
amax=0
for i=0 to n
  sum=sum+a(i)
  sum2=sum2+a(i)*a(i)
  if a(i)>amax then
    amax=a(i)
  end if
next i
t1!=timer
print time$,"(";t1!-t0!;"seconds)."
print "N =", n
print "Sum =", sum
print "Sum2 =", sum2
print "Mean =", sum/n
print "Std Dev =", sqr((sum2-sum*sum/n)/(n-1))
print "Max =", amax
end
