#!/bin/mybasic
  DEFINT A-Z: COLOR 7: PRINT TAB(36);"ELIZA"
  COLOR 5: PRINT TAB(30);"Creative Computing"
  PRINT TAB(28); "Morristown, New Jersey"
  PRINT: PRINT
  REM      ------initialisation------
  RANDOMIZE TIMER
  rem READ NC,NB,NK,NR
  nc=8:nb=47:nk=73:nr=205
  REM DIM S(NB),R(NB),N(NB),NX(NB),CS$(NC),CR$(NC),CS1$(NC),CR1$(NC),KW$(NK),RY$(NR)
  DIM S(47),R(47),N(47),NX(47),CS$(8),CR$(8),CS1$(8),CR1$(8),KW$(73),RY$(205)
  FOR X=1 TO NC: READ CS$(X),CR$(X): NEXT: ' Read in conjugation data
  FOR X=1 TO NC: READ CS1$(X),CR1$(X): NEXT: ' Read in conjugation data
  NS=0: NT=0
  FOR K=1 TO NB
   READ S: FOR Y=1 TO S: READ KW$(NS+Y): NEXT: READ T
   NX(K)=S: S(K)=NT+1: R(K)=NT+INT(T*RND)+1: N(K)=NT+T
   FOR Y=1 TO T: READ RY$(NT+Y): NEXT
   NS=NS+S: NT=NT+T
  NEXT
  IF NT<>NR OR NS<>NK THEN PRINT "Internal consistancy error": STOP
  COLOR 7: PRINT "Hi!  I'm Eliza.  What's your problem?"
  REM
  REM      ------user input section------
  REM      ------------------------------

L.200:
  COLOR 7: PRINT: rem PRINT ": "; CHR$(10); CHR$(30);: COLOR 6: rem COL=POS: rem ROW=CSRLIN

L.201:
  REM LOCATE COL,ROW
  LINE INPUT ": ",i$

L.202:
  IF LEFT$(I$,1)=" " THEN I$=MID$(I$,2): GOTO L.202
  IF RIGHT$(I$,1)=" " THEN I$=LEFT$(I$,LEN(I$)-1): GOTO L.202
  IF LEN(I$)=0 THEN L.201
  I$=" "+I$+"  "
  REM   get rid of apostrophes

L.220:
  L=INSTR(I$,"'"): IF L<>0 THEN I$=LEFT$(I$,L-1)+MID$(I$,L+1): GOTO L.220
  REM   make of copy of I$, then translate I$ to upper case
  I1$=I$
  FOR L=1 TO LEN(I$)
   Z$=MID$(I$,L,1): IF Z$>="a" AND Z$<="z" THEN I$=LEFT$(I$,L-1)+CHR$(ASC(Z$)-32)+MID$(I$,L+1): 'MID$(I$,L,1)=CHR$(ASC(Z$)-32)
  NEXT
  REM   check for exit program
  IF INSTR(I$,"SHUT")<>0 THEN PRINT "Shut up...": COLOR 1: STOP
  IF INSTR(I$,"BYE")<>0 THEN PRINT "Bye bye...": COLOR 1: STOP
  REM   check for repeating input
  IF I$=P$ THEN K=NB: GOTO L.600
  REM
  REM      ------find keyword in I$------
  REM      ------------------------------
  NS=0
  FOR K=1 TO NB-2
   FOR Y=1 TO NX(K)
    L=INSTR(I$,KW$(NS+Y))
    IF L<>0 THEN S=L+LEN(KW$(NS+Y)): GOTO L.430
   NEXT
   NS=NS+NX(K)
  NEXT
  K=NB-1: GOTO L.570: ' We didn't find any keywords
  REM
  REM      take right part of strings and conjugate it
  REM      using the list of strings to be swapped
  REM
  REM skip over keywords

L.430:
  C$=" "+MID$(I$,S)+" "
  C1$=" "+MID$(I1$,S)+" "
  FOR X=1 TO NC
   L=1

L.470:
   LR=INSTR(L,C$,CR$(X)): LS=INSTR(L,C$,CS$(X))
   IF LR=0 THEN
    IF LS=0 THEN L.550 ELSE L.510
   ELSE
    IF LS<>0 THEN IF LR>LS THEN L.510
   END IF
   C$=LEFT$(C$,LR-1)+CS$(X)+MID$(C$,LR+LEN(CR$(X)))
   C1$=LEFT$(C1$,LR-1)+CS1$(X)+MID$(C1$,LR+LEN(CR$(X)))
   L=LR+LEN(CS$(X))-1: GOTO L.470

L.510:
   C$=LEFT$(C$,LS-1)+CR$(X)+MID$(C$,LS+LEN(CS$(X)))
   C1$=LEFT$(C1$,LS-1)+CR1$(X)+MID$(C1$,LS+LEN(CS$(X)))
   L=LS+LEN(CR$(X))-1: GOTO L.470

L.550:
  NEXT

L.555:
  IF LEFT$(C1$,1)=" " THEN C1$=MID$(C1$,2): GOTO L.555

L.556:
  L=INSTR(C1$,"!"): IF L<>0 THEN C1$=LEFT$(C1$,L-1)+MID$(C1$,L+1): GOTO L.556
  REM

L.570:
  REM      now using the keyblock number (k) get reply
  REM      -------------------------------------------

L.600:
  F$=RY$(R(K)): ' REM get right reply
  R(K)=R(K)+1: IF R(K)>N(K) THEN R(K)=S(K)
  COLOR 7: PRINT
  IF RIGHT$(F$,1)<>"*" THEN PRINT F$: GOTO L.640
  PRINT LEFT$(F$,LEN(F$)-1)+" ";
  IF LEN(F$)+LEN(C1$)<=79 THEN PRINT C1$: GOTO L.640

L.633:
  L=INSTR(C1$," ")
  IF L<>0 THEN PRINT LEFT$(C1$,L-1)+" ";: C1$=MID$(C1$,L+1): GOTO L.633
  PRINT C1$

L.640:
  P$=I$: GOTO L.200
  REM
  REM      ------program data follows------
  REM
  REM      # conjugation pairs, # blocks, # keywords, # replies
  REM
  REM DATA 8,47,73,205
  REM
  REM      string data for conjugations
  REM      ----------------------------
  DATA " ARE ", " AM ", "WERE ", "WAS ", " !YOU ", " I ", "YOUR ", "MY "
  DATA " IVE ", " YOUVE ", " IM ", " YOURE "
  DATA " ME ", " YOU ", " MYSELF ", " YOURSELF "
  DATA " are ", " am ", "were ", "was ", " !you ", " I ", "your ", "my "
  DATA " Ive ", " youve ", " Im ", " youre "
  DATA " me ", " you ", " myself ", " yourself "
  REM
  REM      keywords & replies
  REM      ------------------
  DATA 1,"I DRIVE",2
  DATA "Have you always driven*"
  DATA "Why don't you use public transport?"
  DATA 1,"I WALK",2
  DATA "It must take you a long time to walk*"
  DATA "You must live very close to work."
  DATA 1,"DREAM",4
  DATA "What does that dream suggest to you?"
  DATA "Do you dream often?"
  DATA "What persons appear in your dreams?"
  DATA "Are you disturbed by your dreams?"
  DATA 1,"FRIEND",6
  DATA "Why do you bring up the topic of friends?"
  DATA "Do your friends worry you?"
  DATA "Do your friends pick on you?"
  DATA "Are you sure you have any friends?"
  DATA "Do you impose on your friends?"
  DATA "Perhaps your love for friends worries you."
  DATA 2,"COMPUT","MACHINE",7
  DATA "Do computers worry you?"
  DATA "Are you talking about me in particular?"
  DATA "Are you frightened by machines?"
  DATA "Why do you mention computers?"
  DATA "What do you think machines have to do with your problem?"
  DATA "Don't you think computers can help people?"
  DATA "What is it about machines that worries you?"
  DATA 2,"THINK","THOUGHT",8
  DATA "Tell me more about such thoughts"
  DATA "I don't think anyone can think like that"
  DATA "Do you often have such thoughts"
  DATA "What are you thinking of?"
  DATA "What other thoughts do you have on this?"
  DATA "Do you dream these thoughts at night?"
  DATA "I've never thought about it that way"
  DATA "Do others in your family think this way?"
  DATA 1,"ALIKE",6
  DATA "In what way?"
  DATA "What resemblence do you see?"
  DATA "What does the similarity suggest to you?"
  DATA "What other connections do you see?"
  DATA "Could there really be some connection?"
  DATA "How?"
  DATA 1,"ALWAYS",3
  DATA "Can you think of a specific example?"
  DATA "When?"
  DATA "Really, always?"
  DATA 1,"NEVER",3
  DATA "Surely sometimes though..."
  DATA "Are you sure, never?"
  DATA "Well I know of at least one example."
  DATA 2,"MAYBE","PERHAPS",5
  DATA "You don't seem quite certain."
  DATA "Why the uncertain tone?"
  DATA "Can't you be more positive?"
  DATA "You aren't sure?"
  DATA "Don't you know?"
  DATA 1,"CAUSE",6
  DATA "Is that the real reason?"
  DATA "Does that reason explain anything else?"
  DATA "Is that the reason that's bugging you?"
  DATA "What other reasons might there be?"
  DATA "I don't think that's the real reason."
  DATA "Can you think of other reasons?"
  DATA 1,"SORRY",4
  DATA "Please don't apologise!"
  DATA "Apologies are not necessary."
  DATA "What feelings do you have when you apologise?"
  DATA "Don't be so defensive!"
  DATA 1,"NAME",2
  DATA "Names don't interest me."
  DATA "I don't care about names -- Please go on."
  DATA 1,"I DONT",4
  DATA "Don't you really*"
  DATA "Why don't you*"
  DATA "Do you wish to be able to*"
  DATA "How come you don't*"
  DATA 2,"YOU DONT","YOU DO NOT",4
  DATA "What makes you think I don't*"
  DATA "I do so*"
  DATA "Sometimes I*"
  DATA "Well if it makes you feel better then I will*"
  DATA 1,"I FEEL",3
  DATA "Tell me more about such feelings."
  DATA "Do you often feel*"
  DATA "Do you enjoy feeling*"
  DATA 1,"I GO ",3
  DATA "How far did you say you go?"
  DATA "I reckon you should find some other way to go*"
  DATA "You know, I also go*"
  DATA 1,"I TAKE",3
  DATA "From where do you take*"
  DATA "Have you ever thought of not taking*"
  DATA "You know, there are other ways of taking*"
  DATA 1,"WHY DONT YOU",3
  DATA "Do you really believe I don't*"
  DATA "Perhaps in good time I will*"
  DATA "Do you want me to*"
  DATA 1,"WHY CANT I",2
  DATA "Do you think you should be able to*"
  DATA "Why can't you*"
  DATA 2,"I CANT","I CANNOT",3
  DATA "How do you know you can't*"
  DATA "Have you tried?"
  DATA "Perhaps you can now*"
  DATA 2,"I AM"," IM ",8
  DATA "Do you come to me because you are*"
  DATA "How long have you been*"
  DATA "Do you believe it is normal to be*"
  DATA "Do you enjoy being*"
  DATA "Why do you think you are*"
  DATA "Where have you been since you became*"
  DATA "How do you think you became*"
  DATA "Perhaps you are only dreaming that you are*"
  DATA 2,"I WASNT","I WAS NOT",2
  DATA "Why weren't you*"
  DATA "What are the consequences of your not being*"
  DATA 1,"I WAS",3
  DATA "How long is it since you were last*"
  DATA "Why do you believe you were*"
  DATA "How do you think you came to be*"
  DATA 1,"I WANT",5
  DATA "What would it mean to you if I got*"
  DATA "Why do you want*"
  DATA "Suppose you soon got*"
  DATA "What if you never got*"
  DATA "I sometimes also want*"
  DATA 3,"YOURE NOT","YOU ARE NOT","YOU ARENT",3
  DATA "I am so*"
  DATA "I wish I was*"
  DATA "What on earth makes you think I'm not*"
  DATA 2,"YOU ARE","YOURE",8
  DATA "What makes you think I am*"
  DATA "Does it please you to believe I am*"
  DATA "Perhaps you would like to be*"
  DATA "Do you sometimes wish you were*"
  DATA "I am not*"
  DATA "How can you prove that I am*"
  DATA "Computers are not capable of being*"
  DATA "How long have you thought that I'm*"
  DATA 1,"ARE YOU",3
  DATA "Why are you interested in whether or not I am*"
  DATA "Would you prefer if I were not*"
  DATA "Perhaps in your fantasies I am*"
  DATA 2,"CAN I","MAY I",2
  DATA "Perhaps you don't want to*"
  DATA "Do you want to be able to*"
  DATA 1,"CAN YOU",3
  DATA "Don't you believe that I can*"
  DATA "Perhaps you would like to be able to*"
  DATA "You want me to be able to*"
  DATA 2,"I HAVENT","I HAVE NOT",2
  DATA "Why haven't you*"
  DATA "I'm not concerned about whether or not you have*"
  DATA 2,"YOU HAVENT","YOU HAVE NOT",2
  DATA "You're right.  I definitely haven't*"
  DATA "I didn't know that you were aware that I haven't*"
  DATA 2,"I HAVE"," IVE",4
  DATA "Why have you*"
  DATA "Do you know anyone else who has*"
  DATA "Believe it or not, I also have*"
  DATA "I really don't believe that you have*"
  DATA 2,"YOU HAVE","YOUVE",2
  DATA "How did you know that I've*"
  DATA "Why are you interested in whether or not I have*"
  DATA 3," ITS NOT "," IT IS NOT "," IT ISNT",3
  DATA "Have you any idea why it isn't*"
  DATA "I find it hard to believe that it isn't*"
  DATA "How come it's not*"
  DATA 2," IT IS "," ITS ",3
  DATA "Well I also reckon that it's*"
  DATA "I don't understand how it could be*"
  DATA "Who else have you talked to about it?"
  DATA 1,"HELP",3
  DATA "All I want is a conversation.  Please tell me about yourself"
  DATA "Just tell me your problems in a sentence or two.  That's all."
  DATA "I am your doctor/therapist/friend.  Just type in your thoughts."
  DATA 2,"HELLO","HI ",1
  DATA "How do you do... Please state your problem."
  DATA 3,"PLEASE","THANK YOU","THANKS",3
  DATA "Thank you for being so polite.  Please go on..."
  DATA "I like polite patients like you.  Please tell me more about your problems"
  DATA "Thank you for being so polite"
  DATA 1,"YOUR",2
  DATA "Why are you concerned about my*"
  DATA "What about your own*"
  DATA 1," MY ",3
  DATA "Please tell me more about your*"
  DATA "Would you like to know about my own*"
  DATA "I don't know anything about your*"
  DATA 6,"WHAT "," HOW","WHO ","WHERE ","WHEN ","WHY",10
  DATA "Why do you ask?"
  DATA "Does that question interest you?"
  DATA "What answer would please you the most?"
  DATA "What do you think?"
  DATA "Are such questions on your mind often?"
  DATA "What is it that you really want to know?"
  DATA "Have you asked anyone else?"
  DATA "Have you asked such questions before?"
  DATA "What else comes to mind when you ask that?"
  DATA "Don't any other questions come to mind?"
  DATA 1,"YOU ",6
  DATA "We were discussing you -- not me."
  DATA "Please limit this conversation to yourself."
  DATA "You're not really talking about me, are you?"
  DATA "Please talk about yourself, not about me."
  DATA "You're the patient here, not me."
  DATA "Why are you so interested in me?"
  DATA 1,"YES",7
  DATA "Are you sure?"
  DATA "I see.  What do your friends think about this?"
  DATA "I understand."
  DATA "You seem quite positive."
  DATA "Surely not, but please go on..."
  DATA "Well I agree this time"
  DATA "Do you really think so?"
  DATA 1," NO",5
  DATA "Are you saying no just to be negative?"
  DATA "You are being a bit negative."
  DATA "Why not?"
  DATA "Are you sure?"
  DATA "Why no?"
  DATA 1,"NOKEYFOUND",23
  DATA "Say, do you have any psychological problems?"
  DATA "What does that suggest to you?"
  DATA "I see."
  DATA "I'm not sure I understand you fully."
  DATA "Come come elucidate your thoughts."
  DATA "Can you elaborate on that?"
  DATA "That is quite interesting."
  DATA "Does that trouble you?"
  DATA "Please tell me more"
  DATA "I think I've got the picture now"
  DATA "Tell me about your dreams"
  DATA "Have you had any family problems lately?"
  DATA "Why are you here?"
  DATA "Tell me, why don't you talk to others about this?"
  DATA "How acceptable is all this to your friends and family?"
  DATA "Have you seen other computers about your problems?"
  DATA "What do you do for a living?"
  DATA "Are your hobbies taking all your time?"
  DATA "Perhaps you are overworked."
  DATA "How do you get to and from your job?"
  DATA "I'm getting the idea.  Can you give me further details on this?"
  DATA "How comfortable do you feel about all this?"
  DATA "How reasonable do you think this is, in the light of your interests?"
  DATA 1,"REPEATINGHIMSELF",6
  DATA "You're repeating yourself."
  DATA "Please don't repeat yourself."
  DATA "Where have I heard that before???!!"
  DATA "I heard you the first time."
  DATA "I don't like people who keep repeating the same thing.  Please don't"
  DATA "You've already said that"
  ' These are unused
  DATA "But you are not sure you*"
  DATA "Do you doubt you*"
  DATA "Oh, I*"
