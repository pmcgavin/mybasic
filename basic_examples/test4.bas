#!/bin/mybasic
'PROGRAM test4
'INTEGER i,n
'DOUBLE sum,sum2,amax,a(100000)
DEFDBL a-l,o-z: DEFLNG i-n
DIM a(100000)

n=100000
FOR i=0 TO n
  a(i)=CDBL(i)/n
NEXT i
sum=0
sum2=0
amax=0
FOR i=0 TO n
  sum=sum+a(i)
  sum2=sum2+a(i)*a(i)
  IF a(i)>amax THEN
    amax=a(i)
  END IF
NEXT i
PRINT "N =", n
PRINT "Sum =", sum
PRINT "Sum2 =", sum2
PRINT "Mean =", sum/n
PRINT "Std Dev =", SQR((sum2-sum*sum/n)/(n-1))
PRINT "Max =", amax
END
