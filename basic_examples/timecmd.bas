#!/usr/bin/env mybasic
IF _COMMANDCOUNT <= 0 THEN
  PRINT "Usage: "; COMMAND$(0); " shell-command ..."
  END
END IF
S# = TIMER
SHELL COMMAND$
E# = TIMER
PRINT "Shell command '"; COMMAND$ "' took"; CSNG(E# - S#); " seconds"
END
