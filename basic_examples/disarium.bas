' 1^1+7^2+5^3=1+49+125=175
' ergo 175 is a disarium number
PRINT "Disarium numbers from "
PRINT " Range   sta TO fin"
s=1:  f=4
sta=10^s:fin=10^f
DIM n(10)
FOR num = sta to fin
   n=INT(num) :n$=LTRIM$(STR$(n))
   ln=LEN(n$)
   n$=MID$(n$, 1,ln): y=0 
   FOR x=1 TO ln
      n(x) =val(mid$(n$,x,1))^x
      y=y+n(x)
   NEXT x
   IF n=y THEN
      PRINT n$
      Max$=n$
   END IF
NEXT num
PRINT "No more Disarium numbers beyond " ;Max$
