  WHILE 1
    INPUT "Enter temperature in Fahrenheit: ", f
    c = 5.0 * (f - 32) / 9.0
    k = c + 273.15
    PRINT USING "####.##\  \####.##\  \####.###!"; f; "F ="; c; "C ="; k; "K"
    LINE INPUT "Another go? (Y/n): ", a$
    IF LEFT$(a$,1) = "N" OR LEFT$(a$,1) = "n" THEN STOP
  WEND
