defdbl a-h,o-z
deflng i-n

input "Enter number to be approximated: ", x
i = clng(x)
j = 1
for n = 1 to 10
  print i; "/"; j; " = "; cdbl(i) / cdbl(j)
  k = clng(x / (i - j * x))
  i = i * k
  j = j * k + 1
  if i < 0 then
    i = -i
    j = -j
  end if
  i1 = i
  j1 = j
  k = i1 mod j1
  while k <> 0
    i1 = j1
    j1 = k
    k = i1 mod j1
  wend
  i = i \ j1
  j = j \ j1
next n
end
