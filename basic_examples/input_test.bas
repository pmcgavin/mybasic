defint a-z

dim a(10)

input "Enter three numbers: ", a, b, c
print a, b, c

i = 7

input "Enter three numbers: "; a(5), a(6), a(i)
print a(5), a(6), a(i)

print "Enter three numbers: ";
input a, b, c
print a, b, c

print "Enter three numbers: ";
open "/dev/tty" for input as #1
input #1, a, b, c
print a, b, c
close #1

line input "Name: ", n$
print n$, len(n$)

line input "Name: "; n$
print n$, len(n$)

print "Name: ";
line input n$
print n$, len(n$)

print "Name: ";
open "/dev/tty" for input as #1
line input #1, n$
print n$, len(n$)
close #1
