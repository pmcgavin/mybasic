#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "my_mem.h"

#include "mfile.h"

#define MINITIALSIZE 256

/**********************************************************************/
MFILE *mopen (void)
{
  MFILE *m;

  if ((m = (MFILE *)my_malloc (sizeof(MFILE))) != NULL) {
    if ((m->data = my_malloc (MINITIALSIZE)) == NULL) {
      m = my_free (m);
      return NULL;
    }
    m->pos = 0;
    m->size = 0;
    m->allocated_size = MINITIALSIZE;
  }
  return m;
}

/**********************************************************************/
size_t mread (void *ptr, size_t size, size_t n, MFILE *m)
{
  size_t i;

  for (i = 0; i < n && m->pos + size <= m->size; i++) {
    memcpy ((char *)ptr, &m->data[m->pos], size);
    m->pos += size;
    ptr = &((char *)ptr)[size];
  }
  return i;
}

/**********************************************************************/
size_t mwrite (void *ptr, size_t size, size_t n, MFILE *m)
{
  size_t i;
  void *p;

  for (i = 0; i < n; i++) {
    if (m->pos + size > m->allocated_size) {
      do {
        m->allocated_size <<= 1;
      } while (m->pos + size > m->allocated_size);
      if ((p = my_realloc (m->data, m->allocated_size)) == NULL)
        return i;
      m->data = p;
    }
    memcpy (&m->data[m->pos], (char *)ptr, size);
    ptr = &((char *)ptr)[size];
    m->pos += size;
    if (m->pos > m->size)
      m->size = m->pos;
  }
  return n;
}

/**********************************************************************/
int mseek (MFILE *m, long int off, int whence)
{
  switch (whence) {
  case SEEK_SET:
    m->pos = off;
    break;
  case SEEK_CUR:
    m->pos += off;
    break;
  case SEEK_END:
    m->pos = m->size - off;
    break;
  default:
    return -1;
  }
  if (m->pos < 0) {
    m->pos = 0;
    return -1;
  } else if (m->pos > m->size) {
    m->pos = m->size;
    return -1;
  }
  return 0;
}

/**********************************************************************/
long int mtell (MFILE *m)
{
  return m->pos;
}

/**********************************************************************/
void mrewind (MFILE *m)
{
  m->pos = 0;
}

/**********************************************************************/
void *mclose (MFILE *m)
{
  if (m != NULL) {
    if (m->data != NULL)
      m->data = my_free (m->data);
    m = my_free (m);
  }
  return NULL;
}

/**********************************************************************/
