#include <stdio.h>
#include <stdlib.h>

#ifndef DEBUG
#define my_malloc(size) malloc((size))
#define my_realloc(ptr,size) realloc((ptr),(size))
#define my_free(ptr) (free((ptr)), NULL)
#else
void *my_malloc (size_t size);
void *my_realloc (void *ptr, size_t size);
void *my_free (void *ptr);
#endif
