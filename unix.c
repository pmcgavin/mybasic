#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE
#define _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include <termios.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <unistd.h>

#ifdef HAVE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif

#include "mybasic.h"
#include "my_mem.h"

/****************************************************************************/

static char tmp_string[MAX_FNAME_LENGTH];

/****************************************************************************/

int change_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return chdir (tmp_string);
}

/****************************************************************************/

int make_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return mkdir (tmp_string, 0755);
}

/****************************************************************************/

int remove_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return rmdir (tmp_string);
}

/****************************************************************************/

static struct termios stored_settings;
static int tty_state = 0; /* 0 = normal, 1 = buffersize=1byte & echo-off */

static void reset_keypress (void)
{
  if (tty_state == 1) {
    tcsetattr (0, TCSANOW, &stored_settings);
    tty_state = 0;
  }
}

void init_tty (void)
{
  struct termios new_settings;
  static BOOL first = TRUE;

  if (isatty (fileno (stdin)) && tty_state == 0) {
    tcgetattr (0, &stored_settings);
    if (first) {
      atexit (reset_keypress);
      first = FALSE;
    }
    new_settings = stored_settings;
    /* Disable canonical mode, and set buffer size to 1 byte */
    new_settings.c_lflag &= (~ICANON);
    new_settings.c_lflag &= (~ECHO);
    new_settings.c_cc[VTIME] = 0;
    new_settings.c_cc[VMIN] = 1;
    tcsetattr (0, TCSANOW, &new_settings);
    tty_state = 1;
  }
}

/****************************************************************************/

int tty_getc (FILE *f)
{
  int c;

  fflush (stdout);
  c = fgetc (f);
  if (c != EOF && isatty (fileno (f))) {
    if (c == '\r')
      c = '\n';
  }
  return c;
}

/****************************************************************************/

size_t tty_getline (struct string_descr *prompt, char **buf, size_t *bufsiz,
                    FILE *f, BOOL *error)
/* getline(buf,bufsiz,f) with prompt, echo & editing if(isatty) */
{
#ifdef HAVE_READLINE /* use readline() for tty, getline() for files */

  char *p;
  static BOOL first = TRUE;

  *error = FALSE;

  if (!isatty (fileno (f))) {
    ssize_t ssize = getline (buf, bufsiz, f);
    if (ssize < 0) {
      *error = TRUE;
      return 0;
    } else
      return (size_t)ssize;
  }

  if (first) {
#if RL_VERSION_MAJOR >= 7
    atexit (rl_clear_history);
#endif
    first = FALSE;
  }
  if ((p = my_malloc (prompt->length + 1)) == NULL) {
    fprintf (stderr, "Error writing input prompt");
    *error = TRUE;
    return (size_t)0;
  }
  memcpy (p, prompt->body, prompt->length);
  p[prompt->length] = '\0';
  reset_keypress ();
  if (*buf != NULL)
    free (*buf);  /* not my_free() because malloc()'ed by readline() */
  *buf = readline (p);
  init_tty ();
  p = my_free (p);
  if (*buf == NULL) {
    *error = TRUE;
    return (size_t)0;
  }
  else if (*buf[0] != '\0')
    add_history (*buf);
  *bufsiz = strlen(*buf) + 2;
  if ((*buf = realloc (*buf, *bufsiz)) == NULL) { /* not my_realloc() */
    *error = TRUE;
    return 0;
  }
  (*buf)[*bufsiz - 2] = '\n'; /* append '\n' like fgets() and getline() */
  (*buf)[*bufsiz - 1] = '\0';
  return *bufsiz - 1;

#else /* else simulate getline() */

  BOOL is_a_tty;
  char *ptr, *eptr;
  FILE *g;    /* file to write prompt and echo to */
  int c;

  *error = FALSE;

  is_a_tty = isatty (fileno (f));
  g = (f == stdin) ? stdout : f;
  if (is_a_tty) {
    if (prompt != NULL && prompt->length > 0) {
      if (fwrite (prompt->body, 1, prompt->length, g) != prompt->length)
        fprintf (stderr, "Error writing input prompt");
      fflush (g);
    }
  }
  if (*buf == NULL || *bufsiz == 0) {
    *bufsiz = BUFSIZ;
    if ((*buf = malloc (*bufsiz)) == NULL) {
      *error = TRUE;
      return 0;
    }
  }
  for (ptr = *buf, eptr = *buf + *bufsiz;;) {
    if (is_a_tty)
      c = tty_getc(f);
    else
      c = fgetc (f);
    if (c == -1) {
      if (feof (f))
        return ptr == *buf ? -1 : ptr - *buf;
      else {
        *error = TRUE;
        return 0;
      }
    }
    if (is_a_tty) {
      fputc (c, g);  /* echo */
      if (c == '\b' || c == '\x7f') { /* backspace or rubout */
        if (ptr > *buf) {
          if (c == '\x7f')
            fputc ('\b', g);
          fputc (' ', g);
          fputc ('\b', g);
          --ptr;
        }
        fflush (g);
        continue;
      }
      fflush (g);
    }
    *ptr++ = c;
    if (c == '\n') {
      *ptr = '\0';
      return ptr - *buf;
    }
    if (ptr + 2 >= eptr) {
      char *nbuf;
      size_t nbufsiz = *bufsiz * 2;
      ssize_t d = ptr - *buf;
      if ((nbuf = realloc (*buf, nbufsiz)) == NULL) {
        *error = TRUE;
        return 0;
      }
      *buf = nbuf;
      *bufsiz = nbufsiz;
      eptr = nbuf + nbufsiz;
      ptr = nbuf + d;
    }
  }

#endif
}

/****************************************************************************/

size_t tty_fread (void *buf, size_t bsize, size_t n, FILE *f)
{
  int c;
  size_t ret, i;
  char *b;

  if (isatty (fileno (f))) {
    b = buf;
    for (ret = 0; ret < n; ret++)
      for (i = 0; i < bsize; i++) {
        if ((c = fgetc (f)) == EOF)
          return ret;
        *b++ = c;
      }
  } else
    ret = fread (buf, bsize, n, f);
  return ret;
}

/****************************************************************************/

#define NOBLOCK

int inkey (struct string_descr *str)
{
  fd_set rfds;
  struct timeval tv;

  fflush (stdout);
#ifdef NOBLOCK
  if (isatty (fileno (stdin))) {
    FD_ZERO (&rfds);
    FD_SET (fileno (stdin), &rfds);
    tv.tv_sec = 0;
    /*tv.tv_usec = 20000;*/   /* 1/50 second */
    tv.tv_usec = 0;   /* no yield */
    if (select (1 << fileno (stdin), &rfds, NULL, NULL, &tv) != 0) {
      str->body[0] = getchar();
      str->length = 1;
    } else
      str->length = 0;
  } else {
    str->body[0] = getchar();
    str->length = 1;
  }
#else
  str->body[0] = getchar();
  str->length = 1;
#endif
  return 1;
}

/****************************************************************************/

double get_timer (void)
{
  struct timeval tv;

  gettimeofday (&tv, NULL);
  return ((double)(tv.tv_sec % 86400)) + 0.000001 * (double)tv.tv_usec;
}

/****************************************************************************/

BOOL shell_cmd (struct string_descr *str)
{
  char *tmp_string = NULL;
  int result;

  fflush (stdout);
  if ((tmp_string = my_malloc (str->length + 1)) == NULL)
    return FALSE;
  memcpy (tmp_string, str->body, str->length);
  tmp_string[str->length] = '\0';
  result = system (tmp_string);
  tmp_string = my_free (tmp_string);
  return result == 0;
}

/****************************************************************************/

void sleep_delay (double seconds)
{
  fflush (stdout);
  if (isatty (fileno (stdin))) {
    fd_set rfds;
    struct timeval tv;
    FD_ZERO (&rfds);
    FD_SET (fileno (stdin), &rfds);
    tv.tv_sec = (unsigned int)floor(seconds);
    tv.tv_usec = (unsigned int)(1000000 * (seconds - (double)tv.tv_sec));
    select (1, &rfds, NULL, NULL, &tv);
  } else {
    unsigned int s = (unsigned int)floor(seconds);
    if (s > 0)
      sleep (s);
    s = (unsigned int)(1000000 * (seconds - (double)s));
    if (s > 0)
      usleep (s);
  }
}

/****************************************************************************/
