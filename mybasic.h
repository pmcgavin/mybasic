#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
/* #include <memory.h> */
#include <ctype.h>
#include <setjmp.h>
#include <time.h>

#include "mfile.h"

#define MAX_FNAME_LENGTH  256   /* Maximum length of file names */
#define MAX_LINE_LENGTH  1024   /* Maximum length of line in Basic program */
#define MAX_TOKEN_LENGTH   32   /* Maximum length of identifier names */
#define STACK_SIZE        128   /* Size of each evaluation stack */
#define MAX_DIMENSIONS      8   /* For arrays */
#define MAX_ARGUMENTS      64   /* For DEF FN functions */

#define STRINGIFY2(x) #x
#define STRINGIFY(x) STRINGIFY2(x)

#ifndef RAND_MAX
#define RAND_MAX ((1<<31)-1)    /* change 31 to 15 on some machines */
#endif

#ifdef DEBUG
#define debug(x) fprintf x
#else
#define debug(x)
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef BOOL
#define BOOL int
#endif

typedef size_t T_SIZE_T;
typedef short T_SHORT;
typedef /*short*/ int T_INT;
typedef long int T_LONG;
typedef float T_SINGLE;
typedef double T_DOUBLE;
typedef struct string_descr *T_STRING;
typedef struct file_descr *T_FILENUM;

#ifdef THREADED
#ifndef THREADED_BCODE_TYPE
#define THREADED_BCODE_TYPE int
#endif
typedef THREADED_BCODE_TYPE threaded_bcode_type;
#define SIZE_THREADED_BCODE (sizeof(threaded_bcode_type))
#define ALIGN_THREADED_BCODE (sizeof(threaded_bcode_type))
#endif

#define SIZE_BCODE   2 /* sizeof(unsigned short int) */
#define ALIGN_BCODE  2 /* sizeof(unsigned short int) */

#define SIZE_SIZE_T  sizeof(T_SIZE_T)
#define ALIGN_SIZE_T sizeof(T_SIZE_T)

#define SIZE_SHORT   sizeof(T_SHORT)
#define ALIGN_SHORT  sizeof(T_SHORT)

#define SIZE_INT     sizeof(T_INT)
#define ALIGN_INT    sizeof(T_INT)

#define SIZE_LONG    sizeof(T_LONG)
#define ALIGN_LONG   sizeof(T_LONG)

#define SIZE_SINGLE  sizeof(T_SINGLE)
#define ALIGN_SINGLE sizeof(T_SINGLE)

#define SIZE_DOUBLE  sizeof(T_DOUBLE)
#define ALIGN_DOUBLE sizeof(T_DOUBLE)

#define SIZE_STRING  sizeof(T_STRING)
#define ALIGN_STRING sizeof(T_STRING)

#define SIZE_FILE    sizeof(T_FILENUM)
#define ALIGN_FILE   sizeof(T_FILENUM)

#define SIZE_STACK   8
#define ALIGN_STACK  8

#ifndef SEEK_SET
#define SEEK_SET 0
#endif

#ifndef SEEK_END
#define SEEK_END 2
#endif

enum token_type {
        TOKEN_NONE,
        TOKEN_OTHER,
        TOKEN_ID,
        TOKEN_FUNCTION,
        TOKEN_NUMBER,
        TOKEN_STRING,
        TOKEN_FILE,
        TOKEN_REM,
        TOKEN_OPTION,
        TOKEN_BASE,
        TOKEN_DIM,
        TOKEN_CLEAR,
        TOKEN_LET,
        TOKEN_IF,
        TOKEN_THEN,
        TOKEN_ELSEIF,
        TOKEN_ELSE,
        TOKEN_ENDIF,
        TOKEN_DO,
        TOKEN_WHILE,
        TOKEN_UNTIL,
        TOKEN_WEND,
        TOKEN_LOOP,
        TOKEN_FOR,
        TOKEN_TO,
        TOKEN_STEP,
        TOKEN_NEXT,
        TOKEN_EXIT,
        TOKEN_GOSUB,
        TOKEN_RETURN,
        TOKEN_SYSTEM,
        TOKEN_STOP,
        TOKEN_END,
        TOKEN_OPEN,
        TOKEN_CLOSE,
        TOKEN_AS,
        TOKEN_INPUT,
        TOKEN_OUTPUT,
        TOKEN_APPEND,
        TOKEN_KILL,
        TOKEN_NAME,
        TOKEN_CHDIR,
        TOKEN_MKDIR,
        TOKEN_RMDIR,
        TOKEN_SHELL,
        TOKEN_CHAIN,
        TOKEN_PRINT,
        TOKEN_USING,
        TOKEN_TAB,
        TOKEN_SPC,
        TOKEN_BEEP,
        TOKEN_SLEEP,
        TOKEN_CLS,
        TOKEN_LOCATE,
        TOKEN_COLOR,
        TOKEN_LINE,
        TOKEN_LINEINPUT,
        TOKEN_INPUT_DOLLAR,
        TOKEN_INKEY_DOLLAR,
        TOKEN_GOTO,
        TOKEN_GO,
        TOKEN_ON,
        TOKEN_ERROR,
        TOKEN_RESUME,
        TOKEN_ERR,
        TOKEN_ERL,
        TOKEN_RANDOMIZE,
        TOKEN_TIMER,
        TOKEN_SWAP,
        TOKEN_DEFINT,
        TOKEN_DEFLNG,
        TOKEN_DEFSNG,
        TOKEN_DEFDBL,
        TOKEN_DEFSTR,
        TOKEN_DEF,
        TOKEN_INCR,
        TOKEN_DECR,
        TOKEN_COLON,
        TOKEN_SEMICOLON,
        TOKEN_COMMA,
        TOKEN_DOUBLEQUOTE,
        TOKEN_QUOTE,
        TOKEN_QUESTIONMARK,
        TOKEN_EQUAL,
        TOKEN_PLUS,
        TOKEN_MINUS,
        TOKEN_TIMES,
        TOKEN_DIVIDE,
        TOKEN_IDIVIDE,
        TOKEN_MOD,
        TOKEN_POWER,
        TOKEN_NOTEQUAL,
        TOKEN_LESSTHAN,
        TOKEN_GREATERTHAN,
        TOKEN_LESSTHANOREQUAL,
        TOKEN_GREATERTHANOREQUAL,
        TOKEN_LBRACKET,
        TOKEN_RBRACKET,
        TOKEN_AND,
        TOKEN_OR,
        TOKEN_XOR,
        TOKEN_NOT,
        TOKEN_SIN,
        TOKEN_COS,
        TOKEN_TAN,
        TOKEN_ATN,
        TOKEN_LOG,
        TOKEN_LOG2,
        TOKEN_LOG10,
        TOKEN_EXP,
        TOKEN_SQR,
        TOKEN_ABS,
        TOKEN_RND,
        TOKEN_INT,
        TOKEN_CINT,
        TOKEN_CLNG,
        TOKEN_CSNG,
        TOKEN_CDBL,
        TOKEN_FIX,
        TOKEN_SGN,
        TOKEN_LEN,
        TOKEN_LEFT,
        TOKEN_MID,
        TOKEN_RIGHT,
        TOKEN_INSTR,
        TOKEN_RINSTR,
        TOKEN_CHR,
        TOKEN_ASC,
        TOKEN_VAL,
        TOKEN_STR,
        TOKEN_DATE,
        TOKEN_TIME,
        TOKEN_COMMAND,
        TOKEN_COMMANDCOUNT,
        TOKEN_EOF,
        TOKEN_LOF,
        TOKEN_POS,
        TOKEN_MKI,
        TOKEN_MKL,
        TOKEN_MKS,
        TOKEN_MKD,
        TOKEN_CVI,
        TOKEN_CVL,
        TOKEN_CVS,
        TOKEN_CVD,
        TOKEN_BIN,
        TOKEN_OCT,
        TOKEN_HEX,
        TOKEN_LCASE,
        TOKEN_UCASE,
        TOKEN_TRIM,
        TOKEN_LTRIM,
        TOKEN_RTRIM,
        TOKEN_STRING_DOLLAR,
        TOKEN_SPACE,
        TOKEN_GETENV,
        TOKEN_LBOUND,
        TOKEN_UBOUND,
        TOKEN_READ,
        TOKEN_DATA,
        TOKEN_RESTORE
};

#define FIRST_TOKEN TOKEN_NONE
#define LAST_TOKEN  TOKEN_RESTORE

struct token_table {
        char *token_string;
        int dollar;
};

enum var_type_type {
        TYPE_INT,
        TYPE_LONG,
        TYPE_SINGLE,
        TYPE_DOUBLE,
        TYPE_STRING,
        TYPE_LABEL,
        TYPE_FILE,
        TYPE_SHORT,
        TYPE_SIZE_T,
        TYPE_NONE
};

#define NUMBER_OF_TYPES 7
#define FIRST_TYPE TYPE_INT
#define LAST_TYPE  TYPE_FILE

struct string_descr {
        size_t length;
        size_t allocated_length;
        size_t ref_count;
        char *body;
};

enum file_state_type {
        FILE_CLOSED,
        FILE_OPEN_INPUT,
        FILE_OPEN_OUTPUT,
        FILE_OPEN_APPEND
};

struct file_descr {
        FILE *file;
        enum file_state_type file_state;
        T_LONG printpos;
};

union number_union {
        T_SHORT sh;
        T_INT i;
        T_LONG l;
        T_SINGLE s;
        T_DOUBLE d;
        T_STRING str;
        T_SIZE_T index;
};

struct result_type {
        enum var_type_type type;
        BOOL on_stack;
        union number_union value; /* if not on stack */
};

struct token_info_type {
        enum var_type_type token_type;
        char token_string[MAX_TOKEN_LENGTH];
        union number_union value;
        int dim;
        size_t dim_size[MAX_DIMENSIONS];
};

struct arg {
        enum var_type_type type;
        size_t offset;
};

struct var_node {
        struct var_node *left;
        struct var_node *right;
        char *var_name;
        enum var_type_type var_type;
        size_t offset;                 /* offset into variable area */
        size_t data_offset;            /* offset into data area (for labels) */
        size_t count;                  /* reference count */
        int dim;                       /* number of dimensions */
        size_t dim_size[MAX_DIMENSIONS]; /* size of each dimension */
        size_t factor[MAX_DIMENSIONS]; /* product of sizes of last dimensions */
        int nargs;                     /* for DEF FN functions */
        struct arg *arg;               /* for DEF FN functions */
};

struct if_node {
        int got_else;
        char else_string[MAX_TOKEN_LENGTH];
        char endif_string[MAX_TOKEN_LENGTH];
        BOOL one_line;
};

enum loop_kind_type {
        L_WHILE,
        L_DO,
        L_DO_WHILE,
        L_DO_UNTIL
};

struct loop_node {
        enum loop_kind_type loop_kind;
        T_SIZE_T offset;
        char endlabel_string[MAX_TOKEN_LENGTH];
};

struct for_node {
        T_SIZE_T loop_offset;
        char endlabel_string[MAX_TOKEN_LENGTH];
        char label1_string[MAX_TOKEN_LENGTH];
        char label2_string[MAX_TOKEN_LENGTH];
        struct token_info_type endvar, stepvar;
        enum var_type_type var_type;
        T_SIZE_T var_offset, endvar_offset, stepvar_offset;
        BOOL const_endvar;  /* TRUE if endvar is a constant */
        BOOL const_stepvar; /* TRUE if stepvar is a constant */
        union number_union end_value, step_value; /* if they are constant */
};

enum struct_statement_kind {
        S_IF,
        S_LOOP,
        S_FOR
};

struct struct_statement_node {
        struct struct_statement_node *next;
        enum struct_statement_kind struct_statement_kind;
        union {
                struct if_node if_node;
                struct loop_node loop_node;
                struct for_node for_node;
        } node;
};

struct goto_node {
        struct goto_node *next;
        BOOL data;
        T_SIZE_T offset;
        char *label_string;
};

enum bcode_type {
        B_NOP,
        B_ILLEGAL,
        B_STATEMENT_CHECK,
        B_CLEAR,
        B_PUSH_CONST_INT,
        B_PUSH_CONST_INT_16,
        B_PUSH_CONST_LONG,
        B_PUSH_CONST_LONG_16,
        B_PUSH_CONST_SINGLE,
        B_PUSH_CONST_SINGLE_16,
        B_PUSH_CONST_DOUBLE,
        B_PUSH_CONST_DOUBLE_16,
        B_PUSH_CONST_STRING,
        B_PUSH_CONST_INDEX,
        B_PUSH_CONST_INDEX_16,
        B_PUSH_CONST_INT_ZERO,
        B_PUSH_CONST_LONG_ZERO,
        B_PUSH_CONST_SINGLE_ZERO,
        B_PUSH_CONST_DOUBLE_ZERO,
        B_PUSH_CONST_INT_ONE,
        B_PUSH_CONST_LONG_ONE,
        B_PUSH_CONST_SINGLE_ONE,
        B_PUSH_CONST_DOUBLE_ONE,
        B_SET_PRIMARY_FILE,
        B_SET_PRIMARY_FILE_16,
        B_SET_PRIMARY_INPUT_FILE,
        B_SET_PRIMARY_OUTPUT_FILE,
        B_SET_SECONDARY_FILE,
        B_SET_SECONDARY_FILE_16,
        B_SET_SECONDARY_INPUT_FILE,
        B_SET_SECONDARY_OUTPUT_FILE,
        B_PUSH_VAR_INT,
        B_PUSH_VAR_LONG,
        B_PUSH_VAR_SINGLE,
        B_PUSH_VAR_DOUBLE,
        B_PUSH_VAR_STRING,
        B_PUSH_VAR_INT_16,
        B_PUSH_VAR_LONG_16,
        B_PUSH_VAR_SINGLE_16,
        B_PUSH_VAR_DOUBLE_16,
        B_PUSH_VAR_STRING_16,
        B_POP_INT,
        B_POP_LONG,
        B_POP_SINGLE,
        B_POP_DOUBLE,
        B_POP_STRING,
        B_POP_STRING_MID2,
        B_POP_STRING_MID3,
        B_POP_INT_16,
        B_POP_LONG_16,
        B_POP_SINGLE_16,
        B_POP_DOUBLE_16,
        B_POP_STRING_16,
        B_POP_STRING_MID2_16,
        B_POP_STRING_MID3_16,
        B_PUSH_INDIRECT_VAR_INT,
        B_PUSH_INDIRECT_VAR_LONG,
        B_PUSH_INDIRECT_VAR_SINGLE,
        B_PUSH_INDIRECT_VAR_DOUBLE,
        B_PUSH_INDIRECT_VAR_STRING,
        B_PUSH_INDIRECT_VAR_INT_16,
        B_PUSH_INDIRECT_VAR_LONG_16,
        B_PUSH_INDIRECT_VAR_SINGLE_16,
        B_PUSH_INDIRECT_VAR_DOUBLE_16,
        B_PUSH_INDIRECT_VAR_STRING_16,
        B_POP_INDIRECT_INT,
        B_POP_INDIRECT_LONG,
        B_POP_INDIRECT_SINGLE,
        B_POP_INDIRECT_DOUBLE,
        B_POP_INDIRECT_STRING,
        B_POP_INDIRECT_STRING_MID2,
        B_POP_INDIRECT_STRING_MID3,
        B_POP_INDIRECT_INT_16,
        B_POP_INDIRECT_LONG_16,
        B_POP_INDIRECT_SINGLE_16,
        B_POP_INDIRECT_DOUBLE_16,
        B_POP_INDIRECT_STRING_16,
        B_POP_INDIRECT_STRING_MID2_16,
        B_POP_INDIRECT_STRING_MID3_16,
        B_ZERO_VAR_INT,
        B_ZERO_VAR_LONG,
        B_ZERO_VAR_SINGLE,
        B_ZERO_VAR_DOUBLE,
        B_ZERO_VAR_STRING,
        B_ZERO_VAR_INT_16,
        B_ZERO_VAR_LONG_16,
        B_ZERO_VAR_SINGLE_16,
        B_ZERO_VAR_DOUBLE_16,
        B_ZERO_VAR_STRING_16,
        B_ONE_VAR_INT,
        B_ONE_VAR_LONG,
        B_ONE_VAR_SINGLE,
        B_ONE_VAR_DOUBLE,
        B_ONE_VAR_STRING,
        B_ONE_VAR_INT_16,
        B_ONE_VAR_LONG_16,
        B_ONE_VAR_SINGLE_16,
        B_ONE_VAR_DOUBLE_16,
        B_ONE_VAR_STRING_16,
        B_INT_SWAP,
        B_LONG_SWAP,
        B_SINGLE_SWAP,
        B_DOUBLE_SWAP,
        B_STRING_SWAP,
        B_INDEX_DUP,
        B_INDEX_DUP2,
        B_INT_UNARY_MINUS,
        B_LONG_UNARY_MINUS,
        B_SINGLE_UNARY_MINUS,
        B_DOUBLE_UNARY_MINUS,
        B_INT_UNARY_PLUS,
        B_LONG_UNARY_PLUS,
        B_SINGLE_UNARY_PLUS,
        B_DOUBLE_UNARY_PLUS,
        B_INT_PLUS,
        B_INT_PLUS_CONST,
        B_INT_PLUS_CONST_16,
        B_INT_PLUS_VAR,
        B_INT_PLUS_VAR_16,
        B_INT_MINUS,
        B_INT_MINUS_CONST,
        B_INT_MINUS_CONST_16,
        B_INT_MINUS_VAR,
        B_INT_MINUS_VAR_16,
        B_INT_SWAP_MINUS,
        B_INT_SWAP_MINUS_CONST,
        B_INT_SWAP_MINUS_CONST_16,
        B_INT_TIMES,
        B_INT_TIMES_CONST,
        B_INT_TIMES_CONST_16,
        B_INT_TIMES_VAR,
        B_INT_TIMES_VAR_16,
        B_INT_DIVIDE,
        B_INT_DIVIDE_CONST,
        B_INT_DIVIDE_CONST_16,
        B_INT_SWAP_DIVIDE,
        B_INT_SWAP_DIVIDE_CONST,
        B_INT_SWAP_DIVIDE_CONST_16,
        B_INT_MOD,
        B_INT_MOD_CONST,
        B_INT_MOD_CONST_16,
        B_INT_SWAP_MOD,
        B_INT_SWAP_MOD_CONST,
        B_INT_SWAP_MOD_CONST_16,
        B_INT_POWER,
        B_INT_POWER_CONST,
        B_INT_POWER_CONST_16,
        B_INT_SWAP_POWER,
        B_INT_SWAP_POWER_CONST,
        B_INT_SWAP_POWER_CONST_16,
        B_INT_AND,
        B_INT_AND_CONST,
        B_INT_AND_CONST_16,
        B_INT_OR,
        B_INT_OR_CONST,
        B_INT_OR_CONST_16,
        B_INT_XOR,
        B_INT_XOR_CONST,
        B_INT_XOR_CONST_16,
        B_INT_NOT,
        B_INT_ABS,
        B_INT_CLNG,
        B_INT_CSNG,
        B_INT_CDBL,
        B_INT_SGN,
        B_INT_EQUAL,
        B_INT_EQUAL_CONST,
        B_INT_EQUAL_CONST_16,
        B_INT_NOTEQUAL,
        B_INT_NOTEQUAL_CONST,
        B_INT_NOTEQUAL_CONST_16,
        B_INT_LESS,
        B_INT_LESS_CONST,
        B_INT_LESS_CONST_16,
        B_INT_GREATER,
        B_INT_GREATER_CONST,
        B_INT_GREATER_CONST_16,
        B_INT_LESSOREQUAL,
        B_INT_LESSOREQUAL_CONST,
        B_INT_LESSOREQUAL_CONST_16,
        B_INT_GREATEROREQUAL,
        B_INT_GREATEROREQUAL_CONST,
        B_INT_GREATEROREQUAL_CONST_16,
        B_INT_INCREMENT,
        B_INT_INCREMENT_VAR,
        B_INT_INCREMENT_VAR_16,
        B_INT_DECREMENT,
        B_INT_DECREMENT_VAR,
        B_INT_DECREMENT_VAR_16,
        B_INT_COMMANDCOUNT,
        B_LONG_PLUS,
        B_LONG_PLUS_CONST,
        B_LONG_PLUS_CONST_16,
        B_LONG_PLUS_VAR,
        B_LONG_PLUS_VAR_16,
        B_LONG_MINUS,
        B_LONG_MINUS_CONST,
        B_LONG_MINUS_CONST_16,
        B_LONG_MINUS_VAR,
        B_LONG_MINUS_VAR_16,
        B_LONG_SWAP_MINUS,
        B_LONG_SWAP_MINUS_CONST,
        B_LONG_SWAP_MINUS_CONST_16,
        B_LONG_TIMES,
        B_LONG_TIMES_CONST,
        B_LONG_TIMES_CONST_16,
        B_LONG_TIMES_VAR,
        B_LONG_TIMES_VAR_16,
        B_LONG_DIVIDE,
        B_LONG_DIVIDE_CONST,
        B_LONG_DIVIDE_CONST_16,
        B_LONG_SWAP_DIVIDE,
        B_LONG_SWAP_DIVIDE_CONST,
        B_LONG_SWAP_DIVIDE_CONST_16,
        B_LONG_MOD,
        B_LONG_MOD_CONST,
        B_LONG_MOD_CONST_16,
        B_LONG_SWAP_MOD,
        B_LONG_SWAP_MOD_CONST,
        B_LONG_SWAP_MOD_CONST_16,
        B_LONG_POWER,
        B_LONG_POWER_CONST,
        B_LONG_POWER_CONST_16,
        B_LONG_SWAP_POWER,
        B_LONG_SWAP_POWER_CONST,
        B_LONG_SWAP_POWER_CONST_16,
        B_LONG_AND,
        B_LONG_AND_CONST,
        B_LONG_AND_CONST_16,
        B_LONG_OR,
        B_LONG_OR_CONST,
        B_LONG_OR_CONST_16,
        B_LONG_XOR,
        B_LONG_XOR_CONST,
        B_LONG_XOR_CONST_16,
        B_LONG_NOT,
        B_LONG_ABS,
        B_LONG_INT,
        B_LONG_CINT,
        B_LONG_CSNG,
        B_LONG_CDBL,
        B_LONG_FIX,
        B_LONG_SGN,
        B_LONG_EQUAL,
        B_LONG_EQUAL_CONST,
        B_LONG_EQUAL_CONST_16,
        B_LONG_NOTEQUAL,
        B_LONG_NOTEQUAL_CONST,
        B_LONG_NOTEQUAL_CONST_16,
        B_LONG_LESS,
        B_LONG_LESS_CONST,
        B_LONG_LESS_CONST_16,
        B_LONG_GREATER,
        B_LONG_GREATER_CONST,
        B_LONG_GREATER_CONST_16,
        B_LONG_LESSOREQUAL,
        B_LONG_LESSOREQUAL_CONST,
        B_LONG_LESSOREQUAL_CONST_16,
        B_LONG_GREATEROREQUAL,
        B_LONG_GREATEROREQUAL_CONST,
        B_LONG_GREATEROREQUAL_CONST_16,
        B_LONG_INCREMENT,
        B_LONG_INCREMENT_VAR,
        B_LONG_INCREMENT_VAR_16,
        B_LONG_DECREMENT,
        B_LONG_DECREMENT_VAR,
        B_LONG_DECREMENT_VAR_16,
        B_SINGLE_PLUS,
        B_SINGLE_PLUS_CONST,
        B_SINGLE_PLUS_CONST_16,
        B_SINGLE_PLUS_VAR,
        B_SINGLE_PLUS_VAR_16,
        B_SINGLE_MINUS,
        B_SINGLE_MINUS_CONST,
        B_SINGLE_MINUS_CONST_16,
        B_SINGLE_MINUS_VAR,
        B_SINGLE_MINUS_VAR_16,
        B_SINGLE_SWAP_MINUS,
        B_SINGLE_SWAP_MINUS_CONST,
        B_SINGLE_SWAP_MINUS_CONST_16,
        B_SINGLE_TIMES,
        B_SINGLE_TIMES_CONST,
        B_SINGLE_TIMES_CONST_16,
        B_SINGLE_TIMES_VAR,
        B_SINGLE_TIMES_VAR_16,
        B_SINGLE_DIVIDE,
        B_SINGLE_DIVIDE_CONST,
        B_SINGLE_DIVIDE_CONST_16,
        B_SINGLE_SWAP_DIVIDE,
        B_SINGLE_SWAP_DIVIDE_CONST,
        B_SINGLE_SWAP_DIVIDE_CONST_16,
        B_SINGLE_POWER,
        B_SINGLE_POWER_CONST,
        B_SINGLE_POWER_CONST_16,
        B_SINGLE_SWAP_POWER,
        B_SINGLE_SWAP_POWER_CONST,
        B_SINGLE_SWAP_POWER_CONST_16,
        B_SINGLE_SIN,
        B_SINGLE_COS,
        B_SINGLE_TAN,
        B_SINGLE_ATN,
        B_SINGLE_LOG,
        B_SINGLE_LOG2,
        B_SINGLE_LOG10,
        B_SINGLE_EXP,
        B_SINGLE_SQR,
        B_SINGLE_ABS,
        B_SINGLE_RND,
        B_SINGLE_INT,
        B_SINGLE_CINT,
        B_SINGLE_CLNG,
        B_SINGLE_CDBL,
        B_SINGLE_FIX,
        B_SINGLE_SGN,
        B_SINGLE_EQUAL,
        B_SINGLE_EQUAL_CONST,
        B_SINGLE_EQUAL_CONST_16,
        B_SINGLE_NOTEQUAL,
        B_SINGLE_NOTEQUAL_CONST,
        B_SINGLE_NOTEQUAL_CONST_16,
        B_SINGLE_LESS,
        B_SINGLE_LESS_CONST,
        B_SINGLE_LESS_CONST_16,
        B_SINGLE_GREATER,
        B_SINGLE_GREATER_CONST,
        B_SINGLE_GREATER_CONST_16,
        B_SINGLE_LESSOREQUAL,
        B_SINGLE_LESSOREQUAL_CONST,
        B_SINGLE_LESSOREQUAL_CONST_16,
        B_SINGLE_GREATEROREQUAL,
        B_SINGLE_GREATEROREQUAL_CONST,
        B_SINGLE_GREATEROREQUAL_CONST_16,
        B_SINGLE_INCREMENT,
        B_SINGLE_INCREMENT_VAR,
        B_SINGLE_INCREMENT_VAR_16,
        B_SINGLE_DECREMENT,
        B_SINGLE_DECREMENT_VAR,
        B_SINGLE_DECREMENT_VAR_16,
        B_DOUBLE_PLUS,
        B_DOUBLE_PLUS_CONST,
        B_DOUBLE_PLUS_CONST_16,
        B_DOUBLE_PLUS_VAR,
        B_DOUBLE_PLUS_VAR_16,
        B_DOUBLE_MINUS,
        B_DOUBLE_MINUS_CONST,
        B_DOUBLE_MINUS_CONST_16,
        B_DOUBLE_MINUS_VAR,
        B_DOUBLE_MINUS_VAR_16,
        B_DOUBLE_SWAP_MINUS,
        B_DOUBLE_SWAP_MINUS_CONST,
        B_DOUBLE_SWAP_MINUS_CONST_16,
        B_DOUBLE_TIMES,
        B_DOUBLE_TIMES_CONST,
        B_DOUBLE_TIMES_CONST_16,
        B_DOUBLE_TIMES_VAR,
        B_DOUBLE_TIMES_VAR_16,
        B_DOUBLE_DIVIDE,
        B_DOUBLE_DIVIDE_CONST,
        B_DOUBLE_DIVIDE_CONST_16,
        B_DOUBLE_SWAP_DIVIDE,
        B_DOUBLE_SWAP_DIVIDE_CONST,
        B_DOUBLE_SWAP_DIVIDE_CONST_16,
        B_DOUBLE_POWER,
        B_DOUBLE_POWER_CONST,
        B_DOUBLE_POWER_CONST_16,
        B_DOUBLE_SWAP_POWER,
        B_DOUBLE_SWAP_POWER_CONST,
        B_DOUBLE_SWAP_POWER_CONST_16,
        B_DOUBLE_SIN,
        B_DOUBLE_COS,
        B_DOUBLE_TAN,
        B_DOUBLE_ATN,
        B_DOUBLE_LOG,
        B_DOUBLE_LOG2,
        B_DOUBLE_LOG10,
        B_DOUBLE_EXP,
        B_DOUBLE_SQR,
        B_DOUBLE_ABS,
        B_DOUBLE_INT,
        B_DOUBLE_CINT,
        B_DOUBLE_CLNG,
        B_DOUBLE_CSNG,
        B_DOUBLE_FIX,
        B_DOUBLE_SGN,
        B_DOUBLE_EQUAL,
        B_DOUBLE_EQUAL_CONST,
        B_DOUBLE_EQUAL_CONST_16,
        B_DOUBLE_NOTEQUAL,
        B_DOUBLE_NOTEQUAL_CONST,
        B_DOUBLE_NOTEQUAL_CONST_16,
        B_DOUBLE_LESS,
        B_DOUBLE_LESS_CONST,
        B_DOUBLE_LESS_CONST_16,
        B_DOUBLE_GREATER,
        B_DOUBLE_GREATER_CONST,
        B_DOUBLE_GREATER_CONST_16,
        B_DOUBLE_LESSOREQUAL,
        B_DOUBLE_LESSOREQUAL_CONST,
        B_DOUBLE_LESSOREQUAL_CONST_16,
        B_DOUBLE_GREATEROREQUAL,
        B_DOUBLE_GREATEROREQUAL_CONST,
        B_DOUBLE_GREATEROREQUAL_CONST_16,
        B_DOUBLE_INCREMENT,
        B_DOUBLE_INCREMENT_VAR,
        B_DOUBLE_INCREMENT_VAR_16,
        B_DOUBLE_DECREMENT,
        B_DOUBLE_DECREMENT_VAR,
        B_DOUBLE_DECREMENT_VAR_16,
        B_STRING_PLUS,
        B_STRING_EQUAL,
        B_STRING_NOTEQUAL,
        B_STRING_LESS,
        B_STRING_GREATER,
        B_STRING_LESSOREQUAL,
        B_STRING_GREATEROREQUAL,
        B_STRING_LEN,
        B_STRING_LEFT,
        B_STRING_MID2,
        B_STRING_MID3,
        B_STRING_RIGHT,
        B_STRING_INSTR2,
        B_STRING_INSTR3,
        B_STRING_RINSTR2,
        B_STRING_RINSTR3,
        B_STRING_CHR,
        B_STRING_ASC,
        B_STRING_VAL,
        B_STRING_STR_INT,
        B_STRING_STR_LONG,
        B_STRING_STR_SINGLE,
        B_STRING_STR_DOUBLE,
        B_STRING_DATE,
        B_STRING_TIME,
        B_STRING_COMMAND,
        B_STRING_COMMAND1,
        B_DOUBLE_TIMER,
        B_EOF,
        B_LOF,
        B_POS,
        B_STRING_MKI,
        B_STRING_MKL,
        B_STRING_MKS,
        B_STRING_MKD,
        B_INT_CVI,
        B_LONG_CVL,
        B_SINGLE_CVS,
        B_DOUBLE_CVD,
        B_STRING_BIN,
        B_STRING_OCT,
        B_STRING_HEX,
        B_STRING_LCASE,
        B_STRING_UCASE,
        B_STRING_TRIM,
        B_STRING_LTRIM,
        B_STRING_RTRIM,
        B_STRING_STRING,
        B_STRING_STRING2,
        B_STRING_SPACE,
        B_STRING_GETENV,
        B_LONG_LBOUND,
        B_LONG_UBOUND,
        B_INDEX_PLUS,
        B_INDEX_TIMES_CONST,
        B_INDEX_TIMES_CONST_16,
        B_INT_TO_LONG,
        B_INT_TO_SINGLE,
        B_INT_TO_DOUBLE,
        B_INT_TO_INDEX,
        B_INT_TO_INDEX_16,
        B_INT_TO_INDEX_PLUS,
        B_INT_TO_INDEX_PLUS_16,
        B_INT_TO_INDEX_MINUS_1,
        B_INT_TO_INDEX_MINUS_1_16,
        B_INT_TO_INDEX_PLUS_MINUS_1,
        B_INT_TO_INDEX_PLUS_MINUS_1_16,
        B_LONG_TO_INT,
        B_LONG_TO_SINGLE,
        B_LONG_TO_DOUBLE,
        B_LONG_TO_INDEX,
        B_LONG_TO_INDEX_16,
        B_LONG_TO_INDEX_PLUS,
        B_LONG_TO_INDEX_PLUS_16,
        B_LONG_TO_INDEX_MINUS_1,
        B_LONG_TO_INDEX_MINUS_1_16,
        B_LONG_TO_INDEX_PLUS_MINUS_1,
        B_LONG_TO_INDEX_PLUS_MINUS_1_16,
        B_SINGLE_TO_INT,
        B_SINGLE_TO_LONG,
        B_SINGLE_TO_DOUBLE,
        B_SINGLE_TO_INDEX,
        B_SINGLE_TO_INDEX_16,
        B_SINGLE_TO_INDEX_PLUS,
        B_SINGLE_TO_INDEX_PLUS_16,
        B_SINGLE_TO_INDEX_MINUS_1,
        B_SINGLE_TO_INDEX_MINUS_1_16,
        B_SINGLE_TO_INDEX_PLUS_MINUS_1,
        B_SINGLE_TO_INDEX_PLUS_MINUS_1_16,
        B_DOUBLE_TO_INT,
        B_DOUBLE_TO_LONG,
        B_DOUBLE_TO_SINGLE,
        B_DOUBLE_TO_INDEX,
        B_DOUBLE_TO_INDEX_16,
        B_DOUBLE_TO_INDEX_PLUS,
        B_DOUBLE_TO_INDEX_PLUS_16,
        B_DOUBLE_TO_INDEX_MINUS_1,
        B_DOUBLE_TO_INDEX_MINUS_1_16,
        B_DOUBLE_TO_INDEX_PLUS_MINUS_1,
        B_DOUBLE_TO_INDEX_PLUS_MINUS_1_16,
        B_JUMP,
        B_JUMP_16,
        B_JUMP_INT_EQUAL_ZERO,
        B_JUMP_INT_EQUAL_ZERO_16,
        B_JUMP_INT_NOTEQUAL_ZERO,
        B_JUMP_INT_NOTEQUAL_ZERO_16,
        B_JUMP_LONG_EQUAL_ZERO,
        B_JUMP_LONG_EQUAL_ZERO_16,
        B_JUMP_LONG_NOTEQUAL_ZERO,
        B_JUMP_LONG_NOTEQUAL_ZERO_16,
        B_JUMP_SINGLE_EQUAL_ZERO,
        B_JUMP_SINGLE_EQUAL_ZERO_16,
        B_JUMP_SINGLE_NOTEQUAL_ZERO,
        B_JUMP_SINGLE_NOTEQUAL_ZERO_16,
        B_JUMP_DOUBLE_EQUAL_ZERO,
        B_JUMP_DOUBLE_EQUAL_ZERO_16,
        B_JUMP_DOUBLE_NOTEQUAL_ZERO,
        B_JUMP_DOUBLE_NOTEQUAL_ZERO_16,
        B_JUMP_INT_LESS,
        B_JUMP_LONG_LESS,
        B_JUMP_SINGLE_LESS,
        B_JUMP_DOUBLE_LESS,
        B_JUMP_INT_GREATER,
        B_JUMP_LONG_GREATER,
        B_JUMP_SINGLE_GREATER,
        B_JUMP_DOUBLE_GREATER,
        B_GOSUB,
        B_ON_GOTO,
        B_ON_GOSUB,
        B_ON_ERROR_GOTO,
        B_ON_ERROR_GOTO_0,
        B_RESUME,
        B_RESUME_LABEL,
        B_RESUME_NEXT,
        B_ERR,
        B_ERL,
        B_ERROR,
        B_RETURN,
        B_RETURN_LINE,
        B_RANDOMIZE_TIMER,
        B_RANDOMIZE_LONG,
        B_OPEN,
        B_CLOSE,
        B_KILL,
        B_NAME,
        B_CHDIR,
        B_MKDIR,
        B_RMDIR,
        B_SHELL,
        B_CHAIN,
        B_PRINT_INT,
        B_PRINT_LONG,
        B_PRINT_SINGLE,
        B_PRINT_DOUBLE,
        B_PRINT_STRING,
        B_PRINT_COMMA,
        B_PRINT_TAB,
        B_PRINT_SPC,
        B_PRINT_NEWLINE,
        B_PRINT_END,
        B_PRINT_USING_INIT,
        B_PRINT_USING_INT,
        B_PRINT_USING_LONG,
        B_PRINT_USING_SINGLE,
        B_PRINT_USING_DOUBLE,
        B_PRINT_USING_STRING,
        B_PRINT_USING_END,
        B_BEEP,
        B_SLEEP,
        B_CLS,
        B_LOCATE,
        B_COLOR,
        B_BACKGROUND_COLOR,
        B_LINEINPUT,
        B_INPUT_RESTART,
        B_INPUT_CLEAR_PROMPT_STRING,
        B_INPUT_SET_PROMPT_STRING,
        B_INPUT_EXTEND_PROMPT_STRING,
        B_INPUT_START,
        B_INPUT_INT,
        B_INPUT_LONG,
        B_INPUT_SINGLE,
        B_INPUT_DOUBLE,
        B_INPUT_STRING,
        B_INPUT_DOLLAR,
        B_INKEY_DOLLAR,
        B_READ_INT,
        B_READ_LONG,
        B_READ_SINGLE,
        B_READ_DOUBLE,
        B_READ_STRING,
        B_RESTORE,
        B_RESTORE_LINE,
        B_STOP,
        B_END
};

#ifndef BCODE
typedef enum bcode_type BCODE;
#endif

struct type_info_type {
        char *type_name;
        T_SIZE_T type_size;
        int type_numeric;
        int type_integer;
        int type_float;
        BCODE b_push_const;
        BCODE b_push_const_16;
        BCODE b_push_const_zero;
        BCODE b_push_const_one;
        BCODE b_push_var;
        BCODE b_push_var_16;
        BCODE b_pop;
        BCODE b_pop_16;
        BCODE b_push_indirect_var;
        BCODE b_push_indirect_var_16;
        BCODE b_pop_indirect;
        BCODE b_pop_indirect_16;
        BCODE b_zero_var;
        BCODE b_zero_var_16;
        BCODE b_one_var;
        BCODE b_one_var_16;
        BCODE b_swap;
        BCODE b_equal;
        BCODE b_equal_const;
        BCODE b_equal_const_16;
        BCODE b_notequal;
        BCODE b_notequal_const;
        BCODE b_notequal_const_16;
        BCODE b_less;
        BCODE b_less_const;
        BCODE b_less_const_16;
        BCODE b_greater;
        BCODE b_greater_const;
        BCODE b_greater_const_16;
        BCODE b_lessorequal;
        BCODE b_lessorequal_const;
        BCODE b_lessorequal_const_16;
        BCODE b_greaterorequal;
        BCODE b_greaterorequal_const;
        BCODE b_greaterorequal_const_16;
        BCODE b_increment;
        BCODE b_increment_var;
        BCODE b_increment_var_16;
        BCODE b_decrement;
        BCODE b_decrement_var;
        BCODE b_decrement_var_16;
        BCODE b_unary_minus;
        BCODE b_plus;
        BCODE b_plus_const;
        BCODE b_plus_const_16;
        BCODE b_plus_var;
        BCODE b_plus_var_16;
        BCODE b_minus;
        BCODE b_minus_const;
        BCODE b_minus_const_16;
        BCODE b_minus_var;
        BCODE b_minus_var_16;
        BCODE b_swap_minus;
        BCODE b_swap_minus_const;
        BCODE b_swap_minus_const_16;
        BCODE b_times;
        BCODE b_times_const;
        BCODE b_times_const_16;
        BCODE b_times_var;
        BCODE b_times_var_16;
        BCODE b_divide;
        BCODE b_divide_const;
        BCODE b_divide_const_16;
        BCODE b_swap_divide;
        BCODE b_swap_divide_const;
        BCODE b_swap_divide_const_16;
        BCODE b_idivide;
        BCODE b_idivide_const;
        BCODE b_idivide_const_16;
        BCODE b_swap_idivide;
        BCODE b_swap_idivide_const;
        BCODE b_swap_idivide_const_16;
        BCODE b_mod;
        BCODE b_mod_const;
        BCODE b_mod_const_16;
        BCODE b_swap_mod;
        BCODE b_swap_mod_const;
        BCODE b_swap_mod_const_16;
        BCODE b_power;
        BCODE b_power_const;
        BCODE b_power_const_16;
        BCODE b_swap_power;
        BCODE b_swap_power_const;
        BCODE b_swap_power_const_16;
        BCODE b_print;
        BCODE b_print_using;
        BCODE b_input;
        BCODE b_read;
        BCODE b_and;
        BCODE b_and_const;
        BCODE b_and_const_16;
        BCODE b_or;
        BCODE b_or_const;
        BCODE b_or_const_16;
        BCODE b_xor;
        BCODE b_xor_const;
        BCODE b_xor_const_16;
        BCODE b_not;
        BCODE b_sin;
        BCODE b_cos;
        BCODE b_tan;
        BCODE b_atn;
        BCODE b_log;
        BCODE b_log2;
        BCODE b_log10;
        BCODE b_exp;
        BCODE b_sqr;
        BCODE b_abs;
        BCODE b_int;
        BCODE b_cint;
        BCODE b_clng;
        BCODE b_csng;
        BCODE b_cdbl;
        BCODE b_fix;
        BCODE b_sgn;
        BCODE b_str;
        BCODE b_jump_equal_zero;
        BCODE b_jump_equal_zero_16;
        BCODE b_jump_notequal_zero;
        BCODE b_jump_notequal_zero_16;
        BCODE b_jump_less;
        BCODE b_jump_greater;
        BCODE b_convert_to_index;
        BCODE b_convert_to_index_16;
        BCODE b_convert_to_index_plus;
        BCODE b_convert_to_index_plus_16;
        BCODE b_convert_to_index_minus_1;
        BCODE b_convert_to_index_minus_1_16;
        BCODE b_convert_to_index_plus_minus_1;
        BCODE b_convert_to_index_plus_minus_1_16;
        BCODE b_convert[NUMBER_OF_TYPES];
};

struct history {
        BCODE bcode;
        enum var_type_type type;
        union number_union value;
        T_SIZE_T offset;
};

#define MAX_HISTORY 100

struct file {
        MFILE *f;
        T_SIZE_T offset;
        BOOL is_keeping_history;
        int history_count;
        struct history history[MAX_HISTORY];
};

struct b {
        char *fname;           /* name of BASIC input file */
        FILE *f;               /* BASIC source code input file */
        struct file b;         /* tmp file for bcodes to be executed */
        struct file data;      /* tmp file for initialised data */
        MFILE *xref;           /* tmp file used by run-time error handler */
        int argc;              /* number of program arguments */
        char **argv;           /* program arguments*/
        int option_base;       /* option base (-1 == not set yet) */
};

struct xref_record {           /* for each BASIC statement, for error handler */
        T_SIZE_T offset;       /* pc offset at start of statement */
        T_LONG line_number;    /* raw line number in BASIC source code file */
        int erl;               /* previous label in BASIC source code file */
        int statement_number;  /* statement number within the source line */
};

enum fmt_type {
        FMT_NONE,
        FMT_STRING,
        FMT_NUMBER
};

struct using_fmt {
        enum fmt_type type;  /* FMT_NONE, FMT_STRING, FMT_NUMBER */
        int exponential;     /* TRUE = use exponential notation */
        int width;           /* width of main section */
        int precision;       /* width after decimal point */
        int commas;          /* use commas every three steps */
        int sign;            /* prefix sign to number */
        int money;           /* prefix money sign to number */
        int fill;            /* ASCII value for fill character, normally ' ' */
        int minus;           /* postfix minus sign to number */
};

/****************************************************************************/

extern T_STRING alloc_string (size_t length, char *body);
extern T_STRING free_string (T_STRING str);
extern void compile_error (char *msg, ...);
extern char *get_token (char *line, enum token_type *token,
                        struct token_info_type *token_info);
#ifndef NOALIGN
extern char *malloc_align (size_t size, size_t align);
extern void *free_align (char *p);
#else
#define malloc_align(size,align) malloc(size)
#define free_align(p) (free(p), NULL)
#endif

/****************************************************************************/

/* platform dependent routines */

extern void init_tty (void);

extern int tty_getc (FILE *);

extern size_t tty_fread (void *, size_t, size_t, FILE *);

extern size_t tty_getline (struct string_descr *, char **, size_t *, FILE *,
                           BOOL *);

extern int change_dir (struct string_descr *str);

extern int make_dir (struct string_descr *str);

extern int remove_dir (struct string_descr *str);

extern int inkey (struct string_descr *str);

extern double get_timer (void);

extern BOOL shell_cmd (struct string_descr *str);

extern void sleep_delay (double seconds);

/****************************************************************************/
