#ifdef THREADED
#include <limits.h>
#endif
#include "mybasic.h"
#include "runtime.h"
#include "my_mem.h"

/****************************************************************************/
static BOOL reread_tmpfile (struct file *f, char **base, size_t *size);
static void runtime_error (const int err, unsigned char *pc,
                           const char *msg, ...);
static void print_runtime_error (FILE *f, const int err,
                                 struct xref_record *xref_record);
static void string_mid_assign (unsigned char *pc, T_STRING *a, T_LONG b,
                               T_LONG c, T_STRING *d);
static T_STRING alloc_string_check (unsigned char *pc, size_t length,
                                    char *body);
static void check_open_for_input (unsigned char *pc, struct file_descr *f);
static void check_open_for_output (unsigned char *pc, struct file_descr *f);
static T_LONG input_number (char *s, T_LONG printpos, double *d);
static void single_to_string (char *s, double d);
static void double_to_string (char *s, double d);
static void strip_trailing_zeroes (char *s);
static struct using_fmt *parse_fmt (unsigned char *pc, char *using_fmt,
                                    int *using_fmt_pos,
                                    struct file_descr *file);
static void print_using_number (unsigned char *pc, double number,
                                struct using_fmt *fmt,
                                struct file_descr *primary_file);

/****************************************************************************/
/* number of variables of each type */
extern size_t vars_size[NUMBER_OF_TYPES];

/****************************************************************************/
/****************************************************************************/
/*   R U N   F I L E   O F   B C O D E S   */

char *run_tmpfile (struct b *b)
{
  size_t bsize, dsize;
  unsigned char *bcodes, *bdata;
  char *chain_fname = NULL;

  bcodes = NULL;
  bdata = NULL;
  if (reread_tmpfile (&b->b, (char **)&bcodes, &bsize) &&
      reread_tmpfile (&b->data, (char **)&bdata, &dsize)) {
    mrewind (b->xref);
    /* RUN BCODE INTERPRETER HERE */
    chain_fname = run_bcodes (bcodes, bdata, b->xref, b->f, b->argc - 1,
                              &b->argv[1]);
  }
  /* debug ((stderr, "bcodes = %p\n", bcodes)); */
  bcodes = free_align ((char *)bcodes);
  /* debug ((stderr, "bdata  = %p\n", bdata)); */
  bdata = free_align ((char *)bdata);
  return chain_fname;
} /* run_tmpfile */

/****************************************************************************/

static BOOL reread_tmpfile (struct file *f, char **base, size_t *size)
{
  mseek (f->f, 0, SEEK_END);
  *size = (size_t)mtell (f->f);
  if (*size == 0)
    return TRUE;
  if ((*base = (char *)malloc_align (*size, ALIGN_DOUBLE)) == NULL) {
    fprintf (stderr, "Out of memory!\n");
    return FALSE;
  }
/*
  debug ((stderr, "bcodes_base = %p\n", bcodes_base));
  bcodes = bcodes_base;
  ALIGN_UP(bcodes, SIZE_DOUBLE);
*/
  mrewind (f->f);
  if (mread (*base, 1, *size, f->f) != *size) {
    fprintf (stderr, "Error reading tmpfile!\n");
    f->f = mclose (f->f);
    return FALSE;
  }

#ifdef DEBUG
  {
    size_t i;
    debug ((stderr, "Size = %zu\n", *size));
    for (i = 0; i < *size; i++)
      debug ((stderr, "%02x", (unsigned char)((*base)[i])));
    debug ((stderr, "\n"));
  }
#endif

  f->f = mclose (f->f);
  return TRUE;
} /* reread_tmpfile */

/****************************************************************************/
/*   R U N - T I M E   E R R O R   H A N D L E R   */

static jmp_buf runtime_error_jmp;
static char runtime_error_msg[132] = "\n";
static unsigned char *static_pc;

static void runtime_error (const int err, unsigned char *pc,
                           const char *msg, ...)
{
  va_list arglist;
  va_start (arglist, msg);
  vsprintf (runtime_error_msg, msg, arglist);
  va_end (arglist);
  static_pc = pc;
  longjmp (runtime_error_jmp, err);
} /* runtime_error */

/****************************************************************************/

static void print_runtime_error (FILE *f, const int err,
                                 struct xref_record *xref_record)
{
  static char line[MAX_LINE_LENGTH];/* static because too big for small stack */
  T_LONG line_number = 0;
  fprintf (stderr, "mybasic: Runtime error %d: %s\n", err, runtime_error_msg);
  rewind (f);  /* Scan original BASIC source file for line_number */
  do {
    line_number++;
    if (fgets (line, MAX_LINE_LENGTH, f) == NULL) {
      sprintf (line, "Error finding line %ld in source\n",
               xref_record->line_number);
      break;
    }
  } while (line_number < xref_record->line_number);
  fprintf (stderr, "mybasic: In line %ld, statement %d: %s",
           xref_record->line_number, xref_record->statement_number,
           line);
} /* print_runtime_error */

/****************************************************************************/
/*   R U N - T I M E   B C O D E   I N T E R P R E T E R   */

#ifdef NOALIGN
#define ALIGN_UP(pc,a) (pc = pc)
#else
#define ALIGN_UP(pc,a) (pc = (unsigned char *) \
                        (((T_SIZE_T)((pc) + ((a) - 1))) & ~((a) - 1)))
#endif

char *run_bcodes (unsigned char *bcodes, unsigned char *bdata,
                  MFILE *xref, FILE *f, int argc, char *argv[])
{
  static unsigned char *on_error_pc; /* static for setjmp()/longjmp() */
                    /* also, empirically, code runs faster if pc is static */
  static unsigned char *restart_input_pc, *dc, *saved_on_error_pc,
    *resume_pc, *resume_next_pc;
  register unsigned char *pc;
  register BCODE bcode;
  register T_INT *int_vars, *int_sp;
  register T_LONG *long_vars, *long_sp;
  register T_SINGLE *single_vars, *single_sp;
  register T_DOUBLE *double_vars, *double_sp;
  register T_STRING *string_vars, *string_sp;
  register T_SIZE_T *gosub_sp, *index_sp;
#ifdef THREADED
#define STATIC_IF_THREADED static
#else
#define STATIC_IF_THREADED
#endif
  /* The following local variables shouldn't really be static, but */
  /* icx -DTHREADED generates incredibly slow code if they are not static */
  STATIC_IF_THREADED int err = 0, erl = 0;
  STATIC_IF_THREADED T_INT *int_stack;
  STATIC_IF_THREADED T_LONG *long_stack, printpos;
  STATIC_IF_THREADED T_SINGLE *single_stack;
  STATIC_IF_THREADED T_DOUBLE *double_stack;
  STATIC_IF_THREADED T_STRING *string_stack;
  STATIC_IF_THREADED T_SIZE_T *gosub_stack, *index_stack;
  STATIC_IF_THREADED T_STRING input_prompt_string = NULL;
  STATIC_IF_THREADED char *input_string;
  STATIC_IF_THREADED T_SIZE_T input_string_length;
  STATIC_IF_THREADED T_SIZE_T input_string_allocated_length;
  STATIC_IF_THREADED struct file_descr **file_vars, *primary_file,
    *secondary_file, input_file, output_file;
  STATIC_IF_THREADED char *tmp_string, *chain_fname, *using_fmt;
  STATIC_IF_THREADED int using_fmt_pos;
  STATIC_IF_THREADED struct using_fmt *fmt;
  STATIC_IF_THREADED struct xref_record xref_record;

#ifdef THREADED

#define L_BASE L_DOUBLE_SIN /* L_BASE is an arbitrary label (near the middle) */
  static const int bcode_labels[] = {
    &&L_NOP - &&L_BASE,      /* "&&" is a gcc extension - "labels as values" */
    &&L_ILLEGAL - &&L_BASE,
    &&L_STATEMENT_CHECK - &&L_BASE,
    &&L_CLEAR - &&L_BASE,
    &&L_PUSH_CONST_INT - &&L_BASE,
    &&L_PUSH_CONST_INT_16 - &&L_BASE,
    &&L_PUSH_CONST_LONG - &&L_BASE,
    &&L_PUSH_CONST_LONG_16 - &&L_BASE,
    &&L_PUSH_CONST_SINGLE - &&L_BASE,
    &&L_PUSH_CONST_SINGLE_16 - &&L_BASE,
    &&L_PUSH_CONST_DOUBLE - &&L_BASE,
    &&L_PUSH_CONST_DOUBLE_16 - &&L_BASE,
    &&L_PUSH_CONST_STRING - &&L_BASE,
    &&L_PUSH_CONST_INDEX - &&L_BASE,
    &&L_PUSH_CONST_INDEX_16 - &&L_BASE,
    &&L_PUSH_CONST_INT_ZERO - &&L_BASE,
    &&L_PUSH_CONST_LONG_ZERO - &&L_BASE,
    &&L_PUSH_CONST_SINGLE_ZERO - &&L_BASE,
    &&L_PUSH_CONST_DOUBLE_ZERO - &&L_BASE,
    &&L_PUSH_CONST_INT_ONE - &&L_BASE,
    &&L_PUSH_CONST_LONG_ONE - &&L_BASE,
    &&L_PUSH_CONST_SINGLE_ONE - &&L_BASE,
    &&L_PUSH_CONST_DOUBLE_ONE - &&L_BASE,
    &&L_SET_PRIMARY_FILE - &&L_BASE,
    &&L_SET_PRIMARY_FILE_16 - &&L_BASE,
    &&L_SET_PRIMARY_INPUT_FILE - &&L_BASE,
    &&L_SET_PRIMARY_OUTPUT_FILE - &&L_BASE,
    &&L_SET_SECONDARY_FILE - &&L_BASE,
    &&L_SET_SECONDARY_FILE_16 - &&L_BASE,
    &&L_SET_SECONDARY_INPUT_FILE - &&L_BASE,
    &&L_SET_SECONDARY_OUTPUT_FILE - &&L_BASE,
    &&L_PUSH_VAR_INT - &&L_BASE,
    &&L_PUSH_VAR_LONG - &&L_BASE,
    &&L_PUSH_VAR_SINGLE - &&L_BASE,
    &&L_PUSH_VAR_DOUBLE - &&L_BASE,
    &&L_PUSH_VAR_STRING - &&L_BASE,
    &&L_PUSH_VAR_INT_16 - &&L_BASE,
    &&L_PUSH_VAR_LONG_16 - &&L_BASE,
    &&L_PUSH_VAR_SINGLE_16 - &&L_BASE,
    &&L_PUSH_VAR_DOUBLE_16 - &&L_BASE,
    &&L_PUSH_VAR_STRING_16 - &&L_BASE,
    &&L_POP_INT - &&L_BASE,
    &&L_POP_LONG - &&L_BASE,
    &&L_POP_SINGLE - &&L_BASE,
    &&L_POP_DOUBLE - &&L_BASE,
    &&L_POP_STRING - &&L_BASE,
    &&L_POP_STRING_MID2 - &&L_BASE,
    &&L_POP_STRING_MID3 - &&L_BASE,
    &&L_POP_INT_16 - &&L_BASE,
    &&L_POP_LONG_16 - &&L_BASE,
    &&L_POP_SINGLE_16 - &&L_BASE,
    &&L_POP_DOUBLE_16 - &&L_BASE,
    &&L_POP_STRING_16 - &&L_BASE,
    &&L_POP_STRING_MID2_16 - &&L_BASE,
    &&L_POP_STRING_MID3_16 - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_INT - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_LONG - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_SINGLE - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_DOUBLE - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_STRING - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_INT_16 - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_LONG_16 - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_SINGLE_16 - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_DOUBLE_16 - &&L_BASE,
    &&L_PUSH_INDIRECT_VAR_STRING_16 - &&L_BASE,
    &&L_POP_INDIRECT_INT - &&L_BASE,
    &&L_POP_INDIRECT_LONG - &&L_BASE,
    &&L_POP_INDIRECT_SINGLE - &&L_BASE,
    &&L_POP_INDIRECT_DOUBLE - &&L_BASE,
    &&L_POP_INDIRECT_STRING - &&L_BASE,
    &&L_POP_INDIRECT_STRING_MID2 - &&L_BASE,
    &&L_POP_INDIRECT_STRING_MID3 - &&L_BASE,
    &&L_POP_INDIRECT_INT_16 - &&L_BASE,
    &&L_POP_INDIRECT_LONG_16 - &&L_BASE,
    &&L_POP_INDIRECT_SINGLE_16 - &&L_BASE,
    &&L_POP_INDIRECT_DOUBLE_16 - &&L_BASE,
    &&L_POP_INDIRECT_STRING_16 - &&L_BASE,
    &&L_POP_INDIRECT_STRING_MID2_16 - &&L_BASE,
    &&L_POP_INDIRECT_STRING_MID3_16 - &&L_BASE,
    &&L_ZERO_VAR_INT - &&L_BASE,
    &&L_ZERO_VAR_LONG - &&L_BASE,
    &&L_ZERO_VAR_SINGLE - &&L_BASE,
    &&L_ZERO_VAR_DOUBLE - &&L_BASE,
    &&L_ZERO_VAR_STRING - &&L_BASE,
    &&L_ZERO_VAR_INT_16 - &&L_BASE,
    &&L_ZERO_VAR_LONG_16 - &&L_BASE,
    &&L_ZERO_VAR_SINGLE_16 - &&L_BASE,
    &&L_ZERO_VAR_DOUBLE_16 - &&L_BASE,
    &&L_ZERO_VAR_STRING_16 - &&L_BASE,
    &&L_ONE_VAR_INT - &&L_BASE,
    &&L_ONE_VAR_LONG - &&L_BASE,
    &&L_ONE_VAR_SINGLE - &&L_BASE,
    &&L_ONE_VAR_DOUBLE - &&L_BASE,
    &&L_ONE_VAR_STRING - &&L_BASE,
    &&L_ONE_VAR_INT_16 - &&L_BASE,
    &&L_ONE_VAR_LONG_16 - &&L_BASE,
    &&L_ONE_VAR_SINGLE_16 - &&L_BASE,
    &&L_ONE_VAR_DOUBLE_16 - &&L_BASE,
    &&L_ONE_VAR_STRING_16 - &&L_BASE,
    &&L_INT_SWAP - &&L_BASE,
    &&L_LONG_SWAP - &&L_BASE,
    &&L_SINGLE_SWAP - &&L_BASE,
    &&L_DOUBLE_SWAP - &&L_BASE,
    &&L_STRING_SWAP - &&L_BASE,
    &&L_INDEX_DUP - &&L_BASE,
    &&L_INDEX_DUP2 - &&L_BASE,
    &&L_INT_UNARY_MINUS - &&L_BASE,
    &&L_LONG_UNARY_MINUS - &&L_BASE,
    &&L_SINGLE_UNARY_MINUS - &&L_BASE,
    &&L_DOUBLE_UNARY_MINUS - &&L_BASE,
    &&L_INT_UNARY_PLUS - &&L_BASE,
    &&L_LONG_UNARY_PLUS - &&L_BASE,
    &&L_SINGLE_UNARY_PLUS - &&L_BASE,
    &&L_DOUBLE_UNARY_PLUS - &&L_BASE,
    &&L_INT_PLUS - &&L_BASE,
    &&L_INT_PLUS_CONST - &&L_BASE,
    &&L_INT_PLUS_CONST_16 - &&L_BASE,
    &&L_INT_PLUS_VAR - &&L_BASE,
    &&L_INT_PLUS_VAR_16 - &&L_BASE,
    &&L_INT_MINUS - &&L_BASE,
    &&L_INT_MINUS_CONST - &&L_BASE,
    &&L_INT_MINUS_CONST_16 - &&L_BASE,
    &&L_INT_MINUS_VAR - &&L_BASE,
    &&L_INT_MINUS_VAR_16 - &&L_BASE,
    &&L_INT_SWAP_MINUS - &&L_BASE,
    &&L_INT_SWAP_MINUS_CONST - &&L_BASE,
    &&L_INT_SWAP_MINUS_CONST_16 - &&L_BASE,
    &&L_INT_TIMES - &&L_BASE,
    &&L_INT_TIMES_CONST - &&L_BASE,
    &&L_INT_TIMES_CONST_16 - &&L_BASE,
    &&L_INT_TIMES_VAR - &&L_BASE,
    &&L_INT_TIMES_VAR_16 - &&L_BASE,
    &&L_INT_DIVIDE - &&L_BASE,
    &&L_INT_DIVIDE_CONST - &&L_BASE,
    &&L_INT_DIVIDE_CONST_16 - &&L_BASE,
    &&L_INT_SWAP_DIVIDE - &&L_BASE,
    &&L_INT_SWAP_DIVIDE_CONST - &&L_BASE,
    &&L_INT_SWAP_DIVIDE_CONST_16 - &&L_BASE,
    &&L_INT_MOD - &&L_BASE,
    &&L_INT_MOD_CONST - &&L_BASE,
    &&L_INT_MOD_CONST_16 - &&L_BASE,
    &&L_INT_SWAP_MOD - &&L_BASE,
    &&L_INT_SWAP_MOD_CONST - &&L_BASE,
    &&L_INT_SWAP_MOD_CONST_16 - &&L_BASE,
    &&L_INT_POWER - &&L_BASE,
    &&L_INT_POWER_CONST - &&L_BASE,
    &&L_INT_POWER_CONST_16 - &&L_BASE,
    &&L_INT_SWAP_POWER - &&L_BASE,
    &&L_INT_SWAP_POWER_CONST - &&L_BASE,
    &&L_INT_SWAP_POWER_CONST_16 - &&L_BASE,
    &&L_INT_AND - &&L_BASE,
    &&L_INT_AND_CONST - &&L_BASE,
    &&L_INT_AND_CONST_16 - &&L_BASE,
    &&L_INT_OR - &&L_BASE,
    &&L_INT_OR_CONST - &&L_BASE,
    &&L_INT_OR_CONST_16 - &&L_BASE,
    &&L_INT_XOR - &&L_BASE,
    &&L_INT_XOR_CONST - &&L_BASE,
    &&L_INT_XOR_CONST_16 - &&L_BASE,
    &&L_INT_NOT - &&L_BASE,
    &&L_INT_ABS - &&L_BASE,
    &&L_INT_CLNG - &&L_BASE,
    &&L_INT_CSNG - &&L_BASE,
    &&L_INT_CDBL - &&L_BASE,
    &&L_INT_SGN - &&L_BASE,
    &&L_INT_EQUAL - &&L_BASE,
    &&L_INT_EQUAL_CONST - &&L_BASE,
    &&L_INT_EQUAL_CONST_16 - &&L_BASE,
    &&L_INT_NOTEQUAL - &&L_BASE,
    &&L_INT_NOTEQUAL_CONST - &&L_BASE,
    &&L_INT_NOTEQUAL_CONST_16 - &&L_BASE,
    &&L_INT_LESS - &&L_BASE,
    &&L_INT_LESS_CONST - &&L_BASE,
    &&L_INT_LESS_CONST_16 - &&L_BASE,
    &&L_INT_GREATER - &&L_BASE,
    &&L_INT_GREATER_CONST - &&L_BASE,
    &&L_INT_GREATER_CONST_16 - &&L_BASE,
    &&L_INT_LESSOREQUAL - &&L_BASE,
    &&L_INT_LESSOREQUAL_CONST - &&L_BASE,
    &&L_INT_LESSOREQUAL_CONST_16 - &&L_BASE,
    &&L_INT_GREATEROREQUAL - &&L_BASE,
    &&L_INT_GREATEROREQUAL_CONST - &&L_BASE,
    &&L_INT_GREATEROREQUAL_CONST_16 - &&L_BASE,
    &&L_INT_INCREMENT - &&L_BASE,
    &&L_INT_INCREMENT_VAR - &&L_BASE,
    &&L_INT_INCREMENT_VAR_16 - &&L_BASE,
    &&L_INT_DECREMENT - &&L_BASE,
    &&L_INT_DECREMENT_VAR - &&L_BASE,
    &&L_INT_DECREMENT_VAR_16 - &&L_BASE,
    &&L_INT_COMMANDCOUNT - &&L_BASE,
    &&L_LONG_PLUS - &&L_BASE,
    &&L_LONG_PLUS_CONST - &&L_BASE,
    &&L_LONG_PLUS_CONST_16 - &&L_BASE,
    &&L_LONG_PLUS_VAR - &&L_BASE,
    &&L_LONG_PLUS_VAR_16 - &&L_BASE,
    &&L_LONG_MINUS - &&L_BASE,
    &&L_LONG_MINUS_CONST - &&L_BASE,
    &&L_LONG_MINUS_CONST_16 - &&L_BASE,
    &&L_LONG_MINUS_VAR - &&L_BASE,
    &&L_LONG_MINUS_VAR_16 - &&L_BASE,
    &&L_LONG_SWAP_MINUS - &&L_BASE,
    &&L_LONG_SWAP_MINUS_CONST - &&L_BASE,
    &&L_LONG_SWAP_MINUS_CONST_16 - &&L_BASE,
    &&L_LONG_TIMES - &&L_BASE,
    &&L_LONG_TIMES_CONST - &&L_BASE,
    &&L_LONG_TIMES_CONST_16 - &&L_BASE,
    &&L_LONG_TIMES_VAR - &&L_BASE,
    &&L_LONG_TIMES_VAR_16 - &&L_BASE,
    &&L_LONG_DIVIDE - &&L_BASE,
    &&L_LONG_DIVIDE_CONST - &&L_BASE,
    &&L_LONG_DIVIDE_CONST_16 - &&L_BASE,
    &&L_LONG_SWAP_DIVIDE - &&L_BASE,
    &&L_LONG_SWAP_DIVIDE_CONST - &&L_BASE,
    &&L_LONG_SWAP_DIVIDE_CONST_16 - &&L_BASE,
    &&L_LONG_MOD - &&L_BASE,
    &&L_LONG_MOD_CONST - &&L_BASE,
    &&L_LONG_MOD_CONST_16 - &&L_BASE,
    &&L_LONG_SWAP_MOD - &&L_BASE,
    &&L_LONG_SWAP_MOD_CONST - &&L_BASE,
    &&L_LONG_SWAP_MOD_CONST_16 - &&L_BASE,
    &&L_LONG_POWER - &&L_BASE,
    &&L_LONG_POWER_CONST - &&L_BASE,
    &&L_LONG_POWER_CONST_16 - &&L_BASE,
    &&L_LONG_SWAP_POWER - &&L_BASE,
    &&L_LONG_SWAP_POWER_CONST - &&L_BASE,
    &&L_LONG_SWAP_POWER_CONST_16 - &&L_BASE,
    &&L_LONG_AND - &&L_BASE,
    &&L_LONG_AND_CONST - &&L_BASE,
    &&L_LONG_AND_CONST_16 - &&L_BASE,
    &&L_LONG_OR - &&L_BASE,
    &&L_LONG_OR_CONST - &&L_BASE,
    &&L_LONG_OR_CONST_16 - &&L_BASE,
    &&L_LONG_XOR - &&L_BASE,
    &&L_LONG_XOR_CONST - &&L_BASE,
    &&L_LONG_XOR_CONST_16 - &&L_BASE,
    &&L_LONG_NOT - &&L_BASE,
    &&L_LONG_ABS - &&L_BASE,
    &&L_LONG_INT - &&L_BASE,
    &&L_LONG_CINT - &&L_BASE,
    &&L_LONG_CSNG - &&L_BASE,
    &&L_LONG_CDBL - &&L_BASE,
    &&L_LONG_FIX - &&L_BASE,
    &&L_LONG_SGN - &&L_BASE,
    &&L_LONG_EQUAL - &&L_BASE,
    &&L_LONG_EQUAL_CONST - &&L_BASE,
    &&L_LONG_EQUAL_CONST_16 - &&L_BASE,
    &&L_LONG_NOTEQUAL - &&L_BASE,
    &&L_LONG_NOTEQUAL_CONST - &&L_BASE,
    &&L_LONG_NOTEQUAL_CONST_16 - &&L_BASE,
    &&L_LONG_LESS - &&L_BASE,
    &&L_LONG_LESS_CONST - &&L_BASE,
    &&L_LONG_LESS_CONST_16 - &&L_BASE,
    &&L_LONG_GREATER - &&L_BASE,
    &&L_LONG_GREATER_CONST - &&L_BASE,
    &&L_LONG_GREATER_CONST_16 - &&L_BASE,
    &&L_LONG_LESSOREQUAL - &&L_BASE,
    &&L_LONG_LESSOREQUAL_CONST - &&L_BASE,
    &&L_LONG_LESSOREQUAL_CONST_16 - &&L_BASE,
    &&L_LONG_GREATEROREQUAL - &&L_BASE,
    &&L_LONG_GREATEROREQUAL_CONST - &&L_BASE,
    &&L_LONG_GREATEROREQUAL_CONST_16 - &&L_BASE,
    &&L_LONG_INCREMENT - &&L_BASE,
    &&L_LONG_INCREMENT_VAR - &&L_BASE,
    &&L_LONG_INCREMENT_VAR_16 - &&L_BASE,
    &&L_LONG_DECREMENT - &&L_BASE,
    &&L_LONG_DECREMENT_VAR - &&L_BASE,
    &&L_LONG_DECREMENT_VAR_16 - &&L_BASE,
    &&L_SINGLE_PLUS - &&L_BASE,
    &&L_SINGLE_PLUS_CONST - &&L_BASE,
    &&L_SINGLE_PLUS_CONST_16 - &&L_BASE,
    &&L_SINGLE_PLUS_VAR - &&L_BASE,
    &&L_SINGLE_PLUS_VAR_16 - &&L_BASE,
    &&L_SINGLE_MINUS - &&L_BASE,
    &&L_SINGLE_MINUS_CONST - &&L_BASE,
    &&L_SINGLE_MINUS_CONST_16 - &&L_BASE,
    &&L_SINGLE_MINUS_VAR - &&L_BASE,
    &&L_SINGLE_MINUS_VAR_16 - &&L_BASE,
    &&L_SINGLE_SWAP_MINUS - &&L_BASE,
    &&L_SINGLE_SWAP_MINUS_CONST - &&L_BASE,
    &&L_SINGLE_SWAP_MINUS_CONST_16 - &&L_BASE,
    &&L_SINGLE_TIMES - &&L_BASE,
    &&L_SINGLE_TIMES_CONST - &&L_BASE,
    &&L_SINGLE_TIMES_CONST_16 - &&L_BASE,
    &&L_SINGLE_TIMES_VAR - &&L_BASE,
    &&L_SINGLE_TIMES_VAR_16 - &&L_BASE,
    &&L_SINGLE_DIVIDE - &&L_BASE,
    &&L_SINGLE_DIVIDE_CONST - &&L_BASE,
    &&L_SINGLE_DIVIDE_CONST_16 - &&L_BASE,
    &&L_SINGLE_SWAP_DIVIDE - &&L_BASE,
    &&L_SINGLE_SWAP_DIVIDE_CONST - &&L_BASE,
    &&L_SINGLE_SWAP_DIVIDE_CONST_16 - &&L_BASE,
    &&L_SINGLE_POWER - &&L_BASE,
    &&L_SINGLE_POWER_CONST - &&L_BASE,
    &&L_SINGLE_POWER_CONST_16 - &&L_BASE,
    &&L_SINGLE_SWAP_POWER - &&L_BASE,
    &&L_SINGLE_SWAP_POWER_CONST - &&L_BASE,
    &&L_SINGLE_SWAP_POWER_CONST_16 - &&L_BASE,
    &&L_SINGLE_SIN - &&L_BASE,
    &&L_SINGLE_COS - &&L_BASE,
    &&L_SINGLE_TAN - &&L_BASE,
    &&L_SINGLE_ATN - &&L_BASE,
    &&L_SINGLE_LOG - &&L_BASE,
    &&L_SINGLE_LOG2 - &&L_BASE,
    &&L_SINGLE_LOG10 - &&L_BASE,
    &&L_SINGLE_EXP - &&L_BASE,
    &&L_SINGLE_SQR - &&L_BASE,
    &&L_SINGLE_ABS - &&L_BASE,
    &&L_SINGLE_RND - &&L_BASE,
    &&L_SINGLE_INT - &&L_BASE,
    &&L_SINGLE_CINT - &&L_BASE,
    &&L_SINGLE_CLNG - &&L_BASE,
    &&L_SINGLE_CDBL - &&L_BASE,
    &&L_SINGLE_FIX - &&L_BASE,
    &&L_SINGLE_SGN - &&L_BASE,
    &&L_SINGLE_EQUAL - &&L_BASE,
    &&L_SINGLE_EQUAL_CONST - &&L_BASE,
    &&L_SINGLE_EQUAL_CONST_16 - &&L_BASE,
    &&L_SINGLE_NOTEQUAL - &&L_BASE,
    &&L_SINGLE_NOTEQUAL_CONST - &&L_BASE,
    &&L_SINGLE_NOTEQUAL_CONST_16 - &&L_BASE,
    &&L_SINGLE_LESS - &&L_BASE,
    &&L_SINGLE_LESS_CONST - &&L_BASE,
    &&L_SINGLE_LESS_CONST_16 - &&L_BASE,
    &&L_SINGLE_GREATER - &&L_BASE,
    &&L_SINGLE_GREATER_CONST - &&L_BASE,
    &&L_SINGLE_GREATER_CONST_16 - &&L_BASE,
    &&L_SINGLE_LESSOREQUAL - &&L_BASE,
    &&L_SINGLE_LESSOREQUAL_CONST - &&L_BASE,
    &&L_SINGLE_LESSOREQUAL_CONST_16 - &&L_BASE,
    &&L_SINGLE_GREATEROREQUAL - &&L_BASE,
    &&L_SINGLE_GREATEROREQUAL_CONST - &&L_BASE,
    &&L_SINGLE_GREATEROREQUAL_CONST_16 - &&L_BASE,
    &&L_SINGLE_INCREMENT - &&L_BASE,
    &&L_SINGLE_INCREMENT_VAR - &&L_BASE,
    &&L_SINGLE_INCREMENT_VAR_16 - &&L_BASE,
    &&L_SINGLE_DECREMENT - &&L_BASE,
    &&L_SINGLE_DECREMENT_VAR - &&L_BASE,
    &&L_SINGLE_DECREMENT_VAR_16 - &&L_BASE,
    &&L_DOUBLE_PLUS - &&L_BASE,
    &&L_DOUBLE_PLUS_CONST - &&L_BASE,
    &&L_DOUBLE_PLUS_CONST_16 - &&L_BASE,
    &&L_DOUBLE_PLUS_VAR - &&L_BASE,
    &&L_DOUBLE_PLUS_VAR_16 - &&L_BASE,
    &&L_DOUBLE_MINUS - &&L_BASE,
    &&L_DOUBLE_MINUS_CONST - &&L_BASE,
    &&L_DOUBLE_MINUS_CONST_16 - &&L_BASE,
    &&L_DOUBLE_MINUS_VAR - &&L_BASE,
    &&L_DOUBLE_MINUS_VAR_16 - &&L_BASE,
    &&L_DOUBLE_SWAP_MINUS - &&L_BASE,
    &&L_DOUBLE_SWAP_MINUS_CONST - &&L_BASE,
    &&L_DOUBLE_SWAP_MINUS_CONST_16 - &&L_BASE,
    &&L_DOUBLE_TIMES - &&L_BASE,
    &&L_DOUBLE_TIMES_CONST - &&L_BASE,
    &&L_DOUBLE_TIMES_CONST_16 - &&L_BASE,
    &&L_DOUBLE_TIMES_VAR - &&L_BASE,
    &&L_DOUBLE_TIMES_VAR_16 - &&L_BASE,
    &&L_DOUBLE_DIVIDE - &&L_BASE,
    &&L_DOUBLE_DIVIDE_CONST - &&L_BASE,
    &&L_DOUBLE_DIVIDE_CONST_16 - &&L_BASE,
    &&L_DOUBLE_SWAP_DIVIDE - &&L_BASE,
    &&L_DOUBLE_SWAP_DIVIDE_CONST - &&L_BASE,
    &&L_DOUBLE_SWAP_DIVIDE_CONST_16 - &&L_BASE,
    &&L_DOUBLE_POWER - &&L_BASE,
    &&L_DOUBLE_POWER_CONST - &&L_BASE,
    &&L_DOUBLE_POWER_CONST_16 - &&L_BASE,
    &&L_DOUBLE_SWAP_POWER - &&L_BASE,
    &&L_DOUBLE_SWAP_POWER_CONST - &&L_BASE,
    &&L_DOUBLE_SWAP_POWER_CONST_16 - &&L_BASE,
    &&L_DOUBLE_SIN - &&L_BASE,
    &&L_DOUBLE_COS - &&L_BASE,
    &&L_DOUBLE_TAN - &&L_BASE,
    &&L_DOUBLE_ATN - &&L_BASE,
    &&L_DOUBLE_LOG - &&L_BASE,
    &&L_DOUBLE_LOG2 - &&L_BASE,
    &&L_DOUBLE_LOG10 - &&L_BASE,
    &&L_DOUBLE_EXP - &&L_BASE,
    &&L_DOUBLE_SQR - &&L_BASE,
    &&L_DOUBLE_ABS - &&L_BASE,
    &&L_DOUBLE_INT - &&L_BASE,
    &&L_DOUBLE_CINT - &&L_BASE,
    &&L_DOUBLE_CLNG - &&L_BASE,
    &&L_DOUBLE_CSNG - &&L_BASE,
    &&L_DOUBLE_FIX - &&L_BASE,
    &&L_DOUBLE_SGN - &&L_BASE,
    &&L_DOUBLE_EQUAL - &&L_BASE,
    &&L_DOUBLE_EQUAL_CONST - &&L_BASE,
    &&L_DOUBLE_EQUAL_CONST_16 - &&L_BASE,
    &&L_DOUBLE_NOTEQUAL - &&L_BASE,
    &&L_DOUBLE_NOTEQUAL_CONST - &&L_BASE,
    &&L_DOUBLE_NOTEQUAL_CONST_16 - &&L_BASE,
    &&L_DOUBLE_LESS - &&L_BASE,
    &&L_DOUBLE_LESS_CONST - &&L_BASE,
    &&L_DOUBLE_LESS_CONST_16 - &&L_BASE,
    &&L_DOUBLE_GREATER - &&L_BASE,
    &&L_DOUBLE_GREATER_CONST - &&L_BASE,
    &&L_DOUBLE_GREATER_CONST_16 - &&L_BASE,
    &&L_DOUBLE_LESSOREQUAL - &&L_BASE,
    &&L_DOUBLE_LESSOREQUAL_CONST - &&L_BASE,
    &&L_DOUBLE_LESSOREQUAL_CONST_16 - &&L_BASE,
    &&L_DOUBLE_GREATEROREQUAL - &&L_BASE,
    &&L_DOUBLE_GREATEROREQUAL_CONST - &&L_BASE,
    &&L_DOUBLE_GREATEROREQUAL_CONST_16 - &&L_BASE,
    &&L_DOUBLE_INCREMENT - &&L_BASE,
    &&L_DOUBLE_INCREMENT_VAR - &&L_BASE,
    &&L_DOUBLE_INCREMENT_VAR_16 - &&L_BASE,
    &&L_DOUBLE_DECREMENT - &&L_BASE,
    &&L_DOUBLE_DECREMENT_VAR - &&L_BASE,
    &&L_DOUBLE_DECREMENT_VAR_16 - &&L_BASE,
    &&L_STRING_PLUS - &&L_BASE,
    &&L_STRING_EQUAL - &&L_BASE,
    &&L_STRING_NOTEQUAL - &&L_BASE,
    &&L_STRING_LESS - &&L_BASE,
    &&L_STRING_GREATER - &&L_BASE,
    &&L_STRING_LESSOREQUAL - &&L_BASE,
    &&L_STRING_GREATEROREQUAL - &&L_BASE,
    &&L_STRING_LEN - &&L_BASE,
    &&L_STRING_LEFT - &&L_BASE,
    &&L_STRING_MID2 - &&L_BASE,
    &&L_STRING_MID3 - &&L_BASE,
    &&L_STRING_RIGHT - &&L_BASE,
    &&L_STRING_INSTR2 - &&L_BASE,
    &&L_STRING_INSTR3 - &&L_BASE,
    &&L_STRING_RINSTR2 - &&L_BASE,
    &&L_STRING_RINSTR3 - &&L_BASE,
    &&L_STRING_CHR - &&L_BASE,
    &&L_STRING_ASC - &&L_BASE,
    &&L_STRING_VAL - &&L_BASE,
    &&L_STRING_STR_INT - &&L_BASE,
    &&L_STRING_STR_LONG - &&L_BASE,
    &&L_STRING_STR_SINGLE - &&L_BASE,
    &&L_STRING_STR_DOUBLE - &&L_BASE,
    &&L_STRING_DATE - &&L_BASE,
    &&L_STRING_TIME - &&L_BASE,
    &&L_STRING_COMMAND - &&L_BASE,
    &&L_STRING_COMMAND1 - &&L_BASE,
    &&L_DOUBLE_TIMER - &&L_BASE,
    &&L_EOF - &&L_BASE,
    &&L_LOF - &&L_BASE,
    &&L_POS - &&L_BASE,
    &&L_STRING_MKI - &&L_BASE,
    &&L_STRING_MKL - &&L_BASE,
    &&L_STRING_MKS - &&L_BASE,
    &&L_STRING_MKD - &&L_BASE,
    &&L_INT_CVI - &&L_BASE,
    &&L_LONG_CVL - &&L_BASE,
    &&L_SINGLE_CVS - &&L_BASE,
    &&L_DOUBLE_CVD - &&L_BASE,
    &&L_STRING_BIN - &&L_BASE,
    &&L_STRING_OCT - &&L_BASE,
    &&L_STRING_HEX - &&L_BASE,
    &&L_STRING_LCASE - &&L_BASE,
    &&L_STRING_UCASE - &&L_BASE,
    &&L_STRING_TRIM - &&L_BASE,
    &&L_STRING_LTRIM - &&L_BASE,
    &&L_STRING_RTRIM - &&L_BASE,
    &&L_STRING_STRING - &&L_BASE,
    &&L_STRING_STRING2 - &&L_BASE,
    &&L_STRING_SPACE - &&L_BASE,
    &&L_STRING_GETENV - &&L_BASE,
    &&L_LONG_LBOUND - &&L_BASE,
    &&L_LONG_UBOUND - &&L_BASE,
    &&L_INDEX_PLUS - &&L_BASE,
    &&L_INDEX_TIMES_CONST - &&L_BASE,
    &&L_INDEX_TIMES_CONST_16 - &&L_BASE,
    &&L_INT_TO_LONG - &&L_BASE,
    &&L_INT_TO_SINGLE - &&L_BASE,
    &&L_INT_TO_DOUBLE - &&L_BASE,
    &&L_INT_TO_INDEX - &&L_BASE,
    &&L_INT_TO_INDEX_16 - &&L_BASE,
    &&L_INT_TO_INDEX_PLUS - &&L_BASE,
    &&L_INT_TO_INDEX_PLUS_16 - &&L_BASE,
    &&L_INT_TO_INDEX_MINUS_1 - &&L_BASE,
    &&L_INT_TO_INDEX_MINUS_1_16 - &&L_BASE,
    &&L_INT_TO_INDEX_PLUS_MINUS_1 - &&L_BASE,
    &&L_INT_TO_INDEX_PLUS_MINUS_1_16 - &&L_BASE,
    &&L_LONG_TO_INT - &&L_BASE,
    &&L_LONG_TO_SINGLE - &&L_BASE,
    &&L_LONG_TO_DOUBLE - &&L_BASE,
    &&L_LONG_TO_INDEX - &&L_BASE,
    &&L_LONG_TO_INDEX_16 - &&L_BASE,
    &&L_LONG_TO_INDEX_PLUS - &&L_BASE,
    &&L_LONG_TO_INDEX_PLUS_16 - &&L_BASE,
    &&L_LONG_TO_INDEX_MINUS_1 - &&L_BASE,
    &&L_LONG_TO_INDEX_MINUS_1_16 - &&L_BASE,
    &&L_LONG_TO_INDEX_PLUS_MINUS_1 - &&L_BASE,
    &&L_LONG_TO_INDEX_PLUS_MINUS_1_16 - &&L_BASE,
    &&L_SINGLE_TO_INT - &&L_BASE,
    &&L_SINGLE_TO_LONG - &&L_BASE,
    &&L_SINGLE_TO_DOUBLE - &&L_BASE,
    &&L_SINGLE_TO_INDEX - &&L_BASE,
    &&L_SINGLE_TO_INDEX_16 - &&L_BASE,
    &&L_SINGLE_TO_INDEX_PLUS - &&L_BASE,
    &&L_SINGLE_TO_INDEX_PLUS_16 - &&L_BASE,
    &&L_SINGLE_TO_INDEX_MINUS_1 - &&L_BASE,
    &&L_SINGLE_TO_INDEX_MINUS_1_16 - &&L_BASE,
    &&L_SINGLE_TO_INDEX_PLUS_MINUS_1 - &&L_BASE,
    &&L_SINGLE_TO_INDEX_PLUS_MINUS_1_16 - &&L_BASE,
    &&L_DOUBLE_TO_INT - &&L_BASE,
    &&L_DOUBLE_TO_LONG - &&L_BASE,
    &&L_DOUBLE_TO_SINGLE - &&L_BASE,
    &&L_DOUBLE_TO_INDEX - &&L_BASE,
    &&L_DOUBLE_TO_INDEX_16 - &&L_BASE,
    &&L_DOUBLE_TO_INDEX_PLUS - &&L_BASE,
    &&L_DOUBLE_TO_INDEX_PLUS_16 - &&L_BASE,
    &&L_DOUBLE_TO_INDEX_MINUS_1 - &&L_BASE,
    &&L_DOUBLE_TO_INDEX_MINUS_1_16 - &&L_BASE,
    &&L_DOUBLE_TO_INDEX_PLUS_MINUS_1 - &&L_BASE,
    &&L_DOUBLE_TO_INDEX_PLUS_MINUS_1_16 - &&L_BASE,
    &&L_JUMP - &&L_BASE,
    &&L_JUMP_16 - &&L_BASE,
    &&L_JUMP_INT_EQUAL_ZERO - &&L_BASE,
    &&L_JUMP_INT_EQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_INT_NOTEQUAL_ZERO - &&L_BASE,
    &&L_JUMP_INT_NOTEQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_LONG_EQUAL_ZERO - &&L_BASE,
    &&L_JUMP_LONG_EQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_LONG_NOTEQUAL_ZERO - &&L_BASE,
    &&L_JUMP_LONG_NOTEQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_SINGLE_EQUAL_ZERO - &&L_BASE,
    &&L_JUMP_SINGLE_EQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_SINGLE_NOTEQUAL_ZERO - &&L_BASE,
    &&L_JUMP_SINGLE_NOTEQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_DOUBLE_EQUAL_ZERO - &&L_BASE,
    &&L_JUMP_DOUBLE_EQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_DOUBLE_NOTEQUAL_ZERO - &&L_BASE,
    &&L_JUMP_DOUBLE_NOTEQUAL_ZERO_16 - &&L_BASE,
    &&L_JUMP_INT_LESS - &&L_BASE,
    &&L_JUMP_LONG_LESS - &&L_BASE,
    &&L_JUMP_SINGLE_LESS - &&L_BASE,
    &&L_JUMP_DOUBLE_LESS - &&L_BASE,
    &&L_JUMP_INT_GREATER - &&L_BASE,
    &&L_JUMP_LONG_GREATER - &&L_BASE,
    &&L_JUMP_SINGLE_GREATER - &&L_BASE,
    &&L_JUMP_DOUBLE_GREATER - &&L_BASE,
    &&L_GOSUB - &&L_BASE,
    &&L_ON_GOTO - &&L_BASE,
    &&L_ON_GOSUB - &&L_BASE,
    &&L_ON_ERROR_GOTO - &&L_BASE,
    &&L_ON_ERROR_GOTO_0 - &&L_BASE,
    &&L_RESUME - &&L_BASE,
    &&L_RESUME_LABEL - &&L_BASE,
    &&L_RESUME_NEXT - &&L_BASE,
    &&L_ERR - &&L_BASE,
    &&L_ERL - &&L_BASE,
    &&L_ERROR - &&L_BASE,
    &&L_RETURN - &&L_BASE,
    &&L_RETURN_LINE - &&L_BASE,
    &&L_RANDOMIZE_TIMER - &&L_BASE,
    &&L_RANDOMIZE_LONG - &&L_BASE,
    &&L_OPEN - &&L_BASE,
    &&L_CLOSE - &&L_BASE,
    &&L_KILL - &&L_BASE,
    &&L_NAME - &&L_BASE,
    &&L_CHDIR - &&L_BASE,
    &&L_MKDIR - &&L_BASE,
    &&L_RMDIR - &&L_BASE,
    &&L_SHELL - &&L_BASE,
    &&L_CHAIN - &&L_BASE,
    &&L_PRINT_INT - &&L_BASE,
    &&L_PRINT_LONG - &&L_BASE,
    &&L_PRINT_SINGLE - &&L_BASE,
    &&L_PRINT_DOUBLE - &&L_BASE,
    &&L_PRINT_STRING - &&L_BASE,
    &&L_PRINT_COMMA - &&L_BASE,
    &&L_PRINT_TAB - &&L_BASE,
    &&L_PRINT_SPC - &&L_BASE,
    &&L_PRINT_NEWLINE - &&L_BASE,
    &&L_PRINT_END - &&L_BASE,
    &&L_PRINT_USING_INIT - &&L_BASE,
    &&L_PRINT_USING_INT - &&L_BASE,
    &&L_PRINT_USING_LONG - &&L_BASE,
    &&L_PRINT_USING_SINGLE - &&L_BASE,
    &&L_PRINT_USING_DOUBLE - &&L_BASE,
    &&L_PRINT_USING_STRING - &&L_BASE,
    &&L_PRINT_USING_END - &&L_BASE,
    &&L_BEEP - &&L_BASE,
    &&L_SLEEP - &&L_BASE,
    &&L_CLS - &&L_BASE,
    &&L_LOCATE - &&L_BASE,
    &&L_COLOR - &&L_BASE,
    &&L_BACKGROUND_COLOR - &&L_BASE,
    &&L_LINEINPUT - &&L_BASE,
    &&L_INPUT_RESTART - &&L_BASE,
    &&L_INPUT_CLEAR_PROMPT_STRING - &&L_BASE,
    &&L_INPUT_SET_PROMPT_STRING - &&L_BASE,
    &&L_INPUT_EXTEND_PROMPT_STRING - &&L_BASE,
    &&L_INPUT_START - &&L_BASE,
    &&L_INPUT_INT - &&L_BASE,
    &&L_INPUT_LONG - &&L_BASE,
    &&L_INPUT_SINGLE - &&L_BASE,
    &&L_INPUT_DOUBLE - &&L_BASE,
    &&L_INPUT_STRING - &&L_BASE,
    &&L_INPUT_DOLLAR - &&L_BASE,
    &&L_INKEY_DOLLAR - &&L_BASE,
    &&L_READ_INT - &&L_BASE,
    &&L_READ_LONG - &&L_BASE,
    &&L_READ_SINGLE - &&L_BASE,
    &&L_READ_DOUBLE - &&L_BASE,
    &&L_READ_STRING - &&L_BASE,
    &&L_RESTORE - &&L_BASE,
    &&L_RESTORE_LINE - &&L_BASE,
    &&L_STOP - &&L_BASE,
    &&L_END - &&L_BASE
  };

  /* If called with (bcodes == NULL), just return bcode_labels */
  if (bcodes == NULL) {
    if (bcode_labels[B_END] != &&L_END - &&L_BASE)
      compile_error ("Internal error: bcode_labels mismatch");
    return (char *)bcode_labels;
  }

#endif /* THREADED */

#if (MAX_LINE_LENGTH > MAX_FNAME_LENGTH)
#define TMP_STRING_LENGTH MAX_LINE_LENGTH
#else
#define TMP_STRING_LENGTH MAX_FNAME_LENGTH
#endif
  if ((tmp_string = my_malloc (TMP_STRING_LENGTH)) == NULL)
    compile_error ("Out of memory allocating %d bytes!", TMP_STRING_LENGTH);

  int_vars = NULL;
  long_vars = NULL;
  single_vars = NULL;
  double_vars = NULL;
  string_vars = NULL;
  file_vars = NULL;
  int_stack = NULL;
  long_stack = NULL;
  single_stack = NULL;
  double_stack = NULL;
  string_stack = NULL;
  gosub_stack = NULL;
  index_stack = NULL;
  using_fmt = NULL;
  using_fmt_pos = 0;
  chain_fname = NULL;

  if ((vars_size[TYPE_INT] != 0 &&
       (int_vars = (T_INT *)malloc_align (vars_size[TYPE_INT] * SIZE_INT, ALIGN_INT)) == NULL) ||
      (vars_size[TYPE_LONG] != 0 &&
       (long_vars = (T_LONG *)malloc_align (vars_size[TYPE_LONG] * SIZE_LONG, ALIGN_LONG)) == NULL) ||
      (vars_size[TYPE_SINGLE] != 0 &&
       (single_vars = (T_SINGLE *)malloc_align (vars_size[TYPE_SINGLE] * SIZE_SINGLE, ALIGN_SINGLE)) ==
         NULL) ||
      (vars_size[TYPE_DOUBLE] != 0 &&
       (double_vars = (T_DOUBLE *)malloc_align (vars_size[TYPE_DOUBLE] * SIZE_DOUBLE, ALIGN_DOUBLE)) ==
         NULL) ||
      (vars_size[TYPE_STRING] != 0 &&
       (string_vars = (T_STRING *)malloc_align (vars_size[TYPE_STRING] * SIZE_STRING, ALIGN_STRING)) ==
         NULL) ||
      (vars_size[TYPE_FILE] != 0 &&
       (file_vars = (T_FILENUM *)malloc_align (vars_size[TYPE_FILE] * SIZE_FILE, ALIGN_FILE)) ==
         NULL)) {
    fprintf (stderr, "Out of memory for data!\n");
    goto done;
  }

  if (vars_size[TYPE_INT] != 0)
    memset (int_vars, 0, vars_size[TYPE_INT] * SIZE_INT);
  if (vars_size[TYPE_LONG] != 0)
    memset (long_vars, 0, vars_size[TYPE_LONG] * SIZE_LONG);
  if (vars_size[TYPE_SINGLE] != 0)
    memset (single_vars, 0, vars_size[TYPE_SINGLE] * SIZE_SINGLE);
  if (vars_size[TYPE_DOUBLE] != 0)
    memset (double_vars, 0, vars_size[TYPE_DOUBLE] * SIZE_DOUBLE);
  if (vars_size[TYPE_STRING] != 0)
    memset (string_vars, 0, vars_size[TYPE_STRING] * SIZE_STRING);
  if (vars_size[TYPE_FILE] != 0)
    memset (file_vars, 0, vars_size[TYPE_FILE] * SIZE_FILE);

  debug ((stderr, "Done allocating variables\n"));

  {
    T_SIZE_T t;
    for (t = 0; t < vars_size[TYPE_STRING]; t++)
      if ((string_vars[t] = alloc_string (0, NULL)) == NULL) {
        fprintf (stderr, "Out of memory for strings!\n");
        goto done;
      }
  }

  debug ((stderr, "Done allocating strings\n"));

  {
    T_SIZE_T t;
    for (t = 0; t < vars_size[TYPE_FILE]; t++)
      if ((file_vars[t] = (struct file_descr *)my_malloc
           (sizeof(struct file_descr))) == NULL) {
        fprintf (stderr, "Out of memory for files!\n");
        goto done;
      } else {
        file_vars[t]->file = NULL;
        file_vars[t]->file_state = FILE_CLOSED;
        file_vars[t]->printpos = 0;
      }
  }

  debug ((stderr, "Done allocating files\n"));

  if ((int_stack = (T_INT *)malloc_align (STACK_SIZE * SIZE_INT, ALIGN_INT)) == NULL ||
      (long_stack = (T_LONG *)malloc_align (STACK_SIZE * SIZE_LONG, ALIGN_LONG))
      == NULL ||
      (single_stack = (T_SINGLE *)malloc_align (STACK_SIZE * SIZE_SINGLE, ALIGN_SINGLE))
      == NULL ||
      (double_stack = (T_DOUBLE *)malloc_align (STACK_SIZE * SIZE_DOUBLE, ALIGN_DOUBLE))
      == NULL ||
      (string_stack = (T_STRING *)malloc_align (STACK_SIZE * SIZE_STRING, ALIGN_STRING))
      == NULL ||
      (gosub_stack = (T_SIZE_T *)malloc_align (STACK_SIZE * SIZE_SIZE_T, ALIGN_SIZE_T))
      == NULL ||
      (index_stack = (T_SIZE_T *)malloc_align (STACK_SIZE * SIZE_SIZE_T, ALIGN_SIZE_T))
      == NULL) {
    fprintf (stderr, "Out of memory for BASIC stacks!\n");
    goto done;
  }

  memset (string_stack, 0, STACK_SIZE * SIZE_STRING);
  string_sp = string_stack;

  input_file.file = stdin;
  input_file.file_state = FILE_OPEN_INPUT;
  input_file.printpos = 0;
  output_file.file = stdout;
  output_file.file_state = FILE_OPEN_OUTPUT;
  output_file.printpos = 0;
  primary_file = NULL;
  secondary_file = NULL;

  input_string = NULL;
  input_string_length = 0;
  input_string_allocated_length = 0;

  pc = bcodes;
  static_pc = pc;
  resume_pc = NULL;
  resume_next_pc = NULL;
  on_error_pc = NULL;
  saved_on_error_pc = NULL;
  dc = bdata;
  gosub_sp = gosub_stack;

  input_prompt_string = alloc_string_check (pc, 0, "");

  /*   R U N - T I M E   E R R O R   R E S T A R T   P O I N T   */

  err = setjmp (runtime_error_jmp);
  if (err != 0) { /* If a run-time error has occurred */
    /* Scan xref file to find from static_pc: line number, statement number,
       erl, pc of start of this statement and pc of start of next statement */
    T_SIZE_T last_offset = 0;
    xref_record.offset = 0;
    mrewind (xref);
    pc = static_pc;
    do {
      last_offset = xref_record.offset;
      mread ((char *)&xref_record, sizeof(xref_record), 1, xref);
    } while (xref_record.offset < (pc - bcodes));
    if (on_error_pc != NULL) {   /* If an ON ERROR GOTO handler is installed */
      pc = on_error_pc;
      saved_on_error_pc = on_error_pc;
      on_error_pc = NULL;
      resume_pc = bcodes + last_offset;
      resume_next_pc = bcodes + xref_record.offset;
      erl = xref_record.erl;
    } else {                    /* If there is no ON ERROR GOTO error handler */
      print_runtime_error (f, err, &xref_record);
      goto done;
    }
  }

  /* Reset expression calculator stacks after any run-time error */
  restart_input_pc = pc;
  int_sp = int_stack;
  long_sp = long_stack;
  single_sp = single_stack;
  double_sp = double_stack;
  while (string_sp > string_stack) {
    --string_sp;
    *string_sp = free_string (*string_sp);
  }
  index_sp = index_stack;


#ifdef THREADED  /* --- no loop --- each bcode ends with goto *pc++ */

#define BCODE_START(bcode)                                                  \
    L_##bcode

#define BCODE_END /* goto *(&&L_BASE + *pc++);  */                          \
      {                                                                     \
        void *bcode_label = &&L_BASE +                                      \
          (int)*(threaded_bcode_type *)(ALIGN_UP(pc,ALIGN_THREADED_BCODE)); \
        pc += SIZE_THREADED_BCODE;                                          \
        goto *bcode_label;                                                  \
      }

#else  /* not THREADED --- loop with switch(bcode = *pc++) */

#define BCODE_START(bcode)                                                  \
    case B_##bcode

#define BCODE_END                                                           \
      break

#endif /* (not) THREADED */


  /*   M A I N   L O O P   O F   B C O D E   I N T E R P R E T E R   */

#ifdef THREADED  /* no loop --- each bcode ends with goto *pc++ */

  BCODE_END;       /* goto *pc++;    indirect jump to first bcode */

#else  /* not THREADED --- for (;;) { bcode = *pc++; switch(bcode) ... } */

  for (;;) {    /* main loop of bcode interpreter */
    bcode = (BCODE)*(unsigned short int *)(ALIGN_UP(pc,ALIGN_BCODE));
    pc += SIZE_BCODE;
    switch (bcode) {   /* main switch of bcode interpreter */

#endif /* (not) THREADED */

    BCODE_START(NOP):
    BCODE_END;

    BCODE_START(ILLEGAL):
      runtime_error (51, pc, "Invalid or not implemented bcode encountered");

    BCODE_START(STATEMENT_CHECK):
#ifdef DEBUG
      if (int_sp != int_stack)
        runtime_error (51, pc, "Unbalanced INT stack!");
      if (long_sp != long_stack)
        runtime_error (51, pc, "Unbalanced LONG stack!");
      if (single_sp != single_stack)
        runtime_error (51, pc, "Unbalanced SINGLE stack!");
      if (double_sp != double_stack)
        runtime_error (51, pc, "Unbalanced DOUBLE stack!");
      if (string_sp != string_stack)
        runtime_error (51, pc, "Unbalanced STRING stack!");
      if (index_sp != index_stack)
        runtime_error (51, pc, "Unbalanced INDEX stack!");
/*
      if (string_vars != NULL) {
        for (t = 0; t < vars_size[TYPE_STRING]; t++)
          if (string_vars[t] == NULL)
            debug ((stderr, "Ref count[%d] = NULL\n"));
          else
            debug ((stderr, "Ref count[%d] = %d\n", t,
                    string_vars[t]->ref_count));
      }
*/
#endif
    BCODE_END;

    BCODE_START(CLEAR):
      if (vars_size[TYPE_INT] != 0)
        memset (int_vars, 0, vars_size[TYPE_INT] * SIZE_INT);
      if (vars_size[TYPE_LONG] != 0)
        memset (long_vars, 0, vars_size[TYPE_LONG] * SIZE_LONG);
      if (vars_size[TYPE_SINGLE] != 0)
        memset (single_vars, 0, vars_size[TYPE_SINGLE] * SIZE_SINGLE);
      if (vars_size[TYPE_DOUBLE] != 0)
        memset (double_vars, 0, vars_size[TYPE_DOUBLE] * SIZE_DOUBLE);
      {
        T_SIZE_T t;
        for (t = 0; t < vars_size[TYPE_STRING]; t++) {
          string_vars[t] = free_string (string_vars[t]);
          string_vars[t] = alloc_string_check (pc, 0, NULL);
        }
        for (t = 0; t < vars_size[TYPE_FILE]; t++)
          if (file_vars[t] != NULL && file_vars[t]->file != NULL) {
            fclose (file_vars[t]->file);
            file_vars[t]->file = NULL;
            file_vars[t]->file_state = FILE_CLOSED;
          }
      }
    BCODE_END;

    BCODE_START(PUSH_CONST_INT):
      *int_sp++ = *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(PUSH_CONST_INT_16):
      *int_sp++ = *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_CONST_LONG):
      *long_sp++ = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(PUSH_CONST_LONG_16):
      *long_sp++ = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_CONST_SINGLE):
      *single_sp++ = *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(PUSH_CONST_SINGLE_16):
      *single_sp++ = (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_CONST_DOUBLE):
      *double_sp++ = *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(PUSH_CONST_DOUBLE_16):
      *double_sp++ = (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_CONST_STRING):
      *string_sp = *((T_STRING *)ALIGN_UP(pc,ALIGN_STRING));
      (*string_sp++)->ref_count++;
      pc += SIZE_STRING;
    BCODE_END;

    BCODE_START(PUSH_CONST_INDEX):
      *index_sp++ = *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T));
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_CONST_INDEX_16):
      *index_sp++ = *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_CONST_INT_ZERO):
      *int_sp++ = 0;
    BCODE_END;

    BCODE_START(PUSH_CONST_LONG_ZERO):
      *long_sp++ = 0;
    BCODE_END;

    BCODE_START(PUSH_CONST_SINGLE_ZERO):
      *single_sp++ = (T_SINGLE)0.0;
    BCODE_END;

    BCODE_START(PUSH_CONST_DOUBLE_ZERO):
      *double_sp++ = 0.0;
    BCODE_END;

    BCODE_START(PUSH_CONST_INT_ONE):
      *int_sp++ = 1;
    BCODE_END;

    BCODE_START(PUSH_CONST_LONG_ONE):
      *long_sp++ = 1;
    BCODE_END;

    BCODE_START(PUSH_CONST_SINGLE_ONE):
      *single_sp++ = (T_SINGLE)1.0;
    BCODE_END;

    BCODE_START(PUSH_CONST_DOUBLE_ONE):
      *double_sp++ = 1.0;
    BCODE_END;

    BCODE_START(SET_PRIMARY_FILE):
      primary_file = file_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(SET_PRIMARY_FILE_16):
      primary_file = file_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SET_PRIMARY_INPUT_FILE):
      primary_file = &input_file;
    BCODE_END;

    BCODE_START(SET_PRIMARY_OUTPUT_FILE):
      primary_file = &output_file;
    BCODE_END;

    BCODE_START(SET_SECONDARY_FILE):
      secondary_file = file_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(SET_SECONDARY_FILE_16):
      secondary_file = file_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SET_SECONDARY_INPUT_FILE):
      secondary_file = &input_file;
    BCODE_END;

    BCODE_START(SET_SECONDARY_OUTPUT_FILE):
      secondary_file = &output_file;
    BCODE_END;

    BCODE_START(PUSH_VAR_INT):
      *int_sp++ = int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_VAR_LONG):
      *long_sp++ = long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_VAR_SINGLE):
      *single_sp++ = single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_VAR_DOUBLE):
      *double_sp++ = double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_VAR_STRING):
      *string_sp = string_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      (*string_sp++)->ref_count++;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_VAR_INT_16):
      *int_sp++ = int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_VAR_LONG_16):
      *long_sp++ = long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_VAR_SINGLE_16):
      *single_sp++ = single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_VAR_DOUBLE_16):
      *double_sp++ = double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_VAR_STRING_16):
      *string_sp = string_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      (*string_sp++)->ref_count++;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_INT):
      int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = *--int_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_LONG):
      long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = *--long_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_SINGLE):
      single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = *--single_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_DOUBLE):
      double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = *--double_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_STRING):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        *p_str = free_string (*p_str);
        *p_str = *--string_sp;
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(POP_STRING_MID2):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*((T_INT *)ALIGN_UP(pc,ALIGN_INT))];
        string_mid_assign (pc, p_str, *(long_sp-1), (*p_str)->length,
                           --string_sp);
        --long_sp;
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(POP_STRING_MID3):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*((T_INT *)ALIGN_UP(pc,ALIGN_INT))];
        string_mid_assign (pc, p_str, *(long_sp-2), *(long_sp-1), --string_sp);
        long_sp -= 2;
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(POP_INT_16):
      int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = *--int_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_LONG_16):
      long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = *--long_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_SINGLE_16):
      single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = *--single_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_DOUBLE_16):
      double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = *--double_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_STRING_16):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        *p_str = free_string (*p_str);
        *p_str = *--string_sp;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(POP_STRING_MID2_16):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        string_mid_assign (pc, p_str, *(long_sp-1), (*p_str)->length,
                           --string_sp);
        --long_sp;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(POP_STRING_MID3_16):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        string_mid_assign (pc, p_str, *(long_sp-2), *(long_sp-1), --string_sp);
        long_sp -= 2;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_INT):
      *int_sp++ = int_vars[*--index_sp +
                           *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_LONG):
      *long_sp++ = long_vars[*--index_sp +
                             *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_SINGLE):
      *single_sp++ = single_vars[*--index_sp +
                                 *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_DOUBLE):
      *double_sp++ = double_vars[*--index_sp +
                                 *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_STRING):
      *string_sp = string_vars[*--index_sp +
                               *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      (*string_sp++)->ref_count++;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_INT_16):
      *int_sp++ = int_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_LONG_16):
      *long_sp++ = long_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_SINGLE_16):
      *single_sp++ = single_vars[*--index_sp +
                                 *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_DOUBLE_16):
      *double_sp++ = double_vars[*--index_sp +
                                 *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(PUSH_INDIRECT_VAR_STRING_16):
      *string_sp = string_vars[*--index_sp +
                               *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      (*string_sp++)->ref_count++;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_INDIRECT_INT):
      int_vars[*--index_sp + *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] =
        *--int_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_INDIRECT_LONG):
      long_vars[*--index_sp + *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] =
        *--long_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_INDIRECT_SINGLE):
      single_vars[*--index_sp + *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] =
        *--single_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_INDIRECT_DOUBLE):
      double_vars[*--index_sp + *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] =
        *--double_sp;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(POP_INDIRECT_STRING):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*--index_sp +
                             *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        *p_str = free_string (*p_str);
        *p_str = *--string_sp;
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(POP_INDIRECT_STRING_MID2):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*--index_sp +
                             *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        string_mid_assign (pc, p_str, *(long_sp-1), (*p_str)->length,
                           --string_sp);
        --long_sp;
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(POP_INDIRECT_STRING_MID3):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*--index_sp +
                             *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        string_mid_assign (pc, p_str, *(long_sp-2), *(long_sp-1), --string_sp);
        long_sp -= 2;
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(POP_INDIRECT_INT_16):
      int_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] =
        *--int_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_INDIRECT_LONG_16):
      long_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] =
        *--long_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_INDIRECT_SINGLE_16):
      single_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] =
        *--single_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_INDIRECT_DOUBLE_16):
      double_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] =
        *--double_sp;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(POP_INDIRECT_STRING_16):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        *p_str = free_string (*p_str);
        *p_str = *--string_sp;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(POP_INDIRECT_STRING_MID2_16):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        string_mid_assign (pc, p_str, *(long_sp-1), (*p_str)->length,
                           --string_sp);
        --long_sp;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(POP_INDIRECT_STRING_MID3_16):
      {
        register T_STRING *p_str;
        p_str = &string_vars[*--index_sp + *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        string_mid_assign (pc, p_str, *(long_sp-2), *(long_sp-1), --string_sp);
        long_sp -= 2;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(ZERO_VAR_INT):
      int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = 0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ZERO_VAR_LONG):
      long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = 0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ZERO_VAR_SINGLE):
      single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = (T_SINGLE)0.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ZERO_VAR_DOUBLE):
      double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = 0.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ZERO_VAR_STRING):
      runtime_error (73, pc, "B_ZERO_VAR_STRING is not implemented yet");
    BCODE_END;

    BCODE_START(ZERO_VAR_INT_16):
      int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = 0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ZERO_VAR_LONG_16):
      long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = 0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ZERO_VAR_SINGLE_16):
      single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = (T_SINGLE)0.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ZERO_VAR_DOUBLE_16):
      double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = 0.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ZERO_VAR_STRING_16):
      runtime_error (73, pc, "B_ZERO_VAR_STRING_16 is not implemented yet");
    BCODE_END;

    BCODE_START(ONE_VAR_INT):
      int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = 1;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ONE_VAR_LONG):
      long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = 1;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ONE_VAR_SINGLE):
      single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = (T_SINGLE)1.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ONE_VAR_DOUBLE):
      double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] = 1.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ONE_VAR_STRING):
      runtime_error (73, pc, "B_ONE_VAR_STRING is not implemented yet");
    BCODE_END;

    BCODE_START(ONE_VAR_INT_16):
      int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = 1;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ONE_VAR_LONG_16):
      long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = 1;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ONE_VAR_SINGLE_16):
      single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = (T_SINGLE)1.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ONE_VAR_DOUBLE_16):
      double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] = 1.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(ONE_VAR_STRING_16):
      runtime_error (73, pc, "B_ONE_VAR_STRING_16 is not implemented yet");
    BCODE_END;

    BCODE_START(INT_SWAP):
      {
        register T_INT i = *(int_sp - 1);
        *(int_sp - 1) = *(int_sp - 2);
        *(int_sp - 2) = i;
      }
    BCODE_END;

    BCODE_START(LONG_SWAP):
      {
        register T_LONG l = *(long_sp - 1);
        *(long_sp - 1) = *(long_sp - 2);
        *(long_sp - 2) = l;
      }
    BCODE_END;

    BCODE_START(SINGLE_SWAP):
      {
        register T_SINGLE s = *(single_sp - 1);
        *(single_sp - 1) = *(single_sp - 2);
        *(single_sp - 2) = s;
      }
    BCODE_END;

    BCODE_START(DOUBLE_SWAP):
      {
        register T_DOUBLE d = *(double_sp - 1);
        *(double_sp - 1) = *(double_sp - 2);
        *(double_sp - 2) = d;
      }
    BCODE_END;

    BCODE_START(STRING_SWAP):
      {
        register T_STRING str = *(string_sp - 1);
        *(string_sp - 1) = *(string_sp - 2);
        *(string_sp - 2) = str;
      }
    BCODE_END;

    BCODE_START(INDEX_DUP):
      *index_sp = *(index_sp - 1);
      index_sp++;
    BCODE_END;

    BCODE_START(INDEX_DUP2):
      *index_sp = *(index_sp - 2);
      *(index_sp + 1) = *(index_sp - 1);
      index_sp += 2;
    BCODE_END;

    BCODE_START(INT_UNARY_MINUS):
      *(int_sp - 1) = - *(int_sp - 1);
    BCODE_END;

    BCODE_START(LONG_UNARY_MINUS):
      *(long_sp - 1) = - *(long_sp - 1);
    BCODE_END;

    BCODE_START(SINGLE_UNARY_MINUS):
      *(single_sp - 1) = - *(single_sp - 1);
    BCODE_END;

    BCODE_START(DOUBLE_UNARY_MINUS):
      *(double_sp - 1) = - *(double_sp - 1);
    BCODE_END;

    BCODE_START(INT_UNARY_PLUS):
    BCODE_START(LONG_UNARY_PLUS):
    BCODE_START(SINGLE_UNARY_PLUS):
    BCODE_START(DOUBLE_UNARY_PLUS):
    BCODE_END;

    BCODE_START(INT_PLUS):
      --int_sp;
      *(int_sp - 1) += *int_sp;
    BCODE_END;

    BCODE_START(INT_PLUS_CONST):
      *(int_sp - 1) += *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_PLUS_CONST_16):
      *(int_sp - 1) += (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_PLUS_VAR):
      *(int_sp - 1) += int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(INT_PLUS_VAR_16):
      *(int_sp - 1) += int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_MINUS):
      --int_sp;
      *(int_sp - 1) -= *int_sp;
    BCODE_END;

    BCODE_START(INT_MINUS_CONST):
      *(int_sp - 1) -= *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_MINUS_CONST_16):
      *(int_sp - 1) -= (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_MINUS_VAR):
      *(int_sp - 1) -= int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(INT_MINUS_VAR_16):
      *(int_sp - 1) -= int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_SWAP_MINUS):
      --int_sp;
      *(int_sp - 1) = *int_sp - *(int_sp - 1);
    BCODE_END;

    BCODE_START(INT_SWAP_MINUS_CONST):
      *(int_sp - 1) = *((T_INT *)ALIGN_UP(pc,ALIGN_INT)) - *(int_sp - 1);
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_SWAP_MINUS_CONST_16):
      *(int_sp - 1) = (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)) -
                      *(int_sp - 1);
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_TIMES):
      --int_sp;
      *(int_sp - 1) *= *int_sp;
    BCODE_END;

    BCODE_START(INT_TIMES_CONST):
      *(int_sp - 1) *= *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_TIMES_CONST_16):
      *(int_sp - 1) *= (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_TIMES_VAR):
      *(int_sp - 1) *= int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(INT_TIMES_VAR_16):
      *(int_sp - 1) *= int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_DIVIDE):
      {
        register T_INT i = *--int_sp;
        if (i == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) /= i;
      }
    BCODE_END;

    BCODE_START(INT_DIVIDE_CONST):
      {
        register T_INT i = *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
        if (i == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) /= i;
        pc += SIZE_INT;
      }
    BCODE_END;

    BCODE_START(INT_DIVIDE_CONST_16):
      {
        register T_INT i = (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (i == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) /= i;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_SWAP_DIVIDE):
      {
        register T_INT i = *--int_sp;
        if (*(int_sp - 1) == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) = i / *(int_sp - 1);
      }
    BCODE_END;

    BCODE_START(INT_SWAP_DIVIDE_CONST):
      {
        register T_INT i = *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
        if (*(int_sp - 1) == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) = i / *(int_sp - 1);
        pc += SIZE_INT;
      }
    BCODE_END;

    BCODE_START(INT_SWAP_DIVIDE_CONST_16):
      {
        register T_INT i = (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(int_sp - 1) == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) = i / *(int_sp - 1);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_MOD):
      {
        register T_INT i = *--int_sp;
        if (i == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) %= i;
      }
    BCODE_END;

    BCODE_START(INT_MOD_CONST):
      {
        register T_INT i = *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
        if (i == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) %= i;
        pc += SIZE_INT;
      }
    BCODE_END;

    BCODE_START(INT_MOD_CONST_16):
      {
        register T_INT i = (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (i == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) %= i;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_SWAP_MOD):
      {
        register T_INT i = *--int_sp;
        if (*(int_sp - 1) == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) = i % *(int_sp - 1);
      }
    BCODE_END;

    BCODE_START(INT_SWAP_MOD_CONST):
      {
        register T_INT i = *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
        if (*(int_sp - 1) == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) = i % *(int_sp - 1);
        pc += SIZE_INT;
      }
    BCODE_END;

    BCODE_START(INT_SWAP_MOD_CONST_16):
      {
        register T_INT i = (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(int_sp - 1) == 0)
          runtime_error (11, pc, "INT divide by 0!");
        *(int_sp - 1) = i % *(int_sp - 1);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_POWER):
      int_sp -= 2;
      if (*int_sp == 0 && *(int_sp + 1) < 0)
        runtime_error (11, pc, "INT 0 to negative power!");
      *single_sp++ = (T_SINGLE)pow ((double)*int_sp, (double)*(int_sp + 1));
    BCODE_END;

    BCODE_START(INT_POWER_CONST):
      {
        register T_INT i = *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
        --int_sp;
        if (*int_sp == 0 && i < 0)
          runtime_error (11, pc, "INT 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)*int_sp, (double)i);
        pc += SIZE_INT;
      }
    BCODE_END;

    BCODE_START(INT_POWER_CONST_16):
      {
        register T_INT i = (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        --int_sp;
        if (*int_sp == 0 && i < 0)
          runtime_error (11, pc, "INT 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)*int_sp, (double)i);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_SWAP_POWER):
      int_sp -= 2;
      if (*(int_sp + 1) == 0 && *int_sp < 0)
        runtime_error (11, pc, "INT 0 to negative power!");
      *single_sp++ = (T_SINGLE)pow ((double)*(int_sp + 1), (double)*int_sp);
    BCODE_END;

    BCODE_START(INT_SWAP_POWER_CONST):
      {
        register T_INT i = *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
        --int_sp;
        if (i == 0 && *int_sp < 0)
          runtime_error (11, pc, "INT 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)i, (double)*int_sp);
        pc += SIZE_INT;
      }
    BCODE_END;

    BCODE_START(INT_SWAP_POWER_CONST_16):
      {
        register T_INT i = (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        --int_sp;
        if (i == 0 && *int_sp < 0)
          runtime_error (11, pc, "INT 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)i, (double)*int_sp);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_AND):
      --int_sp;
      *(int_sp - 1) &= *int_sp;
    BCODE_END;

    BCODE_START(INT_AND_CONST):
      *(int_sp - 1) &= *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_AND_CONST_16):
      *(int_sp - 1) &= (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_OR):
      --int_sp;
      *(int_sp - 1) |= *int_sp;
    BCODE_END;

    BCODE_START(INT_OR_CONST):
      *(int_sp - 1) |= *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_OR_CONST_16):
      *(int_sp - 1) |= (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_XOR):
      --int_sp;
      *(int_sp - 1) ^= *int_sp;
    BCODE_END;

    BCODE_START(INT_XOR_CONST):
      *(int_sp - 1) ^= *((T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_XOR_CONST_16):
      *(int_sp - 1) ^= (T_INT)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_NOT):
      *(int_sp - 1) = ~*(int_sp - 1);
    BCODE_END;

    BCODE_START(INT_ABS):
      if (*(int_sp - 1) < 0)
        *(int_sp - 1) = - *(int_sp - 1);
    BCODE_END;

    BCODE_START(INT_CLNG):
      *long_sp++ = (T_LONG)*--int_sp;
    BCODE_END;

    BCODE_START(INT_CSNG):
      *single_sp++ = (T_SINGLE)*--int_sp;
    BCODE_END;

    BCODE_START(INT_CDBL):
      *double_sp++ = (T_DOUBLE)*--int_sp;
    BCODE_END;

    BCODE_START(INT_SGN):
      {
        register T_INT i;
        if ((i = *(int_sp - 1)) > 0)
          *(int_sp - 1) = 1;
        else if (i < 0)
          *(int_sp - 1) = -1;
        else
          *(int_sp - 1) = 0;
      }
    BCODE_END;

    BCODE_START(INT_EQUAL):
      --int_sp;
      *(int_sp - 1) = - (*(int_sp - 1) == *int_sp);
    BCODE_END;

    BCODE_START(INT_EQUAL_CONST):
      *(int_sp - 1) = - (*(int_sp - 1) == *(T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_EQUAL_CONST_16):
      *(int_sp - 1) = - (*(int_sp - 1) ==
                         (T_INT)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_NOTEQUAL):
      --int_sp;
      *(int_sp - 1) = - (*(int_sp - 1) != *int_sp);
    BCODE_END;

    BCODE_START(INT_NOTEQUAL_CONST):
      *(int_sp - 1) = - (*(int_sp - 1) != *(T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_NOTEQUAL_CONST_16):
      *(int_sp - 1) = - (*(int_sp - 1) !=
                         (T_INT)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_LESS):
      --int_sp;
      *(int_sp - 1) = - (*(int_sp - 1) < *int_sp);
    BCODE_END;

    BCODE_START(INT_LESS_CONST):
      *(int_sp - 1) = - (*(int_sp - 1) < *(T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_LESS_CONST_16):
      *(int_sp - 1) = - (*(int_sp - 1) <
                         (T_INT)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_GREATER):
      --int_sp;
      *(int_sp - 1) = - (*(int_sp - 1) > *int_sp);
    BCODE_END;

    BCODE_START(INT_GREATER_CONST):
      *(int_sp - 1) = - (*(int_sp - 1) > *(T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_GREATER_CONST_16):
      *(int_sp - 1) = - (*(int_sp - 1) >
                         (T_INT)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_LESSOREQUAL):
      --int_sp;
      *(int_sp - 1) = - (*(int_sp - 1) <= *int_sp);
    BCODE_END;

    BCODE_START(INT_LESSOREQUAL_CONST):
      *(int_sp - 1) = - (*(int_sp - 1) >= *(T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_LESSOREQUAL_CONST_16):
      *(int_sp - 1) = - (*(int_sp - 1) <=
                         (T_INT)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_GREATEROREQUAL):
      --int_sp;
      *(int_sp - 1) = - (*(int_sp - 1) >= *int_sp);
    BCODE_END;

    BCODE_START(INT_GREATEROREQUAL_CONST):
      *(int_sp - 1) = - (*(int_sp - 1) <= *(T_INT *)ALIGN_UP(pc,ALIGN_INT));
      pc += SIZE_INT;
    BCODE_END;

    BCODE_START(INT_GREATEROREQUAL_CONST_16):
      *(int_sp - 1) = - (*(int_sp - 1) >=
                         (T_INT)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_INCREMENT):
      (*(int_sp - 1))++;
    BCODE_END;

    BCODE_START(INT_INCREMENT_VAR):
      int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))]++;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(INT_INCREMENT_VAR_16):
      int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))]++;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_DECREMENT):
      --*(int_sp - 1);
    BCODE_END;

    BCODE_START(INT_DECREMENT_VAR):
      --int_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(INT_DECREMENT_VAR_16):
      --int_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_COMMANDCOUNT):
      *int_sp++ = argc - 1;
    BCODE_END;

    BCODE_START(LONG_PLUS):
      --long_sp;
      *(long_sp - 1) += *long_sp;
    BCODE_END;

    BCODE_START(LONG_PLUS_CONST):
      *(long_sp - 1) += *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_PLUS_CONST_16):
      *(long_sp - 1) += (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_PLUS_VAR):
      *(long_sp - 1) += long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(LONG_PLUS_VAR_16):
      *(long_sp - 1) += long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_MINUS):
      --long_sp;
      *(long_sp - 1) -= *long_sp;
    BCODE_END;

    BCODE_START(LONG_MINUS_CONST):
      *(long_sp - 1) -= *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_MINUS_CONST_16):
      *(long_sp - 1) -= (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_MINUS_VAR):
      *(long_sp - 1) -= long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(LONG_MINUS_VAR_16):
      *(long_sp - 1) -= long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_SWAP_MINUS):
      --long_sp;
      *(long_sp - 1) = *long_sp - *(long_sp - 1);
    BCODE_END;

    BCODE_START(LONG_SWAP_MINUS_CONST):
      *(long_sp - 1) = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG)) - *(long_sp - 1);
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_SWAP_MINUS_CONST_16):
      *(long_sp - 1) = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)) -
                       *(long_sp - 1);
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_TIMES):
      --long_sp;
      *(long_sp - 1) *= *long_sp;
    BCODE_END;

    BCODE_START(LONG_TIMES_CONST):
      *(long_sp - 1) *= *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_TIMES_CONST_16):
      *(long_sp - 1) *= (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_TIMES_VAR):
      *(long_sp - 1) *= long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(LONG_TIMES_VAR_16):
      *(long_sp - 1) *= long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_DIVIDE):
      {
        register T_LONG l;
        if ((l = *--long_sp) == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) /= l;
      }
    BCODE_END;

    BCODE_START(LONG_DIVIDE_CONST):
      {
        register T_LONG l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        if (l == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) /= l;
        pc += SIZE_LONG;
      }
    BCODE_END;

    BCODE_START(LONG_DIVIDE_CONST_16):
      {
        register T_LONG l = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (l == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) /= l;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_DIVIDE):
      {
        register T_LONG l = *--long_sp;
        if (*(long_sp - 1) == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) = l / *(long_sp - 1);
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_DIVIDE_CONST):
      {
        register T_LONG l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        if (*(long_sp - 1) == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) = l / *(long_sp - 1);
        pc += SIZE_LONG;
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_DIVIDE_CONST_16):
      {
        register T_LONG l = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(long_sp - 1) == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) = l / *(long_sp - 1);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_MOD):
      {
        register T_LONG l = *--long_sp;
        if (l == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) %= l;
      }
    BCODE_END;

    BCODE_START(LONG_MOD_CONST):
      {
        register T_LONG l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        if (l == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) %= l;
        pc += SIZE_LONG;
      }
    BCODE_END;

    BCODE_START(LONG_MOD_CONST_16):
      {
        register T_LONG l = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (l == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) %= l;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_MOD):
      {
        register T_LONG i = *--long_sp;
        if (*(long_sp - 1) == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) = i % *(long_sp - 1);
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_MOD_CONST):
      {
        register T_LONG l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        if (*(long_sp - 1) == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) = l % *(long_sp - 1);
        pc += SIZE_LONG;
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_MOD_CONST_16):
      {
        register T_LONG l = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(long_sp - 1) == 0)
          runtime_error (11, pc, "LONG divide by 0!");
        *(long_sp - 1) = l % *(long_sp - 1);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_POWER):
      long_sp -= 2;
      if (*long_sp == 0 && *(long_sp + 1) < 0)
        runtime_error (11, pc, "LONG 0 to negative power!");
      *single_sp++ = (T_SINGLE)pow ((double)*long_sp, (double)*(long_sp + 1));
    BCODE_END;

    BCODE_START(LONG_POWER_CONST):
      {
        register T_LONG l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        --long_sp;
        if (*long_sp == 0 && l < 0)
          runtime_error (11, pc, "LONG 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)*long_sp, (double)l);
        pc += SIZE_LONG;
      }
    BCODE_END;

    BCODE_START(LONG_POWER_CONST_16):
      {
        register T_LONG l = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        --long_sp;
        if (*long_sp == 0 && l < 0)
          runtime_error (11, pc, "LONG 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)*long_sp, (double)l);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_POWER):
      long_sp -= 2;
      if (*(long_sp + 1) == 0 && *long_sp < 0)
        runtime_error (11, pc, "LONG 0 to negative power!");
      *single_sp++ = (T_SINGLE)pow ((double)*(long_sp + 1), (double)*long_sp);
    BCODE_END;

    BCODE_START(LONG_SWAP_POWER_CONST):
      {
        register T_LONG l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        --long_sp;
        if (l == 0 && *long_sp < 0)
          runtime_error (11, pc, "LONG 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)l, (double)*long_sp);
        pc += SIZE_LONG;
      }
    BCODE_END;

    BCODE_START(LONG_SWAP_POWER_CONST_16):
      {
        register T_LONG l = (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        --long_sp;
        if (l == 0 && *long_sp < 0)
          runtime_error (11, pc, "LONG 0 to negative power!");
        *single_sp++ = (T_SINGLE)pow ((double)l, (double)*long_sp);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_AND):
      --long_sp;
      *(long_sp - 1) &= *long_sp;
    BCODE_END;

    BCODE_START(LONG_AND_CONST):
      *(long_sp - 1) &= *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_AND_CONST_16):
      *(long_sp - 1) &= (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_OR):
      --long_sp;
      *(long_sp - 1) |= *long_sp;
    BCODE_END;

    BCODE_START(LONG_OR_CONST):
      *(long_sp - 1) |= *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_OR_CONST_16):
      *(long_sp - 1) |= (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_XOR):
      --long_sp;
      *(long_sp - 1) ^= *long_sp;
    BCODE_END;

    BCODE_START(LONG_XOR_CONST):
      *(long_sp - 1) ^= *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_XOR_CONST_16):
      *(long_sp - 1) ^= (T_LONG)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_NOT):
      *(long_sp - 1) = ~*(long_sp - 1);
    BCODE_END;

    BCODE_START(LONG_ABS):
      if (*(long_sp - 1) < 0)
        *(long_sp - 1) = - *(long_sp - 1);
    BCODE_END;

    BCODE_START(LONG_INT):
    BCODE_END;

    BCODE_START(LONG_CINT):
      *int_sp++ = (T_INT)*--long_sp;
    BCODE_END;

    BCODE_START(LONG_CSNG):
      *single_sp++ = (T_SINGLE)*--long_sp;
    BCODE_END;

    BCODE_START(LONG_CDBL):
      *double_sp++ = (T_DOUBLE)*--long_sp;
    BCODE_END;

    BCODE_START(LONG_FIX):
      *int_sp++ = (T_INT)*--long_sp;
    BCODE_END;

    BCODE_START(LONG_SGN):
      {
        register T_LONG l;
        if ((l = *--long_sp) > 0)
          *int_sp++ = 1;
        else if (l < 0)
          *int_sp++ = -1;
        else
          *int_sp++ = 0;
      }
    BCODE_END;

    BCODE_START(LONG_EQUAL):
      long_sp -= 2;
      *int_sp++ = (T_INT) - (*long_sp == *(long_sp + 1));
    BCODE_END;

    BCODE_START(LONG_EQUAL_CONST):
      *int_sp++ = (T_INT) - (*--long_sp == *(T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_EQUAL_CONST_16):
      *int_sp++ = - (*--long_sp == (T_LONG)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_NOTEQUAL):
      long_sp -= 2;
      *int_sp++ = (T_INT) - (*long_sp != *(long_sp + 1));
    BCODE_END;

    BCODE_START(LONG_NOTEQUAL_CONST):
      *int_sp++ = (T_INT) - (*--long_sp != *(T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_NOTEQUAL_CONST_16):
      *int_sp++ = - (*--long_sp != (T_LONG)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_LESS):
      long_sp -= 2;
      *int_sp++ = (T_INT) - (*long_sp < *(long_sp + 1));
    BCODE_END;

    BCODE_START(LONG_LESS_CONST):
      *int_sp++ = (T_INT) - (*--long_sp < *(T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_LESS_CONST_16):
      *int_sp++ = - (*--long_sp < (T_LONG)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_GREATER):
      long_sp -= 2;
      *int_sp++ = (T_INT) - (*long_sp > *(long_sp + 1));
    BCODE_END;

    BCODE_START(LONG_GREATER_CONST):
      *int_sp++ = (T_INT) - (*--long_sp > *(T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_GREATER_CONST_16):
      *int_sp++ = - (*--long_sp > (T_LONG)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_LESSOREQUAL):
      long_sp -= 2;
      *int_sp++ = (T_INT) - (*long_sp <= *(long_sp + 1));
    BCODE_END;

    BCODE_START(LONG_LESSOREQUAL_CONST):
      *int_sp++ = (T_INT) - (*--long_sp <= *(T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_LESSOREQUAL_CONST_16):
      *int_sp++ = - (*--long_sp <= (T_LONG)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_GREATEROREQUAL):
      long_sp -= 2;
      *int_sp++ = (T_INT) - (*long_sp >= *(long_sp + 1));
    BCODE_END;

    BCODE_START(LONG_GREATEROREQUAL_CONST):
      *int_sp++ = (T_INT) - (*--long_sp >= *(T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
      pc += SIZE_LONG;
    BCODE_END;

    BCODE_START(LONG_GREATEROREQUAL_CONST_16):
      *int_sp++ = - (*--long_sp >= (T_LONG)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_INCREMENT):
      (*(long_sp - 1))++;
    BCODE_END;

    BCODE_START(LONG_INCREMENT_VAR):
      long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))]++;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(LONG_INCREMENT_VAR_16):
      long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))]++;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(LONG_DECREMENT):
      --*(long_sp - 1);
    BCODE_END;

    BCODE_START(LONG_DECREMENT_VAR):
      --long_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(LONG_DECREMENT_VAR_16):
      --long_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_PLUS):
      single_sp--;
      *(single_sp - 1) += *single_sp;
    BCODE_END;

    BCODE_START(SINGLE_PLUS_CONST):
      *(single_sp - 1) += *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_PLUS_CONST_16):
      *(single_sp - 1) += (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_PLUS_VAR):
      *(single_sp - 1) += single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(SINGLE_PLUS_VAR_16):
      *(single_sp - 1) += single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_MINUS):
      single_sp--;
      *(single_sp - 1) -= *single_sp;
    BCODE_END;

    BCODE_START(SINGLE_MINUS_CONST):
      *(single_sp - 1) -= *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_MINUS_CONST_16):
      *(single_sp - 1) -= (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_MINUS_VAR):
      *(single_sp - 1) -= single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(SINGLE_MINUS_VAR_16):
      *(single_sp - 1) -= single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_SWAP_MINUS):
      --single_sp;
      *(single_sp - 1) = *single_sp - *(single_sp - 1);
    BCODE_END;

    BCODE_START(SINGLE_SWAP_MINUS_CONST):
      *(single_sp - 1) = *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE)) -
                         *(single_sp - 1);
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_SWAP_MINUS_CONST_16):
      *(single_sp - 1) = (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)) -
                         *(single_sp - 1);
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_TIMES):
      single_sp--;
      *(single_sp - 1) *= *single_sp;
    BCODE_END;

    BCODE_START(SINGLE_TIMES_CONST):
      *(single_sp - 1) *= *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_TIMES_CONST_16):
      *(single_sp - 1) *= (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_TIMES_VAR):
      *(single_sp - 1) *= single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(SINGLE_TIMES_VAR_16):
      *(single_sp - 1) *= single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_DIVIDE):
      {
        register T_SINGLE s;
        if ((s = *--single_sp) == (T_SINGLE)0.0)
          runtime_error (11, pc, "SINGLE divide by 0.0!");
        *(single_sp - 1) /= s;
      }
    BCODE_END;

    BCODE_START(SINGLE_DIVIDE_CONST):
      {
        register T_SINGLE s = *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
        if (s == 0)
          runtime_error (11, pc, "SINGLE divide by 0!");
        *(single_sp - 1) /= s;
        pc += SIZE_SINGLE;
      }
    BCODE_END;

    BCODE_START(SINGLE_DIVIDE_CONST_16):
      {
        register T_SINGLE s = (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (s == 0)
          runtime_error (11, pc, "SINGLE divide by 0!");
        *(single_sp - 1) /= s;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_SWAP_DIVIDE):
      {
        register T_SINGLE s = *--single_sp;
        if (*(single_sp - 1) == 0)
          runtime_error (11, pc, "SINGLE divide by 0!");
        *(single_sp - 1) = s / *(single_sp - 1);
      }
    BCODE_END;

    BCODE_START(SINGLE_SWAP_DIVIDE_CONST):
      {
        register T_SINGLE s = *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
        if (*(single_sp - 1) == 0)
          runtime_error (11, pc, "SINGLE divide by 0!");
        *(single_sp - 1) = s / *(single_sp - 1);
        pc += SIZE_SINGLE;
      }
    BCODE_END;

    BCODE_START(SINGLE_SWAP_DIVIDE_CONST_16):
      {
        register T_SINGLE s = (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(single_sp - 1) == 0)
          runtime_error (11, pc, "SINGLE divide by 0!");
        *(single_sp - 1) = s / *(single_sp - 1);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_POWER):
      --single_sp;
      if (*(single_sp - 1) == 0.0f && *single_sp < 0)
        runtime_error (11, pc, "SINGLE 0.0 to negative power!");
      *(single_sp - 1) = (T_SINGLE)pow ((double)*(single_sp - 1),
                                      (double)*single_sp);
    BCODE_END;

    BCODE_START(SINGLE_POWER_CONST):
      {
        register T_SINGLE s = *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
        if (*(single_sp - 1) == 0 && s < 0)
          runtime_error (11, pc, "SINGLE 0 to negative power!");
        *(single_sp - 1) = (T_SINGLE)pow ((double)*(single_sp - 1), (double)s);
        pc += SIZE_SINGLE;
      }
    BCODE_END;

    BCODE_START(SINGLE_POWER_CONST_16):
      {
        register T_SINGLE s = (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(single_sp - 1) == 0 && s < 0)
          runtime_error (11, pc, "SINGLE 0 to negative power!");
        *(single_sp - 1) = (T_SINGLE)pow ((double)*(single_sp - 1), (double)s);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_SWAP_POWER):
      --single_sp;
      if (*single_sp == 0.0f && *(single_sp - 1) < 0)
        runtime_error (11, pc, "SINGLE 0.0 to negative power!");
      *(single_sp - 1) = (T_SINGLE)pow ((double)*single_sp,
                                        (double)*(single_sp - 1));
    BCODE_END;

    BCODE_START(SINGLE_SWAP_POWER_CONST):
      {
        register T_SINGLE s = *((T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
        if (s == 0 && *(single_sp - 1) < 0)
          runtime_error (11, pc, "SINGLE 0 to negative power!");
        *(single_sp - 1) = (T_SINGLE)pow ((double)s, (double)*(single_sp - 1));
        pc += SIZE_SINGLE;
      }
    BCODE_END;

    BCODE_START(SINGLE_SWAP_POWER_CONST_16):
      {
        register T_SINGLE s = (T_SINGLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (s == 0 && *(single_sp - 1) < 0)
          runtime_error (11, pc, "SINGLE 0 to negative power!");
        *(single_sp - 1) = (T_SINGLE)pow ((double)s, (double)*(single_sp - 1));
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_SIN):
      *(single_sp - 1) = (T_SINGLE)sin ((double)*(single_sp - 1));
    BCODE_END;

    BCODE_START(SINGLE_COS):
      *(single_sp - 1) = (T_SINGLE)cos ((double)*(single_sp - 1));
    BCODE_END;

    BCODE_START(SINGLE_TAN):
      *(single_sp - 1) = (T_SINGLE)tan ((double)*(single_sp - 1));
    BCODE_END;

    BCODE_START(SINGLE_ATN):
      *(single_sp - 1) = (T_SINGLE)atan ((double)*(single_sp - 1));
    BCODE_END;

    BCODE_START(SINGLE_LOG):
      {
        register T_SINGLE s;
        if ((s = *(single_sp - 1)) <= (T_SINGLE)0.0)
          runtime_error (5, pc, "SINGLE log of negative or 0.0!");
        *(single_sp - 1) = (T_SINGLE)log ((double)s);
      }
    BCODE_END;

    BCODE_START(SINGLE_LOG2):
      {
        register T_SINGLE s;
        if ((s = *(single_sp - 1)) <= (T_SINGLE)0.0)
          runtime_error (5, pc, "SINGLE log of negative or 0.0!");
        *(single_sp - 1) = (T_SINGLE)(log ((double)s) / log (2.0));
      }
    BCODE_END;

    BCODE_START(SINGLE_LOG10):
      {
        register T_SINGLE s;
        if ((s = *(single_sp - 1)) <= (T_SINGLE)0.0)
          runtime_error (5, pc, "SINGLE log of negative or 0.0!");
        *(single_sp - 1) = (T_SINGLE)(log ((double)s) / log (10.0));
      }
    BCODE_END;

    BCODE_START(SINGLE_EXP):
      *(single_sp - 1) = (T_SINGLE)exp ((double)*(single_sp - 1));
    BCODE_END;

    BCODE_START(SINGLE_SQR):
      {
        register T_SINGLE s;
        if ((s = *(single_sp - 1)) < (T_SINGLE)0.0)
          runtime_error (5, pc, "SINGLE square root of negative number!");
        *(single_sp - 1) = (T_SINGLE)sqrt ((double)s);
      }
    BCODE_END;

    BCODE_START(SINGLE_ABS):
      if (*(single_sp - 1) < (T_SINGLE)0.0)
        *(single_sp - 1) = - *(single_sp - 1);
    BCODE_END;

    BCODE_START(SINGLE_RND):
      {
        register T_LONG l;
        if ((l = *--long_sp) < 0)
          srand ((unsigned int)l);
        *single_sp++ = (T_SINGLE)(((T_SINGLE)rand()) / (T_SINGLE)RAND_MAX);
      }
    BCODE_END;

    BCODE_START(SINGLE_INT):
      *(single_sp - 1) = (T_SINGLE)floor((double)*(single_sp - 1));
    BCODE_END;

    BCODE_START(SINGLE_CINT):
    *int_sp++ = (T_INT)floor((double)(*--single_sp + 0.5f));
    BCODE_END;

    BCODE_START(SINGLE_CLNG):
    *long_sp++ = (T_LONG)floor((double)(*--single_sp + 0.5f));
    BCODE_END;

    BCODE_START(SINGLE_CDBL):
      *double_sp++ = (T_DOUBLE)*--single_sp;
    BCODE_END;

    BCODE_START(SINGLE_FIX):
      *int_sp++ = (T_INT)*--single_sp;
    BCODE_END;

    BCODE_START(SINGLE_SGN):
      {
        register T_SINGLE s;
        if ((s = *--single_sp) > (T_SINGLE)0.0)
          *int_sp++ = 1;
        else if (s < (T_SINGLE)0.0)
          *int_sp++ = -1;
        else
          *int_sp++ = 0;
      }
    BCODE_END;

    BCODE_START(SINGLE_EQUAL):
      single_sp -= 2;
      *int_sp++ = (T_INT) - (*single_sp == *(single_sp + 1));
    BCODE_END;

    BCODE_START(SINGLE_EQUAL_CONST):
      *int_sp++ = (T_INT) - (*--single_sp ==
                             *(T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_EQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--single_sp ==
                             (T_SINGLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_NOTEQUAL):
      single_sp -= 2;
      *int_sp++ = (T_INT) - (*single_sp != *(single_sp + 1));
    BCODE_END;

    BCODE_START(SINGLE_NOTEQUAL_CONST):
      *int_sp++ = (T_INT) - (*--single_sp !=
                             *(T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_NOTEQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--single_sp !=
                             (T_SINGLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_LESS):
      single_sp -= 2;
      *int_sp++ = (T_INT) - (*single_sp < *(single_sp + 1));
    BCODE_END;

    BCODE_START(SINGLE_LESS_CONST):
      *int_sp++ = (T_INT) - (*--single_sp <
                             *(T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_LESS_CONST_16):
      *int_sp++ = (T_INT) - (*--single_sp <
                             (T_SINGLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_GREATER):
      single_sp -= 2;
      *int_sp++ = (T_INT) - (*single_sp > *(single_sp + 1));
    BCODE_END;

    BCODE_START(SINGLE_GREATER_CONST):
      *int_sp++ = (T_INT) - (*--single_sp >
                             *(T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_GREATER_CONST_16):
      *int_sp++ = (T_INT) - (*--single_sp >
                             (T_SINGLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_LESSOREQUAL):
      single_sp -= 2;
      *int_sp++ = (T_INT) - (*single_sp <= *(single_sp + 1));
    BCODE_END;

    BCODE_START(SINGLE_LESSOREQUAL_CONST):
      *int_sp++ = (T_INT) - (*--single_sp <=
                             *(T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_LESSOREQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--single_sp <=
                             (T_SINGLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_GREATEROREQUAL):
      single_sp -= 2;
      *int_sp++ = (T_INT) - (*single_sp >= *(single_sp + 1));
    BCODE_END;

    BCODE_START(SINGLE_GREATEROREQUAL_CONST):
      *int_sp++ = (T_INT) - (*--single_sp >=
                             *(T_SINGLE *)ALIGN_UP(pc,ALIGN_SINGLE));
      pc += SIZE_SINGLE;
    BCODE_END;

    BCODE_START(SINGLE_GREATEROREQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--single_sp >=
                             (T_SINGLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_INCREMENT):
      *(single_sp - 1) += 1.0f;
    BCODE_END;

    BCODE_START(SINGLE_INCREMENT_VAR):
      single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] += (T_SINGLE)1.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(SINGLE_INCREMENT_VAR_16):
      single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] += (T_SINGLE)1.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(SINGLE_DECREMENT):
      *(single_sp - 1) -= 1.0f;
    BCODE_END;

    BCODE_START(SINGLE_DECREMENT_VAR):
      single_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] -= (T_SINGLE)1.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(SINGLE_DECREMENT_VAR_16):
      single_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] -= (T_SINGLE)1.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_PLUS):
      double_sp--;
      *(double_sp - 1) += *double_sp;
    BCODE_END;

    BCODE_START(DOUBLE_PLUS_CONST):
      *(double_sp - 1) += *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_PLUS_CONST_16):
      *(double_sp - 1) += (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_PLUS_VAR):
      *(double_sp - 1) += double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(DOUBLE_PLUS_VAR_16):
      *(double_sp - 1) += double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_MINUS):
      double_sp--;
      *(double_sp - 1) -= *double_sp;
    BCODE_END;

    BCODE_START(DOUBLE_MINUS_CONST):
      *(double_sp - 1) -= *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_MINUS_CONST_16):
      *(double_sp - 1) -= (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_MINUS_VAR):
      *(double_sp - 1) -= double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(DOUBLE_MINUS_VAR_16):
      *(double_sp - 1) -= double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_MINUS):
      --double_sp;
      *(double_sp - 1) = *double_sp - *(double_sp - 1);
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_MINUS_CONST):
      *(double_sp - 1) = *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE)) -
                         *(double_sp - 1);
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_MINUS_CONST_16):
      *(double_sp - 1) = (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)) -
                         *(double_sp - 1);
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_TIMES):
      double_sp--;
      *(double_sp - 1) *= *double_sp;
    BCODE_END;

    BCODE_START(DOUBLE_TIMES_CONST):
      *(double_sp - 1) *= *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_TIMES_CONST_16):
      *(double_sp - 1) *= (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_TIMES_VAR):
      *(double_sp - 1) *= double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(DOUBLE_TIMES_VAR_16):
      *(double_sp - 1) *= double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_DIVIDE):
      {
        register T_DOUBLE d;
        if ((d = *--double_sp) == 0.0)
          runtime_error (11, pc, "DOUBLE divide by 0.0!");
        *(double_sp - 1) /= d;
      }
    BCODE_END;

    BCODE_START(DOUBLE_DIVIDE_CONST):
      {
        register T_DOUBLE d = *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
        if (d == 0)
          runtime_error (11, pc, "DOUBLE divide by 0!");
        *(double_sp - 1) /= d;
        pc += SIZE_DOUBLE;
      }
    BCODE_END;

    BCODE_START(DOUBLE_DIVIDE_CONST_16):
      {
        register T_DOUBLE d = (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (d == 0)
          runtime_error (11, pc, "DOUBLE divide by 0!");
        *(double_sp - 1) /= d;
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_DIVIDE):
      {
        register T_DOUBLE d = *--double_sp;
        if (*(double_sp - 1) == 0)
          runtime_error (11, pc, "DOUBLE divide by 0!");
        *(double_sp - 1) = d / *(double_sp - 1);
      }
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_DIVIDE_CONST):
      {
        register T_DOUBLE d = *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
        if (*(double_sp - 1) == 0)
          runtime_error (11, pc, "DOUBLE divide by 0!");
        *(double_sp - 1) = d / *(double_sp - 1);
        pc += SIZE_DOUBLE;
      }
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_DIVIDE_CONST_16):
      {
        register T_DOUBLE d = (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(double_sp - 1) == 0)
          runtime_error (11, pc, "DOUBLE divide by 0!");
        *(double_sp - 1) = d / *(double_sp - 1);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_POWER):
      --double_sp;
      if (*(double_sp - 1) == 0.0 && *double_sp < 0)
        runtime_error (11, pc, "DOUBLE 0.0 to negative power!");
      *(double_sp - 1) = pow (*(double_sp - 1), *double_sp);
    BCODE_END;

    BCODE_START(DOUBLE_POWER_CONST):
      {
        register T_DOUBLE d = *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
        if (*(double_sp - 1) == 0 && d < 0)
          runtime_error (11, pc, "DOUBLE 0 to negative power!");
        *(double_sp - 1) = pow (*(double_sp - 1), d);
        pc += SIZE_DOUBLE;
      }
    BCODE_END;

    BCODE_START(DOUBLE_POWER_CONST_16):
      {
        register T_DOUBLE d = (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (*(double_sp - 1) == 0 && d < 0)
          runtime_error (11, pc, "DOUBLE 0 to negative power!");
        *(double_sp - 1) = pow (*(double_sp - 1), d);
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_POWER):
      --double_sp;
      if (*double_sp == 0.0 && *(double_sp - 1) < 0)
        runtime_error (11, pc, "DOUBLE 0.0 to negative power!");
      *(double_sp - 1) = pow (*double_sp, *(double_sp - 1));
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_POWER_CONST):
      {
        register T_DOUBLE d = *((T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
        if (d == 0 && *(double_sp - 1) < 0)
          runtime_error (11, pc, "DOUBLE 0 to negative power!");
        *(double_sp - 1) = pow (d, *(double_sp - 1));
        pc += SIZE_DOUBLE;
      }
    BCODE_END;

    BCODE_START(DOUBLE_SWAP_POWER_CONST_16):
      {
        register T_DOUBLE d = (T_DOUBLE)*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
        if (d == 0 && *(double_sp - 1) < 0)
          runtime_error (11, pc, "DOUBLE 0 to negative power!");
        *(double_sp - 1) = pow (d, *(double_sp - 1));
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_SIN):
      *(double_sp - 1) = sin (*(double_sp - 1));
    BCODE_END;

    BCODE_START(DOUBLE_COS):
      *(double_sp - 1) = cos (*(double_sp - 1));
    BCODE_END;

    BCODE_START(DOUBLE_TAN):
      *(double_sp - 1) = tan (*(double_sp - 1));
    BCODE_END;

    BCODE_START(DOUBLE_ATN):
      *(double_sp - 1) = atan (*(double_sp - 1));
    BCODE_END;

    BCODE_START(DOUBLE_LOG):
      {
        register T_DOUBLE d;
        if ((d = *(double_sp - 1)) <= 0.0)
          runtime_error (5, pc, "DOUBLE log of negative or 0.0!");
        *(double_sp - 1) = log (d);
      }
    BCODE_END;

    BCODE_START(DOUBLE_LOG2):
      {
        register T_DOUBLE d;
        if ((d = *(double_sp - 1)) <= 0.0)
          runtime_error (5, pc, "DOUBLE log of negative or 0.0!");
        *(double_sp - 1) = log (d) / log (2.0);
      }
    BCODE_END;

    BCODE_START(DOUBLE_LOG10):
      {
        register T_DOUBLE d;
        if ((d = *(double_sp - 1)) <= 0.0)
          runtime_error (5, pc, "DOUBLE log of negative or 0.0!");
        *(double_sp - 1) = log (d) / log (10.0);
      }
    BCODE_END;

    BCODE_START(DOUBLE_EXP):
      *(double_sp - 1) = exp (*(double_sp - 1));
    BCODE_END;

    BCODE_START(DOUBLE_SQR):
      {
        register T_DOUBLE d;
        if ((d = *(double_sp - 1)) < 0.0)
          runtime_error (5, pc, "DOUBLE square root of negative number!");
        *(double_sp - 1) = sqrt (d);
      }
    BCODE_END;

    BCODE_START(DOUBLE_ABS):
      if (*(double_sp - 1) < 0)
        *(double_sp - 1) = - *(double_sp - 1);
    BCODE_END;

    BCODE_START(DOUBLE_INT):
      *(double_sp - 1) = (T_DOUBLE)floor((double)*(double_sp - 1));
    BCODE_END;

    BCODE_START(DOUBLE_CINT):
    *int_sp++ = (T_INT)floor((double)(*--double_sp + 0.5));
    BCODE_END;

    BCODE_START(DOUBLE_CLNG):
    *long_sp++ = (T_LONG)floor((double)(*--double_sp + 0.5));
    BCODE_END;

    BCODE_START(DOUBLE_CSNG):
      *single_sp++ = (T_SINGLE)*--double_sp;
    BCODE_END;

    BCODE_START(DOUBLE_FIX):
      *int_sp++ = (T_INT)*--double_sp;
    BCODE_END;

    BCODE_START(DOUBLE_SGN):
      {
        register T_DOUBLE d;
        if ((d = *--double_sp) > 0.0)
          *int_sp++ = 1;
        else if (d < 0.0)
          *int_sp++ = -1;
        else
          *int_sp++ = 0;
      }
    BCODE_END;

    BCODE_START(DOUBLE_EQUAL):
      double_sp -= 2;
      *int_sp++ = (T_INT) - (*(double_sp) == *(double_sp + 1));
    BCODE_END;

    BCODE_START(DOUBLE_EQUAL_CONST):
      *int_sp++ = (T_INT) - (*--double_sp ==
                             *(T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_EQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--double_sp ==
                             (T_DOUBLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_NOTEQUAL):
      double_sp -= 2;
      *int_sp++ = (T_INT) - (*(double_sp) != *(double_sp + 1));
    BCODE_END;

    BCODE_START(DOUBLE_NOTEQUAL_CONST):
      *int_sp++ = (T_INT) - (*--double_sp !=
                             *(T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_NOTEQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--double_sp !=
                             (T_DOUBLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_LESS):
      double_sp -= 2;
      *int_sp++ = (T_INT) - (*double_sp < *(double_sp + 1));
    BCODE_END;

    BCODE_START(DOUBLE_LESS_CONST):
      *int_sp++ = (T_INT) - (*--double_sp <
                             *(T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_LESS_CONST_16):
      *int_sp++ = (T_INT) - (*--double_sp <
                             (T_DOUBLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_GREATER):
      double_sp -= 2;
      *int_sp++ = (T_INT) - (*double_sp > *(double_sp + 1));
    BCODE_END;

    BCODE_START(DOUBLE_GREATER_CONST):
      *int_sp++ = (T_INT) - (*--double_sp >
                             *(T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_GREATER_CONST_16):
      *int_sp++ = (T_INT) - (*--double_sp >
                             (T_DOUBLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_LESSOREQUAL):
      double_sp -= 2;
      *int_sp++ = (T_INT) - (*double_sp <= *(double_sp + 1));
    BCODE_END;

    BCODE_START(DOUBLE_LESSOREQUAL_CONST):
      *int_sp++ = (T_INT) - (*--double_sp <=
                             *(T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_LESSOREQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--double_sp <=
                             (T_DOUBLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_GREATEROREQUAL):
      double_sp -= 2;
      *int_sp++ = (T_INT) - (*double_sp >= *(double_sp + 1));
    BCODE_END;

    BCODE_START(DOUBLE_GREATEROREQUAL_CONST):
      *int_sp++ = (T_INT) - (*--double_sp >=
                             *(T_DOUBLE *)ALIGN_UP(pc,ALIGN_DOUBLE));
      pc += SIZE_DOUBLE;
    BCODE_END;

    BCODE_START(DOUBLE_GREATEROREQUAL_CONST_16):
      *int_sp++ = (T_INT) - (*--double_sp >=
                             (T_DOUBLE)*(T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_INCREMENT):
      *(double_sp - 1) += 1.0;
    BCODE_END;

    BCODE_START(DOUBLE_INCREMENT_VAR):
      double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] += 1.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(DOUBLE_INCREMENT_VAR_16):
      double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] += 1.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(DOUBLE_DECREMENT):
      *(double_sp - 1) -= 1.0;
    BCODE_END;

    BCODE_START(DOUBLE_DECREMENT_VAR):
      double_vars[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))] -= 1.0;
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(DOUBLE_DECREMENT_VAR_16):
      double_vars[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))] -= 1.0;
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(STRING_PLUS):
      {
        register T_STRING str;
        string_sp--;
        str = alloc_string_check (pc, (*(string_sp - 1))->length +
                                  (*string_sp)->length, NULL);
        memcpy (str->body, (*(string_sp - 1))->body,
                (*(string_sp - 1))->length);
        memcpy (str->body + (*(string_sp - 1))->length, (*string_sp)->body,
                (*string_sp)->length);
        *string_sp = free_string (*string_sp);
        *(string_sp - 1) = free_string (*(string_sp - 1));
        *(string_sp - 1) = str;
      }
    BCODE_END;

    BCODE_START(STRING_EQUAL):
      string_sp -= 2;
      if ((*string_sp)->length == (*(string_sp + 1))->length &&
          memcmp ((*string_sp)->body, (*(string_sp + 1))->body,
                  (*string_sp)->length) == 0)
        *int_sp++ = -1;
      else
        *int_sp++ = 0;
      *(string_sp + 1) = free_string (*(string_sp + 1));
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(STRING_NOTEQUAL):
      string_sp -= 2;
      if ((*string_sp)->length == (*(string_sp + 1))->length &&
          memcmp ((*string_sp)->body, (*(string_sp + 1))->body,
                  (*string_sp)->length) == 0)
        *int_sp++ = 0;
      else
        *int_sp++ = -1;
      *(string_sp + 1) = free_string (*(string_sp + 1));
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(STRING_LESS):
      {
        register int i;
        register T_LONG j;
        register T_SIZE_T length;
        string_sp -= 2;
        if ((j = ((*string_sp)->length < (*(string_sp + 1))->length)))
          length = (*string_sp)->length;
        else
          length = (*(string_sp + 1))->length;
        if ((i = memcmp ((*string_sp)->body, (*(string_sp + 1))->body,
                         length)) == 0)
          if (j)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        else
          if (i < 0)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        *(string_sp + 1) = free_string (*(string_sp + 1));
        *string_sp = free_string (*string_sp);
      }
    BCODE_END;

    BCODE_START(STRING_GREATER):
      {
        register int i;
        register T_LONG j;
        register T_SIZE_T length;
        string_sp -= 2;
        if ((j = ((*string_sp)->length <= (*(string_sp + 1))->length)))
          length = (*string_sp)->length;
        else
          length = (*(string_sp + 1))->length;
        if ((i = memcmp ((*string_sp)->body, (*(string_sp + 1))->body,
                         length)) == 0)
          if (!j)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        else
          if (i > 0)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        *(string_sp + 1) = free_string (*(string_sp + 1));
        *string_sp = free_string (*string_sp);
      }
    BCODE_END;

    BCODE_START(STRING_LESSOREQUAL):
      {
        register int i;
        register T_LONG j;
        register T_SIZE_T length;
        string_sp -= 2;
        if ((j = ((*string_sp)->length <= (*(string_sp + 1))->length)))
          length = (*string_sp)->length;
        else
          length = (*(string_sp + 1))->length;
        if ((i = memcmp ((*string_sp)->body, (*(string_sp + 1))->body,
                         length)) == 0)
          if (j)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        else
          if (i < 0)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        *(string_sp + 1) = free_string (*(string_sp + 1));
        *string_sp = free_string (*string_sp);
      }
    BCODE_END;

    BCODE_START(STRING_GREATEROREQUAL):
      {
        register int i;
        register T_LONG j;
        register T_SIZE_T length;
        string_sp -= 2;
        if ((j = ((*string_sp)->length < (*(string_sp + 1))->length)))
          length = (*string_sp)->length;
        else
          length = (*(string_sp + 1))->length;
        if ((i = memcmp ((*string_sp)->body, (*(string_sp + 1))->body,
                         length)) == 0)
          if (!j)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        else
          if (i > 0)
            *int_sp++ = -1;
          else
            *int_sp++ = 0;
        *(string_sp + 1) = free_string (*(string_sp + 1));
        *string_sp = free_string (*string_sp);
      }
    BCODE_END;

    BCODE_START(STRING_LEN):
      *long_sp++ = (*--string_sp)->length;
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(STRING_LEFT):
      {
        register T_LONG l, l1;
        register T_STRING str;
        l = (*(string_sp - 1))->length;
        l1 = *--long_sp;
        if (l1 < 0)
          runtime_error (9, pc, "String index out of bounds error");
        if (l1 == 0) {
          str = alloc_string_check (pc, 0, NULL);
        } else {
          if (l1 > l)
            l1 = l;
          str = alloc_string_check (pc, (size_t)l1, (*(string_sp - 1))->body);
        }
        *(string_sp - 1) = free_string (*(string_sp - 1));
        *(string_sp - 1) = str;
      }
    BCODE_END;

    BCODE_START(STRING_MID2):
      {
        register T_LONG l, l1;
        register T_STRING str;
        l = (*(string_sp - 1))->length;
        l1 = *--long_sp;
        if (l1 <= 0)
          runtime_error (9, pc, "String index out of bounds error");
        if (l1 > l) {
          str = alloc_string_check (pc, 0, NULL);
        } else {
          str = alloc_string_check (pc, (size_t)(l - l1 + 1),
                                    &(*(string_sp - 1))->body[l1 - 1]);
        }
        *(string_sp - 1) = free_string (*(string_sp - 1));
        *(string_sp - 1) = str;
      }
    BCODE_END;

    BCODE_START(STRING_MID3):
      {
        register T_LONG l, l1;
        register T_STRING str;
        long_sp -= 2;
        l = (*(string_sp - 1))->length;
        l1 = *(long_sp + 1);    /* 3rd arg */
        if (*long_sp <= 0 || l1 < 0)
          runtime_error (9, pc, "String index out of bounds error");
        if (*long_sp > l || l1 == 0) {
          str = alloc_string_check (pc, 0, NULL);
        } else {
          if (l1 > l - *long_sp + 1)
            l1 = l - *long_sp + 1;
          str = alloc_string_check (pc, (size_t)l1,
                                    &(*(string_sp - 1))->body[*long_sp - 1]);
        }
        *(string_sp - 1) = free_string (*(string_sp - 1));
        *(string_sp - 1) = str;
      }
    BCODE_END;

    BCODE_START(STRING_RIGHT):
      {
        register T_LONG l, l1;
        register T_STRING str;
        l = (*(string_sp - 1))->length;
        l1 = *--long_sp;
        if (l1 < 0)
          runtime_error (9, pc, "String index out of bounds error");
        if (l1 == 0) {
          str = alloc_string_check (pc, 0, NULL);
        } else {
          if (l1 > l)
            l1 = l;
          str = alloc_string_check (pc, (size_t)l1,
                                    &(*(string_sp - 1))->body[l - l1]);
        }
        *(string_sp - 1) = free_string (*(string_sp - 1));
        *(string_sp - 1) = str;
      }
    BCODE_END;

    BCODE_START(STRING_INSTR2):
      string_sp -= 2;
      *long_sp = 0;
      if ((*(string_sp + 1))->length == 0)
        *long_sp = 1;
      else if ((*string_sp)->length >= (*(string_sp + 1))->length) {
        register T_SIZE_T t;
        for (t = 0; t <= (*string_sp)->length - (*(string_sp + 1))->length;
             t++)
          if (memcmp (&(*string_sp)->body[t], (*(string_sp + 1))->body,
                      (*(string_sp + 1))->length) == 0) {
            *long_sp = t + 1;
            break;
          }
      }
      *(string_sp + 1) = free_string (*(string_sp + 1));
      *string_sp = free_string (*string_sp);
      long_sp++;
    BCODE_END;

    BCODE_START(STRING_INSTR3):
      {
        register T_LONG l;
        string_sp -= 2;
        if (*(long_sp - 1) <= 0)
          runtime_error (9, pc, "String index out of bounds error");
        l = 0;
        if ((*(string_sp + 1))->length == 0)
          l = *(long_sp - 1);
        else if ((*string_sp)->length >= *(long_sp - 1) &&
                 (*string_sp)->length - *(long_sp - 1) + 1 >=
                 (*(string_sp + 1))->length) {
          register T_SIZE_T t;
          for (t = *(long_sp - 1) - 1;
               t <= (*string_sp)->length - (*(string_sp + 1))->length; t++)
            if (memcmp (&(*string_sp)->body[t], (*(string_sp + 1))->body,
                        (*(string_sp + 1))->length) == 0) {
              l = t + 1;
              break;
            }
        }
        *(string_sp + 1) = free_string (*(string_sp + 1));
        *string_sp = free_string (*string_sp);
        *(long_sp - 1) = l;
      }
    BCODE_END;

    BCODE_START(STRING_RINSTR2):
      string_sp -= 2;
      *long_sp = 0;
      if ((*(string_sp + 1))->length == 0)
        *long_sp = 1;
      else if ((*string_sp)->length >= (*(string_sp + 1))->length) {
        register T_SIZE_T t;
        for (t = (*string_sp)->length - (*(string_sp + 1))->length + 1; t > 0;
             t--)
          if (memcmp (&(*string_sp)->body[t - 1], (*(string_sp + 1))->body,
                      (*(string_sp + 1))->length) == 0) {
            *long_sp = t;
            break;
          }
      }
      *(string_sp + 1) = free_string (*(string_sp + 1));
      *string_sp = free_string (*string_sp);
      long_sp++;
    BCODE_END;

    BCODE_START(STRING_RINSTR3):
      {
        register T_LONG l;
        string_sp -= 2;
        if (*(long_sp - 1) <= 0)
          runtime_error (9, pc, "String index out of bounds error");
        l = 0;
        if ((*(string_sp + 1))->length == 0)
          l = *(long_sp - 1);
        else if ((*string_sp)->length >= *(long_sp - 1) &&
                 (*string_sp)->length - *(long_sp - 1) + 1 >=
                 (*(string_sp + 1))->length) {
          register T_SIZE_T t;
          for (t = (*string_sp)->length - (*(string_sp + 1))->length + 1;
               t > *(long_sp - 1) - 1; t--)
            if (memcmp (&(*string_sp)->body[t - 1], (*(string_sp + 1))->body,
                        (*(string_sp + 1))->length) == 0) {
              l = t;
              break;
            }
        }
        *(string_sp + 1) = free_string (*(string_sp + 1));
        *string_sp = free_string (*string_sp);
        *(long_sp - 1) = l;
      }
    BCODE_END;

    BCODE_START(STRING_CHR):
      {
        register T_STRING str;
        str = alloc_string_check (pc, 1, NULL);
        str->body[0] = (char)*--int_sp;
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_ASC):
      if ((*--string_sp)->length > 0)
        *int_sp++ = (T_INT)(*string_sp)->body[0];
      else
        *int_sp++ = 0;
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(STRING_VAL):
      {
        register T_SIZE_T length;
        --string_sp;
        if ((*string_sp)->length < 255)
          length = (*string_sp)->length;
        else
          length = 255;
        memcpy (tmp_string, (*string_sp)->body, length);
        tmp_string[length] = '\0';
        *double_sp = 0.0;
        if (tmp_string[0] == '&' &&
            (tmp_string[1] == 'h' || tmp_string[1] == 'H')) {
          T_LONG l;
          sscanf (&tmp_string[2], "%lx", &l);
          *double_sp = (T_DOUBLE)l;
        } else
          sscanf (tmp_string, "%lf", double_sp);
        double_sp++;
        *string_sp = free_string (*string_sp);
      }
    BCODE_END;

    BCODE_START(STRING_STR_INT):
      sprintf (tmp_string, "% d", *--int_sp);
      *string_sp++ = alloc_string_check (pc, strlen(tmp_string), tmp_string);
    BCODE_END;

    BCODE_START(STRING_STR_LONG):
      sprintf (tmp_string, "% ld", *--long_sp);
      *string_sp++ = alloc_string_check (pc, strlen(tmp_string), tmp_string);
    BCODE_END;

    BCODE_START(STRING_STR_SINGLE):
      single_to_string (tmp_string, (double)*--single_sp);
      *string_sp++ = alloc_string_check (pc, strlen(tmp_string), tmp_string);
    BCODE_END;

    BCODE_START(STRING_STR_DOUBLE):
      double_to_string (tmp_string, *--double_sp);
      *string_sp++ = alloc_string_check (pc, strlen(tmp_string), tmp_string);
    BCODE_END;

    BCODE_START(STRING_DATE):
      {
        time_t tt;
        register struct tm *tm;
        register T_STRING str;
        str = alloc_string_check (pc, 11, NULL);
        str->length = 10;
        time (&tt);
        tm = localtime (&tt);
        sprintf (str->body, "%02d-%02d-%04d", 1 + tm->tm_mon, tm->tm_mday,
                 1900 + tm->tm_year);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_TIME):
      {
        time_t tt;
        register struct tm *tm;
        register T_STRING str;
        str = alloc_string_check (pc, 9, NULL);
        str->length = 8;
        time (&tt);
        tm = localtime (&tt);
        sprintf (str->body, "%02d:%02d:%02d", tm->tm_hour, tm->tm_min,
                 tm->tm_sec);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_COMMAND):
      {
        register T_INT len = 0, i;
        register T_STRING str;
        for (i = 1; i < argc; i++)
          len += strlen(argv[i]) + 1;
        if (len == 0) {
          str = alloc_string_check (pc, 0, NULL);
        } else {
          char *s;
          if ((s = my_malloc(len + 1)) == NULL)
            runtime_error (14, pc, "Out of memory for string!");
          s[0] = '\0';
          for (i = 1; i < argc; i++) {
            strcat (s, argv[i]);
            strcat (s, " ");
          }
          s[strlen(s) - 1] = '\0';  /* delete trailing space */
          str = alloc_string_check (pc, strlen(s), s);
          s = my_free (s);
        }
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_COMMAND1):
      {
        register T_INT i;
        register T_STRING str;
        if ((i = *--int_sp) < 0 || i >= argc) {
          str = alloc_string_check (pc, 0, NULL);
        } else
          str = alloc_string_check (pc, strlen(argv[i]), argv[i]);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TIMER):
      *double_sp++ = (T_DOUBLE)get_timer ();
    BCODE_END;

    BCODE_START(EOF):
      {
        register int c;
        check_open_for_input (pc, secondary_file);
        c = tty_getc (secondary_file->file);
        if (c == EOF || feof (secondary_file->file) != 0)
          *int_sp++ = (T_INT)-1;
        else
          *int_sp++ = (T_INT)0;
        ungetc (c, secondary_file->file);
      }
    BCODE_END;

    BCODE_START(LOF):
      {
        register long int apos;
        check_open_for_input (pc, secondary_file);
        if ((apos = ftell (secondary_file->file)) == -1 ||
            fseek (secondary_file->file, 0, SEEK_END) == -1 ||
            (*long_sp++ = (T_LONG)ftell (secondary_file->file)) == -1 ||
            fseek (secondary_file->file, apos, SEEK_SET) == -1)
          runtime_error (57, pc, "Error determining size of input file!");
      }
    BCODE_END;

    BCODE_START(POS):
      check_open_for_output (pc, secondary_file);
      *long_sp++ = (T_LONG)secondary_file->printpos;
    BCODE_END;

    BCODE_START(STRING_MKI):
      {
        register T_STRING str;
        str = alloc_string_check (pc, SIZE_INT, NULL);
        str->length = SIZE_INT;
        *(T_INT *)str->body = *--int_sp;
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_MKL):
      {
        register T_STRING str;
        str = alloc_string_check (pc, SIZE_LONG, NULL);
        str->length = SIZE_LONG;
        *(T_LONG *)str->body = *--long_sp;
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_MKS):
      {
        register T_STRING str;
        str = alloc_string_check (pc, SIZE_SINGLE, NULL);
        str->length = SIZE_SINGLE;
        *(T_SINGLE *)str->body = *--single_sp;
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_MKD):
      {
        register T_STRING str;
        str = alloc_string_check (pc, SIZE_DOUBLE, NULL);
        str->length = SIZE_DOUBLE;
        *(T_DOUBLE *)str->body = *--double_sp;
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(INT_CVI):
      {
        register T_STRING str;
        str = *--string_sp;
        if (str->length < SIZE_INT)
          runtime_error (5, pc, "String too short for CVI");
        *int_sp++ = *(T_INT *)str->body;
        str = free_string (str);
      }
    BCODE_END;

    BCODE_START(LONG_CVL):
      {
        register T_STRING str;
        str = *--string_sp;
        if (str->length < SIZE_LONG)
          runtime_error (5, pc, "String too short for CVL");
        *long_sp++ = *(T_LONG *)str->body;
        str = free_string (str);
      }
    BCODE_END;

    BCODE_START(SINGLE_CVS):
      {
        register T_STRING str;
        str = *--string_sp;
        if (str->length < SIZE_SINGLE)
          runtime_error (5, pc, "String too short for CVS");
        *single_sp++ = *(T_SINGLE *)str->body;
        str = free_string (str);
      }
    BCODE_END;

    BCODE_START(DOUBLE_CVD):
      {
        register T_STRING str;
        str = *--string_sp;
        if (str->length < SIZE_DOUBLE)
          runtime_error (5, pc, "String too short for CVD");
        *double_sp++ = *(T_DOUBLE *)str->body;
        str = free_string (str);
      }
    BCODE_END;

    BCODE_START(STRING_BIN):
      runtime_error (73, pc, "BIN$ not implemented yet!");
    BCODE_END;

    BCODE_START(STRING_OCT):
      {
        register T_STRING str;
        sprintf (tmp_string, "%lo", *--long_sp);
        str = alloc_string_check (pc, strlen(tmp_string), tmp_string);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_HEX):
      {
        register T_STRING str;
        sprintf (tmp_string, "%lX", *--long_sp);
        str = alloc_string_check (pc, strlen(tmp_string), tmp_string);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_LCASE):
      {
        register T_STRING str;
        register T_SIZE_T t;
        register T_SIZE_T length = (*(string_sp - 1))->length;
        str = alloc_string_check (pc, length, NULL);
        for (t = 0; t < length; t++)
          str->body[t] = tolower ((*(string_sp - 1))->body[t]);
        *(string_sp - 1) = free_string (*(string_sp - 1));
        *(string_sp - 1) = str;
      }
    BCODE_END;

    BCODE_START(STRING_UCASE):
      {
        register T_STRING str;
        register T_SIZE_T t;
        register T_SIZE_T length = (*(string_sp - 1))->length;
        str = alloc_string_check (pc, length, NULL);
        for (t = 0; t < length; t++)
          str->body[t] = toupper ((*(string_sp - 1))->body[t]);
        *(string_sp - 1) = free_string (*(string_sp - 1));
        *(string_sp - 1) = str;
      }
    BCODE_END;

    BCODE_START(STRING_TRIM):
      {
        register T_LONG l;
        register T_STRING str = *(string_sp - 1);
        register T_SIZE_T length = str->length;
        char c;
        while (length > 0 &&
               ((c = str->body[length - 1]) == ' ' ||
                c == '\t'))
          --length;
        for (l = 0; l < length; l++)
          if ((c = str->body[l]) != ' ' &&
              c != '\t')
            break;
        str = alloc_string_check (pc, length - l, &str->body[l]);
        --string_sp;
        *string_sp = free_string (*string_sp);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_LTRIM):
      {
        register T_LONG l;
        register T_STRING str = *(string_sp - 1);
        register T_SIZE_T length = str->length;
        register char c;
        for (l = 0; l < length; l++)
          if ((c = str->body[l]) != ' ' &&
              c != '\t')
            break;
        str = alloc_string_check (pc, length - l, &str->body[l]);
        --string_sp;
        *string_sp = free_string (*string_sp);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_RTRIM):
      {
        register T_STRING str = *(string_sp - 1);
        register T_SIZE_T length = str->length;
        register char c;
        while (length > 0 &&
               ((c = str->body[length - 1]) == ' ' ||
                c == '\t'))
          --length;
        str = alloc_string_check (pc, length, str->body);
        --string_sp;
        *string_sp = free_string (*string_sp);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_STRING):
      {
        register T_STRING str;
        str = alloc_string_check (pc, *--long_sp, NULL);
        if (*long_sp > 0)
          memset (str->body, (*(string_sp - 1))->body[0], *long_sp);
        --string_sp;
        *string_sp = free_string (*string_sp);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_STRING2):
      {
        register T_STRING str;
        str = alloc_string_check (pc, *--long_sp, NULL);
        if (*long_sp > 0)
          memset (str->body, *--int_sp, *long_sp);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_SPACE):
      {
        register T_STRING str;
        register T_SIZE_T length = *--long_sp;
        str = alloc_string_check (pc, length, NULL);
        str->length = length;
        memset (str->body, ' ', length);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(STRING_GETENV):
      {
        register T_STRING str = *--string_sp;
        register T_SIZE_T length;
        register char *ps;
        if (str->length < 255)
          length = str->length;
        else
          length = 255;
        memcpy (tmp_string, str->body, length);
        tmp_string[length] = '\0';
        str = free_string (str);
        ps = getenv (tmp_string);
        str = alloc_string_check (pc, (ps == NULL) ? 0 : strlen(ps), ps);
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(LONG_LBOUND):
      runtime_error (73, pc, "LBOUND not implemented yet!");
    BCODE_END;

    BCODE_START(LONG_UBOUND):
      runtime_error (73, pc, "UBOUND not implemented yet!");
    BCODE_END;

    BCODE_START(INDEX_PLUS):
      index_sp--;
      *(index_sp - 1) += *index_sp;
    BCODE_END;

    BCODE_START(INDEX_TIMES_CONST):
      *(index_sp - 1) *= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T));
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(INDEX_TIMES_CONST_16):
      *(index_sp - 1) *= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT));
      pc += SIZE_SHORT;
    BCODE_END;

    BCODE_START(INT_TO_LONG):
      *long_sp++ = (T_LONG)*--int_sp;
    BCODE_END;

    BCODE_START(INT_TO_SINGLE):
      *single_sp++ = (T_SINGLE)*--int_sp;
    BCODE_END;

    BCODE_START(INT_TO_DOUBLE):
      *double_sp++ = (T_DOUBLE)*--int_sp;
    BCODE_END;

    BCODE_START(INT_TO_INDEX):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--int_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(INT_TO_INDEX_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--int_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_TO_INDEX_PLUS):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--int_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(INT_TO_INDEX_PLUS_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--int_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_TO_INDEX_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--int_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(INT_TO_INDEX_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--int_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(INT_TO_INDEX_PLUS_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--int_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(INT_TO_INDEX_PLUS_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--int_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INT):
      *int_sp++ = (T_INT)*--long_sp;
    BCODE_END;

    BCODE_START(LONG_TO_SINGLE):
      *single_sp++ = (T_SINGLE)*--long_sp;
    BCODE_END;

    BCODE_START(LONG_TO_DOUBLE):
      *double_sp++ = (T_DOUBLE)*--long_sp;
    BCODE_END;

    BCODE_START(LONG_TO_INDEX):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--long_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INDEX_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--long_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INDEX_PLUS):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--long_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INDEX_PLUS_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--long_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INDEX_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--long_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INDEX_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--long_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INDEX_PLUS_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--long_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(LONG_TO_INDEX_PLUS_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--long_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INT):
    *int_sp++ = (T_INT)floor((double)(*--single_sp + 0.5f));
    BCODE_END;

    BCODE_START(SINGLE_TO_LONG):
    *long_sp++ = (T_LONG)floor((double)(*--single_sp + 0.5f));
    BCODE_END;

    BCODE_START(SINGLE_TO_DOUBLE):
      *double_sp++ = (T_DOUBLE)*--single_sp;
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--single_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--single_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX_PLUS):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--single_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX_PLUS_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--single_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--single_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--single_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX_PLUS_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--single_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(SINGLE_TO_INDEX_PLUS_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--single_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INT):
    *int_sp++ = (T_INT)floor((double)(*--double_sp + 0.5));
    BCODE_END;

    BCODE_START(DOUBLE_TO_LONG):
    *long_sp++ = (T_LONG)floor((double)(*--double_sp + 0.5));
    BCODE_END;

    BCODE_START(DOUBLE_TO_SINGLE):
      *single_sp++ = (T_SINGLE)*--double_sp;
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--double_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = (T_SIZE_T)*--double_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX_PLUS):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--double_sp)) < 0 ||
            index > *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX_PLUS_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = (T_SIZE_T)*--double_sp)) < 0 ||
            index > *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--double_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*index_sp++ = (index = ((T_SIZE_T)*--double_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX_PLUS_MINUS_1):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--double_sp) - 1)) < 0 ||
            index >= *((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(DOUBLE_TO_INDEX_PLUS_MINUS_1_16):
      {
        register T_SIZE_T index;
        if ((*(index_sp - 1) += (index = ((T_SIZE_T)*--double_sp) - 1)) < 0 ||
            index >= *((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT)))
          runtime_error (9, pc, "Array index out of bounds error");
        pc += SIZE_SHORT;
      }
    BCODE_END;

    BCODE_START(JUMP):
      {
        unsigned char *npc = &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_16):
      {
        unsigned char *npc = &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_INT_EQUAL_ZERO):
      if (*--int_sp == 0) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_INT_EQUAL_ZERO_16):
      if (*--int_sp == 0) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_INT_NOTEQUAL_ZERO):
      if (*--int_sp != 0) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_INT_NOTEQUAL_ZERO_16):
      if (*--int_sp != 0) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_LONG_EQUAL_ZERO):
      if (*--long_sp == 0L) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_LONG_EQUAL_ZERO_16):
      if (*--long_sp == 0L) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_LONG_NOTEQUAL_ZERO):
      if (*--long_sp != 0L) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_LONG_NOTEQUAL_ZERO_16):
      if (*--long_sp != 0L) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_SINGLE_EQUAL_ZERO):
      if (*--single_sp == 0.0f) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_SINGLE_EQUAL_ZERO_16):
      if (*--single_sp == 0.0f) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_SINGLE_NOTEQUAL_ZERO):
      if (*--single_sp != 0.0f) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_SINGLE_NOTEQUAL_ZERO_16):
      if (*--single_sp != 0.0f) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_DOUBLE_EQUAL_ZERO):
      if (*--double_sp == 0.0) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_DOUBLE_EQUAL_ZERO_16):
      if (*--double_sp == 0.0) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_DOUBLE_NOTEQUAL_ZERO):
      if (*--double_sp != 0.0) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_DOUBLE_NOTEQUAL_ZERO_16):
      if (*--double_sp != 0.0) {
        register unsigned char *npc =
          &bcodes[*((T_SHORT *)ALIGN_UP(pc,ALIGN_SHORT))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SHORT) + SIZE_SHORT;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_INT_LESS):
      int_sp -= 2;
      if (*int_sp < *(int_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_LONG_LESS):
      long_sp -= 2;
      if (*long_sp < *(long_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_SINGLE_LESS):
      single_sp -= 2;
      if (*single_sp < *(single_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_DOUBLE_LESS):
      double_sp -= 2;
      if (*double_sp < *(double_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_INT_GREATER):
      int_sp -= 2;
      if (*int_sp > *(int_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_LONG_GREATER):
      long_sp -= 2;
      if (*long_sp > *(long_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_SINGLE_GREATER):
      single_sp -= 2;
      if (*single_sp > *(single_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(JUMP_DOUBLE_GREATER):
      double_sp -= 2;
      if (*double_sp > *(double_sp + 1)) {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      } else {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(GOSUB):
      if ((gosub_sp - gosub_stack) >= STACK_SIZE)
        runtime_error (7, pc, "GOSUBs nested too deep!");
      *gosub_sp++ = (pc - bcodes);
      {
        register unsigned char *npc =
          &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      }
    BCODE_END;

    BCODE_START(ON_GOTO):
      {
        register T_LONG l, l1;
        l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        pc += SIZE_LONG;
        l1 = *--long_sp - 1;
        if (l1 >= 0 && l1 < l) {
          register unsigned char *npc;
          pc += l1 * SIZE_SIZE_T;
          npc = &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
          pc = npc;
        } else
          pc += l * SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(ON_GOSUB):
      {
        register T_LONG l, l1;
        l = *((T_LONG *)ALIGN_UP(pc,ALIGN_LONG));
        pc += SIZE_LONG;
        l1 = *--long_sp - 1;
        if (l1 >= 0 && l1 < l) {
          register unsigned char *npc;
          if ((gosub_sp - gosub_stack) >= STACK_SIZE)
            runtime_error (7, pc, "GOSUBs nested too deep!");
          *gosub_sp++ = (pc + (l - 1) * SIZE_SIZE_T - bcodes);
          pc += l1 * SIZE_SIZE_T;
          npc = &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
          pc = npc;
        } else
          pc += l * SIZE_SIZE_T;
      }
    BCODE_END;

    BCODE_START(ON_ERROR_GOTO):
      saved_on_error_pc = NULL;
      on_error_pc = &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(ON_ERROR_GOTO_0):
      if (resume_pc != NULL) { /* if we are already in an ON ERROR handler */
        print_runtime_error (f, err, &xref_record);
        goto done;
      } else {
        saved_on_error_pc = NULL;
        on_error_pc = NULL;
      }
    BCODE_END;

    BCODE_START(RESUME):
      on_error_pc = saved_on_error_pc;
      saved_on_error_pc = NULL;
      if (resume_pc == NULL)
        runtime_error (20, pc, "RESUME without error");
      err = 0;
      erl = 0;
      pc = resume_pc;
      resume_pc = NULL;
      resume_next_pc = NULL;
    BCODE_END;

    BCODE_START(RESUME_LABEL):
      on_error_pc = saved_on_error_pc;
      saved_on_error_pc = NULL;
      if (resume_pc == NULL)
        runtime_error (20, pc, "RESUME line-number|label without error");
      err = 0;
      erl = 0;
      {
        unsigned char *npc = &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      }
      resume_pc = NULL;
      resume_next_pc = NULL;
    BCODE_END;

    BCODE_START(RESUME_NEXT):
      on_error_pc = saved_on_error_pc;
      saved_on_error_pc = NULL;
      if (resume_next_pc == NULL)
        runtime_error (20, pc, "RESUME NEXT without error");
      err = 0;
      erl = 0;
      pc = resume_next_pc;
      resume_pc = NULL;
      resume_next_pc = NULL;
    BCODE_END;

    BCODE_START(ERR):
      *int_sp++ = err;
    BCODE_END;

    BCODE_START(ERL):
      *int_sp++ = erl;
    BCODE_END;

    BCODE_START(ERROR):
      {
        register T_INT i;
        i = *--int_sp;
        runtime_error (i, pc, "\"ERROR %d\" statement executed", i);
      }
    BCODE_END;

    BCODE_START(RETURN):
      if (gosub_sp <= gosub_stack)
        runtime_error (3, pc, "RETURN without GOSUB!");
      pc = &bcodes[*--gosub_sp];
      {
        register unsigned char *npc = ALIGN_UP(pc,ALIGN_SIZE_T) + SIZE_SIZE_T;
        pc = npc;
      }
    BCODE_END;

    BCODE_START(RETURN_LINE):
      if (gosub_sp <= gosub_stack)
        runtime_error (3, pc, "RETURN without GOSUB!");
      --gosub_sp;
      {
        register unsigned char *npc = &bcodes[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
        pc = npc;
      }
    BCODE_END;

    BCODE_START(RANDOMIZE_TIMER):
      srand (((unsigned int)clock()) ^ (unsigned int)time(NULL));
    BCODE_END;

    BCODE_START(RANDOMIZE_LONG):
      srand ((unsigned int)*--long_sp);
    BCODE_END;

    BCODE_START(OPEN):
      {
        register T_SIZE_T length;
        if (primary_file->file_state != FILE_CLOSED)
          runtime_error (55, pc, "Attempt to open already opened file!");
        --string_sp;
        if ((*string_sp)->length < 255)
          length = (*string_sp)->length;
        else
          length = 255;
        memcpy (tmp_string, (*string_sp)->body, length);
        tmp_string[length] = '\0';
        *string_sp = free_string (*string_sp);
        primary_file->file_state = (enum file_state_type)*--int_sp;
        primary_file->printpos = 0;
        switch (primary_file->file_state) {
        case FILE_OPEN_INPUT:
          primary_file->file = fopen (tmp_string, "r");
          break;
        case FILE_OPEN_OUTPUT:
          primary_file->file = fopen (tmp_string, "w");
          break;
        case FILE_OPEN_APPEND:
          primary_file->file = fopen (tmp_string, "a");
          break;
        default:
          break;
        }
        if (primary_file->file == NULL)
          runtime_error (53, pc, "Error opening file %s!", tmp_string);
      }
    BCODE_END;

    BCODE_START(CLOSE):
      if (primary_file->file_state == FILE_CLOSED)
        runtime_error (53, pc, "Attempt to close already closed file!");
      fclose (primary_file->file);
      primary_file->file = NULL;
      primary_file->file_state = FILE_CLOSED;
    BCODE_END;

    BCODE_START(KILL):
      {
        register T_SIZE_T length;
        if ((*--string_sp)->length < 255)
          length = (*string_sp)->length;
        else
          length = 255;
        memcpy (tmp_string, (*string_sp)->body, length);
        tmp_string[length] = '\0';
        *string_sp = free_string (*string_sp);
        if (remove (tmp_string) != 0)
          runtime_error (53, pc, "KILL \"%s\" failed", tmp_string);
      }
    BCODE_END;

    BCODE_START(NAME):
      {
        register T_SIZE_T length;
        register char *tmp_string2;
        if ((*--string_sp)->length < 255)
          length = (*string_sp)->length;
        else
          length = 255;
        if ((tmp_string2 = my_malloc (TMP_STRING_LENGTH)) == NULL)
          runtime_error (14, pc, "Out of memory for string!");
        memcpy (tmp_string2, (*string_sp)->body, length);
        tmp_string2[length] = '\0';
        *string_sp = free_string (*string_sp);
        if ((*--string_sp)->length < 255)
          length = (*string_sp)->length;
        else
          length = 255;
        memcpy (tmp_string, (*string_sp)->body, length);
        tmp_string[length] = '\0';
        *string_sp = free_string (*string_sp);
        if (rename (tmp_string, tmp_string2) != 0)
          runtime_error (53, pc, "NAME \"%s\" AS \"%s\" failed", tmp_string,
                         tmp_string2);
        tmp_string2 = my_free (tmp_string2);
      }
    BCODE_END;

    BCODE_START(CHDIR):
      if (change_dir (*--string_sp) != 0)
        runtime_error (76, pc, "CHDIR failed");
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(MKDIR):
      if (make_dir (*--string_sp) != 0)
        runtime_error (76, pc, "MKDIR failed");
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(RMDIR):
      if (change_dir (*--string_sp) != 0)
        runtime_error (76, pc, "RMDIR failed");
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(SHELL):
      shell_cmd (*--string_sp); /* should this check for error return? */
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(CHAIN):
      --string_sp;
      if ((chain_fname = my_malloc((*string_sp)->length + 5)) == NULL)
        runtime_error (14, pc, "Out of memory for string!");
      memcpy(chain_fname, (*string_sp)->body, (*string_sp)->length);
      chain_fname[(*string_sp)->length] = '\0';
      *string_sp = free_string (*string_sp);
      goto done;
    BCODE_END;

    BCODE_START(PRINT_INT):
      check_open_for_output (pc, primary_file);
      sprintf (tmp_string, "% d ", *--int_sp);
      fputs (tmp_string, primary_file->file);
      primary_file->printpos += strlen(tmp_string);
    BCODE_END;

    BCODE_START(PRINT_LONG):
      check_open_for_output (pc, primary_file);
      sprintf (tmp_string, "% ld ", *--long_sp);
      fputs (tmp_string, primary_file->file);
      primary_file->printpos += strlen(tmp_string);
    BCODE_END;

    BCODE_START(PRINT_SINGLE):
      check_open_for_output (pc, primary_file);
      single_to_string (tmp_string, (double)*--single_sp);
      fprintf (primary_file->file, "%s ", tmp_string);
      primary_file->printpos += strlen(tmp_string) + 1;
    BCODE_END;

    BCODE_START(PRINT_DOUBLE):
      check_open_for_output (pc, primary_file);
      double_to_string (tmp_string, *--double_sp);
      fprintf (primary_file->file, "%s ", tmp_string);
      primary_file->printpos += strlen(tmp_string) + 1;
    BCODE_END;

    BCODE_START(PRINT_STRING):
      check_open_for_output (pc, primary_file);
      --string_sp;
      if (fwrite ((*string_sp)->body, 1, (*string_sp)->length,
                   primary_file->file) != (*string_sp)->length)
        runtime_error (25, pc, "Error writing string");
      primary_file->printpos += (*string_sp)->length;
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(PRINT_COMMA):
      check_open_for_output (pc, primary_file);
      do {
        putc (' ', primary_file->file);
      } while ((++primary_file->printpos & 7) != 0);
    BCODE_END;

    BCODE_START(PRINT_TAB):
      {
        register T_LONG l;
        check_open_for_output (pc, primary_file);
        l = *--long_sp - 1;
        if (l < 0)
          l = 0;
        if (primary_file->printpos > l) {
          putc ('\n', primary_file->file);
          primary_file->printpos = 0;
        }
        while (primary_file->printpos < l) {
          putc (' ', primary_file->file);
          primary_file->printpos++;
        }
      }
    BCODE_END;

    BCODE_START(PRINT_SPC):
      {
        register T_LONG l;
        check_open_for_output (pc, primary_file);
        for (l = *--long_sp; l > 0; l--) {
          putc (' ', primary_file->file);
          primary_file->printpos++;
        }
      }
    BCODE_END;

    BCODE_START(PRINT_NEWLINE):
      check_open_for_output (pc, primary_file);
      putc ('\n', primary_file->file);
      primary_file->printpos = 0;
    BCODE_END;

    BCODE_START(PRINT_END):
      check_open_for_output (pc, primary_file);
      fflush (primary_file->file);
    BCODE_END;

    BCODE_START(PRINT_USING_INIT):
      {
        register T_SIZE_T length;
        check_open_for_output (pc, primary_file);
        --string_sp;
        length = (*string_sp)->length;
        if (length <= 0)
          runtime_error (5, pc, "Invalid PRINT USING format string");
        if (using_fmt != NULL) {
          using_fmt = my_free (using_fmt);
          using_fmt = NULL;
        }
        if ((using_fmt = (char *)my_malloc (length + 1)) == NULL)
          runtime_error (14, pc, "Out of memory for PRINT USING format string");
        memcpy (using_fmt, (*string_sp)->body, length);
        using_fmt[length] = '\0';
        using_fmt_pos = 0;
        *string_sp = free_string (*string_sp);
      }
    BCODE_END;

    BCODE_START(PRINT_USING_INT):
      fmt = parse_fmt (pc, using_fmt, &using_fmt_pos, primary_file);
      print_using_number (pc, (double)*--int_sp, fmt, primary_file);
    BCODE_END;

    BCODE_START(PRINT_USING_LONG):
      fmt = parse_fmt (pc, using_fmt, &using_fmt_pos, primary_file);
      print_using_number (pc, (double)*--long_sp, fmt, primary_file);
    BCODE_END;

    BCODE_START(PRINT_USING_SINGLE):
      fmt = parse_fmt (pc, using_fmt, &using_fmt_pos, primary_file);
      print_using_number (pc, (double)*--single_sp, fmt, primary_file);
    BCODE_END;

    BCODE_START(PRINT_USING_DOUBLE):
      fmt = parse_fmt (pc, using_fmt, &using_fmt_pos, primary_file);
      print_using_number (pc, *--double_sp, fmt, primary_file);
    BCODE_END;

    BCODE_START(PRINT_USING_STRING):
      {
        register T_SIZE_T length;
        check_open_for_output (pc, primary_file);
        --string_sp;
        fmt = parse_fmt (pc, using_fmt, &using_fmt_pos, primary_file);
        if (fmt->type != FMT_STRING)
          runtime_error (5, pc, "Type mismatch in PRINT USING");
        if (fmt->width == -1 || (*string_sp)->length <= fmt->width)
          length = (*string_sp)->length;
        else
          length = fmt->width;
        if (fwrite ((*string_sp)->body, 1, length,
                    primary_file->file) != length)
          runtime_error (25, pc, "Error writing string to standard output");
        primary_file->printpos += length;
        if (fmt->width != -1)
          for ( ; length < fmt->width; length++) {
            putc (' ', primary_file->file);
            primary_file->printpos++;
          }
        *string_sp = free_string (*string_sp);
      }
    BCODE_END;

    BCODE_START(PRINT_USING_END):
      if (using_fmt != NULL) {
        using_fmt = my_free (using_fmt);
        using_fmt = NULL;
      }
    BCODE_END;

    BCODE_START(BEEP):
      check_open_for_output (pc, primary_file);
      putc ('\x07', primary_file->file);
      primary_file->printpos = 0;
    BCODE_END;

    BCODE_START(SLEEP):
      sleep_delay (*--double_sp);
    BCODE_END;

    BCODE_START(CLS):
      check_open_for_output (pc, primary_file);
      fprintf (primary_file->file, "\x1b[H\x1b[J");
      primary_file->printpos = 0;
    BCODE_END;

    BCODE_START(LOCATE):
      check_open_for_output (pc, primary_file);
      primary_file->printpos = *--long_sp;
      fprintf (primary_file->file, "\x1b[%ld;%ldH", *--long_sp,
               primary_file->printpos);
    BCODE_END;

    BCODE_START(COLOR):
      check_open_for_output (pc, primary_file);
      {
        int c = *--int_sp;
        c = (c & 15) + 30;    /* ANSI <esc>[3xm foreground colour */
        if (c >= 38)
          c += (90 - 38);     /* ANSI <esc>[9xm bright foreground colour */
        fprintf (primary_file->file, "\x1b[%dm", c);
      }
    BCODE_END;

    BCODE_START(BACKGROUND_COLOR):
      check_open_for_output (pc, primary_file);
      {
        register int c = *--int_sp;
        c = (c & 15) + 40;    /* ANSI <esc>[4xm background colour */
        if (c >= 48)
          c += (100 - 48);    /* ANSI <esc>[10xm bright background colour */
        fprintf (primary_file->file, "\x1b[%dm", c);
      }
    BCODE_END;

    BCODE_START(LINEINPUT):
      {
        register T_STRING str;
        BOOL error;
        check_open_for_input (pc, primary_file);
        input_string_length = tty_getline (input_prompt_string, &input_string,
                                           &input_string_allocated_length,
                                           primary_file->file, &error);
        if (error || input_string_length == 0)
          runtime_error (57, pc, "Error reading input file in LINEINPUT!");
        if (input_string == NULL)
          runtime_error (62, pc, "LINEINPUT past end of file!");
        input_string[--input_string_length] = '\0'; /* trim '\n' */
        str = alloc_string_check (pc, input_string_length, input_string);
        *string_sp++ = str;
        if (primary_file == &input_file)
          output_file.printpos = 0;
      }
    BCODE_END;

    BCODE_START(INPUT_RESTART):
      int_sp = int_stack;  /* yech */
      long_sp = long_stack;
      single_sp = single_stack;
      double_sp = double_stack;
      string_sp = string_stack;
      index_sp = index_stack;
      restart_input_pc = pc - SIZE_BCODE;
    BCODE_END;

    BCODE_START(INPUT_CLEAR_PROMPT_STRING):
      input_prompt_string = free_string (input_prompt_string);
      input_prompt_string = alloc_string_check (pc, 0, "");
    BCODE_END;

    BCODE_START(INPUT_SET_PROMPT_STRING):
      input_prompt_string = free_string (input_prompt_string);
      --string_sp;
      input_prompt_string = alloc_string_check (pc, (*string_sp)->length,
                                                (*string_sp)->body);
      *string_sp = free_string (*string_sp);
    BCODE_END;

    BCODE_START(INPUT_EXTEND_PROMPT_STRING) /* append "? " to prompt string */:
      {
        register T_STRING str;
        str = alloc_string_check (pc, input_prompt_string->length + 2, NULL);
        memcpy (str->body, input_prompt_string->body,
                input_prompt_string->length);
        str->body[input_prompt_string->length] = '?';
        str->body[input_prompt_string->length + 1] = ' ';
        input_prompt_string = free_string (input_prompt_string);
        input_prompt_string = str;
      }
    BCODE_END;

    BCODE_START(INPUT_START):
      {
        BOOL error;
        check_open_for_input (pc, primary_file);
        input_string_length = tty_getline (input_prompt_string, &input_string,
                                           &input_string_allocated_length,
                                           primary_file->file, &error);
        if (error || input_string_length == 0)
          runtime_error (57, pc, "Error reading input file!");
        input_string[--input_string_length] = '\0'; /* trim '\n' */
        primary_file->printpos = 0;
        if (primary_file == &input_file)
          output_file.printpos = 0;
      }
    BCODE_END;

    BCODE_START(INPUT_INT):
      {
        double d;
        primary_file->printpos = input_number (input_string,
                                               primary_file->printpos, &d);
        if (primary_file->printpos == 0) {
          fprintf (output_file.file, "?Redo from start\n");
          pc = restart_input_pc;
        } else
          *int_sp++ = (T_INT)floor((double)(d + 0.5));
      }
    BCODE_END;

    BCODE_START(INPUT_LONG):
      {
        double d;
        primary_file->printpos = input_number (input_string,
                                               primary_file->printpos, &d);
        if (primary_file->printpos == 0) {
          fprintf (output_file.file, "?Redo from start\n");
          pc = restart_input_pc;
        } else
          *long_sp++ = (T_LONG)floor((double)(d + 0.5));
      }
    BCODE_END;

    BCODE_START(INPUT_SINGLE):
      {
        double d;
        primary_file->printpos = input_number (input_string,
                                               primary_file->printpos, &d);
        if (primary_file->printpos == 0) {
          fprintf (output_file.file, "?Redo from start\n");
          pc = restart_input_pc;
        } else
          *single_sp++ = (T_SINGLE)d;
      }
    BCODE_END;

    BCODE_START(INPUT_DOUBLE):
      {
        double d;
        primary_file->printpos = input_number (input_string,
                                               primary_file->printpos, &d);
        if (primary_file->printpos == 0) {
          fprintf (output_file.file, "?Redo from start\n");
          pc = restart_input_pc;
        } else
          *double_sp++ = d;
      }
    BCODE_END;

    BCODE_START(INPUT_STRING):
      {
        register T_LONG l;
        register T_STRING str;
        printpos = primary_file->printpos;
        if (printpos != 0) {
          while (input_string[printpos] == ' ' ||
                 input_string[printpos] == '\t')
            printpos++;
          if (input_string[printpos++] != ',') {
            fprintf (output_file.file, "?Redo from start\n");
            pc = restart_input_pc;
            BCODE_END;
          }
        }
        while (input_string[printpos] == ' ' || input_string[printpos] == '\t')
          printpos++;
        if (input_string[printpos] == '\"') {
          l = ++printpos;
          while (input_string[printpos] != '\"' &&
                 input_string[printpos] != '\0')
            printpos++;
          if (input_string[printpos] == '\0') {
            fprintf (output_file.file, "?Redo from start\n");
            pc = restart_input_pc;
            BCODE_END;
          }
          str = alloc_string_check (pc, printpos++ - l, &input_string[l]);
        } else {
          register T_LONG l, l1;
          l = printpos;
          while (input_string[printpos] != ',' &&
                 input_string[printpos] != '\0')
            printpos++;
          l1 = printpos - 1;
          while (input_string[l1] == ' ' || input_string[l1] == '\t')
            --l1;
          str = alloc_string_check (pc, l1 - l + 1, &input_string[l]);
        }
        *string_sp++ = str;
        if (primary_file == &input_file)
          output_file.printpos = 0;
        primary_file->printpos = printpos;
      }
    BCODE_END;

    BCODE_START(INPUT_DOLLAR):
      {
        register T_STRING str;
        register T_SIZE_T length;
        check_open_for_input (pc, secondary_file);
        length = *--long_sp;
        str = alloc_string_check (pc, length, NULL);
        if (tty_fread (str->body, 1, length, secondary_file->file) != length)
          runtime_error (57, pc, "Error reading file in INPUT$");
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(INKEY_DOLLAR):
      {
        register T_STRING str;
        str = alloc_string_check (pc, 1, NULL);
        if (inkey (str) == 0)
          runtime_error (57, pc, "INKEY$ failed");
        *string_sp++ = str;
      }
    BCODE_END;

    BCODE_START(READ_INT):
      bcode = (BCODE)*(unsigned short int *)(ALIGN_UP(dc,ALIGN_BCODE));
      dc += SIZE_BCODE;
      switch (bcode) {
        case B_PUSH_CONST_INT_16:
        case B_PUSH_CONST_LONG_16:
        case B_PUSH_CONST_SINGLE_16:
        case B_PUSH_CONST_DOUBLE_16:
          *int_sp++ = *((T_SHORT *)ALIGN_UP(dc,ALIGN_SHORT));
          dc += SIZE_SHORT;
          break;
        case B_PUSH_CONST_INT:
          *int_sp++ = *((T_INT *)ALIGN_UP(dc,ALIGN_INT));
          dc += SIZE_INT;
          break;
        case B_PUSH_CONST_LONG:
          *int_sp++ = (T_INT)*((T_LONG *)ALIGN_UP(dc,ALIGN_LONG));
          dc += SIZE_LONG;
          break;
        case B_PUSH_CONST_SINGLE:
          *int_sp++ = (T_INT)*((T_SINGLE *)ALIGN_UP(dc,ALIGN_SINGLE));
          dc += SIZE_SINGLE;
          break;
        case B_PUSH_CONST_DOUBLE:
          *int_sp++ = (T_INT)*((T_DOUBLE *)ALIGN_UP(dc,ALIGN_DOUBLE));
          dc += SIZE_DOUBLE;
          break;
        case B_PUSH_CONST_INT_ZERO:
        case B_PUSH_CONST_LONG_ZERO:
        case B_PUSH_CONST_SINGLE_ZERO:
        case B_PUSH_CONST_DOUBLE_ZERO:
          *int_sp++ = 0;
          break;
        case B_PUSH_CONST_INT_ONE:
        case B_PUSH_CONST_LONG_ONE:
        case B_PUSH_CONST_SINGLE_ONE:
        case B_PUSH_CONST_DOUBLE_ONE:
          *int_sp++ = 1;
          break;
        case B_PUSH_CONST_STRING:
          runtime_error (5, pc, "Attempt to read INT variable from STRING data");
        case B_END:
          runtime_error (4, pc, "Out of data while reading INT");
        default:
          runtime_error (51, pc, "Internal error in READ_INT");
      }
    BCODE_END;

    BCODE_START(READ_LONG):
      bcode = (BCODE)*(unsigned short int *)(ALIGN_UP(dc,ALIGN_BCODE));
      dc += SIZE_BCODE;
      switch (bcode) {
        case B_PUSH_CONST_INT_16:
        case B_PUSH_CONST_LONG_16:
        case B_PUSH_CONST_SINGLE_16:
        case B_PUSH_CONST_DOUBLE_16:
          *long_sp++ = (T_LONG)*((T_SHORT *)ALIGN_UP(dc,ALIGN_SHORT));
          dc += SIZE_SHORT;
          break;
        case B_PUSH_CONST_INT:
          *long_sp++ = (T_LONG)*((T_INT *)ALIGN_UP(dc,ALIGN_INT));
          dc += SIZE_INT;
          break;
        case B_PUSH_CONST_LONG:
          *long_sp++ = *((T_LONG *)ALIGN_UP(dc,ALIGN_LONG));
          dc += SIZE_LONG;
          break;
        case B_PUSH_CONST_SINGLE:
          *long_sp++ = (T_LONG)*((T_SINGLE *)ALIGN_UP(dc,ALIGN_SINGLE));
          dc += SIZE_SINGLE;
          break;
        case B_PUSH_CONST_DOUBLE:
          *long_sp++ = (T_LONG)*((T_DOUBLE *)ALIGN_UP(dc,ALIGN_DOUBLE));
          dc += SIZE_DOUBLE;
          break;
        case B_PUSH_CONST_INT_ZERO:
        case B_PUSH_CONST_LONG_ZERO:
        case B_PUSH_CONST_SINGLE_ZERO:
        case B_PUSH_CONST_DOUBLE_ZERO:
          *long_sp++ = 0;
          break;
        case B_PUSH_CONST_INT_ONE:
        case B_PUSH_CONST_LONG_ONE:
        case B_PUSH_CONST_SINGLE_ONE:
        case B_PUSH_CONST_DOUBLE_ONE:
          *long_sp++ = 1;
          break;
        case B_PUSH_CONST_STRING:
          runtime_error (5, pc, "Attempt to read T_LONG variable from STRING data");
        case B_END:
          runtime_error (4, pc, "Out of data while reading LONG");
        default:
          runtime_error (51, pc, "Internal error in READ_LONG");
        }
    BCODE_END;

    BCODE_START(READ_SINGLE):
      bcode = (BCODE)*(unsigned short int *)(ALIGN_UP(dc,ALIGN_BCODE));
      dc += SIZE_BCODE;
      switch (bcode) {
        case B_PUSH_CONST_INT_16:
        case B_PUSH_CONST_LONG_16:
        case B_PUSH_CONST_SINGLE_16:
        case B_PUSH_CONST_DOUBLE_16:
          *single_sp++ = (T_SINGLE)*((T_SHORT *)ALIGN_UP(dc,ALIGN_SHORT));
          dc += SIZE_SHORT;
          break;
        case B_PUSH_CONST_INT:
          *single_sp++ = (T_SINGLE)*((T_INT *)ALIGN_UP(dc,ALIGN_INT));
          dc += SIZE_INT;
          break;
        case B_PUSH_CONST_LONG:
          *single_sp++ = (T_SINGLE)*((T_LONG *)ALIGN_UP(dc,ALIGN_LONG));
          dc += SIZE_LONG;
          break;
        case B_PUSH_CONST_SINGLE:
          *single_sp++ = *((T_SINGLE *)ALIGN_UP(dc,ALIGN_SINGLE));
          dc += SIZE_SINGLE;
          break;
        case B_PUSH_CONST_DOUBLE:
          *single_sp++ = (T_SINGLE)*((T_DOUBLE *)ALIGN_UP(dc,ALIGN_DOUBLE));
          dc += SIZE_DOUBLE;
          break;
        case B_PUSH_CONST_INT_ZERO:
        case B_PUSH_CONST_LONG_ZERO:
        case B_PUSH_CONST_SINGLE_ZERO:
        case B_PUSH_CONST_DOUBLE_ZERO:
          *single_sp++ = (T_SINGLE)0.0;
          break;
        case B_PUSH_CONST_INT_ONE:
        case B_PUSH_CONST_LONG_ONE:
        case B_PUSH_CONST_SINGLE_ONE:
        case B_PUSH_CONST_DOUBLE_ONE:
          *single_sp++ = (T_SINGLE)1.0;
          break;
        case B_PUSH_CONST_STRING:
          runtime_error (5, pc, "Attempt to read SINGLE variable from STRING data");
        case B_END:
          runtime_error (4, pc, "Out of data while reading SINGLE");
        default:
          runtime_error (51, pc, "Internal error in READ_SINGLE");
      }
    BCODE_END;

    BCODE_START(READ_DOUBLE):
      bcode = (BCODE)*(unsigned short int *)(ALIGN_UP(dc,ALIGN_BCODE));
      dc += SIZE_BCODE;
      switch (bcode) {
        case B_PUSH_CONST_INT_16:
        case B_PUSH_CONST_LONG_16:
        case B_PUSH_CONST_SINGLE_16:
        case B_PUSH_CONST_DOUBLE_16:
          *double_sp++ = (T_DOUBLE)*((T_SHORT *)ALIGN_UP(dc,ALIGN_SHORT));
          dc += SIZE_SHORT;
          break;
        case B_PUSH_CONST_INT:
          *double_sp++ = (T_DOUBLE)*((T_INT *)ALIGN_UP(dc,ALIGN_INT));
          dc += SIZE_INT;
          break;
        case B_PUSH_CONST_LONG:
          *double_sp++ = (T_DOUBLE)*((T_LONG *)ALIGN_UP(dc,ALIGN_LONG));
          dc += SIZE_LONG;
          break;
        case B_PUSH_CONST_SINGLE:
          *double_sp++ = (T_DOUBLE)*((T_SINGLE *)ALIGN_UP(dc,ALIGN_SINGLE));
          dc += SIZE_SINGLE;
          break;
        case B_PUSH_CONST_DOUBLE:
          *double_sp++ = *((T_DOUBLE *)ALIGN_UP(dc,ALIGN_DOUBLE));
          dc += SIZE_DOUBLE;
          break;
        case B_PUSH_CONST_INT_ZERO:
        case B_PUSH_CONST_LONG_ZERO:
        case B_PUSH_CONST_SINGLE_ZERO:
        case B_PUSH_CONST_DOUBLE_ZERO:
          *double_sp++ = 0.0;
          break;
        case B_PUSH_CONST_INT_ONE:
        case B_PUSH_CONST_LONG_ONE:
        case B_PUSH_CONST_SINGLE_ONE:
        case B_PUSH_CONST_DOUBLE_ONE:
          *double_sp++ = 1.0;
          break;
        case B_PUSH_CONST_STRING:
          runtime_error (5, pc, "Attempt to read DOUBLE variable from STRING data");
        case B_END:
          runtime_error (4, pc, "Out of data while reading DOUBLE");
        default:
          runtime_error (51, pc, "Internal error in READ_DOUBLE");
      }
    BCODE_END;

    BCODE_START(READ_STRING):
      bcode = (BCODE)*(unsigned short int *)(ALIGN_UP(dc,ALIGN_BCODE));
      dc += SIZE_BCODE;
      if (bcode != B_PUSH_CONST_STRING) {
        if (bcode == B_END) {
          runtime_error (4, pc, "Out of data while reading STRING");
        } else {
          runtime_error (5, pc, "Attempt to read STRING variable from numeric data");
        }
      }
      *string_sp = *((T_STRING *)ALIGN_UP(dc,ALIGN_STRING));
      (*string_sp++)->ref_count++;
      dc += SIZE_STRING;
    BCODE_END;

    BCODE_START(RESTORE):
      dc = bdata;
    BCODE_END;

    BCODE_START(RESTORE_LINE):
      dc = &bdata[*((T_SIZE_T *)ALIGN_UP(pc,ALIGN_SIZE_T))];
      pc += SIZE_SIZE_T;
    BCODE_END;

    BCODE_START(STOP):
      puts ("STOP");
    BCODE_START(END):
      goto done;
    BCODE_END;

#ifndef THREADED

    default:
      runtime_error (51, pc, "Corrupt bcodes!");

    } /* end of switch (bcode) */

    /* debug ((stderr, "  %02x  %s\n", (int)bcode, bcode_table[bcode])); */

  } /* end of loop for each bcode */

#endif

done:
  if (input_prompt_string != NULL)
    input_prompt_string = free_string (input_prompt_string);
  if (using_fmt != NULL)
    using_fmt = my_free (using_fmt);
  int_vars = free_align ((char *)int_vars);
  long_vars = free_align ((char *)long_vars);
  single_vars = free_align ((char *)single_vars);
  double_vars = free_align ((char *)double_vars);
  if (string_vars != NULL) {
    T_SIZE_T t;
    for (t = 0; t < vars_size[TYPE_STRING]; t++) {
      debug ((stderr, "Ref count[%zu] = %zu\n", t, string_vars[t]->ref_count));
      string_vars[t] = free_string (string_vars[t]);
    }
    string_vars = free_align ((char *)string_vars);
  }
  if (file_vars != NULL) {
    T_SIZE_T t;
    for (t = 0; t < vars_size[TYPE_FILE]; t++)
      if (file_vars[t] != NULL) {
        if (file_vars[t]->file != NULL) {
          fclose (file_vars[t]->file);
          file_vars[t]->file = NULL;
        }
        file_vars[t] = my_free ((char *)file_vars[t]);
        file_vars[t] = NULL;
      }
    file_vars = free_align ((char *)file_vars);
  }
  int_stack = free_align ((char *)int_stack);
  long_stack = free_align ((char *)long_stack);
  single_stack = free_align ((char *)single_stack);
  double_stack = free_align ((char *)double_stack);
  string_stack = free_align ((char *)string_stack);
  gosub_stack = free_align ((char *)gosub_stack);
  index_stack = free_align ((char *)index_stack);
  tmp_string = my_free (tmp_string);
  if (input_string != NULL) {
    free (input_string); /* not my_free() because malloc()'ed by readline() */
    input_string = NULL;
    input_string_length = 0;
    input_string_allocated_length = 0;
  }

  return chain_fname;

} /* run_bcodes */

/****************************************************************************/
/****************************************************************************/

static void string_mid_assign (unsigned char *pc, T_STRING *a, T_LONG b,
                               T_LONG c, T_STRING *d)
{
  /* MID$(A$,B&,C&)=D$ */
  /*
  printf ("string_mid_assign (%p, %ld, %ld, %p)\n", a, b, c, d);
  printf ("%lu %lu\n", (*a)->length, (*d)->length);
  */
  if (b <= 0 || b > (*a)->length)
    runtime_error (5, pc, "Illegal function call");
  if ((*d)->length < c)
    c = (*d)->length;
  if ((*a)->length - b + 1 < c)
    c = (*a)->length - b + 1;
  if (c > 0)
    memcpy (&(*a)->body[b-1], (*d)->body, c);
  *d = free_string(*d);
}

/****************************************************************************/

static T_STRING alloc_string_check (unsigned char *pc, size_t length,
                                    char *body)
{
  T_STRING str;

  if ((str = alloc_string (length, body)) == NULL)
    runtime_error (14, pc, "Out of memory for string!");
  return str;
}

/****************************************************************************/
/****************************************************************************/

static void check_open_for_input (unsigned char *pc, struct file_descr *f)
{
  if (f == NULL ||
      f->file_state != FILE_OPEN_INPUT)
    runtime_error (5, pc, "Attempt to read output file or unopened file!");
} /* check_open_for_input */

/****************************************************************************/

static void check_open_for_output (unsigned char *pc, struct file_descr *f)
{
  if (f == NULL ||
      (f->file_state != FILE_OPEN_OUTPUT &&
       f->file_state != FILE_OPEN_APPEND))
    runtime_error (5, pc, "Attempt to write to input file or unopened file!");
} /* check_open_for_output */

/****************************************************************************/

static T_LONG input_number (char *s, T_LONG printpos, double *d)
{
  enum token_type token;
  struct token_info_type token_info;
  double sign;

  *d = 0.0;
  if (printpos != 0) {
    printpos = get_token (&s[printpos], &token, NULL) - s;
    if (token != TOKEN_COMMA)
      return 0;
  }
  printpos = get_token (&s[printpos], &token, &token_info) - s;
  if (token == TOKEN_MINUS) {
    sign = -1.0;
    printpos = get_token (&s[printpos], &token, &token_info) - s;
  } else if (token == TOKEN_NUMBER)
    sign = 1.0;
  else
    return 0;
  switch (token_info.token_type) {
    case TYPE_INT:
      *d = (double)token_info.value.i;
      break;
    case TYPE_LONG:
      *d = (double)token_info.value.l;
      break;
    case TYPE_SINGLE:
      *d = (double)token_info.value.s;
      break;
    case TYPE_DOUBLE:
      *d = (double)token_info.value.d;
      break;
    default:
      return 0;
  }
  *d *= sign;
  return printpos;
} /* input_number */

/****************************************************************************/

static void single_to_string (char *s, double d)
{
  if (fabs(d) <= 10000000.0)
    if (d == floor(d))
      sprintf (s, "% ld", (long)d);
    else if (fabs(d) < 0.000001)
      sprintf (s, "% 12.6E", d);
    else {
      if (fabs(d) >= 1000000.0)
        sprintf (s, "% 9.0f", d);
      else if (fabs(d) >= 100000.0)
        sprintf (s, "% 9.1f", d);
      else if (fabs(d) >= 10000.0)
        sprintf (s, "% 9.2f", d);
      else if (fabs(d) >= 1000.0)
        sprintf (s, "% 9.3f", d);
      else if (fabs(d) >= 100.0)
        sprintf (s, "% 9.4f", d);
      else if (fabs(d) >= 10.0)
        sprintf (s, "% 9.5f", d);
      else if (fabs(d) >= 1.0)
        sprintf (s, "% 9.6f", d);
      else if (fabs(d) >= 0.1)
        sprintf (s, "% 9.7f", d);
      else if (fabs(d) >= 0.01)
        sprintf (s, "% 9.8f", d);
      else if (fabs(d) >= 0.001)
        sprintf (s, "% 9.9f", d);
      else if (fabs(d) >= 0.0001)
        sprintf (s, "% 9.10f", d);
      else if (fabs(d) >= 0.00001)
        sprintf (s, "% 9.11f", d);
      else
        sprintf (s, "% 9.12f", d);
      if (strchr(s, '.') != NULL)
        strip_trailing_zeroes (s);
      if (strlen(s) > 12)
        sprintf (s, "% 12.6E", d);
    }
  else
    sprintf (s, "% 12.6E", d);
} /* single_to_string */

/****************************************************************************/

static void double_to_string (char *s, double d)
{
  if (fabs(d) < 10000000000.0)
    if (d == floor(d))
      sprintf (s, "% ld", (long)d);
    else if (fabs(d) < 0.00000000000001)
      sprintf (s, "% 21.15E", d);
    else {
      if (fabs(d) >= 1000000000.0)
        sprintf (s, "% 17.5f", d);
      else if (fabs(d) >= 100000000.0)
        sprintf (s, "% 17.6f", d);
      else if (fabs(d) >= 10000000.0)
        sprintf (s, "% 17.7f", d);
      else if (fabs(d) >= 1000000.0)
        sprintf (s, "% 17.8f", d);
      else if (fabs(d) >= 100000.0)
        sprintf (s, "% 17.9f", d);
      else if (fabs(d) >= 10000.0)
        sprintf (s, "% 17.10f", d);
      else if (fabs(d) >= 1000.0)
        sprintf (s, "% 17.11f", d);
      else if (fabs(d) >= 100.0)
        sprintf (s, "% 17.12f", d);
      else if (fabs(d) >= 10.0)
        sprintf (s, "% 17.13f", d);
      else if (fabs(d) >= 1.0)
        sprintf (s, "% 17.14f", d);
      else if (fabs(d) >= 0.1)
        sprintf (s, "% 17.15f", d);
      else if (fabs(d) >= 0.01)
        sprintf (s, "% 17.16f", d);
      else if (fabs(d) >= 0.001)
        sprintf (s, "% 17.17f", d);
      else if (fabs(d) >= 0.0001)
        sprintf (s, "% 17.18f", d);
      else if (fabs(d) >= 0.00001)
        sprintf (s, "% 17.19f", d);
      else if (fabs(d) >= 0.000001)
        sprintf (s, "% 17.20f", d);
      else if (fabs(d) >= 0.0000001)
        sprintf (s, "% 17.21f", d);
      else if (fabs(d) >= 0.00000001)
        sprintf (s, "% 17.22f", d);
      else if (fabs(d) >= 0.000000001)
        sprintf (s, "% 17.23f", d);
      else if (fabs(d) >= 0.0000000001)
        sprintf (s, "% 17.24f", d);
      else if (fabs(d) >= 0.00000000001)
        sprintf (s, "% 17.25f", d);
      else if (fabs(d) >= 0.000000000001)
        sprintf (s, "% 17.26f", d);
      else if (fabs(d) >= 0.0000000000001)
        sprintf (s, "% 17.27f", d);
      else
        sprintf (s, "% 17.28f", d);
      if (strchr(s, '.') != NULL)
        strip_trailing_zeroes (s);
      if (strlen(s) > 21)
        sprintf (s, "% 21.15E", d);
    }
  else
    sprintf (s, "% 21.15E", d);
} /* double_to_string */

/****************************************************************************/

static void strip_trailing_zeroes (char *s)
{
  int i;

  i = strlen(s);
  while (i > 1 && s[i-1] == '0')
    i--;
  s[i] = '\0';
} /* strip_trailing_zeroes */

/****************************************************************************/

static struct using_fmt *parse_fmt (unsigned char *pc, char *using_fmt,
                                    int *using_fmt_pos, struct file_descr *file)
{
  static struct using_fmt fmt;
  int cycling;
  char c;

  fmt.type = FMT_NONE;
  fmt.exponential = FALSE;
  fmt.width = 0;
  fmt.precision = 0;
  fmt.commas = FALSE;
  fmt.sign = FALSE;
  fmt.money = FALSE;
  fmt.fill = ' ';
  fmt.minus = FALSE;

  cycling = FALSE;
  for (;;) {

    switch ((c = using_fmt[(*using_fmt_pos)++])) {

    case ' ':
      if (fmt.type != FMT_NONE) {
        --(*using_fmt_pos);
        return &fmt;
      }
      putc (' ', file->file);
      file->printpos++;
      break;

    case '\0':
    case '\n':
    case '\r':
      *using_fmt_pos = 0;
      if (fmt.type != FMT_NONE)
        return &fmt;
      if (cycling)
        runtime_error (5, pc, "Type mismatch in PRINT USING");
      cycling = TRUE;
      break;

    case '_':
      putc (using_fmt[(*using_fmt_pos)++], file->file);
      file->printpos++;
      break;

    case '!':
      fmt.type = FMT_STRING;
      fmt.width = 1;
      return &fmt;

    case '&':
      fmt.type = FMT_STRING;
      fmt.width = -1;
      return &fmt;

    case '\\':
      fmt.type = FMT_STRING;
      fmt.width++;
      for ( ; using_fmt[*using_fmt_pos] == ' '; (*using_fmt_pos)++)
        fmt.width++;
      if (using_fmt[*using_fmt_pos] == '\\') {
        fmt.width++;
        (*using_fmt_pos)++;
      }
      return &fmt;

    case '$':
      fmt.width++;
      fmt.money = TRUE;
      if (using_fmt[*using_fmt_pos] == '$') {
        fmt.width++;
        (*using_fmt_pos)++;
      }
      break;

    case '*':
      fmt.width++;
      fmt.fill = '*';
      if (using_fmt[*using_fmt_pos] == '*') {
        fmt.width++;
        (*using_fmt_pos)++;
      }
      break;

    case '+':
      fmt.width++;
      fmt.sign = TRUE;
      break;

    case '#':
    case '.':
    case ',':
      fmt.type = FMT_NUMBER;
      while (c == '#' || c == ',') {
        if (c == ',')
          fmt.commas = TRUE;
        fmt.width++;
        c = using_fmt[(*using_fmt_pos)++];
      }
      if (c == '.') {
        fmt.width++;
        while ((c = using_fmt[(*using_fmt_pos)++]) == '#') {
          fmt.precision++;
          fmt.width++;
        }
      }
      if (c == '-') {
        fmt.minus = TRUE;
        fmt.width++;
        c = using_fmt[(*using_fmt_pos)++];
      }
      if (c == '^') {
        fmt.exponential = TRUE;
        fmt.width++;
        while (using_fmt[(*using_fmt_pos)++] == '^')
          fmt.width++;
      }
      --(*using_fmt_pos);
      return &fmt;

    default:
      putc (c, file->file);
      file->printpos++;
      break;

    }
  }
  return &fmt;
} /* parse_fmt */

/****************************************************************************/

static void print_using_number (unsigned char *pc, double number,
                                struct using_fmt *fmt, struct file_descr *file)
{
  int i, j, dec_pos, dig_pos;
  char buf[1024], tbuf[1024];

  if (fmt->type != FMT_NUMBER)
    runtime_error (5, pc, "Type mismatch in PRINT USING");
  if (fmt->exponential) {
    if (fmt->sign)
      sprintf (buf, "%+*.*E", fmt->width, fmt->precision, number);
    else
      sprintf (buf, "%*.*E", fmt->width, fmt->precision, number);
  } else {
    if (fmt->sign) {
      sprintf (buf, "%+*.*f", fmt->width, fmt->precision, number);
    } else {
      sprintf (buf, "%*.*f", fmt->width, fmt->precision, number);
      if (fmt->minus) {
        for (i = 0; i < strlen(buf); i++) {
          if (buf[i] != ' ') {
            if (buf[i] == '-') {
              for (j = i + 1; j < strlen(buf); j++)
                buf[j - 1] = buf[j];
              buf[strlen(buf) - 1] = '-';
            } else {
              for (j = i; j < strlen(buf); j++)
                buf[j - 1] = buf[j];
              buf[strlen(buf) - 1] = ' ';
            }
            break;
          }
        }
      }
    }
    if (fmt->commas) {
      dig_pos = -1;
      dec_pos = -1;
      for (i = 0; i < strlen(buf); i++) {
        if ((isdigit((int)buf[i]) != 0) && (dig_pos == -1))
          dig_pos = i;
        if ((buf[i] == '.') && (dec_pos == -1))
          dec_pos = i;
        if ((dig_pos != -1) && (dec_pos != -1))
          break;
      }
      if (dec_pos == -1)
        dec_pos = strlen(buf);
      j = 0;
      for (i = 0; i < strlen(buf); i++) {
        if (((dec_pos - i) % 3 == 0) && (i > dig_pos) && (i < dec_pos)) {
          tbuf[j] = ',';
          ++j;
          tbuf[j] = '\0';
        }
        tbuf[j] = buf[i];
        ++j;
        tbuf[j] = '\0';
      }
      strcpy (buf, &tbuf[strlen(tbuf) - strlen(buf)]);
    }
    if (fmt->money) {
      for (i = 0; i < strlen(buf); i++) {
        if (buf[i] != ' ') {
          if (i > 0) {
            if (!isdigit((int)buf[i])) {
              buf[i - 1] = buf[ i ];
              buf[i] = '$';
            } else
              buf[i - 1] = '$';
          }
          break;
        }
      }
    }
  }
  if (fmt->fill == '*')
    for (i = 0; i < strlen(buf); i++) {
      if (buf[i] != ' ')
        break;
      buf[i] = '*';
    }
  if (strlen(buf) > fmt->width) {
    putc ('%', file->file);
    file->printpos++;
  }
  if (fwrite (buf, 1, strlen(buf), file->file) != strlen(buf))
    runtime_error (57, pc, "Error writing number");
  file->printpos += strlen(buf);
} /* print_using_number */

/**********************************************************************/
