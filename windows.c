#undef __STRICT_ANSI__
#define _POSIX_C_SOURCE 1

#include <direct.h>
#include <stdio.h>
#include <stdlib.h>

#include <windows.h>
#ifdef TOKEN_READ
#undef TOKEN_READ
#endif
#include <conio.h>
#include <string.h>
#include <sys/stat.h>
/* #include <sys/time.h> */
#include <sys/types.h>
/* #include <sys/fcntl.h> */
/* #include <unistd.h> */
#include <fcntl.h>
#include <io.h>
#include <stdint.h>

#ifdef HAVE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif

#include "mybasic.h"
#include "my_mem.h"

/****************************************************************************/

static char tmp_string[MAX_FNAME_LENGTH];

/****************************************************************************/

int change_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return _chdir (tmp_string);
}

/****************************************************************************/

int make_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  /*return _mkdir (tmp_string, 0755);*/
  return _mkdir (tmp_string);
}

/****************************************************************************/

int remove_dir (struct string_descr *str)
{
  T_SIZE_T length;

  if (str->length < MAX_FNAME_LENGTH - 1)
    length = str->length;
  else
    length = MAX_FNAME_LENGTH - 1;
  memcpy (tmp_string, str->body, length);
  tmp_string[length] = '\0';
  return _rmdir (tmp_string);
}

/****************************************************************************/

static int tty_in_state = 0; /* 0 = normal, 1 = buffersize=1byte & echo-off */
static int tty_out_state = 0; /* 0 = normal, 1 = buffersize=1byte & echo-off */
static BOOL first_in = TRUE, first_out = TRUE;
static HANDLE hStdin;
static HANDLE hStdout;
static DWORD stored_settings_in;
static DWORD stored_settings_out;
static UINT stored_wCodePageID;

static void reset_tty_in (void)
{
  if (tty_in_state == 1) {
    SetConsoleMode (hStdin, stored_settings_in);
    tty_in_state = 0;
  }
}

static void reset_tty_out (void)
{
  if (tty_out_state == 1) {
    SetConsoleMode (hStdout, stored_settings_out);
    SetConsoleOutputCP (stored_wCodePageID);
    tty_out_state = 0;
  }
}

static BOOL WINAPI console_ctrl_handler_in (DWORD fdwCtrlType)
{
  if (fdwCtrlType == CTRL_C_EVENT ||
      fdwCtrlType == CTRL_CLOSE_EVENT ||
      fdwCtrlType == CTRL_BREAK_EVENT ||
      fdwCtrlType == CTRL_LOGOFF_EVENT ||
      fdwCtrlType == CTRL_SHUTDOWN_EVENT) {
    reset_tty_in ();
  }
  return FALSE;
}

static BOOL WINAPI console_ctrl_handler_out (DWORD fdwCtrlType)
{
  if (fdwCtrlType == CTRL_C_EVENT ||
      fdwCtrlType == CTRL_CLOSE_EVENT ||
      fdwCtrlType == CTRL_BREAK_EVENT ||
      fdwCtrlType == CTRL_LOGOFF_EVENT ||
      fdwCtrlType == CTRL_SHUTDOWN_EVENT) {
    reset_tty_out ();
  }
  return FALSE;
}

void init_tty (void)
{
  DWORD new_settings_out, new_settings_in;

  if (tty_in_state == 0) {
    if ((hStdin = GetStdHandle(STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE) {
      fprintf (stderr, "GetStdHandle(stdin) failed\n");
      exit (EXIT_FAILURE);
    }
    if (GetFileType(hStdin) == FILE_TYPE_CHAR) {
      if (!GetConsoleMode (hStdin, &stored_settings_in)) {
        fprintf (stderr, "GetConsoleMode(stdin) failed\n");
        exit (EXIT_FAILURE);
      }
      if (first_in) {
        atexit (reset_tty_in);
        SetConsoleCtrlHandler (console_ctrl_handler_in, TRUE);
        first_in = FALSE;
      }
      tty_in_state = 1;
      new_settings_in = stored_settings_in;
      new_settings_in &= !ENABLE_LINE_INPUT;
      new_settings_in |= ENABLE_PROCESSED_INPUT;
      new_settings_in |= 0x0200; /* ENABLE_VIRTUAL_TERMINAL_INPUT; */
      if (!SetConsoleMode (hStdin, new_settings_in)) {
        fprintf (stderr, "GetConsoleMode(stdin) failed\n");
        exit (EXIT_FAILURE);
      }
    }
  }
  if (tty_out_state == 0) {
    if ((hStdout = GetStdHandle(STD_OUTPUT_HANDLE)) == INVALID_HANDLE_VALUE) {
      fprintf (stderr, "GetStdHandle(stdout) failed\n");
      exit (EXIT_FAILURE);
    }
    if (GetFileType(hStdout) == FILE_TYPE_CHAR) {
      if (!GetConsoleMode (hStdout, &stored_settings_out)) {
        fprintf (stderr, "GetConsoleMode(stdout) failed\n");
        exit (EXIT_FAILURE);
      }
      if (first_out) {
        atexit (reset_tty_out);
        SetConsoleCtrlHandler (console_ctrl_handler_out, TRUE);
        first_out = FALSE;
      }
      tty_out_state = 1;
      new_settings_out = stored_settings_out;
      new_settings_out |= 0x0004; /* ENABLE_VIRTUAL_TERMINAL_PROCESSING; */
      new_settings_out |= ENABLE_PROCESSED_OUTPUT;
      if (!SetConsoleMode (hStdout, new_settings_out)) {
        fprintf (stderr, "GetConsoleMode(stdout) failed\n");
        exit (EXIT_FAILURE);
      }
      if ((stored_wCodePageID = GetConsoleOutputCP()) == 0) {
        fprintf (stderr, "GetConsoleOutputCP() failed\n");
        exit (EXIT_FAILURE);
      }
      if (SetConsoleOutputCP(CP_UTF8) == 0) {  /* code page 65001 is UTF-8 */
        fprintf (stderr, "SetConsoleOutputCP(CP_UTF8) failed\n");
        exit (EXIT_FAILURE);
      }
      _setmode(fileno(stdout),_O_BINARY);
    }
  }
}

/****************************************************************************/
#if 0
static void tty_putc (int c, FILE *f, FILE *g)
{
  if (f == stdin && GetFileType(hStdin) == FILE_TYPE_CHAR)
    _putch(c);
  else {
    fputc (c, g);
    fflush (g);
  }
}
#endif

/****************************************************************************/

int tty_getc (FILE *f)
{
  int c;

  fflush (stdout);
  if (f == stdin && GetFileType(hStdin) == FILE_TYPE_CHAR) {
    c = _getch();
    if (c == 0x03) { /* ctrl-c */
      fprintf (stderr, "\nBreak into program\n");
      exit (EXIT_FAILURE);
    } else if (c == '\r')
      c = '\n';
  } else {
    c = fgetc (f);
    if (c != EOF && isatty (fileno (f))) {
      if (c == '\r')
        c = '\n';
    }
  }
  return c;
}

/****************************************************************************/

size_t tty_getline (struct string_descr *prompt, char **buf, size_t *bufsiz,
                    FILE *f, BOOL *error)
/* getline(buf,bufsiz,f) with prompt, echo & editing if(isatty) */
{
#ifdef HAVE_READLINE /* use readline() for tty, getline() for files */

  char *p;
  static BOOL first = TRUE;

  *error = FALSE;

  if (!isatty (fileno (f))) {
    ssize_t ssize = getline (buf, bufsiz, f);
    if (ssize < 0) {
      *error = TRUE;
      return 0;
    } else
      return (size_t)ssize;
  }

  if (first) {
#if RL_VERSION_MAJOR >= 7
    atexit (rl_clear_history);
#endif
    first = FALSE;
  }
  if ((p = my_malloc (prompt->length + 1)) == NULL) {
    fprintf (stderr, "Error writing input prompt");
    *error = TRUE;
    return (size_t)0;
  }
  memcpy (p, prompt->body, prompt->length);
  p[prompt->length] = '\0';
  reset_keypress ();
  if (*buf != NULL)
    free (*buf);  /* not my_free() because malloc()'ed by readline() */
  *buf = readline (p);
  init_tty ();
  p = my_free (p);
  if (*buf == NULL) {
    *error = TRUE;
    return (size_t)0;
  }
  else if (*buf[0] != '\0')
    add_history (*buf);
  *bufsiz = strlen(*buf) + 2;
  if ((*buf = realloc (*buf, *bufsiz)) == NULL) { /* not my_realloc() */
    *error = TRUE;
    return 0;
  }
  (*buf)[*bufsiz - 2] = '\n'; /* append '\n' like fgets() and getline() */
  (*buf)[*bufsiz - 1] = '\0';
  return *bufsiz - 1;

#else /* no readline(), print prompt and simulate getline() */

  BOOL is_a_tty;
  char *ptr, *eptr;
  FILE *g;    /* file to write prompt and echo to */
  int c;

  *error = FALSE;

  is_a_tty = isatty (fileno (f));
  g = (f == stdin) ? stdout : f;
  if (is_a_tty) {
    if (prompt != NULL && prompt->length > 0) {
      if (fwrite (prompt->body, 1, prompt->length, g) != prompt->length)
        fprintf (stderr, "Error writing input prompt");
      fflush (g);
    }
  }
  if (*buf == NULL || *bufsiz == 0) {
    *bufsiz = BUFSIZ;
    if ((*buf = malloc (*bufsiz)) == NULL) {
      *error = TRUE;
      return 0;
    }
  }
  for (ptr = *buf, eptr = *buf + *bufsiz;;) {
    if (is_a_tty)
      c = tty_getc(f);
    else
      c = fgetc (f);
    if (c == -1) {
      if (feof (f))
        return ptr == *buf ? -1 : ptr - *buf;
      else {
        *error = TRUE;
        return 0;
      }
    }
    if (is_a_tty) {
      fputc (c, g);  /* echo */
      if (c == '\b' || c == '\x7f') { /* backspace or rubout */
        if (ptr > *buf) {
          if (c == '\x7f')
            fputc ('\b', g);
          fputc (' ', g);
          fputc ('\b', g);
          --ptr;
        }
        fflush (g);
        continue;
      }
      fflush (g);
    }
    *ptr++ = c;
    if (c == '\n') {
      *ptr = '\0';
      return ptr - *buf;
    }
    if (ptr + 2 >= eptr) {
      char *nbuf;
      size_t nbufsiz = *bufsiz * 2;
      size_t d = ptr - *buf;
      if ((nbuf = realloc (*buf, nbufsiz)) == NULL) {
        *error = TRUE;
        return 0;
      }
      *buf = nbuf;
      *bufsiz = nbufsiz;
      eptr = nbuf + nbufsiz;
      ptr = nbuf + d;
    }
  }

#endif
}

/****************************************************************************/

size_t tty_fread (void *buf, size_t bsize, size_t n, FILE *f)
{
  int c;
  size_t ret, i;
  char *b;

  if (isatty (fileno (f))) {
    b = buf;
    for (ret = 0; ret < n; ret++)
      for (i = 0; i < bsize; i++) {
        if ((c = fgetc (f)) == EOF)
          return ret;
        *b++ = c;
      }
  } else
    ret = fread (buf, bsize, n, f);
  return ret;
}

/****************************************************************************/

#define NOBLOCK

int inkey (struct string_descr *str)
{
  fflush (stdout);
  if (GetFileType(hStdin) == FILE_TYPE_CHAR) {
#ifdef NOBLOCK
    if (_kbhit()) {
      int c = _getch();
      str->body[0] = (c == 13) ? 10 : (char)c;
      str->length = 1;
    } else {
      /* Sleep(20); */ /* delay 1/50 second to avoid potential busy wait */
      str->length = 0;
    }
#else
    int c = _getch();
    str->body[0] = (c == 13) ? 10 : (char)c;
    str->length = 1;
#endif
  } else {
    str->body[0] = getchar();
    str->length = 1;
  }
  return 1;
}

/****************************************************************************/

double get_timer (void)
{
  SYSTEMTIME system_time;
  FILETIME file_time;
  uint64_t time;
  struct timeval tv;
  static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

  GetSystemTime (&system_time);
  SystemTimeToFileTime (&system_time, &file_time);
  time = (uint64_t)file_time.dwLowDateTime;
  time += ((uint64_t)file_time.dwHighDateTime) << 32;
  tv.tv_sec = (long int) ((time - EPOCH) / 10000000L);
  tv.tv_usec = (long int) (system_time.wMilliseconds * 1000);
  /* gettimeofday (&tv, NULL); */
  return ((double)(tv.tv_sec % 86400)) + 0.000001 * (double)tv.tv_usec;
}

/****************************************************************************/

BOOL shell_cmd (struct string_descr *str)
{
  char *tmp_string = NULL;
  int result;

  fflush (stdout);
  if ((tmp_string = my_malloc (str->length + 1)) == NULL)
    return FALSE;
  memcpy (tmp_string, str->body, str->length);
  tmp_string[str->length] = '\0';
  result = system (tmp_string);
  tmp_string = my_free (tmp_string);
  return result == 0;
}

/****************************************************************************/

void sleep_delay (double seconds)
{
  fflush (stdout);
  if (seconds > 0.02 && GetFileType(hStdin) == FILE_TYPE_CHAR) {
    double start = get_timer();
    while ((get_timer() - start) < seconds && !_kbhit()) {
      Sleep(1); /* yield CPU for 1 millisecond to avoid potential busy wait */
    }
  } else {
    Sleep ((DWORD)(1000.0 * seconds));
  }
}

/****************************************************************************/
