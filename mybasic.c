/* Portable BASIC compiler/interpreter by Peter McGavin.  ANSI C  */

#ifdef THREADED
#include <limits.h>
#endif
#include "mybasic.h"
#include "runtime.h"
#include "my_mem.h"

#ifdef THREADED
static BOOL check_threaded_code_fits (void);
#endif
static void vcompile_error (char *msg, va_list arglist);
static char *check_token (char *line, enum token_type token_to_check_for,
                          struct token_info_type *token_info,
                          char *errmsg, ...);
static BOOL compile_file (struct b *b);
static void compile_line (char *line, struct b *b, T_LONG line_number, int *erl);
static char *compile_conditional_jump (char *p, BOOL jump_if_notequal,
                                       char *label_string, struct b *b);
static int compile_make_label (char *name, struct b *b);
static struct goto_node *add_goto_node (struct goto_node *goto_list_root,
                                 BOOL data, T_SIZE_T code_offset,
                                 char *label_string);
static void resolve_gotos (struct b *b, struct goto_node *goto_list_root);
static char *compile_def (char *p, enum var_type_type var_type);
static char *compile_deffn (char *p, struct b *b);
static char *compile_let (enum token_type token,
                          struct token_info_type *token_info, char *line,
                          struct b *b);
static char *compile_lhs_variable (struct token_info_type *token_info,
                                   struct var_node **var,
                                   struct result_type *result,
                                   char *line, struct b *b);
static void compile_lhs_assign (struct var_node *var,
                                struct result_type *index_result,
                                struct result_type *rhs_result,
                                struct b *b);
static void compile_assign_result (struct result_type *result, size_t offset,
                                   struct b *b);
static char *compile_mid_assign (char *line, struct b *b);
static char *compile_swap (char *line, struct b *b);
static char *compile_read (char *line, struct b *b);
static char *compile_data (char *line, struct b *b);
static char *compile_restore (char *line, struct b *b);
static char *compile_open (char *line, struct b *b);
static char *compile_close (char *line, struct b *b);
static char *compile_print (char *line, struct b *b);
static char *compile_locate (char *line, struct b *b);
static char *compile_color (char *line, struct b *b);
static char *compile_lineinput (char *line, struct b *b);
static char *compile_input (char *line, struct b *b);
static char *compile_prompt_or_file (char *line, BOOL is_lineinput,
                                     struct b *b);
static char *compile_expression (char *line, struct b *b,
                                 struct result_type *result1);
static char *compile_or_operand (char *line, struct b *b,
                                 struct result_type *result1);
static char *compile_and_operand (char *line, struct b *b,
                                  struct result_type *result1);
static char *compile_not_operand (char *line, struct b *b,
                                  struct result_type *result1);
static char *compile_comparand (char *line, struct b *b,
                                struct result_type *result1);
static char *compile_term (char *line, struct b *b,
                           struct result_type *result1);
static char *compile_mod_operand (char *line, struct b *b,
                                  struct result_type *result1);
static char *compile_idivide_operand (char *line, struct b *b,
                                      struct result_type *result1);
static char *compile_factor (char *line, struct b *b,
                             struct result_type *result1);
static char *compile_power_operand (char *line, struct b *b,
                                    struct result_type *result1);
static char *compile_single_argument (char *p, struct b *b,
                                      struct result_type *result1);
static char *compile_subscripts (char *p, struct token_info_type *token_info,
                                 struct var_node **var,
                                 struct result_type *result, struct b *b);
static char *compile_file_number (char *p, struct b *b,
                                  struct token_info_type *token_info,
                                  char *where, struct var_node **var);
static void compile_match_type (struct result_type *result1,
                                struct result_type *result2,
                                BOOL commutative, struct b *b);
static void compile_push_and_operate (struct result_type *result1,
                                      struct result_type *result2,
                                      BOOL commutative, BCODE bcode,
                                      BCODE bcode_const, BCODE bcode_const_16,
                                      BCODE swap_bcode, BCODE swap_bcode_const,
                                      BCODE swap_bcode_const_16, struct b *b);
static void compile_push_const (enum var_type_type type,
                                union number_union *value, struct file *b);
static void gen_bcode_with_const (BCODE bcode, BCODE bcode_16,
                                  enum var_type_type type,
                                  union number_union *value, struct file *b);
static void compile_push_result (struct result_type *result, struct file *b);
static BOOL value_is_equal (enum var_type_type type, union number_union value,
                            int number);
static void compile_check_numeric (enum var_type_type type);
static void compile_check_numeric_or_string (enum var_type_type type);
static void compile_check_string (enum var_type_type type);
static void compile_convert_to_int_or_long (struct result_type *result,
                                            struct b *b);
static void compile_convert_to_single_or_double (struct result_type *result,
                                                 struct b *b);
static void compile_convert_type (struct result_type *src,
                                  enum var_type_type dst_type, struct b *b);
static struct var_node *lookup_var (struct var_node **p,
                             struct token_info_type *token_info,
                             BOOL create_new_entry, struct b *b);
static void free_const_strings (void);
static void *free_tree (struct var_node *p);
static void gen_bcode (BCODE bcode, struct file *b);
static void gen_bcode_size_t (BCODE bcode, BCODE bcode_16, T_SIZE_T s,
                              struct file *b);
static void gen_size_t (T_SIZE_T s, struct file *b);
static void gen_short (short i, struct file *b);
static void gen_int (T_INT i, struct file *b);
static void gen_long (T_LONG l, struct file *b);
static void gen_single (T_SINGLE s, struct file *b);
static void gen_double (T_DOUBLE d, struct file *b);
static void gen_string (T_STRING str, struct file *b);
static void write_b (char *buff, size_t size, size_t align, struct file *b);
#ifndef NOALIGN
static void pad_align (size_t align, struct file *b);
#else
#define pad_align(align,b) /* do nothing */
#endif
static void mwrite_check (char *buff, size_t size, MFILE *f);

/****************************************************************************/

#if (defined(DEBUG) || defined(THREADED))
static const char *bcode_table[] = {
        "B_NOP",
        "B_ILLEGAL",
        "B_STATEMENT_CHECK",
        "B_CLEAR",
        "B_PUSH_CONST_INT",
        "B_PUSH_CONST_INT_16",
        "B_PUSH_CONST_LONG",
        "B_PUSH_CONST_LONG_16",
        "B_PUSH_CONST_SINGLE",
        "B_PUSH_CONST_SINGLE_16",
        "B_PUSH_CONST_DOUBLE",
        "B_PUSH_CONST_DOUBLE_16",
        "B_PUSH_CONST_STRING",
        "B_PUSH_CONST_INDEX",
        "B_PUSH_CONST_INDEX_16",
        "B_PUSH_CONST_INT_ZERO",
        "B_PUSH_CONST_LONG_ZERO",
        "B_PUSH_CONST_SINGLE_ZERO",
        "B_PUSH_CONST_DOUBLE_ZERO",
        "B_PUSH_CONST_INT_ONE",
        "B_PUSH_CONST_LONG_ONE",
        "B_PUSH_CONST_SINGLE_ONE",
        "B_PUSH_CONST_DOUBLE_ONE",
        "B_SET_PRIMARY_FILE",
        "B_SET_PRIMARY_FILE_16",
        "B_SET_PRIMARY_INPUT_FILE",
        "B_SET_PRIMARY_OUTPUT_FILE",
        "B_SET_SECONDARY_FILE",
        "B_SET_SECONDARY_FILE_16",
        "B_SET_SECONDARY_INPUT_FILE",
        "B_SET_SECONDARY_OUTPUT_FILE",
        "B_PUSH_VAR_INT",
        "B_PUSH_VAR_LONG",
        "B_PUSH_VAR_SINGLE",
        "B_PUSH_VAR_DOUBLE",
        "B_PUSH_VAR_STRING",
        "B_PUSH_VAR_INT_16",
        "B_PUSH_VAR_LONG_16",
        "B_PUSH_VAR_SINGLE_16",
        "B_PUSH_VAR_DOUBLE_16",
        "B_PUSH_VAR_STRING_16",
        "B_POP_INT",
        "B_POP_LONG",
        "B_POP_SINGLE",
        "B_POP_DOUBLE",
        "B_POP_STRING",
        "B_POP_STRING_MID2",
        "B_POP_STRING_MID3",
        "B_POP_INT_16",
        "B_POP_LONG_16",
        "B_POP_SINGLE_16",
        "B_POP_DOUBLE_16",
        "B_POP_STRING_16",
        "B_POP_STRING_MID2_16",
        "B_POP_STRING_MID3_16",
        "B_PUSH_INDIRECT_VAR_INT",
        "B_PUSH_INDIRECT_VAR_LONG",
        "B_PUSH_INDIRECT_VAR_SINGLE",
        "B_PUSH_INDIRECT_VAR_DOUBLE",
        "B_PUSH_INDIRECT_VAR_STRING",
        "B_PUSH_INDIRECT_VAR_INT_16",
        "B_PUSH_INDIRECT_VAR_LONG_16",
        "B_PUSH_INDIRECT_VAR_SINGLE_16",
        "B_PUSH_INDIRECT_VAR_DOUBLE_16",
        "B_PUSH_INDIRECT_VAR_STRING_16",
        "B_POP_INDIRECT_INT",
        "B_POP_INDIRECT_LONG",
        "B_POP_INDIRECT_SINGLE",
        "B_POP_INDIRECT_DOUBLE",
        "B_POP_INDIRECT_STRING",
        "B_POP_INDIRECT_STRING_MID2",
        "B_POP_INDIRECT_STRING_MID3",
        "B_POP_INDIRECT_INT_16",
        "B_POP_INDIRECT_LONG_16",
        "B_POP_INDIRECT_SINGLE_16",
        "B_POP_INDIRECT_DOUBLE_16",
        "B_POP_INDIRECT_STRING_16",
        "B_POP_INDIRECT_STRING_MID2_16",
        "B_POP_INDIRECT_STRING_MID3_16",
        "B_ZERO_VAR_INT",
        "B_ZERO_VAR_LONG",
        "B_ZERO_VAR_SINGLE",
        "B_ZERO_VAR_DOUBLE",
        "B_ZERO_VAR_STRING",
        "B_ZERO_VAR_INT_16",
        "B_ZERO_VAR_LONG_16",
        "B_ZERO_VAR_SINGLE_16",
        "B_ZERO_VAR_DOUBLE_16",
        "B_ZERO_VAR_STRING_16",
        "B_ONE_VAR_INT",
        "B_ONE_VAR_LONG",
        "B_ONE_VAR_SINGLE",
        "B_ONE_VAR_DOUBLE",
        "B_ONE_VAR_STRING",
        "B_ONE_VAR_INT_16",
        "B_ONE_VAR_LONG_16",
        "B_ONE_VAR_SINGLE_16",
        "B_ONE_VAR_DOUBLE_16",
        "B_ONE_VAR_STRING_16",
        "B_INT_SWAP",
        "B_LONG_SWAP",
        "B_SINGLE_SWAP",
        "B_DOUBLE_SWAP",
        "B_STRING_SWAP",
        "B_INDEX_DUP",
        "B_INDEX_DUP2",
        "B_INT_UNARY_MINUS",
        "B_LONG_UNARY_MINUS",
        "B_SINGLE_UNARY_MINUS",
        "B_DOUBLE_UNARY_MINUS",
        "B_INT_UNARY_PLUS",
        "B_LONG_UNARY_PLUS",
        "B_SINGLE_UNARY_PLUS",
        "B_DOUBLE_UNARY_PLUS",
        "B_INT_PLUS",
        "B_INT_PLUS_CONST",
        "B_INT_PLUS_CONST_16",
        "B_INT_PLUS_VAR",
        "B_INT_PLUS_VAR_16",
        "B_INT_MINUS",
        "B_INT_MINUS_CONST",
        "B_INT_MINUS_CONST_16",
        "B_INT_MINUS_VAR",
        "B_INT_MINUS_VAR_16",
        "B_INT_SWAP_MINUS",
        "B_INT_SWAP_MINUS_CONST",
        "B_INT_SWAP_MINUS_CONST_16",
        "B_INT_TIMES",
        "B_INT_TIMES_CONST",
        "B_INT_TIMES_CONST_16",
        "B_INT_TIMES_VAR",
        "B_INT_TIMES_VAR_16",
        "B_INT_DIVIDE",
        "B_INT_DIVIDE_CONST",
        "B_INT_DIVIDE_CONST_16",
        "B_INT_SWAP_DIVIDE",
        "B_INT_SWAP_DIVIDE_CONST",
        "B_INT_SWAP_DIVIDE_CONST_16",
        "B_INT_MOD",
        "B_INT_MOD_CONST",
        "B_INT_MOD_CONST_16",
        "B_INT_SWAP_MOD",
        "B_INT_SWAP_MOD_CONST",
        "B_INT_SWAP_MOD_CONST_16",
        "B_INT_POWER",
        "B_INT_POWER_CONST",
        "B_INT_POWER_CONST_16",
        "B_INT_SWAP_POWER",
        "B_INT_SWAP_POWER_CONST",
        "B_INT_SWAP_POWER_CONST_16",
        "B_INT_AND",
        "B_INT_AND_CONST",
        "B_INT_AND_CONST_16",
        "B_INT_OR",
        "B_INT_OR_CONST",
        "B_INT_OR_CONST_16",
        "B_INT_XOR",
        "B_INT_XOR_CONST",
        "B_INT_XOR_CONST_16",
        "B_INT_NOT",
        "B_INT_ABS",
        "B_INT_CLNG",
        "B_INT_CSNG",
        "B_INT_CDBL",
        "B_INT_SGN",
        "B_INT_EQUAL",
        "B_INT_EQUAL_CONST",
        "B_INT_EQUAL_CONST_16",
        "B_INT_NOTEQUAL",
        "B_INT_NOTEQUAL_CONST",
        "B_INT_NOTEQUAL_CONST_16",
        "B_INT_LESS",
        "B_INT_LESS_CONST",
        "B_INT_LESS_CONST_16",
        "B_INT_GREATER",
        "B_INT_GREATER_CONST",
        "B_INT_GREATER_CONST_16",
        "B_INT_LESSOREQUAL",
        "B_INT_LESSOREQUAL_CONST",
        "B_INT_LESSOREQUAL_CONST_16",
        "B_INT_GREATEROREQUAL",
        "B_INT_GREATEROREQUAL_CONST",
        "B_INT_GREATEROREQUAL_CONST_16",
        "B_INT_INCREMENT",
        "B_INT_INCREMENT_VAR",
        "B_INT_INCREMENT_VAR_16",
        "B_INT_DECREMENT",
        "B_INT_DECREMENT_VAR",
        "B_INT_DECREMENT_VAR_16",
        "B_INT_COMMANDCOUNT",
        "B_LONG_PLUS",
        "B_LONG_PLUS_CONST",
        "B_LONG_PLUS_CONST_16",
        "B_LONG_PLUS_VAR",
        "B_LONG_PLUS_VAR_16",
        "B_LONG_MINUS",
        "B_LONG_MINUS_CONST",
        "B_LONG_MINUS_CONST_16",
        "B_LONG_MINUS_VAR",
        "B_LONG_MINUS_VAR_16",
        "B_LONG_SWAP_MINUS",
        "B_LONG_SWAP_MINUS_CONST",
        "B_LONG_SWAP_MINUS_CONST_16",
        "B_LONG_TIMES",
        "B_LONG_TIMES_CONST",
        "B_LONG_TIMES_CONST_16",
        "B_LONG_TIMES_VAR",
        "B_LONG_TIMES_VAR_16",
        "B_LONG_DIVIDE",
        "B_LONG_DIVIDE_CONST",
        "B_LONG_DIVIDE_CONST_16",
        "B_LONG_SWAP_DIVIDE",
        "B_LONG_SWAP_DIVIDE_CONST",
        "B_LONG_SWAP_DIVIDE_CONST_16",
        "B_LONG_MOD",
        "B_LONG_MOD_CONST",
        "B_LONG_MOD_CONST_16",
        "B_LONG_SWAP_MOD",
        "B_LONG_SWAP_MOD_CONST",
        "B_LONG_SWAP_MOD_CONST_16",
        "B_LONG_POWER",
        "B_LONG_POWER_CONST",
        "B_LONG_POWER_CONST_16",
        "B_LONG_SWAP_POWER",
        "B_LONG_SWAP_POWER_CONST",
        "B_LONG_SWAP_POWER_CONST_16",
        "B_LONG_AND",
        "B_LONG_AND_CONST",
        "B_LONG_AND_CONST_16",
        "B_LONG_OR",
        "B_LONG_OR_CONST",
        "B_LONG_OR_CONST_16",
        "B_LONG_XOR",
        "B_LONG_XOR_CONST",
        "B_LONG_XOR_CONST_16",
        "B_LONG_NOT",
        "B_LONG_ABS",
        "B_LONG_INT",
        "B_LONG_CINT",
        "B_LONG_CSNG",
        "B_LONG_CDBL",
        "B_LONG_FIX",
        "B_LONG_SGN",
        "B_LONG_EQUAL",
        "B_LONG_EQUAL_CONST",
        "B_LONG_EQUAL_CONST_16",
        "B_LONG_NOTEQUAL",
        "B_LONG_NOTEQUAL_CONST",
        "B_LONG_NOTEQUAL_CONST_16",
        "B_LONG_LESS",
        "B_LONG_LESS_CONST",
        "B_LONG_LESS_CONST_16",
        "B_LONG_GREATER",
        "B_LONG_GREATER_CONST",
        "B_LONG_GREATER_CONST_16",
        "B_LONG_LESSOREQUAL",
        "B_LONG_LESSOREQUAL_CONST",
        "B_LONG_LESSOREQUAL_CONST_16",
        "B_LONG_GREATEROREQUAL",
        "B_LONG_GREATEROREQUAL_CONST",
        "B_LONG_GREATEROREQUAL_CONST_16",
        "B_LONG_INCREMENT",
        "B_LONG_INCREMENT_VAR",
        "B_LONG_INCREMENT_VAR_16",
        "B_LONG_DECREMENT",
        "B_LONG_DECREMENT_VAR",
        "B_LONG_DECREMENT_VAR_16",
        "B_SINGLE_PLUS",
        "B_SINGLE_PLUS_CONST",
        "B_SINGLE_PLUS_CONST_16",
        "B_SINGLE_PLUS_VAR",
        "B_SINGLE_PLUS_VAR_16",
        "B_SINGLE_MINUS",
        "B_SINGLE_MINUS_CONST",
        "B_SINGLE_MINUS_CONST_16",
        "B_SINGLE_MINUS_VAR",
        "B_SINGLE_MINUS_VAR_16",
        "B_SINGLE_SWAP_MINUS",
        "B_SINGLE_SWAP_MINUS_CONST",
        "B_SINGLE_SWAP_MINUS_CONST_16",
        "B_SINGLE_TIMES",
        "B_SINGLE_TIMES_CONST",
        "B_SINGLE_TIMES_CONST_16",
        "B_SINGLE_TIMES_VAR",
        "B_SINGLE_TIMES_VAR_16",
        "B_SINGLE_DIVIDE",
        "B_SINGLE_DIVIDE_CONST",
        "B_SINGLE_DIVIDE_CONST_16",
        "B_SINGLE_SWAP_DIVIDE",
        "B_SINGLE_SWAP_DIVIDE_CONST",
        "B_SINGLE_SWAP_DIVIDE_CONST_16",
        "B_SINGLE_POWER",
        "B_SINGLE_POWER_CONST",
        "B_SINGLE_POWER_CONST_16",
        "B_SINGLE_SWAP_POWER",
        "B_SINGLE_SWAP_POWER_CONST",
        "B_SINGLE_SWAP_POWER_CONST_16",
        "B_SINGLE_SIN",
        "B_SINGLE_COS",
        "B_SINGLE_TAN",
        "B_SINGLE_ATN",
        "B_SINGLE_LOG",
        "B_SINGLE_LOG2",
        "B_SINGLE_LOG10",
        "B_SINGLE_EXP",
        "B_SINGLE_SQR",
        "B_SINGLE_ABS",
        "B_SINGLE_RND",
        "B_SINGLE_INT",
        "B_SINGLE_CINT",
        "B_SINGLE_CLNG",
        "B_SINGLE_CDBL",
        "B_SINGLE_FIX",
        "B_SINGLE_SGN",
        "B_SINGLE_EQUAL",
        "B_SINGLE_EQUAL_CONST",
        "B_SINGLE_EQUAL_CONST_16",
        "B_SINGLE_NOTEQUAL",
        "B_SINGLE_NOTEQUAL_CONST",
        "B_SINGLE_NOTEQUAL_CONST_16",
        "B_SINGLE_LESS",
        "B_SINGLE_LESS_CONST",
        "B_SINGLE_LESS_CONST_16",
        "B_SINGLE_GREATER",
        "B_SINGLE_GREATER_CONST",
        "B_SINGLE_GREATER_CONST_16",
        "B_SINGLE_LESSOREQUAL",
        "B_SINGLE_LESSOREQUAL_CONST",
        "B_SINGLE_LESSOREQUAL_CONST_16",
        "B_SINGLE_GREATEROREQUAL",
        "B_SINGLE_GREATEROREQUAL_CONST",
        "B_SINGLE_GREATEROREQUAL_CONST_16",
        "B_SINGLE_INCREMENT",
        "B_SINGLE_INCREMENT_VAR",
        "B_SINGLE_INCREMENT_VAR_16",
        "B_SINGLE_DECREMENT",
        "B_SINGLE_DECREMENT_VAR",
        "B_SINGLE_DECREMENT_VAR_16",
        "B_DOUBLE_PLUS",
        "B_DOUBLE_PLUS_CONST",
        "B_DOUBLE_PLUS_CONST_16",
        "B_DOUBLE_PLUS_VAR",
        "B_DOUBLE_PLUS_VAR_16",
        "B_DOUBLE_MINUS",
        "B_DOUBLE_MINUS_CONST",
        "B_DOUBLE_MINUS_CONST_16",
        "B_DOUBLE_MINUS_VAR",
        "B_DOUBLE_MINUS_VAR_16",
        "B_DOUBLE_SWAP_MINUS",
        "B_DOUBLE_SWAP_MINUS_CONST",
        "B_DOUBLE_SWAP_MINUS_CONST_16",
        "B_DOUBLE_TIMES",
        "B_DOUBLE_TIMES_CONST",
        "B_DOUBLE_TIMES_CONST_16",
        "B_DOUBLE_TIMES_VAR",
        "B_DOUBLE_TIMES_VAR_16",
        "B_DOUBLE_DIVIDE",
        "B_DOUBLE_DIVIDE_CONST",
        "B_DOUBLE_DIVIDE_CONST_16",
        "B_DOUBLE_SWAP_DIVIDE",
        "B_DOUBLE_SWAP_DIVIDE_CONST",
        "B_DOUBLE_SWAP_DIVIDE_CONST_16",
        "B_DOUBLE_POWER",
        "B_DOUBLE_POWER_CONST",
        "B_DOUBLE_POWER_CONST_16",
        "B_DOUBLE_SWAP_POWER",
        "B_DOUBLE_SWAP_POWER_CONST",
        "B_DOUBLE_SWAP_POWER_CONST_16",
        "B_DOUBLE_SIN",
        "B_DOUBLE_COS",
        "B_DOUBLE_TAN",
        "B_DOUBLE_ATN",
        "B_DOUBLE_LOG",
        "B_DOUBLE_LOG2",
        "B_DOUBLE_LOG10",
        "B_DOUBLE_EXP",
        "B_DOUBLE_SQR",
        "B_DOUBLE_ABS",
        "B_DOUBLE_INT",
        "B_DOUBLE_CINT",
        "B_DOUBLE_CLNG",
        "B_DOUBLE_CSNG",
        "B_DOUBLE_FIX",
        "B_DOUBLE_SGN",
        "B_DOUBLE_EQUAL",
        "B_DOUBLE_EQUAL_CONST",
        "B_DOUBLE_EQUAL_CONST_16",
        "B_DOUBLE_NOTEQUAL",
        "B_DOUBLE_NOTEQUAL_CONST",
        "B_DOUBLE_NOTEQUAL_CONST_16",
        "B_DOUBLE_LESS",
        "B_DOUBLE_LESS_CONST",
        "B_DOUBLE_LESS_CONST_16",
        "B_DOUBLE_GREATER",
        "B_DOUBLE_GREATER_CONST",
        "B_DOUBLE_GREATER_CONST_16",
        "B_DOUBLE_LESSOREQUAL",
        "B_DOUBLE_LESSOREQUAL_CONST",
        "B_DOUBLE_LESSOREQUAL_CONST_16",
        "B_DOUBLE_GREATEROREQUAL",
        "B_DOUBLE_GREATEROREQUAL_CONST",
        "B_DOUBLE_GREATEROREQUAL_CONST_16",
        "B_DOUBLE_INCREMENT",
        "B_DOUBLE_INCREMENT_VAR",
        "B_DOUBLE_INCREMENT_VAR_16",
        "B_DOUBLE_DECREMENT",
        "B_DOUBLE_DECREMENT_VAR",
        "B_DOUBLE_DECREMENT_VAR_16",
        "B_STRING_PLUS",
        "B_STRING_EQUAL",
        "B_STRING_NOTEQUAL",
        "B_STRING_LESS",
        "B_STRING_GREATER",
        "B_STRING_LESSOREQUAL",
        "B_STRING_GREATEROREQUAL",
        "B_STRING_LEN",
        "B_STRING_LEFT",
        "B_STRING_MID2",
        "B_STRING_MID3",
        "B_STRING_RIGHT",
        "B_STRING_INSTR2",
        "B_STRING_INSTR3",
        "B_STRING_RINSTR2",
        "B_STRING_RINSTR3",
        "B_STRING_CHR",
        "B_STRING_ASC",
        "B_STRING_VAL",
        "B_STRING_STR_INT",
        "B_STRING_STR_LONG",
        "B_STRING_STR_SINGLE",
        "B_STRING_STR_DOUBLE",
        "B_STRING_DATE",
        "B_STRING_TIME",
        "B_STRING_COMMAND",
        "B_STRING_COMMAND1",
        "B_DOUBLE_TIMER",
        "B_EOF",
        "B_LOF",
        "B_POS",
        "B_STRING_MKI",
        "B_STRING_MKL",
        "B_STRING_MKS",
        "B_STRING_MKD",
        "B_INT_CVI",
        "B_LONG_CVL",
        "B_SINGLE_CVS",
        "B_DOUBLE_CVD",
        "B_STRING_BIN",
        "B_STRING_OCT",
        "B_STRING_HEX",
        "B_STRING_LCASE",
        "B_STRING_UCASE",
        "B_STRING_TRIM",
        "B_STRING_LTRIM",
        "B_STRING_RTRIM",
        "B_STRING_STRING",
        "B_STRING_STRING2",
        "B_STRING_SPACE",
        "B_STRING_GETENV",
        "B_LONG_LBOUND",
        "B_LONG_UBOUND",
        "B_INDEX_PLUS",
        "B_INDEX_TIMES_CONST",
        "B_INDEX_TIMES_CONST_16",
        "B_INT_TO_LONG",
        "B_INT_TO_SINGLE",
        "B_INT_TO_DOUBLE",
        "B_INT_TO_INDEX",
        "B_INT_TO_INDEX_16",
        "B_INT_TO_INDEX_PLUS",
        "B_INT_TO_INDEX_PLUS_16",
        "B_INT_TO_INDEX_MINUS_1",
        "B_INT_TO_INDEX_MINUS_1_16",
        "B_INT_TO_INDEX_PLUS_MINUS_1",
        "B_INT_TO_INDEX_PLUS_MINUS_1_16",
        "B_LONG_TO_INT",
        "B_LONG_TO_SINGLE",
        "B_LONG_TO_DOUBLE",
        "B_LONG_TO_INDEX",
        "B_LONG_TO_INDEX_16",
        "B_LONG_TO_INDEX_PLUS",
        "B_LONG_TO_INDEX_PLUS_16",
        "B_LONG_TO_INDEX_MINUS_1",
        "B_LONG_TO_INDEX_MINUS_1_16",
        "B_LONG_TO_INDEX_PLUS_MINUS_1",
        "B_LONG_TO_INDEX_PLUS_MINUS_1_16",
        "B_SINGLE_TO_INT",
        "B_SINGLE_TO_LONG",
        "B_SINGLE_TO_DOUBLE",
        "B_SINGLE_TO_INDEX",
        "B_SINGLE_TO_INDEX_16",
        "B_SINGLE_TO_INDEX_PLUS",
        "B_SINGLE_TO_INDEX_PLUS_16",
        "B_SINGLE_TO_INDEX_MINUS_1",
        "B_SINGLE_TO_INDEX_MINUS_1_16",
        "B_SINGLE_TO_INDEX_PLUS_MINUS_1",
        "B_SINGLE_TO_INDEX_PLUS_MINUS_1_16",
        "B_DOUBLE_TO_INT",
        "B_DOUBLE_TO_LONG",
        "B_DOUBLE_TO_SINGLE",
        "B_DOUBLE_TO_INDEX",
        "B_DOUBLE_TO_INDEX_16",
        "B_DOUBLE_TO_INDEX_PLUS",
        "B_DOUBLE_TO_INDEX_PLUS_16",
        "B_DOUBLE_TO_INDEX_MINUS_1",
        "B_DOUBLE_TO_INDEX_MINUS_1_16",
        "B_DOUBLE_TO_INDEX_PLUS_MINUS_1",
        "B_DOUBLE_TO_INDEX_PLUS_MINUS_1_16",
        "B_JUMP",
        "B_JUMP_16",
        "B_JUMP_INT_EQUAL_ZERO",
        "B_JUMP_INT_EQUAL_ZERO_16",
        "B_JUMP_INT_NOTEQUAL_ZERO",
        "B_JUMP_INT_NOTEQUAL_ZERO_16",
        "B_JUMP_LONG_EQUAL_ZERO",
        "B_JUMP_LONG_EQUAL_ZERO_16",
        "B_JUMP_LONG_NOTEQUAL_ZERO",
        "B_JUMP_LONG_NOTEQUAL_ZERO_16",
        "B_JUMP_SINGLE_EQUAL_ZERO",
        "B_JUMP_SINGLE_EQUAL_ZERO_16",
        "B_JUMP_SINGLE_NOTEQUAL_ZERO",
        "B_JUMP_SINGLE_NOTEQUAL_ZERO_16",
        "B_JUMP_DOUBLE_EQUAL_ZERO",
        "B_JUMP_DOUBLE_EQUAL_ZERO_16",
        "B_JUMP_DOUBLE_NOTEQUAL_ZERO",
        "B_JUMP_DOUBLE_NOTEQUAL_ZERO_16",
        "B_JUMP_INT_LESS",
        "B_JUMP_LONG_LESS",
        "B_JUMP_SINGLE_LESS",
        "B_JUMP_DOUBLE_LESS",
        "B_JUMP_INT_GREATER",
        "B_JUMP_LONG_GREATER",
        "B_JUMP_SINGLE_GREATER",
        "B_JUMP_DOUBLE_GREATER",
        "B_GOSUB",
        "B_ON_GOTO",
        "B_ON_GOSUB",
        "B_ON_ERROR_GOTO",
        "B_ON_ERROR_GOTO_0",
        "B_RESUME",
        "B_RESUME_LABEL",
        "B_RESUME_NEXT",
        "B_ERR",
        "B_ERL",
        "B_ERROR",
        "B_RETURN",
        "B_RETURN_LINE",
        "B_RANDOMIZE_TIMER",
        "B_RANDOMIZE_LONG",
        "B_OPEN",
        "B_CLOSE",
        "B_KILL",
        "B_NAME",
        "B_CHDIR",
        "B_MKDIR",
        "B_RMDIR",
        "B_SHELL",
        "B_CHAIN",
        "B_PRINT_INT",
        "B_PRINT_LONG",
        "B_PRINT_SINGLE",
        "B_PRINT_DOUBLE",
        "B_PRINT_STRING",
        "B_PRINT_COMMA",
        "B_PRINT_TAB",
        "B_PRINT_SPC",
        "B_PRINT_NEWLINE",
        "B_PRINT_END",
        "B_PRINT_USING_INIT",
        "B_PRINT_USING_INT",
        "B_PRINT_USING_LONG",
        "B_PRINT_USING_SINGLE",
        "B_PRINT_USING_DOUBLE",
        "B_PRINT_USING_STRING",
        "B_PRINT_USING_END",
        "B_BEEP",
        "B_SLEEP",
        "B_CLS",
        "B_LOCATE",
        "B_COLOR",
        "B_BACKGROUND_COLOR",
        "B_LINEINPUT",
        "B_INPUT_RESTART",
        "B_INPUT_CLEAR_PROMPT_STRING",
        "B_INPUT_SET_PROMPT_STRING",
        "B_INPUT_EXTEND_PROMPT_STRING",
        "B_INPUT_START",
        "B_INPUT_INT",
        "B_INPUT_LONG",
        "B_INPUT_SINGLE",
        "B_INPUT_DOUBLE",
        "B_INPUT_STRING",
        "B_INPUT_DOLLAR",
        "B_INKEY_DOLLAR",
        "B_READ_INT",
        "B_READ_LONG",
        "B_READ_SINGLE",
        "B_READ_DOUBLE",
        "B_READ_STRING",
        "B_RESTORE",
        "B_RESTORE_LINE",
        "B_STOP",
        "B_END"
};
#endif

static const struct token_table token_table[] = {
        {"\0",           FALSE},       /* TOKEN_NONE */
        {"\0",           FALSE},       /* TOKEN_OTHER */
        {"\0",           FALSE},       /* TOKEN_ID */
        {"\0",           FALSE},       /* TOKEN_FUNCTION */
        {"\0",           FALSE},       /* TOKEN_NUMBER */
        {"\0",           FALSE},       /* TOKEN_STRING */
        {"\0",           FALSE},       /* TOKEN_FILE */
        {"REM",          FALSE},       /* TOKEN_REM */
        {"OPTION",       FALSE},       /* TOKEN_OPTION */
        {"BASE",         FALSE},       /* TOKEN_BASE */
        {"DIM",          FALSE},       /* TOKEN_DIM */
        {"CLEAR",        FALSE},       /* TOKEN_CLEAR */
        {"LET",          FALSE},       /* TOKEN_LET */
        {"IF",           FALSE},       /* TOKEN_IF */
        {"THEN",         FALSE},       /* TOKEN_THEN */
        {"ELSEIF",       FALSE},       /* TOKEN_ELSEIF */
        {"ELSE",         FALSE},       /* TOKEN_ELSE */
        {"ENDIF",        FALSE},       /* TOKEN_ENDIF */
        {"DO",           FALSE},       /* TOKEN_DO */
        {"WHILE",        FALSE},       /* TOKEN_WHILE */
        {"UNTIL",        FALSE},       /* TOKEN_UNTIL */
        {"WEND",         FALSE},       /* TOKEN_WEND */
        {"LOOP",         FALSE},       /* TOKEN_LOOP */
        {"FOR",          FALSE},       /* TOKEN_FOR */
        {"TO",           FALSE},       /* TOKEN_TO */
        {"STEP",         FALSE},       /* TOKEN_STEP */
        {"NEXT",         FALSE},       /* TOKEN_NEXT */
        {"EXIT",         FALSE},       /* TOKEN_EXIT */
        {"GOSUB",        FALSE},       /* TOKEN_GOSUB */
        {"RETURN",       FALSE},       /* TOKEN_RETURN */
        {"SYSTEM",       FALSE},       /* TOKEN_SYSTEM */
        {"STOP",         FALSE},       /* TOKEN_STOP */
        {"END",          FALSE},       /* TOKEN_END */
        {"OPEN",         FALSE},       /* TOKEN_OPEN */
        {"CLOSE",        FALSE},       /* TOKEN_CLOSE */
        {"AS",           FALSE},       /* TOKEN_AS */
        {"INPUT",        FALSE},       /* TOKEN_INPUT */
        {"OUTPUT",       FALSE},       /* TOKEN_OUTPUT */
        {"APPEND",       FALSE},       /* TOKEN_APPEND */
        {"KILL",         FALSE},       /* TOKEN_KILL */
        {"NAME",         FALSE},       /* TOKEN_NAME */
        {"CHDIR",        FALSE},       /* TOKEN_CHDIR */
        {"MKDIR",        FALSE},       /* TOKEN_MKDIR */
        {"RMDIR",        FALSE},       /* TOKEN_RMDIR */
        {"SHELL",        FALSE},       /* TOKEN_SHELL */
        {"CHAIN",        FALSE},       /* TOKEN_CHAIN */
        {"PRINT",        FALSE},       /* TOKEN_PRINT */
        {"USING",        FALSE},       /* TOKEN_USING */
        {"TAB",          FALSE},       /* TOKEN_TAB */
        {"SPC",          FALSE},       /* TOKEN_SPC */
        {"BEEP",         FALSE},       /* TOKEN_BEEP */
        {"SLEEP",        FALSE},       /* TOKEN_SLEEP */
        {"CLS",          FALSE},       /* TOKEN_CLS */
        {"LOCATE",       FALSE},       /* TOKEN_LOCATE */
        {"COLOR",        FALSE},       /* TOKEN_COLOR */
        {"LINE",         FALSE},       /* TOKEN_LINE */
        {"LINEINPUT",    FALSE},       /* TOKEN_LINEINPUT */
        {"INPUT",         TRUE},       /* TOKEN_INPUT_DOLLAR */
        {"INKEY",         TRUE},       /* TOKEN_INKEY_DOLLAR */
        {"GOTO",         FALSE},       /* TOKEN_GOTO */
        {"GO",           FALSE},       /* TOKEN_GO */
        {"ON",           FALSE},       /* TOKEN_ON */
        {"ERROR",        FALSE},       /* TOKEN_ERROR */
        {"RESUME",       FALSE},       /* TOKEN_RESUME */
        {"ERR",          FALSE},       /* TOKEN_ERR */
        {"ERL",          FALSE},       /* TOKEN_ERL */
        {"RANDOMIZE",    FALSE},       /* TOKEN_RANDOMIZE */
        {"TIMER",        FALSE},       /* TOKEN_TIMER */
        {"SWAP",         FALSE},       /* TOKEN_SWAP */
        {"DEFINT",       FALSE},       /* TOKEN_DEFINT */
        {"DEFLNG",       FALSE},       /* TOKEN_DEFLNG */
        {"DEFSNG",       FALSE},       /* TOKEN_DEFSNG */
        {"DEFDBL",       FALSE},       /* TOKEN_DEFDBL */
        {"DEFSTR",       FALSE},       /* TOKEN_DEFSTR */
        {"DEF",          FALSE},       /* TOKEN_DEF */
        {"INCR",         FALSE},       /* TOKEN_INCR */
        {"DECR",         FALSE},       /* TOKEN_DECR */
        {":",            FALSE},       /* TOKEN_COLON */
        {";",            FALSE},       /* TOKEN_SEMICOLON */
        {",",            FALSE},       /* TOKEN_COMMA */
        {"\"",           FALSE},       /* TOKEN_DOUBLEQUOTE */
        {"'",            FALSE},       /* TOKEN_QUOTE */
        {"?",            FALSE},       /* TOKEN_QUESTIONMARK */
        {"=",            FALSE},       /* TOKEN_EQUAL */
        {"+",            FALSE},       /* TOKEN_PLUS */
        {"-",            FALSE},       /* TOKEN_MINUS */
        {"*",            FALSE},       /* TOKEN_TIMES */
        {"/",            FALSE},       /* TOKEN_DIVIDE */
        {"\\",           FALSE},       /* TOKEN_IDIVIDE */
        {"MOD",          FALSE},       /* TOKEN_MOD */
        {"^",            FALSE},       /* TOKEN_POWER */
        {"<>",           FALSE},       /* TOKEN_NOTEQUAL */
        {"<",            FALSE},       /* TOKEN_LESSTHAN */
        {">",            FALSE},       /* TOKEN_GREATERTHAN */
        {"<=",           FALSE},       /* TOKEN_LESSTHANOREQUAL */
        {">=",           FALSE},       /* TOKEN_GREATERTHANOREQUAL */
        {"(",            FALSE},       /* TOKEN_LBRACKET */
        {")",            FALSE},       /* TOKEN_RBRACKET */
        {"AND",          FALSE},       /* TOKEN_AND */
        {"OR",           FALSE},       /* TOKEN_OR */
        {"XOR",          FALSE},       /* TOKEN_XOR */
        {"NOT",          FALSE},       /* TOKEN_NOT */
        {"SIN",          FALSE},       /* TOKEN_SIN */
        {"COS",          FALSE},       /* TOKEN_COS */
        {"TAN",          FALSE},       /* TOKEN_TAN */
        {"ATN",          FALSE},       /* TOKEN_ATN */
        {"LOG",          FALSE},       /* TOKEN_LOG */
        {"LOG2",         FALSE},       /* TOKEN_LOG2 */
        {"LOG10",        FALSE},       /* TOKEN_LOG10 */
        {"EXP",          FALSE},       /* TOKEN_EXP */
        {"SQR",          FALSE},       /* TOKEN_SQR */
        {"ABS",          FALSE},       /* TOKEN_ABS */
        {"RND",          FALSE},       /* TOKEN_RND */
        {"INT",          FALSE},       /* TOKEN_INT */
        {"CINT",         FALSE},       /* TOKEN_CINT */
        {"CLNG",         FALSE},       /* TOKEN_CLNG */
        {"CSNG",         FALSE},       /* TOKEN_CSNG */
        {"CDBL",         FALSE},       /* TOKEN_CDBL */
        {"FIX",          FALSE},       /* TOKEN_FIX */
        {"SGN",          FALSE},       /* TOKEN_SGN */
        {"LEN",          FALSE},       /* TOKEN_LEN */
        {"LEFT",          TRUE},       /* TOKEN_LEFT */
        {"MID",           TRUE},       /* TOKEN_MID */
        {"RIGHT",         TRUE},       /* TOKEN_RIGHT */
        {"INSTR",        FALSE},       /* TOKEN_INSTR */
        {"RINSTR",       FALSE},       /* TOKEN_RINSTR */
        {"CHR",           TRUE},       /* TOKEN_CHR */
        {"ASC",          FALSE},       /* TOKEN_ASC */
        {"VAL",          FALSE},       /* TOKEN_VAL */
        {"STR",           TRUE},       /* TOKEN_STR */
        {"DATE",          TRUE},       /* TOKEN_DATE */
        {"TIME",          TRUE},       /* TOKEN_TIME */
        {"COMMAND",       TRUE},       /* TOKEN_COMMAND */
        {"_COMMANDCOUNT",FALSE},       /* TOKEN_COMMANDCOUNT */
        {"EOF",          FALSE},       /* TOKEN_EOF */
        {"LOF",          FALSE},       /* TOKEN_LOF */
        {"POS",          FALSE},       /* TOKEN_POS */
        {"MKI",           TRUE},       /* TOKEN_MKI */
        {"MKL",           TRUE},       /* TOKEN_MKL */
        {"MKS",           TRUE},       /* TOKEN_MKS */
        {"MKD",           TRUE},       /* TOKEN_MKD */
        {"CVI",          FALSE},       /* TOKEN_CVI */
        {"CVL",          FALSE},       /* TOKEN_CVL */
        {"CVS",          FALSE},       /* TOKEN_CVS */
        {"CVD",          FALSE},       /* TOKEN_CVD */
        {"BIN",           TRUE},       /* TOKEN_BIN */
        {"OCT",           TRUE},       /* TOKEN_OCT */
        {"HEX",           TRUE},       /* TOKEN_HEX */
        {"LCASE",         TRUE},       /* TOKEN_LCASE */
        {"UCASE",         TRUE},       /* TOKEN_UCASE */
        {"TRIM",          TRUE},       /* TOKEN_TRIM */
        {"LTRIM",         TRUE},       /* TOKEN_LTRIM */
        {"RTRIM",         TRUE},       /* TOKEN_RTRIM */
        {"STRING",        TRUE},       /* TOKEN_STRING_DOLLAR */
        {"SPACE",         TRUE},       /* TOKEN_SPACE */
        {"GETENV",        TRUE},       /* TOKEN_GETENV */
        {"LBOUND",       FALSE},       /* TOKEN_LBOUND */
        {"UBOUND",       FALSE},       /* TOKEN_UBOUN */
        {"READ",         FALSE},       /* TOKEN_READ */
        {"DATA",         FALSE},       /* TOKEN_DATA */
        {"RESTORE",      FALSE},       /* TOKEN_RESTORE */
        {NULL,           FALSE}
};

static const struct type_info_type type_info[NUMBER_OF_TYPES] = {{
        "INT",                    /* type_name */
        SIZE_INT,                 /* type_size */
        TRUE,                     /* type_numeric */
        TRUE,                     /* type_integer */
        FALSE,                    /* type_float */
        B_PUSH_CONST_INT,         /* b_push_const */
        B_PUSH_CONST_INT_16,      /* b_push_const_16 */
        B_PUSH_CONST_INT_ZERO,    /* b_push_const_zero */
        B_PUSH_CONST_INT_ONE,     /* b_push_const_one */
        B_PUSH_VAR_INT,           /* b_push_var */
        B_PUSH_VAR_INT_16,        /* b_push_var_16 */
        B_POP_INT,                /* b_pop */
        B_POP_INT_16,             /* b_pop_16 */
        B_PUSH_INDIRECT_VAR_INT,  /* b_push_indirect_var */
        B_PUSH_INDIRECT_VAR_INT_16,/* b_push_indirect_var_16 */
        B_POP_INDIRECT_INT,       /* b_pop_indirect */
        B_POP_INDIRECT_INT_16,    /* b_pop_indirect_16 */
        B_ZERO_VAR_INT,           /* b_zero_var */
        B_ZERO_VAR_INT_16,        /* b_zero_var_16 */
        B_ONE_VAR_INT,            /* b_one_var */
        B_ONE_VAR_INT_16,         /* b_one_var_16 */
        B_INT_SWAP,               /* b_swap */
        B_INT_EQUAL,              /* b_equal */
        B_INT_EQUAL_CONST,        /* b_equal_const */
        B_INT_EQUAL_CONST_16,     /* b_equal_const_16 */
        B_INT_NOTEQUAL,           /* b_notequal */
        B_INT_NOTEQUAL_CONST,     /* b_notequal_const */
        B_INT_NOTEQUAL_CONST_16,  /* b_notequal_const_16 */
        B_INT_LESS,           /* b_less */
        B_INT_LESS_CONST,     /* b_less_const */
        B_INT_LESS_CONST_16,  /* b_less_const_16 */
        B_INT_GREATER,        /* b_greater */
        B_INT_GREATER_CONST,  /* b_greater_const */
        B_INT_GREATER_CONST_16,/* b_greater_const_16 */
        B_INT_LESSOREQUAL,    /* b_lessorequal */
        B_INT_LESSOREQUAL_CONST,/* b_lessorequal_const */
        B_INT_LESSOREQUAL_CONST_16,/* b_lessorequal_const_16 */
        B_INT_GREATEROREQUAL, /* b_greaterorequal */
        B_INT_GREATEROREQUAL_CONST,/* b_greaterorequal_const */
        B_INT_GREATEROREQUAL_CONST_16,/* b_greaterorequal_const_16 */
        B_INT_INCREMENT,          /* b_increment */
        B_INT_INCREMENT_VAR,      /* b_increment_var */
        B_INT_INCREMENT_VAR_16,   /* b_increment_var_16 */
        B_INT_DECREMENT,          /* b_decrement */
        B_INT_DECREMENT_VAR,      /* b_decrement_var */
        B_INT_DECREMENT_VAR_16,   /* b_decrement_var_16 */
        B_INT_UNARY_MINUS,        /* b_unary_minus */
        B_INT_PLUS,               /* b_plus */
        B_INT_PLUS_CONST,         /* b_plus_const */
        B_INT_PLUS_CONST_16,      /* b_plus_const_16 */
        B_INT_PLUS_VAR,           /* b_plus_var */
        B_INT_PLUS_VAR_16,        /* b_plus_var_16 */
        B_INT_MINUS,              /* b_minus */
        B_INT_MINUS_CONST,        /* b_minus_const */
        B_INT_MINUS_CONST_16,     /* b_minus_const_16 */
        B_INT_MINUS_VAR,        /* b_minus_var */
        B_INT_MINUS_VAR_16,     /* b_minus_var_16 */
        B_INT_SWAP_MINUS,         /* b_swap_minus */
        B_INT_SWAP_MINUS_CONST,   /* b_swap_minus_const */
        B_INT_SWAP_MINUS_CONST_16,/* b_swap_minus_const_16 */
        B_INT_TIMES,              /* b_times */
        B_INT_TIMES_CONST,        /* b_times_const */
        B_INT_TIMES_CONST_16,     /* b_times_const_16 */
        B_INT_TIMES_VAR,          /* b_times_var */
        B_INT_TIMES_VAR_16,       /* b_times_var_16 */
        B_ILLEGAL,                /* b_divide */
        B_ILLEGAL,                /* b_divide_const */
        B_ILLEGAL,                /* b_divide_const_16 */
        B_ILLEGAL,                /* b_swap_divide */
        B_ILLEGAL,                /* b_swap_divide_const */
        B_ILLEGAL,                /* b_swap_divide_const_16 */
        B_INT_DIVIDE,             /* b_idivide */
        B_INT_DIVIDE_CONST,       /* b_idivide_const */
        B_INT_DIVIDE_CONST_16,    /* b_idivide_const_16 */
        B_INT_SWAP_DIVIDE,        /* b_swap_idivide */
        B_INT_SWAP_DIVIDE_CONST,  /* b_swap_idivide_const */
        B_INT_SWAP_DIVIDE_CONST_16,/* b_swap_idivide_const_16 */
        B_INT_MOD,                /* b_mod */
        B_INT_MOD_CONST,          /* b_mod_const */
        B_INT_MOD_CONST_16,       /* b_mod_const_16 */
        B_INT_SWAP_MOD,           /* b_swap_mod */
        B_INT_SWAP_MOD_CONST,     /* b_swap_mod_const */
        B_INT_SWAP_MOD_CONST_16,  /* b_swap_mod_const_16 */
        B_INT_POWER,              /* b_power */
        B_INT_POWER_CONST,        /* b_power_const */
        B_INT_POWER_CONST_16,     /* b_power_const_16 */
        B_INT_SWAP_POWER,         /* b_swap_power */
        B_INT_SWAP_POWER_CONST,   /* b_swap_power_const */
        B_INT_SWAP_POWER_CONST_16,/* b_swap_power_const_16 */
        B_PRINT_INT,              /* b_print */
        B_PRINT_USING_INT,        /* b_print_using */
        B_INPUT_INT,              /* b_input */
        B_READ_INT,               /* b_read */
        B_INT_AND,                /* b_and */
        B_INT_AND_CONST,          /* b_and_const */
        B_INT_AND_CONST_16,       /* b_and_const_16 */
        B_INT_OR,                 /* b_or */
        B_INT_OR_CONST,           /* b_or_const */
        B_INT_OR_CONST_16,        /* b_or_const_16 */
        B_INT_XOR,                /* b_xor */
        B_INT_XOR_CONST,          /* b_xor_const */
        B_INT_XOR_CONST_16,       /* b_xor_const_16 */
        B_INT_NOT,                /* b_not */
        B_ILLEGAL,                /* b_sin */
        B_ILLEGAL,                /* b_cos */
        B_ILLEGAL,                /* b_tan */
        B_ILLEGAL,                /* b_atn */
        B_ILLEGAL,                /* b_log */
        B_ILLEGAL,                /* b_log2 */
        B_ILLEGAL,                /* b_log10 */
        B_ILLEGAL,                /* b_exp */
        B_ILLEGAL,                /* b_sqr */
        B_INT_ABS,                /* b_abs */
        B_ILLEGAL,                /* b_int */
        B_ILLEGAL,                /* b_cint */
        B_INT_CLNG,               /* b_clng */
        B_INT_CSNG,               /* b_csng */
        B_INT_CDBL,               /* b_cdbl */
        B_ILLEGAL,                /* b_fix */
        B_INT_SGN,                /* b_sgn */
        B_STRING_STR_INT,         /* b_str */
        B_JUMP_INT_EQUAL_ZERO,    /* b_jump_equal_zero */
        B_JUMP_INT_EQUAL_ZERO_16, /* b_jump_equal_zero_16 */
        B_JUMP_INT_NOTEQUAL_ZERO, /* b_jump_notequal_zero */
        B_JUMP_INT_NOTEQUAL_ZERO_16, /* b_jump_notequal_zero_16 */
        B_JUMP_INT_LESS,      /* b_jump_less */
        B_JUMP_INT_GREATER,   /* b_jump_greater */
        B_INT_TO_INDEX,           /* b_convert_to_index */
        B_INT_TO_INDEX_16,        /* b_convert_to_index_16 */
        B_INT_TO_INDEX_PLUS,      /* b_convert_to_index_plus */
        B_INT_TO_INDEX_PLUS_16,   /* b_convert_to_index_plus_16 */
        B_INT_TO_INDEX_MINUS_1,           /* b_convert_to_index_minus_1 */
        B_INT_TO_INDEX_MINUS_1_16,        /* b_convert_to_index_minus_1_16 */
        B_INT_TO_INDEX_PLUS_MINUS_1,      /* b_convert_to_index_plus_minus_1 */
        B_INT_TO_INDEX_PLUS_MINUS_1_16,   /* b_convert_to_index_plus_minus_1_16 */
        {B_NOP, B_INT_TO_LONG, B_INT_TO_SINGLE, B_INT_TO_DOUBLE,
         B_ILLEGAL, B_ILLEGAL, B_ILLEGAL}},{ /* b_convert[NUMBER_OF_TYPES] */

        "LONG",                      /* type_name */
        SIZE_LONG,                   /* type_size */
        TRUE,                        /* type_numeric */
        TRUE,                        /* type_integer */
        FALSE,                       /* type_float */
        B_PUSH_CONST_LONG,           /* b_push_const */
        B_PUSH_CONST_LONG_16,        /* b_push_const_16 */
        B_PUSH_CONST_LONG_ZERO,      /* b_push_const_zero */
        B_PUSH_CONST_LONG_ONE,       /* b_push_const_one */
        B_PUSH_VAR_LONG,             /* b_push_var */
        B_PUSH_VAR_LONG_16,          /* b_push_var_16 */
        B_POP_LONG,                  /* b_pop */
        B_POP_LONG_16,               /* b_pop_16 */
        B_PUSH_INDIRECT_VAR_LONG,    /* b_push_indirect_var */
        B_PUSH_INDIRECT_VAR_LONG_16, /* b_push_indirect_var_16 */
        B_POP_INDIRECT_LONG,         /* b_pop_indirect */
        B_POP_INDIRECT_LONG_16,      /* b_pop_indirect_16 */
        B_ZERO_VAR_LONG,             /* b_zero_var */
        B_ZERO_VAR_LONG_16,          /* b_zero_var_16 */
        B_ONE_VAR_LONG,              /* b_one_var */
        B_ONE_VAR_LONG_16,           /* b_one_var_16 */
        B_LONG_SWAP,                 /* b_swap */
        B_LONG_EQUAL,                /* b_equal */
        B_LONG_EQUAL_CONST,          /* b_equal_const */
        B_LONG_EQUAL_CONST_16,       /* b_equal_const_16 */
        B_LONG_NOTEQUAL,             /* b_notequal */
        B_LONG_NOTEQUAL_CONST,       /* b_notequal_const */
        B_LONG_NOTEQUAL_CONST_16,    /* b_notequal_const_16 */
        B_LONG_LESS,             /* b_less */
        B_LONG_LESS_CONST,       /* b_less_const */
        B_LONG_LESS_CONST_16,    /* b_less_const_16 */
        B_LONG_GREATER,          /* b_greater */
        B_LONG_GREATER_CONST,    /* b_greater_const */
        B_LONG_GREATER_CONST_16, /* b_greater_const_16 */
        B_LONG_LESSOREQUAL,      /* b_lessorequal */
        B_LONG_LESSOREQUAL_CONST,/* b_lessorequal_const */
        B_LONG_LESSOREQUAL_CONST_16,/* b_lessorequal_const_16 */
        B_LONG_GREATEROREQUAL,   /* b_greaterorequal */
        B_LONG_GREATEROREQUAL_CONST,/* b_greaterorequal_const */
        B_LONG_GREATEROREQUAL_CONST_16,/* b_greaterorequal_const_16 */
        B_LONG_INCREMENT,            /* b_increment */
        B_LONG_INCREMENT_VAR,        /* b_increment_var */
        B_LONG_INCREMENT_VAR_16,     /* b_increment_var_16 */
        B_LONG_DECREMENT,            /* b_decrement */
        B_LONG_DECREMENT_VAR,        /* b_decrement_var */
        B_LONG_DECREMENT_VAR_16,     /* b_decrement_var_16 */
        B_LONG_UNARY_MINUS,          /* b_unary_minus */
        B_LONG_PLUS,                 /* b_plus */
        B_LONG_PLUS_CONST,           /* b_plus_const */
        B_LONG_PLUS_CONST_16,        /* b_plus_const_16 */
        B_LONG_PLUS_VAR,             /* b_plus_var */
        B_LONG_PLUS_VAR_16,          /* b_plus_var_16 */
        B_LONG_MINUS,                /* b_minus */
        B_LONG_MINUS_CONST,          /* b_minus_const */
        B_LONG_MINUS_CONST_16,       /* b_minus_const_16 */
        B_LONG_MINUS_VAR,          /* b_minus_var */
        B_LONG_MINUS_VAR_16,       /* b_minus_var_16 */
        B_LONG_SWAP_MINUS,           /* b_swap_minus */
        B_LONG_SWAP_MINUS_CONST,     /* b_swap_minus_const */
        B_LONG_SWAP_MINUS_CONST_16,  /* b_swap_minus_const_16 */
        B_LONG_TIMES,                /* b_times */
        B_LONG_TIMES_CONST,          /* b_times_const */
        B_LONG_TIMES_CONST_16,       /* b_times_const_16 */
        B_LONG_TIMES_VAR,            /* b_times_var */
        B_LONG_TIMES_VAR_16,         /* b_times_var_16 */
        B_ILLEGAL,                   /* b_divide */
        B_ILLEGAL,                   /* b_divide_const */
        B_ILLEGAL,                   /* b_divide_const_16 */
        B_ILLEGAL,                   /* b_swap_divide */
        B_ILLEGAL,                   /* b_swap_divide_const */
        B_ILLEGAL,                   /* b_swap_divide_const_16 */
        B_LONG_DIVIDE,               /* b_idivide */
        B_LONG_DIVIDE_CONST,         /* b_idivide_const */
        B_LONG_DIVIDE_CONST_16,      /* b_idivide_const_16 */
        B_LONG_SWAP_DIVIDE,          /* b_swap_idivide */
        B_LONG_SWAP_DIVIDE_CONST,    /* b_swap_idivide_const */
        B_LONG_SWAP_DIVIDE_CONST_16, /* b_swap_idivide_const_16 */
        B_LONG_MOD,                  /* b_mod */
        B_LONG_MOD_CONST,            /* b_mod_const */
        B_LONG_MOD_CONST_16,         /* b_mod_const_16 */
        B_LONG_SWAP_MOD,             /* b_swap_mod */
        B_LONG_SWAP_MOD_CONST,       /* b_swap_mod_const */
        B_LONG_SWAP_MOD_CONST_16,    /* b_swap_mod_const_16 */
        B_LONG_POWER,                /* b_power */
        B_LONG_POWER_CONST,          /* b_power_const */
        B_LONG_POWER_CONST_16,       /* b_power_const_16 */
        B_LONG_SWAP_POWER,           /* b_swap_power */
        B_LONG_SWAP_POWER_CONST,     /* b_swap_power_const */
        B_LONG_SWAP_POWER_CONST_16,  /* b_swap_power_const_16 */
        B_PRINT_LONG,                /* b_print */
        B_PRINT_USING_LONG,          /* b_print_using */
        B_INPUT_LONG,                /* b_input */
        B_READ_LONG,                 /* b_read */
        B_LONG_AND,                  /* b_and */
        B_LONG_AND_CONST,            /* b_and_const */
        B_LONG_AND_CONST_16,         /* b_and_const_16 */
        B_LONG_OR,                   /* b_or */
        B_LONG_OR_CONST,             /* b_or_const */
        B_LONG_OR_CONST_16,          /* b_or_const_16 */
        B_LONG_XOR,                  /* b_xor */
        B_LONG_XOR_CONST,            /* b_xor_const */
        B_LONG_XOR_CONST_16,         /* b_xor_const_16 */
        B_LONG_NOT,                  /* b_not */
        B_ILLEGAL,                   /* b_sin */
        B_ILLEGAL,                   /* b_cos */
        B_ILLEGAL,                   /* b_tan */
        B_ILLEGAL,                   /* b_atn */
        B_ILLEGAL,                   /* b_log */
        B_ILLEGAL,                   /* b_log2 */
        B_ILLEGAL,                   /* b_log10 */
        B_ILLEGAL,                   /* b_exp */
        B_ILLEGAL,                   /* b_sqr */
        B_LONG_ABS,                  /* b_abs */
        B_LONG_INT,                  /* b_int */
        B_LONG_CINT,                 /* b_cint */
        B_ILLEGAL,                   /* b_clng */
        B_LONG_CSNG,                 /* b_csng */
        B_LONG_CDBL,                 /* b_cdbl */
        B_LONG_FIX,                  /* b_fix */
        B_LONG_SGN,                  /* b_sgn */
        B_STRING_STR_LONG,           /* b_str */
        B_JUMP_LONG_EQUAL_ZERO,      /* b_jump_equal_zero */
        B_JUMP_LONG_EQUAL_ZERO_16,   /* b_jump_equal_zero_16 */
        B_JUMP_LONG_NOTEQUAL_ZERO,   /* b_jump_notequal_zero */
        B_JUMP_LONG_NOTEQUAL_ZERO_16,/* b_jump_notequal_zero_16 */
        B_JUMP_LONG_LESS,        /* b_jump_less */
        B_JUMP_LONG_GREATER,     /* b_jump_greater */
        B_LONG_TO_INDEX,             /* b_convert_to_index */
        B_LONG_TO_INDEX_16,          /* b_convert_to_index_16 */
        B_LONG_TO_INDEX_PLUS,        /* b_convert_to_index_plus */
        B_LONG_TO_INDEX_PLUS_16,     /* b_convert_to_index_plus_16 */
        B_LONG_TO_INDEX_MINUS_1,             /* b_convert_to_index_minus_1 */
        B_LONG_TO_INDEX_MINUS_1_16,          /* b_convert_to_index_minus_1_16 */
        B_LONG_TO_INDEX_PLUS_MINUS_1,        /* b_convert_to_index_plus_minus_1 */
        B_LONG_TO_INDEX_PLUS_MINUS_1_16,     /* b_convert_to_index_plus_minus_1_16 */
        {B_LONG_TO_INT, B_NOP, B_LONG_TO_SINGLE, B_LONG_TO_DOUBLE,
         B_ILLEGAL, B_ILLEGAL, B_ILLEGAL}},{ /* b_convert[NUMBER_OF_TYPES] */

        "SINGLE",                    /* type_name */
        SIZE_SINGLE,                 /* type_size */
        TRUE,                        /* type_numeric */
        FALSE,                       /* type_integer */
        TRUE,                        /* type_float */
        B_PUSH_CONST_SINGLE,         /* b_push_const */
        B_PUSH_CONST_SINGLE_16,      /* b_push_const_16 */
        B_PUSH_CONST_SINGLE_ZERO,    /* b_push_const_zero */
        B_PUSH_CONST_SINGLE_ONE,     /* b_push_const_one */
        B_PUSH_VAR_SINGLE,           /* b_push_var */
        B_PUSH_VAR_SINGLE_16,        /* b_push_var_16 */
        B_POP_SINGLE,                /* b_pop */
        B_POP_SINGLE_16,             /* b_pop_16 */
        B_PUSH_INDIRECT_VAR_SINGLE,  /* b_push_indirect_var */
        B_PUSH_INDIRECT_VAR_SINGLE_16,/* b_push_indirect_var_16 */
        B_POP_INDIRECT_SINGLE,       /* b_pop_indirect */
        B_POP_INDIRECT_SINGLE_16,    /* b_pop_indirect_16 */
        B_ZERO_VAR_SINGLE,           /* b_zero_var */
        B_ZERO_VAR_SINGLE_16,        /* b_zero_var_16 */
        B_ONE_VAR_SINGLE,            /* b_one_var */
        B_ONE_VAR_SINGLE_16,         /* b_one_var_16 */
        B_SINGLE_SWAP,               /* b_swap */
        B_SINGLE_EQUAL,              /* b_equal */
        B_SINGLE_EQUAL_CONST,        /* b_equal_const */
        B_SINGLE_EQUAL_CONST_16,     /* b_equal_const_16 */
        B_SINGLE_NOTEQUAL,           /* b_notequal */
        B_SINGLE_NOTEQUAL_CONST,     /* b_notequal_const */
        B_SINGLE_NOTEQUAL_CONST_16,  /* b_notequal_const_16 */
        B_SINGLE_LESS,           /* b_less */
        B_SINGLE_LESS_CONST,     /* b_less_const */
        B_SINGLE_LESS_CONST_16,  /* b_less_const_16 */
        B_SINGLE_GREATER,        /* b_greater */
        B_SINGLE_GREATER_CONST,  /* b_greater_const */
        B_SINGLE_GREATER_CONST_16,/* b_greater_const_16 */
        B_SINGLE_LESSOREQUAL,    /* b_lessorequal */
        B_SINGLE_LESSOREQUAL_CONST,/* b_lessorequal_const */
        B_SINGLE_LESSOREQUAL_CONST_16,/* b_lessorequal_const_16 */
        B_SINGLE_GREATEROREQUAL, /* b_greaterorequal */
        B_SINGLE_GREATEROREQUAL_CONST, /* b_greaterorequal_const */
        B_SINGLE_GREATEROREQUAL_CONST_16, /* b_greaterorequal_const_16 */
        B_SINGLE_INCREMENT,          /* b_increment */
        B_SINGLE_INCREMENT_VAR,      /* b_increment_var */
        B_SINGLE_INCREMENT_VAR_16,   /* b_increment_var_16 */
        B_SINGLE_DECREMENT,          /* b_decrement */
        B_SINGLE_DECREMENT_VAR,      /* b_decrement_var */
        B_SINGLE_DECREMENT_VAR_16,   /* b_decrement_var_16 */
        B_SINGLE_UNARY_MINUS,        /* b_unary_minus */
        B_SINGLE_PLUS,               /* b_plus */
        B_SINGLE_PLUS_CONST,         /* b_plus_const */
        B_SINGLE_PLUS_CONST_16,      /* b_plus_const_16 */
        B_SINGLE_PLUS_VAR,           /* b_plus_var */
        B_SINGLE_PLUS_VAR_16,        /* b_plus_var_16 */
        B_SINGLE_MINUS,              /* b_minus */
        B_SINGLE_MINUS_CONST,        /* b_minus_const */
        B_SINGLE_MINUS_CONST_16,     /* b_minus_const_16 */
        B_SINGLE_MINUS_VAR,        /* b_minus_var */
        B_SINGLE_MINUS_VAR_16,     /* b_minus_var_16 */
        B_SINGLE_SWAP_MINUS,         /* b_swap_minus */
        B_SINGLE_SWAP_MINUS_CONST,   /* b_swap_minus_const */
        B_SINGLE_SWAP_MINUS_CONST_16,/* b_swap_minus_const_16 */
        B_SINGLE_TIMES,              /* b_times */
        B_SINGLE_TIMES_CONST,        /* b_times_const */
        B_SINGLE_TIMES_CONST_16,     /* b_times_const_16 */
        B_SINGLE_TIMES_VAR,          /* b_times_var */
        B_SINGLE_TIMES_VAR_16,       /* b_times_var_16 */
        B_SINGLE_DIVIDE,             /* b_divide */
        B_SINGLE_DIVIDE_CONST,       /* b_divide_const */
        B_SINGLE_DIVIDE_CONST_16,    /* b_divide_const_16 */
        B_SINGLE_SWAP_DIVIDE,        /* b_swap_divide */
        B_SINGLE_SWAP_DIVIDE_CONST,  /* b_swap_divide_const */
        B_SINGLE_SWAP_DIVIDE_CONST_16,/* b_swap_divide_const_16 */
        B_ILLEGAL,                   /* b_idivide */
        B_ILLEGAL,                   /* b_idivide_const */
        B_ILLEGAL,                   /* b_idivide_const_16 */
        B_ILLEGAL,                   /* b_swap_idivide */
        B_ILLEGAL,                   /* b_swap_idivide_const */
        B_ILLEGAL,                   /* b_swap_idivide_const_16 */
        B_ILLEGAL,                   /* b_mod */
        B_ILLEGAL,                   /* b_mod_const */
        B_ILLEGAL,                   /* b_mod_const_16 */
        B_ILLEGAL,                   /* b_swap_mod */
        B_ILLEGAL,                   /* b_swap_mod_const */
        B_ILLEGAL,                   /* b_swap_mod_const_16 */
        B_SINGLE_POWER,              /* b_power */
        B_SINGLE_POWER_CONST,        /* b_power_const */
        B_SINGLE_POWER_CONST_16,     /* b_power_const_16 */
        B_SINGLE_SWAP_POWER,         /* b_swap_power */
        B_SINGLE_SWAP_POWER_CONST,   /* b_swap_power_const */
        B_SINGLE_SWAP_POWER_CONST_16,/* b_swap_power_const_16 */
        B_PRINT_SINGLE,              /* b_print */
        B_PRINT_USING_SINGLE,        /* b_print_using */
        B_INPUT_SINGLE,              /* b_input */
        B_READ_SINGLE,               /* b_read */
        B_ILLEGAL,                   /* b_and */
        B_ILLEGAL,                   /* b_and_const */
        B_ILLEGAL,                   /* b_and_const_16 */
        B_ILLEGAL,                   /* b_or */
        B_ILLEGAL,                   /* b_or_const */
        B_ILLEGAL,                   /* b_or_const_16 */
        B_ILLEGAL,                   /* b_xor */
        B_ILLEGAL,                   /* b_xor_const */
        B_ILLEGAL,                   /* b_xor_const_16 */
        B_ILLEGAL,                   /* b_not */
        B_SINGLE_SIN,                /* b_sin */
        B_SINGLE_COS,                /* b_cos */
        B_SINGLE_TAN,                /* b_tan */
        B_SINGLE_ATN,                /* b_atn */
        B_SINGLE_LOG,                /* b_log */
        B_SINGLE_LOG2,               /* b_log2 */
        B_SINGLE_LOG10,              /* b_log10 */
        B_SINGLE_EXP,                /* b_exp */
        B_SINGLE_SQR,                /* b_sqr */
        B_SINGLE_ABS,                /* b_abs */
        B_SINGLE_INT,                /* b_int */
        B_SINGLE_CINT,               /* b_cint */
        B_SINGLE_CLNG,               /* b_clng */
        B_ILLEGAL,                   /* b_csng */
        B_SINGLE_CDBL,               /* b_cdbl */
        B_SINGLE_FIX,                /* b_fix */
        B_SINGLE_SGN,                /* b_sgn */
        B_STRING_STR_SINGLE,         /* b_str */
        B_JUMP_SINGLE_EQUAL_ZERO,    /* b_jump_equal_zero */
        B_JUMP_SINGLE_EQUAL_ZERO_16, /* b_jump_equal_zero_16 */
        B_JUMP_SINGLE_NOTEQUAL_ZERO, /* b_jump_notequal_zero */
        B_JUMP_SINGLE_NOTEQUAL_ZERO_16,      /* b_jump_notequal_zero_16 */
        B_JUMP_SINGLE_LESS,      /* b_jump_less */
        B_JUMP_SINGLE_GREATER,   /* b_jump_greater */
        B_SINGLE_TO_INDEX,           /* b_convert_to_index */
        B_SINGLE_TO_INDEX_16,        /* b_convert_to_index_16 */
        B_SINGLE_TO_INDEX_PLUS,      /* b_convert_to_index_plus */
        B_SINGLE_TO_INDEX_PLUS_16,   /* b_convert_to_index_plus_16 */
        B_SINGLE_TO_INDEX_MINUS_1,           /* b_convert_to_index_minus_1 */
        B_SINGLE_TO_INDEX_MINUS_1_16,        /* b_convert_to_index_minus_1_16 */
        B_SINGLE_TO_INDEX_PLUS_MINUS_1,      /* b_convert_to_index_plus_minus_1 */
        B_SINGLE_TO_INDEX_PLUS_MINUS_1_16,   /* b_convert_to_index_plus_minus_1_16 */
        {B_SINGLE_TO_INT, B_SINGLE_TO_LONG, B_NOP, B_SINGLE_TO_DOUBLE,
         B_ILLEGAL, B_ILLEGAL, B_ILLEGAL}},{ /* b_convert[NUMBER_OF_TYPES] */

        "DOUBLE",                    /* type_name */
        SIZE_DOUBLE,                 /* type_size */
        TRUE,                        /* type_numeric */
        FALSE,                       /* type_integer */
        TRUE,                        /* type_float */
        B_PUSH_CONST_DOUBLE,         /* b_push_const */
        B_PUSH_CONST_DOUBLE_16,      /* b_push_const_16 */
        B_PUSH_CONST_DOUBLE_ZERO,    /* b_push_const_zero */
        B_PUSH_CONST_DOUBLE_ONE,     /* b_push_const_one */
        B_PUSH_VAR_DOUBLE,           /* b_push_var */
        B_PUSH_VAR_DOUBLE_16,        /* b_push_var_16 */
        B_POP_DOUBLE,                /* b_pop */
        B_POP_DOUBLE_16,             /* b_pop_16 */
        B_PUSH_INDIRECT_VAR_DOUBLE,  /* b_push_indirect_var */
        B_PUSH_INDIRECT_VAR_DOUBLE_16,/* b_push_indirect_var_16 */
        B_POP_INDIRECT_DOUBLE,       /* b_pop_indirect */
        B_POP_INDIRECT_DOUBLE_16,    /* b_pop_indirect_16 */
        B_ZERO_VAR_DOUBLE,           /* b_zero_var */
        B_ZERO_VAR_DOUBLE_16,        /* b_zero_var_16 */
        B_ONE_VAR_DOUBLE,            /* b_one_var */
        B_ONE_VAR_DOUBLE_16,         /* b_one_var_16 */
        B_DOUBLE_SWAP,               /* b_swap */
        B_DOUBLE_EQUAL,              /* b_equal */
        B_DOUBLE_EQUAL_CONST,        /* b_equal_const */
        B_DOUBLE_EQUAL_CONST_16,     /* b_equal_const_16 */
        B_DOUBLE_NOTEQUAL,           /* b_notequal */
        B_DOUBLE_NOTEQUAL_CONST,     /* b_notequal_const */
        B_DOUBLE_NOTEQUAL_CONST_16,  /* b_notequal_const_16 */
        B_DOUBLE_LESS,           /* b_less */
        B_DOUBLE_LESS_CONST,     /* b_less_const */
        B_DOUBLE_LESS_CONST_16,  /* b_less_const_16 */
        B_DOUBLE_GREATER,        /* b_greater */
        B_DOUBLE_GREATER_CONST,  /* b_greater_const */
        B_DOUBLE_GREATER_CONST_16,/* b_greater_const_16 */
        B_DOUBLE_LESSOREQUAL,    /* b_lessorequal */
        B_DOUBLE_LESSOREQUAL_CONST,/* b_lessorequal_const */
        B_DOUBLE_LESSOREQUAL_CONST_16,/* b_lessorequal_const_16 */
        B_DOUBLE_GREATEROREQUAL, /* b_greaterorequal */
        B_DOUBLE_GREATEROREQUAL_CONST, /* b_greaterorequal_const */
        B_DOUBLE_GREATEROREQUAL_CONST_16, /* b_greaterorequal_const_16 */
        B_DOUBLE_INCREMENT,          /* b_increment */
        B_DOUBLE_INCREMENT_VAR,      /* b_increment_var */
        B_DOUBLE_INCREMENT_VAR_16,   /* b_increment_var_16 */
        B_DOUBLE_DECREMENT,          /* b_decrement */
        B_DOUBLE_DECREMENT_VAR,      /* b_decrement_var */
        B_DOUBLE_DECREMENT_VAR_16,   /* b_decrement_var_16 */
        B_DOUBLE_UNARY_MINUS,        /* b_unary_minus */
        B_DOUBLE_PLUS,               /* b_plus */
        B_DOUBLE_PLUS_CONST,         /* b_plus_const */
        B_DOUBLE_PLUS_CONST_16,      /* b_plus_const_16 */
        B_DOUBLE_PLUS_VAR,           /* b_plus_var */
        B_DOUBLE_PLUS_VAR_16,        /* b_plus_var_16 */
        B_DOUBLE_MINUS,              /* b_minus */
        B_DOUBLE_MINUS_CONST,        /* b_minus_const */
        B_DOUBLE_MINUS_CONST_16,     /* b_minus_const_16 */
        B_DOUBLE_MINUS_VAR,        /* b_minus_var */
        B_DOUBLE_MINUS_VAR_16,     /* b_minus_var_16 */
        B_DOUBLE_SWAP_MINUS,         /* b_swap_minus */
        B_DOUBLE_SWAP_MINUS_CONST,   /* b_swap_minus_const */
        B_DOUBLE_SWAP_MINUS_CONST_16,/* b_swap_minus_const_16 */
        B_DOUBLE_TIMES,              /* b_times */
        B_DOUBLE_TIMES_CONST,        /* b_times_const */
        B_DOUBLE_TIMES_CONST_16,     /* b_times_const_16 */
        B_DOUBLE_TIMES_VAR,          /* b_times_var */
        B_DOUBLE_TIMES_VAR_16,       /* b_times_var_16 */
        B_DOUBLE_DIVIDE,             /* b_divide */
        B_DOUBLE_DIVIDE_CONST,       /* b_divide_const */
        B_DOUBLE_DIVIDE_CONST_16,    /* b_divide_const_16 */
        B_DOUBLE_SWAP_DIVIDE,        /* b_swap_divide */
        B_DOUBLE_SWAP_DIVIDE_CONST,  /* b_swap_divide_const */
        B_DOUBLE_SWAP_DIVIDE_CONST_16,/* b_swap_divide_const_16 */
        B_ILLEGAL,                   /* b_idivide */
        B_ILLEGAL,                   /* b_idivide_const */
        B_ILLEGAL,                   /* b_idivide_const_16 */
        B_ILLEGAL,                   /* b_swap_idivide */
        B_ILLEGAL,                   /* b_swap_idivide_const */
        B_ILLEGAL,                   /* b_swap_idivide_const_16 */
        B_ILLEGAL,                   /* b_mod */
        B_ILLEGAL,                   /* b_mod_const */
        B_ILLEGAL,                   /* b_mod_const_16 */
        B_ILLEGAL,                   /* b_swap_mod */
        B_ILLEGAL,                   /* b_swap_mod_const */
        B_ILLEGAL,                   /* b_swap_mod_const_16 */
        B_DOUBLE_POWER,              /* b_power */
        B_DOUBLE_POWER_CONST,        /* b_power_const */
        B_DOUBLE_POWER_CONST_16,     /* b_power_const_16 */
        B_DOUBLE_SWAP_POWER,         /* b_swap_power */
        B_DOUBLE_SWAP_POWER_CONST,   /* b_swap_power_const */
        B_DOUBLE_SWAP_POWER_CONST_16,/* b_swap_power_const_16 */
        B_PRINT_DOUBLE,              /* b_print */
        B_PRINT_USING_DOUBLE,        /* b_print_using */
        B_INPUT_DOUBLE,              /* b_input */
        B_READ_DOUBLE,               /* b_read */
        B_ILLEGAL,                   /* b_and */
        B_ILLEGAL,                   /* b_and_const */
        B_ILLEGAL,                   /* b_and_const_16 */
        B_ILLEGAL,                   /* b_or */
        B_ILLEGAL,                   /* b_or_const */
        B_ILLEGAL,                   /* b_or_const_16 */
        B_ILLEGAL,                   /* b_xor */
        B_ILLEGAL,                   /* b_xor_const */
        B_ILLEGAL,                   /* b_xor_const_16 */
        B_ILLEGAL,                   /* b_not */
        B_DOUBLE_SIN,                /* b_sin */
        B_DOUBLE_COS,                /* b_cos */
        B_DOUBLE_TAN,                /* b_tan */
        B_DOUBLE_ATN,                /* b_atn */
        B_DOUBLE_LOG,                /* b_log */
        B_DOUBLE_LOG2,               /* b_log2 */
        B_DOUBLE_LOG10,              /* b_log10 */
        B_DOUBLE_EXP,                /* b_exp */
        B_DOUBLE_SQR,                /* b_sqr */
        B_DOUBLE_ABS,                /* b_abs */
        B_DOUBLE_INT,                /* b_int */
        B_DOUBLE_CINT,               /* b_cint */
        B_DOUBLE_CLNG,               /* b_clng */
        B_DOUBLE_CSNG,               /* b_csng */
        B_ILLEGAL,                   /* b_cdbl */
        B_DOUBLE_FIX,                /* b_fix */
        B_DOUBLE_SGN,                /* b_sgn */
        B_STRING_STR_DOUBLE,         /* b_str */
        B_JUMP_DOUBLE_EQUAL_ZERO,    /* b_jump_equal_zero */
        B_JUMP_DOUBLE_EQUAL_ZERO_16, /* b_jump_equal_zero_16 */
        B_JUMP_DOUBLE_NOTEQUAL_ZERO, /* b_jump_notequal_zero */
        B_JUMP_DOUBLE_NOTEQUAL_ZERO_16,      /* b_jump_notequal_zero_16 */
        B_JUMP_DOUBLE_LESS,      /* b_jump_less */
        B_JUMP_DOUBLE_GREATER,   /* b_jump_greater */
        B_DOUBLE_TO_INDEX,           /* b_convert_to_index */
        B_DOUBLE_TO_INDEX_16,        /* b_convert_to_index_16 */
        B_DOUBLE_TO_INDEX_PLUS,      /* b_convert_to_index_plus */
        B_DOUBLE_TO_INDEX_PLUS_16,   /* b_convert_to_index_plus_16 */
        B_DOUBLE_TO_INDEX_MINUS_1,           /* b_convert_to_index_minus_1 */
        B_DOUBLE_TO_INDEX_MINUS_1_16,        /* b_convert_to_index_minus_1_16 */
        B_DOUBLE_TO_INDEX_PLUS_MINUS_1,      /* b_convert_to_index_plus_minus_1 */
        B_DOUBLE_TO_INDEX_PLUS_MINUS_1_16,   /* b_convert_to_index_plus_minus_1_16 */
        {B_DOUBLE_TO_INT, B_DOUBLE_TO_LONG, B_DOUBLE_TO_SINGLE, B_NOP,
         B_ILLEGAL, B_ILLEGAL, B_ILLEGAL}},{ /* b_convert[NUMBER_OF_TYPES] */

        "STRING",                    /* type_name */
        SIZE_STRING,                 /* type_size */
        FALSE,                       /* type_numeric */
        FALSE,                       /* type_integer */
        FALSE,                       /* type_float */
        B_PUSH_CONST_STRING,         /* b_push_const */
        B_ILLEGAL,                   /* b_push_const_16 */
        B_ILLEGAL,                   /* b_push_const_zero */
        B_ILLEGAL,                   /* b_push_const_one */
        B_PUSH_VAR_STRING,           /* b_push_var */
        B_PUSH_VAR_STRING_16,        /* b_push_var_16 */
        B_POP_STRING,                /* b_pop */
        B_POP_STRING_16,             /* b_pop_16 */
        B_PUSH_INDIRECT_VAR_STRING,  /* b_push_indirect_var */
        B_PUSH_INDIRECT_VAR_STRING_16,/* b_push_indirect_var_16 */
        B_POP_INDIRECT_STRING,       /* b_pop_indirect */
        B_POP_INDIRECT_STRING_16,    /* b_pop_indirect_16 */
        B_ZERO_VAR_STRING,           /* b_zero_var */
        B_ZERO_VAR_STRING_16,        /* b_zero_var_16 */
        B_ONE_VAR_STRING,            /* b_one_var */
        B_ONE_VAR_STRING_16,         /* b_one_var_16 */
        B_STRING_SWAP,               /* b_swap */
        B_STRING_EQUAL,              /* b_equal */
        B_ILLEGAL,                   /* b_equal_const */
        B_ILLEGAL,                   /* b_equal_const_16 */
        B_STRING_NOTEQUAL,           /* b_notequal */
        B_ILLEGAL,                   /* b_notequal_const */
        B_ILLEGAL,                   /* b_notequal_const_16 */
        B_STRING_LESS,           /* b_less */
        B_ILLEGAL,                   /* b_less_const */
        B_ILLEGAL,                   /* b_less_const_16 */
        B_STRING_GREATER,        /* b_greater */
        B_ILLEGAL,                   /* b_greater_const */
        B_ILLEGAL,                   /* b_greater_const_16 */
        B_STRING_LESSOREQUAL,    /* b_lessorequal */
        B_ILLEGAL,                   /* b_lessorequal_const */
        B_ILLEGAL,                   /* b_lessorequal_const_16 */
        B_STRING_GREATEROREQUAL, /* b_greaterorequal */
        B_ILLEGAL,                   /* b_greaterorequal_const */
        B_ILLEGAL,                   /* b_greaterorequal_const_16 */
        B_ILLEGAL,                   /* b_increment */
        B_ILLEGAL,                   /* b_increment_var */
        B_ILLEGAL,                   /* b_increment_var_16 */
        B_ILLEGAL,                   /* b_decrement */
        B_ILLEGAL,                   /* b_decrement_var */
        B_ILLEGAL,                   /* b_decrement_var_16 */
        B_ILLEGAL,                   /* b_unary_minus */
        B_STRING_PLUS,               /* b_plus */
        B_ILLEGAL,                   /* b_plus_const */
        B_ILLEGAL,                   /* b_plus_const_16 */
        B_ILLEGAL,                   /* b_plus_var */
        B_ILLEGAL,                   /* b_plus_var_16 */
        B_ILLEGAL,                   /* b_minus */
        B_ILLEGAL,                   /* b_minus_const */
        B_ILLEGAL,                   /* b_minus_const_16 */
        B_ILLEGAL,                   /* b_minus_var */
        B_ILLEGAL,                   /* b_minus_var_16 */
        B_ILLEGAL,                   /* b_swap_minus */
        B_ILLEGAL,                   /* b_swap_minus_const */
        B_ILLEGAL,                   /* b_swap_minus_const_16 */
        B_ILLEGAL,                   /* b_times */
        B_ILLEGAL,                   /* b_times_const */
        B_ILLEGAL,                   /* b_times_const_16 */
        B_ILLEGAL,                   /* b_times_var */
        B_ILLEGAL,                   /* b_times_var_16 */
        B_ILLEGAL,                   /* b_divide */
        B_ILLEGAL,                   /* b_divide_const */
        B_ILLEGAL,                   /* b_divide_const_16 */
        B_ILLEGAL,                   /* b_swap_divide */
        B_ILLEGAL,                   /* b_swap_divide_const */
        B_ILLEGAL,                   /* b_swap_divide_const_16 */
        B_ILLEGAL,                   /* b_idivide */
        B_ILLEGAL,                   /* b_idivide_const */
        B_ILLEGAL,                   /* b_idivide_const_16 */
        B_ILLEGAL,                   /* b_swap_idivide */
        B_ILLEGAL,                   /* b_swap_idivide_const */
        B_ILLEGAL,                   /* b_swap_idivide_const_16 */
        B_ILLEGAL,                   /* b_mod */
        B_ILLEGAL,                   /* b_mod_const */
        B_ILLEGAL,                   /* b_mod_const_16 */
        B_ILLEGAL,                   /* b_swap_mod */
        B_ILLEGAL,                   /* b_swap_mod_const */
        B_ILLEGAL,                   /* b_swap_mod_const_16 */
        B_ILLEGAL,                   /* b_power */
        B_ILLEGAL,                   /* b_power_const */
        B_ILLEGAL,                   /* b_power_const_16 */
        B_ILLEGAL,                   /* b_swap_power */
        B_ILLEGAL,                   /* b_swap_power_const */
        B_ILLEGAL,                   /* b_swap_power_const_16 */
        B_PRINT_STRING,              /* b_print */
        B_PRINT_USING_STRING,        /* b_print_using */
        B_INPUT_STRING,              /* b_input */
        B_READ_STRING,               /* b_read */
        B_ILLEGAL,                   /* b_and */
        B_ILLEGAL,                   /* b_and_const */
        B_ILLEGAL,                   /* b_and_const_16 */
        B_ILLEGAL,                   /* b_or */
        B_ILLEGAL,                   /* b_or_const */
        B_ILLEGAL,                   /* b_or_const_16 */
        B_ILLEGAL,                   /* b_xor */
        B_ILLEGAL,                   /* b_xor_const */
        B_ILLEGAL,                   /* b_xor_const_16 */
        B_ILLEGAL,                   /* b_not */
        B_ILLEGAL,                   /* b_sin */
        B_ILLEGAL,                   /* b_cos */
        B_ILLEGAL,                   /* b_tan */
        B_ILLEGAL,                   /* b_atn */
        B_ILLEGAL,                   /* b_log */
        B_ILLEGAL,                   /* b_log2 */
        B_ILLEGAL,                   /* b_log10 */
        B_ILLEGAL,                   /* b_exp */
        B_ILLEGAL,                   /* b_sqr */
        B_ILLEGAL,                   /* b_abs */
        B_ILLEGAL,                   /* b_int */
        B_ILLEGAL,                   /* b_cint */
        B_ILLEGAL,                   /* b_clng */
        B_ILLEGAL,                   /* b_csng */
        B_ILLEGAL,                   /* b_cdbl */
        B_ILLEGAL,                   /* b_fix */
        B_ILLEGAL,                   /* b_sgn */
        B_ILLEGAL,                   /* b_str */
        B_ILLEGAL,                   /* b_jump_equal_zero */
        B_ILLEGAL,                   /* b_jump_equal_zero_16 */
        B_ILLEGAL,                   /* b_jump_notequal_zero */
        B_ILLEGAL,                   /* b_jump_notequal_zero_16 */
        B_ILLEGAL,                   /* b_jump_less */
        B_ILLEGAL,                   /* b_jump_greater */
        B_ILLEGAL,                   /* b_convert_to_index */
        B_ILLEGAL,                   /* b_convert_to_index_16 */
        B_ILLEGAL,                   /* b_convert_to_index_plus */
        B_ILLEGAL,                   /* b_convert_to_index_plus_16 */
        B_ILLEGAL,                   /* b_convert_to_index_minus_1 */
        B_ILLEGAL,                   /* b_convert_to_index_minus_1_16 */
        B_ILLEGAL,                   /* b_convert_to_index_plus_minus_1 */
        B_ILLEGAL,                   /* b_convert_to_index_plus_minus_1_16 */
        {B_ILLEGAL, B_ILLEGAL, B_ILLEGAL, B_ILLEGAL,
         B_NOP, B_ILLEGAL, B_ILLEGAL}},{ /* b_convert[NUMBER_OF_TYPES] */

        "LABEL",                     /* type_name */
        0,                           /* type_size */
        FALSE,                       /* type_numeric */
        FALSE,                       /* type_integer */
        FALSE,                       /* type_float */
        B_ILLEGAL,                   /* b_push_const */
        B_ILLEGAL,                   /* b_push_const_16 */
        B_ILLEGAL,                   /* b_push_const_zero */
        B_ILLEGAL,                   /* b_push_const_one */
        B_ILLEGAL,                   /* b_push_var */
        B_ILLEGAL,                   /* b_push_var_16 */
        B_ILLEGAL,                   /* b_pop */
        B_ILLEGAL,                   /* b_pop_16 */
        B_ILLEGAL,                   /* b_push_indirect_var */
        B_ILLEGAL,                   /* b_push_indirect_var_16 */
        B_ILLEGAL,                   /* b_pop_indirect */
        B_ILLEGAL,                   /* b_pop_indirect_16 */
        B_ILLEGAL,                   /* b_zero_var */
        B_ILLEGAL,                   /* b_zero_var_16 */
        B_ILLEGAL,                   /* b_one_var */
        B_ILLEGAL,                   /* b_one_var_16 */
        B_ILLEGAL,                   /* b_swap */
        B_ILLEGAL,                   /* b_equal */
        B_ILLEGAL,                   /* b_equal */
        B_ILLEGAL,                   /* b_equal */
        B_ILLEGAL,                   /* b_notequal */
        B_ILLEGAL,                   /* b_notequal_const */
        B_ILLEGAL,                   /* b_notequal_const_16 */
        B_ILLEGAL,                   /* b_less */
        B_ILLEGAL,                   /* b_less_const */
        B_ILLEGAL,                   /* b_less_const_16 */
        B_ILLEGAL,                   /* b_greater */
        B_ILLEGAL,                   /* b_greater_const */
        B_ILLEGAL,                   /* b_greater_const_16 */
        B_ILLEGAL,                   /* b_lessorequal */
        B_ILLEGAL,                   /* b_lessorequal_const */
        B_ILLEGAL,                   /* b_lessorequal_const_16 */
        B_ILLEGAL,                   /* b_greaterorequal */
        B_ILLEGAL,                   /* b_greaterorequal_const */
        B_ILLEGAL,                   /* b_greaterorequal_const_16 */
        B_ILLEGAL,                   /* b_increment */
        B_ILLEGAL,                   /* b_increment_var */
        B_ILLEGAL,                   /* b_increment_var_16 */
        B_ILLEGAL,                   /* b_decrement */
        B_ILLEGAL,                   /* b_decrement_var */
        B_ILLEGAL,                   /* b_decrement_var_16 */
        B_ILLEGAL,                   /* b_unary_minus */
        B_ILLEGAL,                   /* b_plus */
        B_ILLEGAL,                   /* b_plus_const */
        B_ILLEGAL,                   /* b_plus_const_16 */
        B_ILLEGAL,                   /* b_plus_var */
        B_ILLEGAL,                   /* b_plus_var_16 */
        B_ILLEGAL,                   /* b_minus */
        B_ILLEGAL,                   /* b_minus_const */
        B_ILLEGAL,                   /* b_minus_const_16 */
        B_ILLEGAL,                   /* b_minus_var */
        B_ILLEGAL,                   /* b_minus_var_16 */
        B_ILLEGAL,                   /* b_swap_minus */
        B_ILLEGAL,                   /* b_swap_minus_const */
        B_ILLEGAL,                   /* b_swap_minus_const_16 */
        B_ILLEGAL,                   /* b_times */
        B_ILLEGAL,                   /* b_times_const */
        B_ILLEGAL,                   /* b_times_const_16 */
        B_ILLEGAL,                   /* b_times_var */
        B_ILLEGAL,                   /* b_times_var_16 */
        B_ILLEGAL,                   /* b_divide */
        B_ILLEGAL,                   /* b_divide_const */
        B_ILLEGAL,                   /* b_divide_const_16 */
        B_ILLEGAL,                   /* b_swap_divide */
        B_ILLEGAL,                   /* b_swap_divide_const */
        B_ILLEGAL,                   /* b_swap_divide_const_16 */
        B_ILLEGAL,                   /* b_idivide */
        B_ILLEGAL,                   /* b_idivide_const */
        B_ILLEGAL,                   /* b_idivide_const_16 */
        B_ILLEGAL,                   /* b_swap_idivide */
        B_ILLEGAL,                   /* b_swap_idivide_const */
        B_ILLEGAL,                   /* b_swap_idivide_const_16 */
        B_ILLEGAL,                   /* b_mod */
        B_ILLEGAL,                   /* b_mod_const */
        B_ILLEGAL,                   /* b_mod_const_16 */
        B_ILLEGAL,                   /* b_swap_mod */
        B_ILLEGAL,                   /* b_swap_mod_const */
        B_ILLEGAL,                   /* b_swap_mod_const_16 */
        B_ILLEGAL,                   /* b_power */
        B_ILLEGAL,                   /* b_power_const */
        B_ILLEGAL,                   /* b_power_const_16 */
        B_ILLEGAL,                   /* b_swap_power */
        B_ILLEGAL,                   /* b_swap_power_const */
        B_ILLEGAL,                   /* b_swap_power_const_16 */
        B_ILLEGAL,                   /* b_print */
        B_ILLEGAL,                   /* b_print_using */
        B_ILLEGAL,                   /* b_input */
        B_ILLEGAL,                   /* b_read */
        B_ILLEGAL,                   /* b_and */
        B_ILLEGAL,                   /* b_and_const */
        B_ILLEGAL,                   /* b_and_const_16 */
        B_ILLEGAL,                   /* b_or */
        B_ILLEGAL,                   /* b_or_const */
        B_ILLEGAL,                   /* b_or_const_16 */
        B_ILLEGAL,                   /* b_xor */
        B_ILLEGAL,                   /* b_xor_const */
        B_ILLEGAL,                   /* b_xor_const_16 */
        B_ILLEGAL,                   /* b_not */
        B_ILLEGAL,                   /* b_sin */
        B_ILLEGAL,                   /* b_cos */
        B_ILLEGAL,                   /* b_tan */
        B_ILLEGAL,                   /* b_atn */
        B_ILLEGAL,                   /* b_log */
        B_ILLEGAL,                   /* b_log2 */
        B_ILLEGAL,                   /* b_log10 */
        B_ILLEGAL,                   /* b_exp */
        B_ILLEGAL,                   /* b_sqr */
        B_ILLEGAL,                   /* b_abs */
        B_ILLEGAL,                   /* b_int */
        B_ILLEGAL,                   /* b_cint */
        B_ILLEGAL,                   /* b_clng */
        B_ILLEGAL,                   /* b_csng */
        B_ILLEGAL,                   /* b_cdbl */
        B_ILLEGAL,                   /* b_fix */
        B_ILLEGAL,                   /* b_sgn */
        B_ILLEGAL,                   /* b_str */
        B_ILLEGAL,                   /* b_jump_equal_zero */
        B_ILLEGAL,                   /* b_jump_equal_zero_16 */
        B_ILLEGAL,                   /* b_jump_notequal_zero */
        B_ILLEGAL,                   /* b_jump_notequal_zero_16 */
        B_ILLEGAL,                   /* b_jump_less */
        B_ILLEGAL,                   /* b_jump_greater */
        B_ILLEGAL,                   /* b_convert_to_index */
        B_ILLEGAL,                   /* b_convert_to_index_16 */
        B_ILLEGAL,                   /* b_convert_to_index_plus */
        B_ILLEGAL,                   /* b_convert_to_index_plus_16 */
        B_ILLEGAL,                   /* b_convert_to_index_minus_1 */
        B_ILLEGAL,                   /* b_convert_to_index_minus_1_16 */
        B_ILLEGAL,                   /* b_convert_to_index_plus_minus_1 */
        B_ILLEGAL,                   /* b_convert_to_index_plus_minus_1_16 */
        {B_ILLEGAL, B_ILLEGAL, B_ILLEGAL, B_ILLEGAL,
         B_ILLEGAL, B_NOP, B_ILLEGAL}},{ /* b_convert[NUMBER_OF_TYPES] */

        "FILE",                      /* type_name */
        SIZE_FILE,                   /* type_size */
        FALSE,                       /* type_numeric */
        FALSE,                       /* type_integer */
        FALSE,                       /* type_float */
        B_ILLEGAL,                   /* b_push_const */
        B_ILLEGAL,                   /* b_push_const_16 */
        B_ILLEGAL,                   /* b_push_const_zero */
        B_ILLEGAL,                   /* b_push_const_one */
        B_ILLEGAL,                   /* b_push_var */
        B_ILLEGAL,                   /* b_push_var_16 */
        B_ILLEGAL,                   /* b_pop */
        B_ILLEGAL,                   /* b_pop_16 */
        B_ILLEGAL,                   /* b_push_indirect_var */
        B_ILLEGAL,                   /* b_push_indirect_var_16 */
        B_ILLEGAL,                   /* b_pop_indirect */
        B_ILLEGAL,                   /* b_pop_indirect_16 */
        B_ILLEGAL,                   /* b_zero_var */
        B_ILLEGAL,                   /* b_zero_var_16 */
        B_ILLEGAL,                   /* b_one_var */
        B_ILLEGAL,                   /* b_one_var_16 */
        B_ILLEGAL,                   /* b_swap */
        B_ILLEGAL,                   /* b_equal */
        B_ILLEGAL,                   /* b_equal */
        B_ILLEGAL,                   /* b_equal */
        B_ILLEGAL,                   /* b_notequal */
        B_ILLEGAL,                   /* b_notequal_const */
        B_ILLEGAL,                   /* b_notequal_const_16 */
        B_ILLEGAL,                   /* b_less */
        B_ILLEGAL,                   /* b_less_const */
        B_ILLEGAL,                   /* b_less_const_16 */
        B_ILLEGAL,                   /* b_greater */
        B_ILLEGAL,                   /* b_greater_const */
        B_ILLEGAL,                   /* b_greater_const_16 */
        B_ILLEGAL,                   /* b_lessorequal */
        B_ILLEGAL,                   /* b_lessorequal_const */
        B_ILLEGAL,                   /* b_lessorequal_const_16 */
        B_ILLEGAL,                   /* b_greaterorequal */
        B_ILLEGAL,                   /* b_greaterorequal_const */
        B_ILLEGAL,                   /* b_greaterorequal_const_16 */
        B_ILLEGAL,                   /* b_increment */
        B_ILLEGAL,                   /* b_increment_var */
        B_ILLEGAL,                   /* b_increment_var_16 */
        B_ILLEGAL,                   /* b_decrement */
        B_ILLEGAL,                   /* b_decrement_var */
        B_ILLEGAL,                   /* b_decrement_var_16 */
        B_ILLEGAL,                   /* b_unary_minus */
        B_ILLEGAL,                   /* b_plus */
        B_ILLEGAL,                   /* b_plus_const */
        B_ILLEGAL,                   /* b_plus_const_16 */
        B_ILLEGAL,                   /* b_plus_var */
        B_ILLEGAL,                   /* b_plus_var_16 */
        B_ILLEGAL,                   /* b_minus */
        B_ILLEGAL,                   /* b_minus_const */
        B_ILLEGAL,                   /* b_minus_const_16 */
        B_ILLEGAL,                   /* b_minus_var */
        B_ILLEGAL,                   /* b_minus_var_16 */
        B_ILLEGAL,                   /* b_swap_minus */
        B_ILLEGAL,                   /* b_swap_minus_const */
        B_ILLEGAL,                   /* b_swap_minus_const_16 */
        B_ILLEGAL,                   /* b_times */
        B_ILLEGAL,                   /* b_times_const */
        B_ILLEGAL,                   /* b_times_const_16 */
        B_ILLEGAL,                   /* b_times_var */
        B_ILLEGAL,                   /* b_times_var_16 */
        B_ILLEGAL,                   /* b_divide */
        B_ILLEGAL,                   /* b_divide_const */
        B_ILLEGAL,                   /* b_divide_const_16 */
        B_ILLEGAL,                   /* b_swap_divide */
        B_ILLEGAL,                   /* b_swap_divide_const */
        B_ILLEGAL,                   /* b_swap_divide_const_16 */
        B_ILLEGAL,                   /* b_idivide */
        B_ILLEGAL,                   /* b_idivide_const */
        B_ILLEGAL,                   /* b_idivide_const_16 */
        B_ILLEGAL,                   /* b_swap_idivide */
        B_ILLEGAL,                   /* b_swap_idivide_const */
        B_ILLEGAL,                   /* b_swap_idivide_const_16 */
        B_ILLEGAL,                   /* b_mod */
        B_ILLEGAL,                   /* b_mod_const */
        B_ILLEGAL,                   /* b_mod_const_16 */
        B_ILLEGAL,                   /* b_swap_mod */
        B_ILLEGAL,                   /* b_swap_mod_const */
        B_ILLEGAL,                   /* b_swap_mod_const_16 */
        B_ILLEGAL,                   /* b_power */
        B_ILLEGAL,                   /* b_power_const */
        B_ILLEGAL,                   /* b_power_const_16 */
        B_ILLEGAL,                   /* b_swap_power */
        B_ILLEGAL,                   /* b_swap_power_const */
        B_ILLEGAL,                   /* b_swap_power_const_16 */
        B_ILLEGAL,                   /* b_print */
        B_ILLEGAL,                   /* b_print_using */
        B_ILLEGAL,                   /* b_input */
        B_ILLEGAL,                   /* b_read */
        B_ILLEGAL,                   /* b_and */
        B_ILLEGAL,                   /* b_and_const */
        B_ILLEGAL,                   /* b_and_const_16 */
        B_ILLEGAL,                   /* b_or */
        B_ILLEGAL,                   /* b_or_const */
        B_ILLEGAL,                   /* b_or_const_16 */
        B_ILLEGAL,                   /* b_xor */
        B_ILLEGAL,                   /* b_xor_const */
        B_ILLEGAL,                   /* b_xor_const_16 */
        B_ILLEGAL,                   /* b_not */
        B_ILLEGAL,                   /* b_sin */
        B_ILLEGAL,                   /* b_cos */
        B_ILLEGAL,                   /* b_tan */
        B_ILLEGAL,                   /* b_atn */
        B_ILLEGAL,                   /* b_log */
        B_ILLEGAL,                   /* b_log2 */
        B_ILLEGAL,                   /* b_log10 */
        B_ILLEGAL,                   /* b_exp */
        B_ILLEGAL,                   /* b_sqr */
        B_ILLEGAL,                   /* b_abs */
        B_ILLEGAL,                   /* b_int */
        B_ILLEGAL,                   /* b_cint */
        B_ILLEGAL,                   /* b_clng */
        B_ILLEGAL,                   /* b_csng */
        B_ILLEGAL,                   /* b_cdbl */
        B_ILLEGAL,                   /* b_fix */
        B_ILLEGAL,                   /* b_sgn */
        B_ILLEGAL,                   /* b_str */
        B_ILLEGAL,                   /* b_jump_equal_zero */
        B_ILLEGAL,                   /* b_jump_equal_zero_16 */
        B_ILLEGAL,                   /* b_jump_notequal_zero */
        B_ILLEGAL,                   /* b_jump_notequal_zero_16 */
        B_ILLEGAL,                   /* b_jump_less */
        B_ILLEGAL,                   /* b_jump_greater */
        B_ILLEGAL,                   /* b_convert_to_index */
        B_ILLEGAL,                   /* b_convert_to_index_16 */
        B_ILLEGAL,                   /* b_convert_to_index_plus */
        B_ILLEGAL,                   /* b_convert_to_index_plus_16 */
        B_ILLEGAL,                   /* b_convert_to_index_minus_1 */
        B_ILLEGAL,                   /* b_convert_to_index_minus_1_16 */
        B_ILLEGAL,                   /* b_convert_to_index_plus_minus_1 */
        B_ILLEGAL,                   /* b_convert_to_index_plus_minus_1_16 */
        {B_ILLEGAL, B_ILLEGAL, B_ILLEGAL, B_ILLEGAL,
         B_ILLEGAL, B_ILLEGAL, B_NOP}} /* b_convert[NUMBER_OF_TYPES] */
};

/****************************************************************************/
/****************************************************************************/
/*   G L O B A L   V A R I A B L E S   */

/* number of variables of each type */
size_t vars_size[NUMBER_OF_TYPES] = {0};

/* symbol tables (trees) */
static struct var_node *var_root[NUMBER_OF_TYPES] = {NULL},
                       *array_root[NUMBER_OF_TYPES] = {NULL},
                       *function_root[NUMBER_OF_TYPES] = {NULL},
                       *arg_root[NUMBER_OF_TYPES] = {NULL};

/* table (tree) of unresolved gotos */
static struct goto_node *goto_list_root = NULL;

/* nesting */
static struct struct_statement_node *struct_statement_stack_root = NULL;

static int struct_statement_count = 0;  /* current nesting level */

static MFILE *mtrack = NULL; /* track memory allocations for constant strings */

static enum var_type_type token_type_array[26] = { /* for DEFxxx */
        TYPE_SINGLE,        /* A */
        TYPE_SINGLE,        /* B */
        TYPE_SINGLE,        /* C */
        TYPE_SINGLE,        /* D */
        TYPE_SINGLE,        /* E */
        TYPE_SINGLE,        /* F */
        TYPE_SINGLE,        /* G */
        TYPE_SINGLE,        /* H */
        TYPE_SINGLE,        /* I */
        TYPE_SINGLE,        /* J */
        TYPE_SINGLE,        /* K */
        TYPE_SINGLE,        /* L */
        TYPE_SINGLE,        /* M */
        TYPE_SINGLE,        /* N */
        TYPE_SINGLE,        /* O */
        TYPE_SINGLE,        /* P */
        TYPE_SINGLE,        /* Q */
        TYPE_SINGLE,        /* R */
        TYPE_SINGLE,        /* S */
        TYPE_SINGLE,        /* T */
        TYPE_SINGLE,        /* U */
        TYPE_SINGLE,        /* V */
        TYPE_SINGLE,        /* W */
        TYPE_SINGLE,        /* X */
        TYPE_SINGLE,        /* Y */
        TYPE_SINGLE};       /* Z */

#ifdef THREADED
static int *static_bcode_labels;
#endif

/****************************************************************************/
/****************************************************************************/
/*   M A I N   P R O G R A M   */

int main (int argc, char *argv[])
{
  struct b b;
  char *chain_fname;
  enum var_type_type type;
  BOOL ok;
  static BOOL first;

  first = TRUE;
  memset (&b, 0, sizeof(struct b));
  b.argc = argc;
  b.argv = argv;
  b.option_base = -1; /* -1 == not set yet */
  chain_fname = NULL;
  ok = TRUE;
  if (argc < 2) {
    fprintf (stderr, "Usage: %s <basic-program-file> [arg1] [arg2] ...\n",
             argv[0]);
    ok = FALSE;
  }
#ifdef THREADED
  if (ok) {
    /* find the addresses/offsets of bcode_labels */
    static_bcode_labels = (int *)run_bcodes (NULL, NULL, NULL, NULL, 0, NULL);
    ok = check_threaded_code_fits ();
  }
#endif
  if (ok)
    if ((b.fname = my_malloc (strlen(argv[1]) + 5)) == NULL) {
      fprintf (stderr, "Out of memory allocating %d bytes\n",
               (int)(strlen(argv[1]) + 5));
      ok = FALSE;
    }
  if (ok)
    strcpy (b.fname, argv[1]);
  /* Loop back to here on CHAIN fname */
  while (ok && b.fname != NULL) {
    if ((b.f = fopen (b.fname, "r")) == NULL) {
      strcat (b.fname, ".bas");
      if ((b.f = fopen (b.fname, "r")) == NULL) {
        b.fname[strlen(b.fname) - 4] = '\0';
        fprintf (stderr, "%s: Can't open %s for read\n", argv[0], b.fname);
        ok = FALSE;
      }
    }
    debug ((stderr, "\n"));
    if (ok)
      if ((b.b.f = mopen ()) == NULL) {    /* tmp file for bcodes */
        fprintf (stderr, "%s: Can't open tmpfile for code\n", argv[0]);
        ok = FALSE;
      }
    if (ok)
      if ((b.data.f = mopen ()) == NULL) { /* tmp file for initialised data */
        fprintf (stderr, "%s: Can't open tmpfile for data\n", argv[0]);
        ok = FALSE;
      }
    if (ok)
      if ((b.xref = mopen ()) == NULL) {   /* tmp file for ON ERROR */
        fprintf (stderr, "%s: Can't open tmpfile for xref\n", argv[0]);
        ok = FALSE;
      }
    if (ok)
      if ((mtrack = mopen ()) == NULL) { /* tmp file for str alloc tracking */
        fprintf (stderr, "%s: Can't open tmpfile for mtrack\n", argv[0]);
        ok = FALSE;
      }
    if (ok)
      ok = compile_file (&b);              /* COMPILE A FILE TO BCODES */
    for (type = FIRST_TYPE; type <= LAST_TYPE; type++) {
      var_root[type] = free_tree (var_root[type]);
      array_root[type] = free_tree (array_root[type]);
      function_root[type] = free_tree (function_root[type]);
      arg_root[type] = free_tree (arg_root[type]);
    }
    if (ok && b.b.f != NULL && b.data.f != NULL) {
      if (first) {
        init_tty ();
        first = FALSE;
      }
      chain_fname = run_tmpfile (&b);      /* RUN BCODES */
      b.fname = my_free (b.fname);
      if (chain_fname != NULL) {
        b.fname = chain_fname;
        chain_fname = NULL;
      }
    }
    if (mtrack != NULL) {
      free_const_strings ();
      mtrack = mclose (mtrack);
    }
    if (b.f != NULL)
      fclose (b.f);
    if (b.data.f != NULL)
      b.data.f = mclose (b.data.f);
    if (b.b.f != NULL)
      b.b.f = mclose (b.b.f);
    if (b.xref != NULL)
      b.xref = mclose (b.xref);
  } /* end of CHAIN loop */
  if (b.fname != NULL)
    b.fname = my_free (b.fname);
  if (chain_fname != NULL)
    chain_fname = my_free (chain_fname);
  if (ok)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
} /* main */

/****************************************************************************/

#ifdef THREADED
static BOOL check_threaded_code_fits (void) {
  /* do the compiled bcodes fit in 64 kB? */
  enum bcode_type b;
  int minb = INT_MAX, maxb = INT_MIN;
  for (b = B_NOP; b <= B_END; b++) {
    if (static_bcode_labels[b] < minb)
      minb = static_bcode_labels[b];
    if (static_bcode_labels[b] > maxb)
      maxb = static_bcode_labels[b];
  }
  if (SIZE_THREADED_BCODE == 2 && (minb < -32768 || maxb > 32767)) {
    for (b = B_NOP; b <= B_END; b++)
      printf ("%7d  %s\n", static_bcode_labels[(int)b], bcode_table[(int)b]);
    fprintf (stderr, "Threaded bcode_labels[] range is %d to %d\n",
             minb, maxb);
    fprintf (stderr, "Fatal error: compiled bcodes do not fit in range of "
             "THREADED_BCODE_TYPE=" STRINGIFY(THREADED_BCODE_TYPE) "\n");
    fprintf (stderr, "Please recompile mybasic without -DTHREADED_BCODE_TYPE="
             STRINGIFY(THREADED_BCODE_TYPE) "\n");
    return FALSE;
  }
  /* printf ("Threaded bcode_labels[] range is %d to %d\n", minb, maxb); */
  return TRUE;
} /* check_threaded_code_fits */
#endif

/****************************************************************************/
/****************************************************************************/
/*   C O M P I L E - T I M E   E R R O R   H A N D L E R  */

static jmp_buf err_jmp;

static void vcompile_error (char *msg, va_list arglist)
{
  fprintf (stderr, "mybasic: ");
  vfprintf (stderr, msg, arglist);
  fprintf (stderr, "\n");
  longjmp (err_jmp, 1);
} /* vcompile_error */

void compile_error (char *msg, ...)
{
  va_list arglist;

  va_start (arglist, msg);
  vcompile_error (msg, arglist);
  va_end (arglist);
} /* compile_error */

/****************************************************************************/
/*   C O M P I L E   A   F I L E   T O   B C O D E S   */

static BOOL compile_file (struct b *b)
{
  char *line;
  volatile T_LONG line_number;
  int erl;
  enum var_type_type type;
  BOOL ret;

  for (type = FIRST_TYPE; type <= LAST_TYPE; type++) {
    var_root[type] = NULL;
    array_root[type] = NULL;
    function_root[type] = NULL;
    arg_root[type] = NULL;
    vars_size[type] = 0;
  }
  goto_list_root = NULL;
  struct_statement_stack_root = NULL;
  struct_statement_count = 0;
  b->b.offset = 0;
  b->b.is_keeping_history = TRUE;
  b->b.history_count = 0;
  b->data.offset = 0;
  b->data.is_keeping_history = FALSE;
  b->data.history_count = 0;
  line_number = 0;
  erl = 0;
  if ((line = my_malloc (MAX_LINE_LENGTH)) == NULL)
    compile_error ("Out of memory allocating %d bytes!", MAX_LINE_LENGTH);
  if (setjmp (err_jmp) == 0) {
    while (fgets (line, MAX_LINE_LENGTH, b->f) != NULL) {
      line_number++;
      debug ((stderr, "\n%04ld: %.*s", line_number,
              strlen(line) > 0 ? (int)(strlen(line) - 1): 0, line));
      if (line[0] != '#' && line[0] != '$')
        compile_line (line, b, line_number, &erl); /* COMPILE LINE TO BCODES */
    }
    gen_bcode (B_END, &b->b);
    gen_bcode (B_END, &b->data);
    if (struct_statement_stack_root != NULL)
      compile_error ("Unterminated IF, FOR, WHILE or DO statement!");
    resolve_gotos (b, goto_list_root);
    ret = TRUE;
  } else {
    fprintf (stderr, "Compile error in line %ld: %s\n", line_number, line);
    b->b.f = mclose (b->b.f);
    b->data.f = mclose (b->data.f);
    ret = FALSE;
  }
  line = my_free (line);
  return ret;
} /* compile_file */

/****************************************************************************/
/*   C O M P I L E   A   L I N E   O F   A   F I L E   T O   B C O D E S   */

static void compile_line (char *line, struct b *b, T_LONG line_number, int *erl)
{
  enum token_type token;
  char *p, *p2;
  struct token_info_type token_info;
  struct var_node *var, *endvar, *stepvar;
  static const T_SIZE_T zero = 0;
  struct result_type result, result2;
  enum var_type_type type;
  struct struct_statement_node *struct_statement_node;
  struct loop_node *loop_node;
  struct for_node *for_node;
  struct xref_record xref_record;
  T_LONG l;
  T_SIZE_T last_offset;
  int statement_number, erl2;
  enum loop_kind_type loop_kind;

  last_offset = 0;
  statement_number = 0;  /* statement number within the line */
  p = line;
  /* loop for each statement in the line  */
  do {

#ifdef DEBUG
    /*gen_bcode (B_STATEMENT_CHECK, &b->b);*/
#endif

    p = get_token (p, &token, &token_info);
    switch (token) {

      case TOKEN_NONE:
      case TOKEN_COLON:
        break;

      case TOKEN_ID:
        p2 = get_token (p, &token, NULL);
        if (token == TOKEN_COLON) {
          p = p2;
          compile_make_label (token_info.token_string, b);
          continue;
        } else
          p = compile_let (token, &token_info, p, b);
        break;

      case TOKEN_NUMBER:
        if (token_info.token_type != TYPE_INT &&
            token_info.token_type != TYPE_LONG)
          compile_error ("Invalid linenumber %s", token_info.token_string);
        erl2 = compile_make_label (token_info.token_string, b);
        if (erl2 != 0)
          *erl = erl2;
        continue;

      case TOKEN_REM:
      case TOKEN_QUOTE:
        while (*p != '\0')
          p++;
        token = TOKEN_NONE;
        /*
        do {
          p = get_token (p, &token, NULL);
        } while (token != TOKEN_NONE);
        */
        break;

      case TOKEN_OPTION:
        p = check_token (p, TOKEN_BASE, NULL, "Expected BASE after OPTION");
        if (b->option_base != -1)
          compile_error ("Array already dimensioned at OPTION BASE");
        p = get_token (p, &token, &token_info);
        if (token != TOKEN_NUMBER || token_info.token_type != TYPE_INT ||
            (token_info.value.i != 0 && token_info.value.i != 1))
          compile_error ("OPTION BASE must be 0 or 1");
        b->option_base = token_info.value.i;
        break;

      case TOKEN_DIM:
        do {
          p = check_token (p, TOKEN_ID, &token_info,
                           "Identifier expected at %s in DIM statement",
                           token_info.token_string);
          p2 = get_token (p, &token, NULL);
          if (token != TOKEN_LBRACKET) {
            token_info.dim = 0;
            var = lookup_var (&var_root[token_info.token_type], &token_info,
                              TRUE, b);
            if (var->count++ != 0)
              compile_error ("Duplicate definition of %s",
                             token_info.token_string);
          } else {
            p = check_token (p, TOKEN_LBRACKET, NULL,
                             "Left bracket expected at %s in DIM statement",
                             token_info.token_string);
            if (b->option_base == -1)
              b->option_base = 0;
            token_info.dim = 0;
            do {
              if (token_info.dim >= MAX_DIMENSIONS)
                compile_error ("Too many dimensions, limit is %d",
                               MAX_DIMENSIONS);
              p = compile_expression (p, b, &result);
              compile_convert_type (&result, TYPE_LONG, b);
              if (result.on_stack)
                compile_error ("Dimension cannot be a variable at %s",
                               token_info.token_string);
              token_info.dim_size[token_info.dim++] =
                result.value.l - b->option_base;
              p = get_token (p, &token, NULL);
            } while (token == TOKEN_COMMA);
            if (token != TOKEN_RBRACKET)
              compile_error ("Right bracket expected at %s in DIM statement",
                             token_info.token_string);
            var = lookup_var (&array_root[token_info.token_type], &token_info,
                              TRUE, b);
            if (var->count++ != 0)
              compile_error ("Duplicate definition of %s",
                             token_info.token_string);
            p2 = get_token (p, &token, NULL);
          }
          if (token == TOKEN_COMMA)
            p = p2;
        } while (token == TOKEN_COMMA);
        break;

      case TOKEN_CLEAR:
        gen_bcode (B_CLEAR, &b->b);
        break;

      case TOKEN_LET:
        p = check_token (p, TOKEN_ID, &token_info,
                         "Identifier expected at %s in LET statement",
                         token_info.token_string);
        p = compile_let (token, &token_info, p, b);
        break;

      case TOKEN_MID:
        p = compile_let (token, &token_info, p, b);
        break;

      case TOKEN_IF:
        if ((struct_statement_node = (struct struct_statement_node *)my_malloc
             (sizeof(struct struct_statement_node))) == NULL)
          compile_error ("Out of memory for structured statements stack");
        struct_statement_node->next = struct_statement_stack_root;
        struct_statement_node->struct_statement_kind = S_IF;
        struct_statement_node->node.if_node.got_else = FALSE;
        struct_statement_stack_root = struct_statement_node;
        sprintf (struct_statement_node->node.if_node.endif_string, "_IF_%d",
                 struct_statement_count++);
        sprintf (struct_statement_node->node.if_node.else_string, "_IF_%d",
                 struct_statement_count++);
        p = compile_conditional_jump (p, FALSE,
                 struct_statement_node->node.if_node.else_string, b);
        p2 = get_token (p, &token, NULL);
        if (token == TOKEN_GO || token == TOKEN_GOTO) {
          struct_statement_node->node.if_node.one_line = TRUE;
          continue;
        } else {
          p = check_token (p, TOKEN_THEN, NULL, "THEN or GOTO expected");
          struct_statement_node->node.if_node.one_line = FALSE;
          p2 = get_token (p, &token, &token_info);
          if (token != TOKEN_NONE && token != TOKEN_COLON &&
              token != TOKEN_QUOTE) {
            struct_statement_node->node.if_node.one_line = TRUE;
            if (token == TOKEN_ID || token == TOKEN_NUMBER) {
              get_token (p2, &token, NULL);
              if (token == TOKEN_COLON ||
                  token == TOKEN_QUOTE ||
                  token == TOKEN_NONE ||
                  token == TOKEN_ELSE) {
                gen_bcode (B_JUMP, &b->b);
                goto_list_root = add_goto_node (goto_list_root, FALSE,
                                                b->b.offset,
                                                token_info.token_string);
                gen_size_t (zero, &b->b);
                b->b.history_count = 0;
                p = p2;
              } else
                continue;
            } else
              continue;
          }
        }
        break;

      case TOKEN_ELSEIF:
        if (struct_statement_stack_root == NULL)
          compile_error ("ELSEIF without IF");
        if (struct_statement_stack_root->struct_statement_kind != S_IF)
          compile_error ("ELSEIF does not have matching IF");
        if (struct_statement_stack_root->node.if_node.got_else)
          compile_error ("Can't have ELSEIF after ELSE");
        if (struct_statement_stack_root->node.if_node.one_line)
          compile_error ("Can't use ELSEIF in a one-line IF");
        gen_bcode (B_JUMP, &b->b);
        goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                            struct_statement_stack_root->node.if_node.
                            endif_string);
        gen_size_t (zero, &b->b);
        compile_make_label (struct_statement_stack_root->node.if_node.
                            else_string, b);
        sprintf (struct_statement_stack_root->node.if_node.else_string,
                 "_IF_%d", struct_statement_count++);
        p = compile_conditional_jump (p, FALSE,
                 struct_statement_stack_root->node.if_node.else_string, b);
        p = check_token (p, TOKEN_THEN, NULL, "THEN expected");
        break;

      case TOKEN_ELSE:
        if (struct_statement_stack_root == NULL)
          compile_error ("ELSE without IF");
        if (struct_statement_stack_root->struct_statement_kind != S_IF)
          compile_error ("ELSE does not have matching IF");
        while (struct_statement_stack_root->node.if_node.got_else) {
          struct_statement_node = struct_statement_stack_root;
          struct_statement_stack_root = struct_statement_stack_root->next;
          compile_make_label (struct_statement_node->node.
                              if_node.endif_string, b);
          struct_statement_node = my_free ((char *)struct_statement_node);
          if (struct_statement_stack_root == NULL ||
              !struct_statement_stack_root->node.if_node.one_line)
            compile_error ("ELSE without IF");
          if (struct_statement_stack_root->struct_statement_kind != S_IF)
            compile_error ("ELSE does not have matching IF");
        }
        struct_statement_stack_root->node.if_node.got_else = TRUE;
        gen_bcode (B_JUMP, &b->b);
        goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                            struct_statement_stack_root->node.if_node.
                                        endif_string);
        gen_size_t (zero, &b->b);
        compile_make_label (struct_statement_stack_root->node.if_node.
                            else_string, b);

        p2 = get_token (p, &token, &token_info);
        if (token != TOKEN_NONE && token != TOKEN_COLON &&
            token != TOKEN_QUOTE &&
            struct_statement_stack_root->node.if_node.one_line) {
          if (token == TOKEN_ID || token == TOKEN_NUMBER) {
            get_token (p2, &token, NULL);
            if (token == TOKEN_COLON ||
                token == TOKEN_QUOTE ||
                token == TOKEN_NONE ||
                token == TOKEN_ELSE) {
              gen_bcode (B_JUMP, &b->b);
              goto_list_root = add_goto_node (goto_list_root, FALSE,
                                              b->b.offset,
                                              token_info.token_string);
              gen_size_t (zero, &b->b);
              b->b.history_count = 0;
              p = p2;
            } else
              continue;
          } else
            continue;
        }
        break;

      case TOKEN_WHILE:
      case TOKEN_DO:
        if ((struct_statement_node = (struct struct_statement_node *)my_malloc
             (sizeof(struct struct_statement_node))) == NULL)
          compile_error ("Out of memory for structured statements stack");
        struct_statement_node->next = struct_statement_stack_root;
        struct_statement_node->struct_statement_kind = S_LOOP;
        struct_statement_stack_root = struct_statement_node;
        loop_node = &struct_statement_node->node.loop_node;
        loop_node->offset = b->b.offset;
        b->b.history_count = 0;
        sprintf (loop_node->endlabel_string, "_LOOP_END_%d",
                 struct_statement_count++);
        if (token == TOKEN_WHILE) {
          loop_node->loop_kind = L_WHILE;
          p = compile_conditional_jump (p, FALSE, loop_node->endlabel_string, b);
        } else { /* token == TOKEN_DO */
          p2 = get_token (p, &token, NULL);
          if (token == TOKEN_WHILE) {
            loop_node->loop_kind = L_DO_WHILE;
            p = compile_conditional_jump (p2, FALSE, loop_node->endlabel_string,
                                          b);
          } else if (token == TOKEN_UNTIL) {
            loop_node->loop_kind = L_DO_UNTIL;
            p = compile_conditional_jump (p2, TRUE, loop_node->endlabel_string,
                                          b);
          } else {
            loop_node->loop_kind = L_DO;
          }
        }
        break;

      case TOKEN_WEND:
      case TOKEN_LOOP:
        struct_statement_node = struct_statement_stack_root;
        if (struct_statement_node == NULL)
          compile_error ("WEND or LOOP without DO or WHILE");
        struct_statement_stack_root = struct_statement_node->next;
        loop_kind = struct_statement_node->node.loop_node.loop_kind;
        if (token == TOKEN_WEND && loop_kind != L_WHILE)
          compile_error ("WEND without WHILE");
        if (token == TOKEN_LOOP && loop_kind != L_DO &&
            loop_kind != L_DO_WHILE && loop_kind != L_DO_UNTIL)
          compile_error ("LOOP with DO, DO WHILE or DO UNTIL");
        if (token == TOKEN_LOOP && loop_kind == L_DO &&
            (p2 = get_token (p, &token, NULL),
             (token == TOKEN_WHILE || token == TOKEN_UNTIL))) {
          p = compile_expression (p2, b, &result);
          compile_check_numeric (result.type);
          if (token == TOKEN_WHILE) /* DO...LOOP WHILE */
            gen_bcode_size_t (type_info[result.type].b_jump_notequal_zero,
                              type_info[result.type].b_jump_notequal_zero_16,
                              struct_statement_node->node.loop_node.offset,
                              &b->b);
          else /* DO...LOOP UNTIL */
            gen_bcode_size_t (type_info[result.type].b_jump_equal_zero,
                              type_info[result.type].b_jump_equal_zero_16,
                              struct_statement_node->node.loop_node.offset,
                              &b->b);
        } else { /* WHILE...WEND  or  DO WHILE...LOOP  or  DO UNTIL...LOOP */
          gen_bcode_size_t (B_JUMP, B_JUMP_16,
                           struct_statement_node->node.loop_node.offset, &b->b);
        }
        compile_make_label (struct_statement_node->node.loop_node.endlabel_string,
                            b);
        struct_statement_node = my_free ((char *)struct_statement_node);
        break;

      case TOKEN_EXIT:
        p = get_token (p, &token, NULL);
        if (token != TOKEN_WHILE && token != TOKEN_FOR && token != TOKEN_DO)
          compile_error ("EXIT without FOR or DO");
        struct_statement_node = struct_statement_stack_root;
        while (struct_statement_node != NULL &&
               (token != TOKEN_DO ||
                struct_statement_node->struct_statement_kind != S_LOOP ||
                struct_statement_node->node.loop_node.loop_kind == L_WHILE) &&
               (token != TOKEN_FOR ||
                struct_statement_node->struct_statement_kind != S_FOR))
          struct_statement_node = struct_statement_node->next;
        if (struct_statement_node == NULL)
          compile_error ("EXIT without FOR or DO");
        gen_bcode (B_JUMP, &b->b);
        if (struct_statement_node->struct_statement_kind == S_LOOP)
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                          struct_statement_node->node.loop_node.endlabel_string);
        else if (struct_statement_node->struct_statement_kind == S_FOR)
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                          struct_statement_node->node.for_node.endlabel_string);
        else
          compile_error ("Internal error in EXIT");
        gen_size_t (zero, &b->b);
        b->b.history_count = 0;
        break;

      case TOKEN_FOR:
        /*
          Say we are compiling following FOR/NEXT loop:
            FOR var = expr1 TO expr2 STEP expr3
              ...
            NEXT var
          That is implemented as follows:
            LET endvar = expr2
            LET stepvar = expr3
            LET var = expr1 : REM var is assigned -after- endvar and stepvar
            loop:
              IF stepvar > 0 THEN IF var > endvar THEN GOTO endlabel
              IF stepvar < 0 THEN IF var < endvar THEN GOTO endlabel
              ...
              LET var = var + stepvar
              GOTO loop
            endlabel:
        */
        if ((struct_statement_node = (struct struct_statement_node *)my_malloc
             (sizeof(struct struct_statement_node))) == NULL)
          compile_error ("Out of memory for structured statements stack");
        struct_statement_node->next = struct_statement_stack_root;
        struct_statement_node->struct_statement_kind = S_FOR;
        struct_statement_stack_root = struct_statement_node;
        for_node = &struct_statement_node->node.for_node;
        sprintf (for_node->endlabel_string, "_FOR_ENDLABEL_%d",
                 struct_statement_count);

        /* LET var = expr1 */
        p = check_token (p, TOKEN_ID, &token_info,
                         "Identifier expected at %s in FOR statement",
                         token_info.token_string);
        compile_check_numeric (token_info.token_type);
        token_info.dim = 0;
        type = token_info.token_type;
        var = lookup_var (&var_root[type], &token_info, TRUE, b);
        for_node->var_type = type;
        for_node->var_offset = var->offset;
        p = check_token (p, TOKEN_EQUAL, NULL,
                         "'=' expected after %s in FOR statement",
                         token_info.token_string);
        p = compile_expression (p, b, &result);
        compile_convert_type (&result, type, b);
        /*compile_push_result (&result, &b->b);*/
        /* leave expr1 result on the stack until endvar and stepvar are set */

        /* LET endvar = expr2 */
        p = check_token (p, TOKEN_TO, &token_info,
                         "'TO' expected at %s in FOR statement",
                         token_info.token_string);
        p = compile_expression (p, b, &result2);
        compile_convert_type (&result2, type, b);
        for_node->endvar.token_type = type;
        if (!result2.on_stack) {
          for_node->const_endvar = TRUE;
          for_node->end_value = result2.value;
        } else {
          sprintf (for_node->endvar.token_string, "_FOR_ENDVAR_%d",
                   struct_statement_count);
          for_node->endvar.dim = 0;
          endvar = lookup_var (&var_root[type], &for_node->endvar, TRUE, b);
          for_node->endvar_offset = endvar->offset;
          for_node->const_endvar = FALSE;
          /* pop expr2 result2 to endvar */
          compile_assign_result (&result2, endvar->offset, b);
        }

        /* LET stepvar = expr3 */
        for_node->stepvar.token_type = type;
        p2 = get_token (p, &token, NULL);
        if (token == TOKEN_STEP) { /* if there is a an explicit STEP clause */
          p = compile_expression (p2, b, &result2);
          compile_convert_type (&result2, type, b);
          if (!result2.on_stack) { /* if step is a constant */
            for_node->const_stepvar = TRUE;
            for_node->step_value = result2.value;
          } else { /* else step is a variable */
            for_node->const_stepvar = FALSE;
            sprintf (for_node->label1_string, "_FOR_LABEL1_%d",
                     struct_statement_count);
            sprintf (for_node->label2_string, "_FOR_LABEL2_%d",
                     struct_statement_count);
            sprintf (for_node->stepvar.token_string, "_FOR_STEPVAR_%d",
                     struct_statement_count);
            for_node->stepvar.dim = 0;
            stepvar = lookup_var (&var_root[type], &for_node->stepvar, TRUE, b);
            for_node->stepvar_offset = stepvar->offset;
            compile_assign_result (&result2, stepvar->offset, b);
          }
        } else { /* else no STEP clause => default step is constant 1 */
          result2.on_stack = FALSE;
          result2.type = TYPE_INT;
          result2.value.i = 1;
          compile_convert_type (&result2, type, b);
          for_node->const_stepvar = TRUE;
          for_node->step_value = result2.value;
        }

        /* only now pop expr1 to var */
        compile_assign_result (&result, var->offset, b);
        /*gen_bcode_size_t (type_info[type].b_pop, type_info[type].b_pop_16,
                            var->offset, &b->b);*/

        /* loop:  NEXT jumps back to code generated from here */
        for_node->loop_offset = b->b.offset;
        b->b.history_count = 0;

        if (for_node->const_stepvar) { /* if stepvar is a constant */

          /* if var > endvar then goto endlabel ( or < for -step) */
          gen_bcode_size_t (type_info[type].b_push_var,
                            type_info[type].b_push_var_16, for_node->var_offset,
                            &b->b);
          if (for_node->const_endvar) /* if endvar is a constant */
            compile_push_const (type, &for_node->end_value, &b->b);
          else /* else endvar is a variable */
            gen_bcode_size_t (type_info[type].b_push_var,
                              type_info[type].b_push_var_16,
                              for_node->endvar_offset, &b->b);
          /* if step_value > 0 */
          if ((type == TYPE_INT && for_node->step_value.i > 0) ||
              (type == TYPE_LONG && for_node->step_value.l > 0) ||
              (type == TYPE_SINGLE && for_node->step_value.s > 0.0f) ||
              (type == TYPE_DOUBLE && for_node->step_value.d > 0.0))
            gen_bcode (type_info[type].b_jump_greater, &b->b);
          else /* else step_value <= 0 */
            gen_bcode (type_info[type].b_jump_less, &b->b);
          /*gen_bcode (B_JUMP_INT_NOTEQUAL, &b->b);*/
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                          for_node->endlabel_string);
          gen_size_t (zero, &b->b);
          b->b.history_count = 0;

        } else { /* else stepvar is a variable */

          /* if stepvar < 0 then goto label1 */
          gen_bcode_size_t (type_info[type].b_push_var,
                            type_info[type].b_push_var_16,
                            for_node->stepvar_offset, &b->b);
          gen_bcode (type_info[type].b_push_const_zero, &b->b);
          gen_bcode (type_info[type].b_jump_less, &b->b);
          /*gen_bcode (B_JUMP_INT_NOTEQUAL, &b->b);*/
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                          for_node->label1_string);
          gen_size_t (zero, &b->b);
          b->b.history_count = 0;

          /* if var > endvar then goto endlabel */
          gen_bcode_size_t (type_info[type].b_push_var,
                            type_info[type].b_push_var_16, for_node->var_offset,
                            &b->b);
          if (for_node->const_endvar) /* endvar is a constant */
            compile_push_const (type, &for_node->end_value, &b->b);
          else /* endvar is a variable */
            gen_bcode_size_t (type_info[type].b_push_var,
                              type_info[type].b_push_var_16,
                              for_node->endvar_offset, &b->b);
          gen_bcode (type_info[type].b_jump_greater, &b->b);
          /*gen_bcode (B_JUMP_INT_NOTEQUAL, &b->b);*/
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                          for_node->endlabel_string);
          gen_size_t (zero, &b->b);
          b->b.history_count = 0;

          /* goto label2 */
          gen_bcode (B_JUMP, &b->b);
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                          for_node->label2_string);
          gen_size_t (zero, &b->b);

          /* label1: */
          compile_make_label (for_node->label1_string, b);

          /* if var < endvar then goto endlabel */
          gen_bcode_size_t (type_info[type].b_push_var,
                            type_info[type].b_push_var_16, for_node->var_offset,
                            &b->b);
          if (for_node->const_endvar) /* endvar is a constant */
            compile_push_const (type, &for_node->end_value, &b->b);
          else /* endvar is a variable */
            gen_bcode_size_t (type_info[type].b_push_var,
                              type_info[type].b_push_var_16,
                              for_node->endvar_offset, &b->b);
          gen_bcode (type_info[type].b_jump_less, &b->b);
          /*gen_bcode (B_JUMP_INT_NOTEQUAL, &b->b);*/
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                          for_node->endlabel_string);
          gen_size_t (zero, &b->b);

          /* label2: */
          compile_make_label (for_node->label2_string, b);
        }

        struct_statement_count++;

        break;

      case TOKEN_NEXT:
        do {
          if (struct_statement_stack_root == NULL)
            compile_error ("NEXT without FOR");
          if (struct_statement_stack_root->struct_statement_kind != S_FOR)
            compile_error ("NEXT does not have matching FOR");
          struct_statement_node = struct_statement_stack_root;
          struct_statement_stack_root = struct_statement_node->next;
          for_node = &struct_statement_node->node.for_node;

          p2 = get_token (p, &token, &token_info);
          if (token == TOKEN_ID) {
            p = p2;
            token_info.dim = 0;
            var = lookup_var (&var_root[token_info.token_type], &token_info,
                              TRUE, b);
            if (var->var_type != for_node->var_type ||
                var->offset != for_node->var_offset)
              compile_error ("Mismatched FOR/NEXT variables");
          }

          /* LET var = var + stepvar */
          type = for_node->var_type;
          if (for_node->const_stepvar) { /* if step is a constant */
            union number_union value = for_node->step_value;
            if (value_is_equal (type, value, 1)) {
              /* if step is exactly 1 */
              gen_bcode_size_t (type_info[type].b_increment_var,
                                type_info[type].b_increment_var_16,
                                for_node->var_offset, &b->b);
            } else if (value_is_equal (type, value, -1)) {
              /* if step is exactly -1 */
              gen_bcode_size_t (type_info[type].b_decrement_var,
                                type_info[type].b_decrement_var_16,
                                for_node->var_offset, &b->b);
            } else { /* else step is a constant other than 1 */
              gen_bcode_size_t (type_info[type].b_push_var,
                                type_info[type].b_push_var_16,
                                for_node->var_offset, &b->b);
              compile_push_const (type, &for_node->step_value, &b->b);
              gen_bcode (type_info[type].b_plus, &b->b);
              result.type = type;
              result.on_stack = TRUE;
              compile_assign_result (&result, for_node->var_offset, b);
              /*gen_bcode_size_t (type_info[type].b_pop,
                                  type_info[type].b_pop_16, for_node->var_offset,
                                  &b->b);*/
            }
          } else { /* else step is a variable */
            gen_bcode_size_t (type_info[type].b_push_var,
                              type_info[type].b_push_var_16,
                              for_node->var_offset, &b->b);
            gen_bcode_size_t (type_info[type].b_push_var,
                              type_info[type].b_push_var_16,
                              for_node->stepvar_offset, &b->b);
            gen_bcode (type_info[type].b_plus, &b->b);
            result.type = type;
            result.on_stack = TRUE;
            compile_assign_result (&result, for_node->var_offset, b);
            /*gen_bcode_size_t (type_info[type].b_pop,
                                type_info[type].b_pop_16, for_node->var_offset,
                                &b->b);*/
          }

          /* GOTO loop */
          gen_bcode_size_t (B_JUMP, B_JUMP_16, for_node->loop_offset, &b->b);

          /* endlabel: */
          compile_make_label (for_node->endlabel_string, b);

          struct_statement_node = my_free ((char *)struct_statement_node);

          p2 = get_token (p, &token, &token_info);
          if (token == TOKEN_COMMA)
            p = p2;
        } while (token == TOKEN_COMMA);
        break;

      case TOKEN_GOSUB:
        p = get_token (p, &token, &token_info);
        if (token == TOKEN_ID || token == TOKEN_NUMBER) {
          gen_bcode (B_GOSUB, &b->b);
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                          token_info.token_string);
          gen_size_t (zero, &b->b);
          b->b.history_count = 0;
        } else
          compile_error ("Label expected at %s in GOSUB statement",
                         token_info.token_string);
        break;

      case TOKEN_RETURN:
        p2 = get_token (p, &token, &token_info);
        if (token == TOKEN_ID || token == TOKEN_NUMBER) {
          gen_bcode (B_RETURN_LINE, &b->b);
          goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                          token_info.token_string);
          gen_size_t (zero, &b->b);
          p = p2;
        } else
          gen_bcode (B_RETURN, &b->b);
        b->b.history_count = 0;
        break;

      case TOKEN_STOP:
        gen_bcode (B_STOP, &b->b);
        break;

      case TOKEN_SYSTEM:
        gen_bcode (B_END, &b->b);
        break;

      case TOKEN_END:
        p2 = get_token (p, &token, NULL);
        if (token != TOKEN_IF) {
          gen_bcode (B_END, &b->b);
          break;
        }
        p = p2;
      case TOKEN_ENDIF:
        if (struct_statement_stack_root == NULL ||
            struct_statement_stack_root->struct_statement_kind != S_IF)
          compile_error ("END IF does not have matching IF");
        struct_statement_node = struct_statement_stack_root;
        struct_statement_stack_root = struct_statement_node->next;
        if (!struct_statement_node->node.if_node.got_else)
          compile_make_label (struct_statement_node->node.
                              if_node.else_string, b);
        compile_make_label (struct_statement_node->node.
                            if_node.endif_string, b);
        struct_statement_node = my_free ((char *)struct_statement_node);
        break;

      case TOKEN_RANDOMIZE:
        p2 = get_token (p, &token, NULL);
        if (token == TOKEN_TIMER) {
          p = p2;
          gen_bcode (B_RANDOMIZE_TIMER, &b->b);
        } else if (token != TOKEN_COLON &&
                   token != TOKEN_QUOTE &&
                   token != TOKEN_NONE &&
                   token != TOKEN_ELSE) {
          p = compile_expression (p, b, &result);
          compile_convert_type (&result, TYPE_LONG, b);
          compile_push_result (&result, &b->b);
          gen_bcode (B_RANDOMIZE_LONG, &b->b);
        } else {
          compile_input ("\"Random number seed (-32768 to 32767)\";_RSEED%", b);
          strcpy (token_info.token_string, "_RSEED");
          token_info.token_type = TYPE_INT;
          token_info.dim = 0;
          var = lookup_var (&var_root[token_info.token_type], &token_info, TRUE,
                            b);
          gen_bcode_size_t (B_PUSH_VAR_INT, B_PUSH_VAR_INT_16,
                            var->offset, &b->b);
          gen_bcode (B_INT_TO_LONG, &b->b);
          gen_bcode (B_RANDOMIZE_LONG, &b->b);
        }
        break;

      case TOKEN_DEFINT:
        p = compile_def (p, TYPE_INT);
        break;

      case TOKEN_DEFLNG:
        p = compile_def (p, TYPE_LONG);
        break;

      case TOKEN_DEFSNG:
        p = compile_def (p, TYPE_SINGLE);
        break;

      case TOKEN_DEFDBL:
        p = compile_def (p, TYPE_DOUBLE);
        break;

      case TOKEN_DEFSTR:
        p = compile_def (p, TYPE_STRING);
        break;

      case TOKEN_DEF:
        p = compile_deffn (p, b);
        break;

      case TOKEN_INCR:
        p = check_token (p, TOKEN_LBRACKET, NULL,
                         "Left bracket expected at %s in INCR statement",
                         NULL);
        p = check_token (p, TOKEN_ID, &token_info,
                         "Identifier expected at %s in INCR statement",
                         token_info.token_string);
        compile_check_numeric (token_info.token_type);
        p = compile_lhs_variable (&token_info, &var, &result, p, b);
        if (var->dim > 0) {
          if (!result.on_stack) {
            gen_bcode_size_t (type_info[var->var_type].b_increment_var,
                              type_info[var->var_type].b_increment_var_16,
                              var->offset + result.value.index, &b->b);
          } else {
            gen_bcode (B_INDEX_DUP, &b->b);
            gen_bcode_size_t (type_info[var->var_type].b_push_indirect_var,
                              type_info[var->var_type].b_push_indirect_var_16,
                              var->offset, &b->b);
            gen_bcode (type_info[var->var_type].b_increment, &b->b);
            gen_bcode_size_t (type_info[var->var_type].b_pop_indirect,
                              type_info[var->var_type].b_pop_indirect_16,
                              var->offset, &b->b);
          }
        } else {
          gen_bcode_size_t (type_info[var->var_type].b_increment_var,
                            type_info[var->var_type].b_increment_var_16,
                            var->offset, &b->b);
        }
        p = check_token (p, TOKEN_RBRACKET, NULL,
                         "Right bracket expected at %s in INCR statement",
                         token_info.token_string);
        break;

      case TOKEN_DECR:
        p = check_token (p, TOKEN_LBRACKET, NULL,
                         "Left bracket expected at %s in DECR statement",
                         NULL);
        p = check_token (p, TOKEN_ID, &token_info,
                         "Identifier expected at %s in DECR statement",
                         token_info.token_string);
        compile_check_numeric (token_info.token_type);
        p = compile_lhs_variable (&token_info, &var, &result, p, b);
        if (var->dim > 0) {
          if (!result.on_stack) {
            gen_bcode_size_t (type_info[var->var_type].b_decrement_var,
                              type_info[var->var_type].b_decrement_var_16,
                              var->offset + result.value.index, &b->b);
          } else {
            gen_bcode (B_INDEX_DUP, &b->b);
            gen_bcode_size_t (type_info[var->var_type].b_push_indirect_var,
                              type_info[var->var_type].b_push_indirect_var_16,
                              var->offset, &b->b);
            gen_bcode (type_info[var->var_type].b_decrement, &b->b);
            gen_bcode_size_t (type_info[var->var_type].b_pop_indirect,
                              type_info[var->var_type].b_pop_indirect_16,
                              var->offset, &b->b);
          }
        } else {
          gen_bcode_size_t (type_info[var->var_type].b_decrement_var,
                            type_info[var->var_type].b_decrement_var_16,
                            var->offset, &b->b);
        }
        p = check_token (p, TOKEN_RBRACKET, NULL,
                         "Right bracket expected at %s in DECR statement",
                         token_info.token_string);
        break;

      case TOKEN_OPEN:
        p = compile_open (p, b);
        break;

      case TOKEN_CLOSE:
        p = compile_close (p, b);
        break;

      case TOKEN_KILL:
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        gen_bcode (B_KILL, &b->b);
        break;

      case TOKEN_NAME:
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        p = check_token (p, TOKEN_AS, NULL, "AS expected in NAME statement");
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        gen_bcode (B_NAME, &b->b);
        break;

      case TOKEN_CHDIR:
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        gen_bcode (B_CHDIR, &b->b);
        break;

      case TOKEN_MKDIR:
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        gen_bcode (B_MKDIR, &b->b);
        break;

      case TOKEN_RMDIR:
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        gen_bcode (B_RMDIR, &b->b);
        break;

      case TOKEN_SHELL:
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        gen_bcode (B_SHELL, &b->b);
        break;

      case TOKEN_CHAIN:
        p = compile_expression (p, b, &result);
        compile_check_string (result.type);
        compile_push_result (&result, &b->b);
        gen_bcode (B_CHAIN, &b->b);
        break;

      case TOKEN_PRINT:
      case TOKEN_QUESTIONMARK:
        p = compile_print (p, b);
        break;

      case TOKEN_BEEP:
        gen_bcode (B_SET_PRIMARY_OUTPUT_FILE, &b->b);
        gen_bcode (B_BEEP, &b->b);
        break;

      case TOKEN_SLEEP:
        p = compile_expression (p, b, &result);
        compile_convert_type (&result, TYPE_DOUBLE, b);
        compile_push_result (&result, &b->b);
        gen_bcode (B_SLEEP, &b->b);
        break;

      case TOKEN_CLS:
        gen_bcode (B_SET_PRIMARY_OUTPUT_FILE, &b->b);
        gen_bcode (B_CLS, &b->b);
        break;

      case TOKEN_LOCATE:
        p = compile_locate (p, b);
        break;

      case TOKEN_COLOR:
        p = compile_color (p, b);
        break;

      case TOKEN_LINE:
        p = check_token (p, TOKEN_INPUT, NULL, "LINE not implemented yet");
      case TOKEN_LINEINPUT:
        p = compile_lineinput (p, b);
        break;

      case TOKEN_INPUT:
        p = compile_input (p, b);
        break;

      case TOKEN_GO:
        p = check_token (p, TOKEN_TO, NULL, "GO without TO");
      case TOKEN_GOTO:
        p = get_token (p, &token, &token_info);
        if (token != TOKEN_ID && token != TOKEN_NUMBER)
          compile_error ("Label identifier expected at %s in GOTO statement",
                         token_info.token_string);
        gen_bcode (B_JUMP, &b->b);
        goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                        token_info.token_string);
        gen_size_t (zero, &b->b);
        b->b.history_count = 0;
        break;

      case TOKEN_ON:
        p2 = get_token (p, &token, NULL);
        if (token == TOKEN_ERROR) {
          p = check_token (p2, TOKEN_GOTO, NULL, "ON ERROR GOTO syntax error");
          p = get_token (p, &token, &token_info);
          if (token != TOKEN_ID && token != TOKEN_NUMBER)
            compile_error ("Label identifier expected at %s in ON ERROR GOTO statement",
                           token_info.token_string);
          if (token == TOKEN_NUMBER &&
              strcmp (token_info.token_string, "0") == 0) {
            gen_bcode (B_ON_ERROR_GOTO_0, &b->b);
          } else {
            gen_bcode (B_ON_ERROR_GOTO, &b->b);
            goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                            token_info.token_string);
            gen_size_t (zero, &b->b);
            b->b.history_count = 0;
          }
          break;
        }
        p = compile_expression (p, b, &result);
        compile_convert_type (&result, TYPE_LONG, b);
        compile_push_result (&result, &b->b);
        p = get_token (p, &token, &token_info);
        if (token == TOKEN_GOTO)
          gen_bcode (B_ON_GOTO, &b->b);
        else if (token == TOKEN_GOSUB)
          gen_bcode (B_ON_GOSUB, &b->b);
        else
          compile_error ("GOTO or GOSUB expected in ON statement");
        pad_align (ALIGN_LONG, &b->b);
        l = 0;
        do {
          p = get_token (p, &token, &token_info);
          if (token != TOKEN_ID && token != TOKEN_NUMBER)
            compile_error ("Label identifier expected at %s in ON statement",
                         token_info.token_string);
          goto_list_root = add_goto_node (goto_list_root, FALSE,
                                          b->b.offset + SIZE_LONG + l++ * SIZE_SIZE_T,
                                          token_info.token_string);
          p2 = get_token (p, &token, &token_info);
          if (token == TOKEN_COMMA)
            p = p2;
        } while (token == TOKEN_COMMA);
        gen_long (l, &b->b);
        for ( ; l > 0; l--)
          gen_size_t (zero, &b->b);
        break;

      case TOKEN_RESUME:
        p2 = get_token (p, &token, &token_info);
        if (token == TOKEN_NEXT) {
          gen_bcode (B_RESUME_NEXT, &b->b);
          p = p2;
        } else if (token == TOKEN_ID || token == TOKEN_NUMBER) {
          if (token == TOKEN_NUMBER && token_info.token_type == TYPE_INT &&
              token_info.value.i == 0) {
            gen_bcode (B_RESUME, &b->b);
          } else {
            gen_bcode (B_RESUME_LABEL, &b->b);
            goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                            token_info.token_string);
            gen_size_t (zero, &b->b);
            b->b.history_count = 0;
            p = p2;
          }
        } else if (token == TOKEN_COLON || token == TOKEN_QUOTE ||
                   token == TOKEN_NONE || token == TOKEN_ELSE)
          gen_bcode (B_RESUME, &b->b);
        else
          compile_error ("Syntex error at %s in RESUME statement",
                         token_info.token_string);
        break;

      case TOKEN_ERROR:
        p = compile_expression (p, b, &result);
        compile_convert_type (&result, TYPE_INT, b);
        compile_push_result (&result, &b->b);
        gen_bcode (B_ERROR, &b->b);
        break;

      case TOKEN_SWAP:
        p = compile_swap (p, b);
        break;

      case TOKEN_READ:
        p = compile_read (p, b);
        break;

      case TOKEN_DATA:
        p = compile_data (p, b);
        break;

      case TOKEN_RESTORE:
        p = compile_restore (p, b);
        break;

      default:
        compile_error ("Unexpected token at %s", token_info.token_string);
        break;
    }

    /* get first token after end of statement */
    p2 = get_token (p, &token, &token_info);
    if (token == TOKEN_ELSE &&
        struct_statement_stack_root != NULL &&
        struct_statement_stack_root->struct_statement_kind == S_IF &&
        struct_statement_stack_root->node.if_node.one_line)
      /* do nothing */ ;
    else if (token == TOKEN_NONE)
      /* handle end of (nested) one-line IF statement */
      while (struct_statement_stack_root != NULL &&
             struct_statement_stack_root->struct_statement_kind == S_IF &&
             struct_statement_stack_root->node.if_node.one_line) {
        struct_statement_node = struct_statement_stack_root;
        struct_statement_stack_root = struct_statement_node->next;
        if (!struct_statement_node->node.if_node.got_else)
          compile_make_label (struct_statement_node->node.
                              if_node.else_string, b);
        compile_make_label (struct_statement_node->node.
                            if_node.endif_string, b);
        struct_statement_node = my_free ((char *)struct_statement_node);
      }
    else if (token == TOKEN_COLON)
      p = p2;
    else if (token == TOKEN_QUOTE) /* end of line comment */
      /* do nothing */;
    else
      compile_error ("End-of-line or colon expected");

    if (b->b.offset != last_offset) {
      /* end of statement detected */
      /* write xref record for finding info from pc on BASIC run-time error */
      /* given pc (=bcodes+offset) when run-time error occurs, we can look up:
           starting offset of this BASIC statement;
           starting offset of next BASIC statement;
           line number of error in BASIC source code;
           statement number within the BASIC line;
           last numeric label: erl   */

      xref_record.offset = b->b.offset;
      xref_record.line_number = line_number;
      xref_record.erl = *erl;
      xref_record.statement_number = ++statement_number;
      mwrite_check ((char *)&xref_record, sizeof(xref_record), b->xref);

      last_offset = b->b.offset;
    }

  } while (token != TOKEN_NONE); /* end loop for each statement in the line */
} /* compile_line */

/****************************************************************************/

static char *compile_conditional_jump (char *p, BOOL jump_if_notequal_zero,
                                       char *label_string, struct b *b)
{
  struct result_type result;
  static const T_SIZE_T zero = 0;

  p = compile_expression (p, b, &result);
  compile_check_numeric (result.type);
  compile_push_result (&result, &b->b);
  if (jump_if_notequal_zero)
    gen_bcode (type_info[result.type].b_jump_notequal_zero, &b->b);
  else
    gen_bcode (type_info[result.type].b_jump_equal_zero, &b->b);
  goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                  label_string);
  gen_size_t (zero, &b->b);
  b->b.history_count = 0;
  return p;
} /* compile_conditional_jump */

/****************************************************************************/

static int compile_make_label (char *name, struct b *b)
{
  struct token_info_type token_info;
  struct var_node *label_node;

  strcpy (token_info.token_string, name);
  token_info.token_type = TYPE_LABEL;
  label_node = lookup_var (&var_root[TYPE_LABEL], &token_info, TRUE, b);
  if (label_node->count++ != 0)
    compile_error ("Duplicate label, %s", token_info.token_string);
  b->b.history_count = 0;
  return atoi(name);
} /* compile_make_label */

/****************************************************************************/

static struct goto_node *add_goto_node (struct goto_node *goto_list_root,
                                 BOOL data, T_SIZE_T code_offset,
                                 char *label_string)
{
  struct goto_node *p;

  if ((p = (struct goto_node *)my_malloc (sizeof(struct goto_node))) == NULL)
    compile_error ("Out of memory for labels!");
  p->next = goto_list_root;
  p->data = data;
  p->offset = code_offset;
  if ((p->label_string = (char *)my_malloc (strlen(label_string) + 1)) == NULL)
    compile_error ("Out of memory for labels!");
  strcpy (p->label_string, label_string);
  return p;
} /* add_goto_node */

/****************************************************************************/

static void resolve_gotos (struct b *b, struct goto_node *goto_list_root)
{
  struct token_info_type token_info;
  struct var_node *label_node;
  struct goto_node *p;

  debug ((stderr, "\nResolving gotos:"));
  while (goto_list_root != NULL) {
    strcpy (token_info.token_string, goto_list_root->label_string);
    token_info.token_type = TYPE_LABEL;
    label_node = lookup_var (&var_root[TYPE_LABEL], &token_info, TRUE, b);
    if (label_node->count == 0)
      compile_error ("Unresolved reference to label '%s'",
                     token_info.token_string);
    debug ((stderr, "\n  %4zx| ", goto_list_root->offset));
    if (mseek (b->b.f, goto_list_root->offset, SEEK_SET) != 0)
      compile_error ("Error seeking in temporary file");
    b->b.offset = goto_list_root->offset;
    if (goto_list_root->data)
      gen_size_t (label_node->data_offset, &b->b);
    else
      gen_size_t (label_node->offset, &b->b);
    goto_list_root->label_string = my_free (goto_list_root->label_string);
    p = goto_list_root->next;
    goto_list_root = my_free ((char *)goto_list_root);
    goto_list_root = p;
  }
  debug ((stderr, "\n"));
} /* resolve_gotos */

/****************************************************************************/

static char *compile_def (char *p2, enum var_type_type var_type)
{
  enum token_type token;
  struct token_info_type token_info;
  char first, last, i, *p;

  do {
    p = get_token (p2, &token, &token_info);
    first = token_info.token_string[0];
    if (first < 'A' || first > 'Z' ||
        token_info.token_string[1] != '\0')
      compile_error ("Syntax error at %s in DEF statement",
                     token_info.token_string);
    p2 = get_token (p, &token, &token_info);
    if (token == TOKEN_COMMA || token == TOKEN_NONE || token == TOKEN_COLON ||
        token == TOKEN_QUOTE)
      last = first;
    else {
      if (token != TOKEN_MINUS)
        compile_error ("Syntax error at %s in DEF statement",
                       token_info.token_string);
      p = get_token (p2, &token, &token_info);
      last = token_info.token_string[0];
      if (token != TOKEN_ID ||
          last < first || last > 'Z' ||
          token_info.token_string[1] != '\0')
        compile_error ("Syntax error at %s in DEF statement",
                       token_info.token_string);
      p2 = get_token (p, &token, &token_info);
    }
    for (i = first; i <= last; i++)
      token_type_array[i - 'A'] = var_type;
  } while (token == TOKEN_COMMA);
  if (token != TOKEN_NONE && token != TOKEN_COLON && token != TOKEN_QUOTE)
    compile_error ("Syntax error at %s in DEF statement",
                   token_info.token_string);
  return p;
} /* compile_def */

/****************************************************************************/

static char *compile_deffn (char *p, struct b *b)
{
  enum token_type token;
  struct token_info_type token_info;
  struct var_node *var, *arg;
  struct result_type result;
  static int enddeffn_count = 0;
  char *deffn_string = NULL, *enddeffn_string = NULL;
  static const T_SIZE_T zero = 0;
  enum var_type_type type;

  if ((deffn_string = my_malloc (MAX_TOKEN_LENGTH+10)) == NULL ||
      (enddeffn_string = my_malloc (16)) == NULL)
    compile_error ("Out of memory allocating %d bytes", MAX_TOKEN_LENGTH+26);
  p = get_token (p, &token, &token_info);
  if (token != TOKEN_FUNCTION)
    compile_error ("Syntax error in DEF FN statement");
  token_info.dim = 0;
  var = lookup_var (&function_root[token_info.token_type], &token_info, TRUE, b);
  if (var->count++ != 0)
    compile_error ("Function %s is defined twice!", token_info.token_string);
  if ((var->arg = (struct arg *)my_malloc(MAX_ARGUMENTS * sizeof(struct arg))) == NULL)
    compile_error ("Out of memory for function arguments!");
  gen_bcode (B_JUMP, &b->b);
  sprintf (enddeffn_string, "_DEFFN_%d", enddeffn_count++);
  goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                  enddeffn_string);
  gen_size_t (zero, &b->b);
  b->b.history_count = 0;
  var->offset = b->b.offset;
  sprintf (deffn_string, "_%s_%d", var->var_name, var->var_type);
  compile_make_label (deffn_string, b);
  p = get_token (p, &token, NULL);
  if (token == TOKEN_LBRACKET) {
    do {
      p = check_token (p, TOKEN_ID, &token_info,
                       "Argument name expected in DEF FN at %s",
                       token_info.token_string);
      token_info.dim = 0;
      arg = lookup_var (&arg_root[token_info.token_type], &token_info, TRUE, b);
      if (var->nargs > MAX_ARGUMENTS)
        compile_error ("too many DEF FN arguments");
      var->arg[var->nargs].type = arg->var_type;
      var->arg[var->nargs].offset = arg->offset;
      var->nargs++;
      p = get_token (p, &token, NULL);
    } while (token == TOKEN_COMMA);
    if (token != TOKEN_RBRACKET)
      compile_error ("Right bracket expected in DEF FN statement");
    p = get_token (p, &token, NULL);
  }
  if (token != TOKEN_EQUAL)
    compile_error ("'=' expected at %s", token_info.token_string);
  p = compile_expression (p, b, &result);
  compile_convert_type (&result, var->var_type, b);
  compile_push_result (&result, &b->b);
  gen_bcode (B_RETURN, &b->b);
  compile_make_label (enddeffn_string, b);
  for (type = FIRST_TYPE; type <= LAST_TYPE; type++) {
    arg_root[type] = free_tree (arg_root[type]);
    arg_root[type] = NULL;
  }
  deffn_string = my_free (deffn_string);
  enddeffn_string = my_free (enddeffn_string);
  return p;
} /* compile_deffn */

/****************************************************************************/

static char *compile_let (enum token_type token,
                          struct token_info_type *token_info, char *line,
                          struct b *b)
{
  char *p;
  struct var_node *var;
  struct result_type index_result, rhs_result;

  if (token == TOKEN_MID) {
    p = compile_mid_assign (line, b);
  } else {
    p = compile_lhs_variable (token_info, &var, &index_result, line, b);
    p = check_token (p, TOKEN_EQUAL, NULL,
                     "'=' expected at %s", token_info->token_string);
    p = compile_expression (p, b, &rhs_result);
    compile_lhs_assign (var, &index_result, &rhs_result, b);
  }
  return p;
} /* compile_let */

/****************************************************************************/

static char *compile_lhs_variable (struct token_info_type *token_info,
                                   struct var_node **var,
                                   struct result_type *index_result,
                                   char *line, struct b *b)
{
  char *p, *p2;
  enum token_type token;

  compile_check_numeric_or_string (token_info->token_type);
  p2 = get_token (line, &token, NULL);
  if (token == TOKEN_LBRACKET) {
    p = compile_subscripts (p2, token_info, var, index_result, b);
  } else {
    p = line;
    token_info->dim = 0;
    *var = lookup_var (&var_root[token_info->token_type], token_info, TRUE, b);
  }
  return p;
} /* compile_lhs_variable */

/****************************************************************************/

static void compile_lhs_assign (struct var_node *var,
                                struct result_type *index_result,
                                struct result_type *rhs_result,
                                struct b *b)
{
  compile_convert_type (rhs_result, var->var_type, b);
  if (var->dim > 0) { /* lhs is an array element */
    if (!index_result->on_stack) { /* lhs array index is a constant */
      compile_assign_result (rhs_result,
                             var->offset + index_result->value.index, b);
    } else { /* lhs array index is on the stack */
      compile_push_result (rhs_result, &b->b);
      gen_bcode_size_t (type_info[var->var_type].b_pop_indirect,
                        type_info[var->var_type].b_pop_indirect_16, var->offset,
                        &b->b);
    }
  } else { /* lhs is a simple variable */
#ifdef DEBUG
    if (rhs_result->type != var->var_type)
      compile_error ("Internal error, type mismatch in compile_lhs_assign");
#endif
    compile_assign_result (rhs_result, var->offset, b);
  }
} /* compile_lhs_assign */

/****************************************************************************/
static void compile_assign_result (struct result_type *result, size_t offset,
                                   struct b *b)
{
  if (!result->on_stack) { /* result is a constant */
    if (value_is_equal (result->type, result->value, 0)) {
      gen_bcode_size_t (type_info[result->type].b_zero_var,
                        type_info[result->type].b_zero_var_16, offset, &b->b);
    } else if (value_is_equal (result->type, result->value, 1)) {
      gen_bcode_size_t (type_info[result->type].b_one_var,
                        type_info[result->type].b_one_var_16, offset, &b->b);
    } else { /* result is non-zero, non-one constant */
      compile_push_result (result, &b->b);
      gen_bcode_size_t (type_info[result->type].b_pop,
                        type_info[result->type].b_pop_16, offset, &b->b);
    }
  } else { /* result is on the stack */
    /* detect and optimise X=X+1 and X=X-1 */
    enum var_type_type type = result->type;
    int history_count = b->b.history_count;
    struct history *history = b->b.history;
#ifdef DEBUG
    /*
    if (history_count >= 2)
      printf ("History: %d: %ld %s %lx %s\n", history_count,
              history[history_count - 2].offset,
              bcode_table[history[history_count - 2].bcode],
              history[history_count - 2].value.index,
              bcode_table[history[history_count - 1].bcode]);
    */
#endif
    /* LET X=X+1 would normally compile to:
         B_PUSH X; B_INCREMENT; B_POP X;
       We detect this sequence in the history when about to generate B_POP X,
       Delete already generated B_PUSH X and B_INCREMENT
       and generate B_INCREMENT_VAR X instead
    */
    if (b->b.is_keeping_history &&
        history_count >= 2 && history_count < MAX_HISTORY &&
        (history[history_count - 1].bcode == type_info[type].b_increment ||
         history[history_count - 1].bcode == type_info[type].b_decrement) &&
        ((history[history_count - 2].bcode == type_info[type].b_push_var &&
         history[history_count - 2].value.index == offset) ||
         (history[history_count - 2].bcode == type_info[type].b_push_var_16 &&
          history[history_count - 2].value.sh == offset))) {
      debug ((stderr, "\n      |      Deleting the last two BCODEs"));
      debug ((stderr, "\n      |      Seeking to offset: %zx",
              history[history_count - 2].offset));
      b->b.offset = history[history_count - 2].offset;
      if (mseek (b->b.f, b->b.offset, SEEK_SET))
        compile_error ("Error seeking in BCODE file");
      b->b.history_count -= 2;
      if (history[history_count - 1].bcode == type_info[type].b_increment) {
        gen_bcode_size_t (type_info[type].b_increment_var,
                          type_info[type].b_increment_var_16, offset, &b->b);
      } else { /* decrement */
        gen_bcode_size_t (type_info[type].b_decrement_var,
                          type_info[type].b_decrement_var_16, offset, &b->b);
      }
      return;
    } /* end of detect and optimise X=X+1 and X=X-1 */
    gen_bcode_size_t (type_info[type].b_pop, type_info[type].b_pop_16,
                      offset, &b->b);
  }
} /* compile_assign_result */

/****************************************************************************/

static char *compile_mid_assign (char *line, struct b *b)
{
  char *p, *p2;
  struct var_node *var;
  struct result_type result, index_result, result3;
  enum token_type token;
  struct token_info_type token_info;

  p = check_token (line, TOKEN_LBRACKET, &token_info,
                   "'(' expected at %s in LET MID$()= statement",
                   token_info.token_string);
  p = check_token (p, TOKEN_ID, &token_info,
                   "Identifier expected at %s in LET MID$()= statement",
                   token_info.token_string);
  compile_check_string (token_info.token_type);
  p = compile_lhs_variable (&token_info, &var, &index_result, p, b);
  p = check_token (p, TOKEN_COMMA, NULL,
                   "',' expected in LET MID$()= statement");
  p = compile_expression (p, b, &result3);
  compile_convert_type (&result3, TYPE_LONG, b);
  compile_push_result (&result3, &b->b);
  p2 = get_token (p, &token, NULL);
  if (token == TOKEN_RBRACKET) {
    p = check_token (p, TOKEN_EQUAL, NULL,
                     "'=' expected in LET MID$()= statement");
    p = compile_expression (p, b, &result);
    compile_convert_type (&result, TYPE_STRING, b);
    compile_push_result (&result, &b->b);
    if (var->dim > 0) {
      if (!index_result.on_stack) {
        gen_bcode_size_t (B_POP_STRING_MID2, B_POP_STRING_MID2_16,
                          var->offset + index_result.value.index, &b->b);
      } else {
        gen_bcode_size_t (B_POP_INDIRECT_STRING_MID2,
                          B_POP_INDIRECT_STRING_MID2_16, var->offset, &b->b);
      }
    } else {
      gen_bcode_size_t (B_POP_STRING_MID2, B_POP_STRING_MID2_16, var->offset,
                        &b->b);
    }
  } else {
    p = check_token (p, TOKEN_COMMA, NULL,
                     "',' or ')' expected in LET MID$()= statement");
    p = compile_expression (p2, b, &result3);
    compile_convert_type (&result3, TYPE_LONG, b);
    compile_push_result (&result3, &b->b);
    p = check_token (p, TOKEN_RBRACKET, NULL,
                     "')' expected in LET MID$()= statement");
    p = check_token (p, TOKEN_EQUAL, NULL,
                     "'=' expected in LET MID$()= statement");
    p = compile_expression (p, b, &result);
    compile_convert_type (&result, TYPE_STRING, b);
    compile_push_result (&result, &b->b);
    if (var->dim > 0) {
      if (!index_result.on_stack) {
        gen_bcode_size_t (B_POP_STRING_MID3, B_POP_STRING_MID3_16,
                          var->offset + index_result.value.index, &b->b);
      } else {
        gen_bcode_size_t (B_POP_INDIRECT_STRING_MID3,
                          B_POP_INDIRECT_STRING_MID3_16, var->offset, &b->b);
      }
    } else {
      gen_bcode_size_t (B_POP_STRING_MID3, B_POP_STRING_MID3_16, var->offset,
                        &b->b);
    }
  }
  return p;
} /* compile_mid_assign */

/****************************************************************************/

static char *compile_swap (char *line, struct b *b)
{
  char *p, *p2;
  struct var_node *var1, *var2;
  struct token_info_type token_info;
  struct result_type result1, result2;
  enum token_type token;
  enum var_type_type type;

  p = check_token (line, TOKEN_ID, &token_info,
                   "Variable expected in SWAP at %s",
                   token_info.token_string);
  p2 = get_token (p, &token, NULL);
  if (token == TOKEN_LBRACKET)
    p = compile_subscripts (p2, &token_info, &var1, &result1, b);
  else {
    token_info.dim = 0;
    var1 = lookup_var (&var_root[token_info.token_type], &token_info, TRUE, b);
  }
  p = check_token (p, TOKEN_COMMA, &token_info,
                   "comma expected at %s", token_info.token_string);
  p = check_token (p, TOKEN_ID, &token_info,
                   "Variable expected in SWAP at %s",
                   token_info.token_string);
  p2 = get_token (p, &token, NULL);
  if (token == TOKEN_LBRACKET)
    p = compile_subscripts (p2, &token_info, &var2, &result2, b);
  else {
    token_info.dim = 0;
    var2 = lookup_var (&var_root[token_info.token_type], &token_info, TRUE, b);
  }
  type = var1->var_type;
  if (var2->var_type != type)
    compile_error ("Type mismatch in SWAP!");

  if (var1->dim != 0 && result1.on_stack &&
      var2->dim != 0 && result2.on_stack)
    gen_bcode (B_INDEX_DUP2, &b->b);
  else if (var1->dim != 0 && result1.on_stack)
    gen_bcode (B_INDEX_DUP, &b->b);
  else if (var2->dim != 0 && result2.on_stack)
    gen_bcode (B_INDEX_DUP, &b->b);

  if (var1->dim == 0)
    gen_bcode_size_t (type_info[type].b_push_var, type_info[type].b_push_var_16,
                      var1->offset, &b->b);
  else if (!result1.on_stack)
    gen_bcode_size_t (type_info[type].b_push_var, type_info[type].b_push_var_16,
                      var1->offset + result1.value.index, &b->b);
  else
    gen_bcode_size_t (type_info[type].b_push_indirect_var,
                      type_info[type].b_push_indirect_var_16, var1->offset,
                      &b->b);
  if (var2->dim == 0)
    gen_bcode_size_t (type_info[type].b_push_var, type_info[type].b_push_var_16,
                      var2->offset, &b->b);
  else if (!result2.on_stack)
    gen_bcode_size_t (type_info[type].b_push_var, type_info[type].b_push_var_16,
                      var2->offset + result2.value.index, &b->b);
  else
    gen_bcode_size_t (type_info[type].b_push_indirect_var,
                      type_info[type].b_push_indirect_var_16, var2->offset,
                      &b->b);
  if (var1->dim == 0)
    gen_bcode_size_t (type_info[type].b_pop, type_info[type].b_pop_16,
                      var1->offset, &b->b);
  else if (!result1.on_stack)
    gen_bcode_size_t (type_info[type].b_pop, type_info[type].b_pop_16,
                      var1->offset + result1.value.index, &b->b);
  else
    gen_bcode_size_t (type_info[type].b_pop_indirect,
                      type_info[type].b_pop_indirect_16, var1->offset, &b->b);
  if (var2->dim == 0)
    gen_bcode_size_t (type_info[type].b_pop, type_info[type].b_pop_16,
                      var2->offset, &b->b);
  else if (!result2.on_stack)
    gen_bcode_size_t (type_info[type].b_pop, type_info[type].b_pop_16,
                      var2->offset + result2.value.index, &b->b);
  else
    gen_bcode_size_t (type_info[type].b_pop_indirect,
                      type_info[type].b_pop_indirect_16, var2->offset, &b->b);

  return p;
} /* compile_swap */

/****************************************************************************/

static char *compile_read (char *p, struct b *b)
{
  char *p2;
  enum token_type token;
  struct token_info_type token_info;
  struct var_node *var;
  struct result_type index_result;

  for (;;) {
    p = check_token (p, TOKEN_ID, &token_info,
                     "Variable expected in READ statement");
    p = compile_lhs_variable (&token_info, &var, &index_result, p, b);
    gen_bcode (type_info[var->var_type].b_read, &b->b);
    if (var->dim > 0) {
      if (!index_result.on_stack) {
        gen_bcode_size_t (type_info[var->var_type].b_pop,
                          type_info[var->var_type].b_pop_16,
                          var->offset + index_result.value.index, &b->b);
      } else {
        gen_bcode_size_t (type_info[var->var_type].b_pop_indirect,
                          type_info[var->var_type].b_pop_indirect_16,
                          var->offset, &b->b);
      }
    } else {
      gen_bcode_size_t (type_info[var->var_type].b_pop,
                        type_info[var->var_type].b_pop_16, var->offset, &b->b);
    }
    p2 = get_token (p, &token, NULL);
    if (token != TOKEN_COMMA)
      break;
    p = p2;
  }
  return p;
} /* compile_read */

/****************************************************************************/

static char *compile_data (char *p, struct b *b)
{
  enum token_type token;
  struct result_type result;
  char *p2;

  do {
    while (*p == ' ' || *p == '\t')
      p++;
    get_token (p, &token, NULL);
    if (*p == '-' || token == TOKEN_NUMBER || token == TOKEN_STRING) {
      p = compile_expression (p, b, &result);
      compile_check_numeric_or_string (result.type);
      if (result.on_stack)
        compile_error ("can't have variable expression in DATA statement");
    } else {
      if (*p == '\0' || *p == '\n' || *p == '\r') {
        token = TOKEN_NONE;
        return p;
      }
      token = TOKEN_STRING;
      p2 = p;
      while (*p != ',' && *p != ':' && *p != '\0' && *p != '\n' && *p != '\r')
        p++;
      while (*(p-1) == ' ' || *(p-1) == '\t')
        --p;
      result.type = TYPE_STRING;
      result.on_stack = FALSE;
      if ((result.value.str = alloc_string (p - p2, p2)) == NULL)
        compile_error ("Out of memory for string!");
    }
    compile_push_result (&result, &b->data);
    p2 = get_token (p, &token, NULL);
    if (token == TOKEN_COMMA)
      p = p2;
  } while (token == TOKEN_COMMA);
  return p;
} /* compile_data */

/****************************************************************************/

static char *compile_restore (char *p, struct b *b)
{
  enum token_type token;
  char *p2;
  struct token_info_type token_info;
  static const T_SIZE_T zero = 0;

  p2 = get_token (p, &token, &token_info);
  if (token == TOKEN_ID || token == TOKEN_NUMBER) {
    p = p2;
    gen_bcode (B_RESTORE_LINE, &b->b);
    goto_list_root = add_goto_node (goto_list_root, TRUE, b->b.offset,
                                    token_info.token_string);
    gen_size_t (zero, &b->b);
  } else
    gen_bcode (B_RESTORE, &b->b);
  return p;
} /* compile_restore */

/****************************************************************************/

static char *compile_open (char *line, struct b *b)
{
  enum token_type token;
  char *p;
  struct token_info_type token_info;
  struct result_type result;
  struct var_node *var;

  /* debug ((stderr, "      compile_open %s", line)); */
  p = compile_expression (line, b, &result);
  compile_check_string (result.type);
  compile_push_result (&result, &b->b);
  p = check_token (p, TOKEN_FOR, &token_info,
                   "FOR expected at %s in OPEN statement",
                   token_info.token_string);
  p = get_token (p, &token, &token_info);
  gen_bcode (B_PUSH_CONST_INT, &b->b);
  if (token == TOKEN_INPUT)
    gen_int ((T_INT)FILE_OPEN_INPUT, &b->b);
  else if (token == TOKEN_OUTPUT)
    gen_int ((T_INT)FILE_OPEN_OUTPUT, &b->b);
  else if (token == TOKEN_APPEND)
    gen_int ((T_INT)FILE_OPEN_APPEND, &b->b);
  else
    compile_error ("INPUT, OUTPUT or APPEND expected at %s in OPEN statement",
                   token_info.token_string);
  p = check_token (p, TOKEN_AS, &token_info,
                   "AS expected at %s in OPEN statement",
                   token_info.token_string);
  p = compile_file_number (p, b, &token_info, "OPEN", &var);
  gen_bcode_size_t (B_SET_PRIMARY_FILE, B_SET_PRIMARY_FILE_16, var->offset,
                    &b->b);
  gen_bcode (B_OPEN, &b->b);
  return p;
} /* compile_open */

/****************************************************************************/

static char *compile_close (char *line, struct b *b)
{
  char *p;
  struct token_info_type token_info;
  struct var_node *var;

  /* debug ((stderr, "      compile_close %s", line)); */
  p = compile_file_number (line, b, &token_info, "CLOSE", &var);
  gen_bcode_size_t (B_SET_PRIMARY_FILE, B_SET_PRIMARY_FILE_16, var->offset,
                    &b->b);
  gen_bcode (B_CLOSE, &b->b);
  return p;
} /* compile_close */

/****************************************************************************/

static char *compile_print (char *line, struct b *b)
{
  enum token_type token;
  char *p, *p2;
  struct token_info_type token_info;
  int trailing_semicolon, using;
  struct result_type result;
  struct var_node *var;

  /* debug ((stderr, "      compile_print %s", line)); */
  trailing_semicolon = FALSE;
  using = FALSE;
  p = line;
  p2 = get_token (p, &token, &token_info);
  if (token == TOKEN_FILE) {
    var = lookup_var (&var_root[TYPE_FILE], &token_info, TRUE, b);
    gen_bcode_size_t (B_SET_PRIMARY_FILE, B_SET_PRIMARY_FILE_16, var->offset,
                      &b->b);
    p = get_token (p2, &token, NULL);
    if (token == TOKEN_COLON || token == TOKEN_QUOTE ||
        token == TOKEN_NONE || token == TOKEN_ELSE) {
      gen_bcode (B_PRINT_NEWLINE, &b->b);
      return p2;
    }
    if (token == TOKEN_SEMICOLON) {
      p = p2;
      trailing_semicolon = TRUE;
    } else if (token != TOKEN_COMMA) {
      compile_error ("Comma or semicolon expected");
    }
    p2 = get_token (p, &token, &token_info);
  } else
    gen_bcode (B_SET_PRIMARY_OUTPUT_FILE, &b->b);
  if (token == TOKEN_USING) {
    using = TRUE;
    p = compile_expression (p2, b, &result);
    compile_check_string (result.type);
    compile_push_result (&result, &b->b);
    gen_bcode (B_PRINT_USING_INIT, &b->b);
    p = get_token (p, &token, NULL);
    if (token != TOKEN_SEMICOLON && token != TOKEN_COMMA) {
      compile_error ("Semicolon missing after PRINT USING format");
    }
    p2 = get_token (p, &token, &token_info);
  }
  for (;;) {
    if (token == TOKEN_COLON || token == TOKEN_QUOTE ||
        token == TOKEN_NONE || token == TOKEN_ELSE)
      break;
    trailing_semicolon = (token == TOKEN_SEMICOLON || token == TOKEN_COMMA);
    if (token == TOKEN_SEMICOLON)
      p = p2;
    else if (token == TOKEN_COMMA) {
      p = p2;
      if (!using)
        gen_bcode (B_PRINT_COMMA, &b->b);
    } else if (token == TOKEN_TAB) {
      p = compile_single_argument (p2, b, &result);
      compile_convert_type (&result, TYPE_LONG, b);
      compile_push_result (&result, &b->b);
      gen_bcode (B_PRINT_TAB, &b->b);
    } else if (token == TOKEN_SPC) {
      p = compile_single_argument (p2, b, &result);
      compile_convert_type (&result, TYPE_LONG, b);
      compile_push_result (&result, &b->b);
      gen_bcode (B_PRINT_SPC, &b->b);
    } else {
      if (token == TOKEN_STRING)
        token_info.value.str = free_string (token_info.value.str);
      p = compile_expression (p, b, &result);
      compile_check_numeric_or_string (result.type);
      compile_push_result (&result, &b->b);
      if (using)
        gen_bcode (type_info[result.type].b_print_using, &b->b);
      else
        gen_bcode (type_info[result.type].b_print, &b->b);
    }
    p2 = get_token (p, &token, &token_info);
  }
  if (using)
    gen_bcode (B_PRINT_USING_END, &b->b);
  if (!trailing_semicolon)
    gen_bcode (B_PRINT_NEWLINE, &b->b);
  gen_bcode (B_PRINT_END, &b->b);
  return p;
} /* compile_print */

/****************************************************************************/

static char *compile_locate (char *p, struct b *b)
{
  struct result_type result;
  struct token_info_type token_info;

  gen_bcode (B_SET_PRIMARY_OUTPUT_FILE, &b->b);
  p = compile_expression (p, b, &result);
  compile_convert_type (&result, TYPE_LONG, b);
  compile_push_result (&result, &b->b);
  p = check_token (p, TOKEN_COMMA, &token_info,
                   "comma expected at %s", token_info.token_string);
  p = compile_expression (p, b, &result);
  compile_convert_type (&result, TYPE_LONG, b);
  compile_push_result (&result, &b->b);
  gen_bcode (B_LOCATE, &b->b);
  return p;
} /* compile_locate */

/****************************************************************************/

static char *compile_color (char *p, struct b *b)
{
  enum token_type token;
  struct result_type result;
  char *p2;

  p2 = get_token (p, &token, NULL);
  if (token != TOKEN_COMMA) {
    gen_bcode (B_SET_PRIMARY_OUTPUT_FILE, &b->b);
    p = compile_expression (p, b, &result);
    compile_convert_type (&result, TYPE_INT, b);
    compile_push_result (&result, &b->b);
    gen_bcode (B_COLOR, &b->b);
  }
  p2 = get_token (p, &token, NULL);
  if (token == TOKEN_COMMA) {
    gen_bcode (B_SET_PRIMARY_OUTPUT_FILE, &b->b);
    p = compile_expression (p2, b, &result);
    compile_convert_type (&result, TYPE_INT, b);
    compile_push_result (&result, &b->b);
    gen_bcode (B_BACKGROUND_COLOR, &b->b);
  }
  return p;
} /* compile_color */

/****************************************************************************/

static char *compile_lineinput (char *line, struct b *b)
{
  char *p;
  struct token_info_type token_info;
  struct var_node *var;
  struct result_type index_result;

  p = compile_prompt_or_file (line, TRUE, b);
  gen_bcode (B_LINEINPUT, &b->b);
  p = check_token (p, TOKEN_ID, &token_info,
                   "String variable expected in LINEINPUT statement");
  compile_check_string (token_info.token_type);
  p = compile_lhs_variable (&token_info, &var, &index_result, p, b);
  if (var->dim > 0) {
    if (!index_result.on_stack) {
      gen_bcode_size_t (B_POP_STRING, B_POP_STRING_16,
                        var->offset + index_result.value.index,
                        &b->b);
    } else {
      gen_bcode_size_t (B_POP_INDIRECT_STRING, B_POP_INDIRECT_STRING_16,
                        var->offset, &b->b);
    }
  } else {
    gen_bcode_size_t (B_POP_STRING, B_POP_STRING_16, var->offset, &b->b);
  }
  return p;
} /* compile_lineinput */

/****************************************************************************/

static char *compile_input (char *line, struct b *b)
{
  enum token_type token;
  char *p, *p2;
  struct token_info_type token_info;
  struct result_type index_result;
  struct var_node *var;

  /* debug ((stderr, "      compile_input %s", line)); */
  gen_bcode (B_INPUT_RESTART, &b->b);
  p = compile_prompt_or_file (line, FALSE, b);
  gen_bcode (B_INPUT_START, &b->b);
  for (;;) {
    p = check_token (p, TOKEN_ID, &token_info,
                     "Variable expected in INPUT statement");
    p = compile_lhs_variable (&token_info, &var, &index_result, p, b);
    gen_bcode (type_info[var->var_type].b_input, &b->b);
    if (var->dim > 0) {
      if (!index_result.on_stack) {
        gen_bcode_size_t (type_info[var->var_type].b_pop,
                          type_info[var->var_type].b_pop_16,
                          var->offset + index_result.value.index, &b->b);
      } else {
        gen_bcode_size_t (type_info[var->var_type].b_pop_indirect,
                          type_info[var->var_type].b_pop_indirect_16,
                          var->offset, &b->b);
      }
    } else {
      gen_bcode_size_t (type_info[var->var_type].b_pop,
                        type_info[var->var_type].b_pop_16, var->offset, &b->b);
    }
    p2 = get_token (p, &token, NULL);
    if (token != TOKEN_COMMA)
      break;
    p = p2;
  }
  return p;
} /* compile_input */

/****************************************************************************/

static char *compile_prompt_or_file (char *line, BOOL is_lineinput,
                                     struct b *b)
{
  enum token_type token;
  struct token_info_type token_info;
  char *p, *p2;
  struct var_node *var;

  p = line;
  p2 = get_token (p, &token, &token_info);
  if (token == TOKEN_STRING) {
    gen_bcode (B_PUSH_CONST_STRING, &b->b);
    gen_string (token_info.value.str, &b->b);
    gen_bcode (B_INPUT_SET_PROMPT_STRING, &b->b);
    gen_bcode (B_SET_PRIMARY_INPUT_FILE, &b->b);
    p = get_token (p2, &token, NULL);
    if (token != TOKEN_SEMICOLON && token != TOKEN_COMMA)
      compile_error ("Comma or semicolon expected");
    if (!is_lineinput && token == TOKEN_SEMICOLON)
      gen_bcode (B_INPUT_EXTEND_PROMPT_STRING, &b->b);
  } else {
    gen_bcode (B_INPUT_CLEAR_PROMPT_STRING, &b->b);
    if (token == TOKEN_FILE) {
      var = lookup_var (&var_root[TYPE_FILE], &token_info, TRUE, b);
      gen_bcode_size_t (B_SET_PRIMARY_FILE, B_SET_PRIMARY_FILE_16, var->offset,
                        &b->b);
      p = get_token (p2, &token, NULL);
      if (token != TOKEN_SEMICOLON && token != TOKEN_COMMA) {
        compile_error ("Comma or semicolon expected");
      }
    } else {
      gen_bcode (B_SET_PRIMARY_INPUT_FILE, &b->b);
      if (!is_lineinput) {
        gen_bcode (B_INPUT_EXTEND_PROMPT_STRING, &b->b);
      }
    }
  }
  return p;
} /* compile_prompt_or_file */

/****************************************************************************/
/****************************************************************************/
/*   E X P R E S S I O N   C A L C U L A T O R   /   C O M P I L E R   */

static char *compile_expression (char *line, struct b *b,
                                 struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;

  /* debug ((stderr, "      compile_expression %s", line)); */
  p = compile_or_operand (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_OR || token == TOKEN_XOR) {
    compile_convert_to_int_or_long (result1, b);
    p = compile_or_operand (p2, b, &result2);
    compile_convert_to_int_or_long (&result2, b);
    compile_match_type (result1, &result2, TRUE, b);
    if (token == TOKEN_OR)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i |= result2.value.i;
            break;
          case TYPE_LONG:
            result1->value.l |= result2.value.l;
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, TRUE,
                                  type_info[result2.type].b_or,
                                  type_info[result2.type].b_or_const,
                                  type_info[result2.type].b_or_const_16,
                                  type_info[result2.type].b_or,
                                  type_info[result2.type].b_or_const,
                                  type_info[result2.type].b_or_const_16, b);
    else if (token == TOKEN_XOR)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i ^= result2.value.i;
            break;
          case TYPE_LONG:
            result1->value.l ^= result2.value.l;
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, TRUE,
                                  type_info[result2.type].b_xor,
                                  type_info[result2.type].b_xor_const,
                                  type_info[result2.type].b_xor_const_16,
                                  type_info[result2.type].b_xor,
                                  type_info[result2.type].b_xor_const,
                                  type_info[result2.type].b_xor_const_16, b);
    else
      compile_error ("Internal error in compile_expression");
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_expression */

/****************************************************************************/

static char *compile_or_operand (char *line, struct b *b,
                                 struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;

  /* debug ((stderr, "      compile_or_operand %s", line)); */
  p = compile_and_operand (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_AND) {
    compile_convert_to_int_or_long (result1, b);
    p = compile_and_operand (p2, b, &result2);
    compile_convert_to_int_or_long (&result2, b);
    compile_match_type (result1, &result2, TRUE, b);
    if (!result1->on_stack && !result2.on_stack)
      switch (result2.type) {
        case TYPE_INT:
          result1->value.i &= result2.value.i;
          break;
        case TYPE_LONG:
          result1->value.l &= result2.value.l;
          break;
        default:
          break;
      }
    else
      compile_push_and_operate (result1, &result2, TRUE,
                                type_info[result2.type].b_and,
                                type_info[result2.type].b_and_const,
                                type_info[result2.type].b_and_const_16,
                                type_info[result2.type].b_and,
                                type_info[result2.type].b_and_const,
                                type_info[result2.type].b_and_const_16, b);
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_or_operand */

/****************************************************************************/

static char *compile_and_operand (char *line, struct b *b,
                                  struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;

  /* debug ((stderr, "      compile_and_operand %s", line)); */
  p2 = get_token (line, &token, NULL);
  if (token == TOKEN_NOT) {
    p = compile_and_operand (p2, b, result1);
    compile_convert_to_int_or_long (result1, b);
    if (!result1->on_stack)
      switch (result1->type) {
        case TYPE_INT:
          result1->value.i = ~result1->value.i;
          break;
        case TYPE_LONG:
          result1->value.l = ~result1->value.l;
          break;
        default:
          break;
      }
    else
      gen_bcode (type_info[result1->type].b_not, &b->b);
  } else
    p = compile_not_operand (line, b, result1);
  return p;
} /* compile_and_operand */

/****************************************************************************/

static char *compile_not_operand (char *line, struct b *b,
                                  struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;
  T_SIZE_T length;
  int i, j;

  /* debug ((stderr, "      compile_not_operand %s", line)); */
  p = compile_comparand (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_EQUAL || token == TOKEN_NOTEQUAL ||
         token == TOKEN_LESSTHAN || token == TOKEN_GREATERTHAN ||
         token == TOKEN_LESSTHANOREQUAL || token == TOKEN_GREATERTHANOREQUAL) {
    compile_check_numeric_or_string (result1->type);
    p = compile_comparand (p2, b, &result2);
    compile_match_type (result1, &result2,
                        (token == TOKEN_EQUAL || token == TOKEN_NOTEQUAL), b);
    if (token == TOKEN_EQUAL)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i = -(result1->value.i == result2.value.i);
            break;
          case TYPE_LONG:
            result1->value.i = -(result1->value.l == result2.value.l);
            break;
          case TYPE_SINGLE:
            result1->value.i = -(result1->value.s == result2.value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.i = -(result1->value.d == result2.value.d);
            break;
          case TYPE_STRING:
            if (result1->value.str->length == result2.value.str->length &&
                memcmp (result1->value.str->body, result2.value.str->body,
                        result1->value.str->length) == 0) {
              result1->value.str = free_string (result1->value.str);
              result1->value.i = -1;
            } else {
              result1->value.str = free_string (result1->value.str);
              result1->value.i = 0;
            }
            result2.value.str = free_string (result2.value.str);
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, TRUE,
                                  type_info[result2.type].b_equal,
                                  type_info[result2.type].b_equal_const,
                                  type_info[result2.type].b_equal_const_16,
                                  type_info[result2.type].b_equal,
                                  type_info[result2.type].b_equal_const,
                                  type_info[result2.type].b_equal_const_16, b);
    else if (token == TOKEN_NOTEQUAL)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i = -(result1->value.i != result2.value.i);
            break;
          case TYPE_LONG:
            result1->value.i = -(result1->value.l != result2.value.l);
            break;
          case TYPE_SINGLE:
            result1->value.i = -(result1->value.s != result2.value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.i = -(result1->value.d != result2.value.d);
            break;
          case TYPE_STRING:
            if (result1->value.str->length == result2.value.str->length &&
                memcmp (result1->value.str->body, result2.value.str->body,
                        result1->value.str->length) == 0) {
              result1->value.str = free_string (result1->value.str);
              result1->value.i = 0;
            } else {
              result1->value.str = free_string (result1->value.str);
              result1->value.i = -1;
            }
            result2.value.str = free_string (result2.value.str);
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, TRUE,
                                  type_info[result2.type].b_notequal,
                                  type_info[result2.type].b_notequal_const,
                                  type_info[result2.type].b_notequal_const_16,
                                  type_info[result2.type].b_notequal,
                                  type_info[result2.type].b_notequal_const,
                                  type_info[result2.type].b_notequal_const_16, b);
    else if (token == TOKEN_LESSTHAN)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i = -(result1->value.i < result2.value.i);
            break;
          case TYPE_LONG:
            result1->value.i = -(result1->value.l < result2.value.l);
            break;
          case TYPE_SINGLE:
            result1->value.i = -(result1->value.s < result2.value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.i = -(result1->value.d < result2.value.d);
            break;
          case TYPE_STRING:
            if ((j = (result1->value.str->length < result2.value.str->length)))
              length = result1->value.str->length;
            else
              length = result2.value.str->length;
            if ((i = memcmp (result1->value.str->body, result2.value.str->body,
                             length)) == 0) {
              result1->value.str = free_string (result1->value.str);
              if (j)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            } else {
              result1->value.str = free_string (result1->value.str);
              if (i < 0)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            }
            result2.value.str = free_string (result2.value.str);
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, FALSE,
                                  type_info[result2.type].b_less,
                                  type_info[result2.type].b_less_const,
                                  type_info[result2.type].b_less_const_16,
                                  type_info[result2.type].b_greaterorequal,
                                  type_info[result2.type].b_greaterorequal_const,
                                  type_info[result2.type].b_greaterorequal_const_16,
                                  b);
    else if (token == TOKEN_GREATERTHAN)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i = -(result1->value.i > result2.value.i);
            break;
          case TYPE_LONG:
            result1->value.i = -(result1->value.l > result2.value.l);
            break;
          case TYPE_SINGLE:
            result1->value.i = -(result1->value.s > result2.value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.i = -(result1->value.d > result2.value.d);
            break;
          case TYPE_STRING:
            if ((j = (result1->value.str->length <= result2.value.str->length)))
              length = result1->value.str->length;
            else
              length = result2.value.str->length;
            if ((i = memcmp (result1->value.str->body, result2.value.str->body,
                             length)) == 0) {
              result1->value.str = free_string (result1->value.str);
              if (!j)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            } else {
              result1->value.str = free_string (result1->value.str);
              if (i > 0)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            }
            result2.value.str = free_string (result2.value.str);
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, FALSE,
                                  type_info[result2.type].b_greater,
                                  type_info[result2.type].b_greater_const,
                                  type_info[result2.type].b_greater_const_16,
                                  type_info[result2.type].b_lessorequal,
                                  type_info[result2.type].b_lessorequal_const,
                                  type_info[result2.type].b_lessorequal_const_16,
                                  b);
    else if (token == TOKEN_LESSTHANOREQUAL)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i = -(result1->value.i <= result2.value.i);
            break;
          case TYPE_LONG:
            result1->value.i = -(result1->value.l <= result2.value.l);
            break;
          case TYPE_SINGLE:
            result1->value.i = -(result1->value.s <= result2.value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.i = -(result1->value.d <= result2.value.d);
            break;
          case TYPE_STRING:
            if ((j = (result1->value.str->length <= result2.value.str->length)))
              length = result1->value.str->length;
            else
              length = result2.value.str->length;
            if ((i = memcmp (result1->value.str->body, result2.value.str->body,
                             length)) == 0) {
              result1->value.str = free_string (result1->value.str);
              if (j)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            } else {
              result1->value.str = free_string (result1->value.str);
              if (i < 0)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            }
            result2.value.str = free_string (result2.value.str);
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, FALSE,
                                  type_info[result2.type].b_lessorequal,
                                  type_info[result2.type].b_lessorequal_const,
                                  type_info[result2.type].b_lessorequal_const_16,
                                  type_info[result2.type].b_greater,
                                  type_info[result2.type].b_greater_const,
                                  type_info[result2.type].b_greater_const_16, b);
    else if (token == TOKEN_GREATERTHANOREQUAL)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i = -(result1->value.i >= result2.value.i);
            break;
          case TYPE_LONG:
            result1->value.i = -(result1->value.l >= result2.value.l);
            break;
          case TYPE_SINGLE:
            result1->value.i = -(result1->value.s >= result2.value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.i = -(result1->value.d >= result2.value.d);
            break;
          case TYPE_STRING:
            if ((j = (result1->value.str->length < result2.value.str->length)))
              length = result1->value.str->length;
            else
              length = result2.value.str->length;
            if ((i = memcmp (result1->value.str->body, result2.value.str->body,
                             length)) == 0) {
              result1->value.str = free_string (result1->value.str);
              if (!j)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            } else {
              result1->value.str = free_string (result1->value.str);
              if (i > 0)
                result1->value.i = -1;
              else
                result1->value.i = 0;
            }
            result2.value.str = free_string (result2.value.str);
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, FALSE,
                                  type_info[result2.type].b_greaterorequal,
                                  type_info[result2.type].b_greaterorequal_const,
                                  type_info[result2.type].b_greaterorequal_const_16,
                                  type_info[result2.type].b_less,
                                  type_info[result2.type].b_less_const,
                                  type_info[result2.type].b_less_const_16, b);
    else
      compile_error ("Internal error in compile_not_operand");
    result1->type = TYPE_INT;
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_not_operand */

/****************************************************************************/

static char *compile_comparand (char *line, struct b *b,
                                struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;
  T_STRING str;

  /* debug ((stderr, "      compile_comparand %s", line)); */
  p = compile_term (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_PLUS || token == TOKEN_MINUS) {
    if (token == TOKEN_PLUS)
      compile_check_numeric_or_string (result1->type);
    else
      compile_check_numeric (result1->type);
    p = compile_term (p2, b, &result2);
    compile_match_type (result1, &result2,
                        (token == TOKEN_PLUS) && (result1->type != TYPE_STRING),
                        b);
    if (token == TOKEN_MINUS)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i -= result2.value.i;
            break;
          case TYPE_LONG:
            result1->value.l -= result2.value.l;
            break;
          case TYPE_SINGLE:
            result1->value.s -= result2.value.s;
            break;
          case TYPE_DOUBLE:
            result1->value.d -= result2.value.d;
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, FALSE,
                                  type_info[result2.type].b_minus,
                                  type_info[result2.type].b_minus_const,
                                  type_info[result2.type].b_minus_const_16,
                                  type_info[result2.type].b_swap_minus,
                                  type_info[result2.type].b_swap_minus_const,
                                  type_info[result2.type].b_swap_minus_const_16, b);
    else if (token == TOKEN_PLUS)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i += result2.value.i;
            break;
          case TYPE_LONG:
            result1->value.l += result2.value.l;
            break;
          case TYPE_SINGLE:
            result1->value.s += result2.value.s;
            break;
          case TYPE_DOUBLE:
            result1->value.d += result2.value.d;
            break;
          case TYPE_STRING:
            if ((str = alloc_string (result1->value.str->length +
                                     result2.value.str->length, NULL)) == NULL)
              compile_error ("Out of memory for string!");
            memcpy (str->body, result1->value.str->body,
                    result1->value.str->length);
            memcpy (str->body + result1->value.str->length,
                    result2.value.str->body, result2.value.str->length);
            result1->value.str = free_string (result1->value.str);
            result2.value.str = free_string (result2.value.str);
            result1->value.str = str;
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2,
                                  (result1->type != TYPE_STRING),
                                  type_info[result2.type].b_plus,
                                  type_info[result2.type].b_plus_const,
                                  type_info[result2.type].b_plus_const_16,
                                  type_info[result2.type].b_plus,
                                  type_info[result2.type].b_plus_const,
                                  type_info[result2.type].b_plus_const_16, b);
    else
      compile_error ("Internal error in compile_comparand");
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_comparand */

/****************************************************************************/

static char *compile_term (char *line, struct b *b,
                           struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;

  /* debug ((stderr, "      compile_term %s", line)); */
  p = compile_mod_operand (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_MOD) {
    compile_convert_to_int_or_long (result1, b);
    p = compile_mod_operand (p2, b, &result2);
    compile_convert_to_int_or_long (&result2, b);
    compile_match_type (result1, &result2, FALSE, b);
    if (!result1->on_stack && !result2.on_stack)
      switch (result2.type) {
        case TYPE_INT:
          if (result2.value.i == 0)
            compile_error ("MOD of constant 0!");
          result1->value.i %= result2.value.i;
          break;
        case TYPE_LONG:
          if (result2.value.l == 0)
            compile_error ("MOD of constant 0!");
          result1->value.l %= result2.value.l;
          break;
        default:
          break;
      }
    else
      compile_push_and_operate (result1, &result2, FALSE,
                                type_info[result2.type].b_mod,
                                type_info[result2.type].b_mod_const,
                                type_info[result2.type].b_mod_const_16,
                                type_info[result2.type].b_swap_mod,
                                type_info[result2.type].b_swap_mod_const,
                                type_info[result2.type].b_swap_mod_const_16, b);
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_term */

/****************************************************************************/

static char *compile_mod_operand (char *line, struct b *b,
                                  struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;

  /* debug ((stderr, "      compile_mod_operand %s", line)); */
  p = compile_idivide_operand (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_IDIVIDE) {
    compile_convert_to_int_or_long (result1, b);
    p = compile_idivide_operand (p2, b, &result2);
    compile_convert_to_int_or_long (&result2, b);
    compile_match_type (result1, &result2, FALSE, b);
    if (!result1->on_stack && !result2.on_stack)
      switch (result2.type) {
        case TYPE_INT:
          if (result2.value.i == 0)
            compile_error ("Integer divide by constant 0!");
          result1->value.i /= result2.value.i;
          break;
        case TYPE_LONG:
          if (result2.value.l == 0)
            compile_error ("Integer divide by constant 0!");
          result1->value.l /= result2.value.l;
          break;
        default:
          break;
      }
    else
      compile_push_and_operate (result1, &result2, FALSE,
                                type_info[result2.type].b_idivide,
                                type_info[result2.type].b_idivide_const,
                                type_info[result2.type].b_idivide_const_16,
                                type_info[result2.type].b_swap_idivide,
                                type_info[result2.type].b_swap_idivide_const,
                                type_info[result2.type].b_swap_idivide_const_16, b);
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_mod_operand */

/****************************************************************************/

static char *compile_idivide_operand (char *line, struct b *b,
                                      struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;

  /* debug ((stderr, "      compile_idivide_operand %s", line)); */
  p = compile_factor (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_TIMES || token == TOKEN_DIVIDE) {
    compile_check_numeric (result1->type);
    if (token == TOKEN_DIVIDE)
      compile_convert_to_single_or_double (result1, b);
    p = compile_factor (p2, b, &result2);
    compile_check_numeric (result2.type);
    compile_match_type (result1, &result2, (token == TOKEN_TIMES), b);
    if (token == TOKEN_TIMES)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_INT:
            result1->value.i *= result2.value.i;
            break;
          case TYPE_LONG:
            result1->value.l *= result2.value.l;
            break;
          case TYPE_SINGLE:
            result1->value.s *= result2.value.s;
            break;
          case TYPE_DOUBLE:
            result1->value.d *= result2.value.d;
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, TRUE,
                                  type_info[result2.type].b_times,
                                  type_info[result2.type].b_times_const,
                                  type_info[result2.type].b_times_const_16,
                                  type_info[result2.type].b_times,
                                  type_info[result2.type].b_times_const,
                                  type_info[result2.type].b_times_const_16, b);
    else if (token == TOKEN_DIVIDE)
      if (!result1->on_stack && !result2.on_stack)
        switch (result2.type) {
          case TYPE_SINGLE:
            if (result2.value.s == 0.0)
              compile_error ("Divide by constant 0.0!");
            result1->value.s /= result2.value.s;
            break;
          case TYPE_DOUBLE:
            if (result2.value.d == 0.0)
              compile_error ("Divide by constant 0.0!");
            result1->value.d /= result2.value.d;
            break;
          default:
            break;
        }
      else
        compile_push_and_operate (result1, &result2, FALSE,
                                  type_info[result2.type].b_divide,
                                  type_info[result2.type].b_divide_const,
                                  type_info[result2.type].b_divide_const_16,
                                  type_info[result2.type].b_swap_divide,
                                  type_info[result2.type].b_swap_divide_const,
                                  type_info[result2.type].b_swap_divide_const_16, b);
    else
      compile_error ("Internal error in compile_term");
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_idivide_operand */

/****************************************************************************/

static char *compile_factor (char *line, struct b *b,
                             struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct result_type result2;

  /* debug ((stderr, "      compile_factor %s", line)); */

  p = compile_power_operand (line, b, result1);
  p2 = get_token (p, &token, NULL);
  while (token == TOKEN_POWER) {
    compile_check_numeric (result1->type);
    p = compile_power_operand (p2, b, &result2);
    compile_check_numeric (result2.type);
    compile_match_type (result1, &result2, FALSE, b);
    if (!result1->on_stack && !result2.on_stack)
      switch (result2.type) {
        case TYPE_INT:
          result1->value.s = (T_SINGLE)pow ((double)result1->value.i,
                                       (double)result2.value.i);
          break;
        case TYPE_LONG:
          result1->value.s = (T_SINGLE)pow ((double)result1->value.l,
                                        (double)result2.value.l);
          break;
        case TYPE_SINGLE:
          result1->value.s = (T_SINGLE)pow ((double)result1->value.s,
                                          (double)result2.value.s);
          break;
        case TYPE_DOUBLE:
          result1->value.d = pow (result1->value.d, result2.value.d);
          break;
        default:
          break;
      }
    else
      compile_push_and_operate (result1, &result2, FALSE,
                                type_info[result2.type].b_power,
                                type_info[result2.type].b_power_const,
                                type_info[result2.type].b_power_const_16,
                                type_info[result2.type].b_swap_power,
                                type_info[result2.type].b_swap_power_const,
                                type_info[result2.type].b_swap_power_const_16, b);
    if (result1->type != TYPE_DOUBLE)
      result1->type = TYPE_SINGLE;
    p2 = get_token (p, &token, NULL);
  }
  return p;
} /* compile_factor */

/****************************************************************************/

static char *compile_power_operand (char *line, struct b *b,
                                    struct result_type *result1)
{
  enum token_type token;
  char *p, *p2;
  struct token_info_type token_info;
  struct var_node *var;
  struct result_type result2;
  BCODE bcode;
  T_STRING str;
  int i;
  static const T_SIZE_T zero = 0;
  char *deffn_string = NULL;

  /* debug ((stderr, "      compile_power_operand %s", line)); */

  if ((deffn_string = my_malloc (MAX_TOKEN_LENGTH+10)) == NULL)
    compile_error ("Out of memory allocating %d bytes", MAX_TOKEN_LENGTH+10);

  p2 = get_token (line, &token, NULL);
  if (token == TOKEN_PLUS || token == TOKEN_MINUS) {
    p = compile_factor (p2, b, result1);
    compile_check_numeric (result1->type);
    if (token == TOKEN_MINUS) {
      if (!result1->on_stack) {
        switch (result1->type) {
          case TYPE_INT:
            result1->value.i = -result1->value.i;
            break;
          case TYPE_LONG:
            result1->value.l = -result1->value.l;
            break;
          case TYPE_SINGLE:
            result1->value.s = -result1->value.s;
            break;
          case TYPE_DOUBLE:
            result1->value.d = -result1->value.d;
            break;
          default:
            break;
        }
      } else {
        gen_bcode (type_info[result1->type].b_unary_minus, &b->b);
      }
    }
    return p;
  }

  p = get_token (line, &token, &token_info);

  switch (token) {

    case TOKEN_ID:
      compile_check_numeric_or_string (token_info.token_type);
      result1->type = token_info.token_type;
      p2 = get_token (p, &token, NULL);
      if (token == TOKEN_LBRACKET) {
        p = compile_subscripts (p2, &token_info, &var, &result2, b);
        if (!result2.on_stack)
          gen_bcode_size_t (type_info[result1->type].b_push_var,
                            type_info[result1->type].b_push_var_16,
                            var->offset + result2.value.index, &b->b);
        else
          gen_bcode_size_t (type_info[result1->type].b_push_indirect_var,
                            type_info[result1->type].b_push_indirect_var_16,
                            var->offset, &b->b);
      } else {
        token_info.dim = 0;
        if ((var = lookup_var (&arg_root[token_info.token_type], &token_info,
                               FALSE, b)) == NULL)
          var = lookup_var (&var_root[token_info.token_type], &token_info,
                            TRUE, b);
        gen_bcode_size_t (type_info[result1->type].b_push_var,
                          type_info[result1->type].b_push_var_16, var->offset,
                          &b->b);
      }
      result1->on_stack = TRUE;
      break;

    case TOKEN_STRING:
    case TOKEN_NUMBER:
      compile_check_numeric_or_string (token_info.token_type);
      result1->type = token_info.token_type;
      switch (result1->type) {
        case TYPE_INT:
          result1->value.i = token_info.value.i;
          break;
        case TYPE_LONG:
          result1->value.l = token_info.value.l;
          break;
        case TYPE_SINGLE:
          result1->value.s = token_info.value.s;
          break;
        case TYPE_DOUBLE:
          result1->value.d = token_info.value.d;
          break;
        case TYPE_STRING:
          result1->value.str = token_info.value.str;
          break;
        default:
          compile_error ("Expecting numeric or string, found %s instead",
                         type_info[result1->type].type_name);
          break;
      }
      result1->on_stack = FALSE;
      break;

    case TOKEN_FUNCTION:
      result1->type = token_info.token_type;
      token_info.dim = 0;
      var = lookup_var (&function_root[result1->type], &token_info, FALSE, b);
      if (var == NULL)
        compile_error ("Function %s is not defined!", token_info.token_string);
      if (var->nargs > 0) {
        for (i = 0; i < var->nargs; i++) {
          if (i == 0)
            p = check_token (p, TOKEN_LBRACKET, &token_info,
                             "Left bracket expected at %s",
                             token_info.token_string);
          else
            p = check_token (p, TOKEN_COMMA, &token_info,
                             "Comma expected at %s",
                             token_info.token_string);
          p = compile_expression (p, b, &result2);
          compile_convert_type (&result2, var->arg[i].type, b);
          compile_push_result (&result2, &b->b);
          gen_bcode_size_t (type_info[var->arg[i].type].b_pop,
                            type_info[var->arg[i].type].b_pop_16,
                            var->arg[i].offset, &b->b);
        }
        if (var->nargs > 0)
          p = check_token (p, TOKEN_RBRACKET, &token_info,
                           "Right bracket expected at %s",
                           token_info.token_string);
      }
      gen_bcode (B_GOSUB, &b->b);
      sprintf (deffn_string, "_%s_%d", var->var_name, var->var_type);
      goto_list_root = add_goto_node (goto_list_root, FALSE, b->b.offset,
                                      deffn_string);
      gen_size_t (zero, &b->b);
      b->b.history_count = 0;
      result1->on_stack = TRUE;
      break;

    case TOKEN_LBRACKET:
      p = compile_expression (p, b, result1);
      p = check_token (p, TOKEN_RBRACKET, &token_info,
                       "Right bracket expected at %s",
                       token_info.token_string);
      break;

    case TOKEN_SIN:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            result1->value.s = (T_SINGLE)sin ((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.d = sin (result1->value.d);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_sin, &b->b);
      break;

    case TOKEN_COS:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            result1->value.s = (T_SINGLE)cos ((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.d = cos (result1->value.d);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_cos, &b->b);
      break;

    case TOKEN_TAN:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            result1->value.s = (T_SINGLE)tan ((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.d = tan (result1->value.d);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_tan, &b->b);
      break;

    case TOKEN_ATN:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            result1->value.s = (T_SINGLE)atan ((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.d = atan (result1->value.d);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_atn, &b->b);
      break;

    case TOKEN_LOG:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            if (result1->value.s <= 0.0)
              compile_error ("log of constant negative or 0.0!");
            result1->value.s = (T_SINGLE)log ((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            if (result1->value.d <= 0.0)
              compile_error ("log of constant negative or 0.0!");
            result1->value.d = log (result1->value.d);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_log, &b->b);
      break;

    case TOKEN_LOG2:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            if (result1->value.s <= 0.0)
              compile_error ("log of constant negative or 0.0!");
            result1->value.s = (T_SINGLE)(log ((double)result1->value.s) /
                                        log(2.0));
            break;
          case TYPE_DOUBLE:
            if (result1->value.d <= 0.0)
              compile_error ("log of constant negative or 0.0!");
            result1->value.d = log (result1->value.d) / log (2.0);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_log2, &b->b);
      break;

    case TOKEN_LOG10:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            if (result1->value.s <= 0.0)
              compile_error ("log of constant negative or 0.0!");
            result1->value.s = (T_SINGLE)(log ((double)result1->value.s) /
                                        log (10.0));
            break;
          case TYPE_DOUBLE:
            if (result1->value.d <= 0.0)
              compile_error ("log of constant negative or 0.0!");
            result1->value.d = log (result1->value.d) / log (10.0);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_log10, &b->b);
      break;

    case TOKEN_EXP:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            result1->value.s = (T_SINGLE)exp ((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.d = exp (result1->value.d);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_exp, &b->b);
      break;

    case TOKEN_SQR:
      p = compile_single_argument (p, b, result1);
      compile_convert_to_single_or_double (result1, b);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_SINGLE:
            if (result1->value.s <= 0.0)
              compile_error ("SINGLE square root of negative number!");
            result1->value.s = (T_SINGLE)sqrt ((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            if (result1->value.d <= 0.0)
              compile_error ("DOUBLE square root of negative number!");
            result1->value.d = sqrt (result1->value.d);
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_sqr, &b->b);
      break;

    case TOKEN_ABS:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            if (result1->value.i < 0)
              result1->value.i = - result1->value.i;
            break;
          case TYPE_LONG:
            if (result1->value.l < 0)
              result1->value.l = - result1->value.l;
            break;
          case TYPE_SINGLE:
            if (result1->value.s < 0.0)
              result1->value.s = - result1->value.s;
            break;
          case TYPE_DOUBLE:
            if (result1->value.d < 0.0)
              result1->value.d = - result1->value.d;
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_abs, &b->b);
      break;

    case TOKEN_RND:
      get_token (p, &token, NULL);
      if (token == TOKEN_LBRACKET) {
        p = compile_single_argument (p, b, result1);
        compile_convert_type (result1, TYPE_LONG, b);
      } else {
        result1->type = TYPE_LONG;
        result1->value.l = 1;
        result1->on_stack = FALSE;
      }
      compile_push_result (result1, &b->b);
      gen_bcode (B_SINGLE_RND, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_SINGLE;
      break;

    case TOKEN_INT:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            break;
          case TYPE_LONG:
            break;
          case TYPE_SINGLE:
            result1->value.s = (T_SINGLE)floor((double)result1->value.s);
            break;
          case TYPE_DOUBLE:
            result1->value.d = (T_DOUBLE)floor((double)result1->value.d);
            break;
          default:
            break;
        }
      else
        if (result1->type != TYPE_INT && result1->type != TYPE_LONG)
          gen_bcode (type_info[result1->type].b_int, &b->b);
      break;

    case TOKEN_CINT:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            break;
          case TYPE_LONG:
            result1->value.i = (T_INT)result1->value.l;
            break;
          case TYPE_SINGLE:
            result1->value.i = (T_INT)floor((double)(result1->value.s + 0.5f));
            break;
          case TYPE_DOUBLE:
            result1->value.i = (T_INT)floor((double)(result1->value.d + 0.5));
            break;
          default:
            break;
        }
      else
        if (result1->type != TYPE_INT)
          gen_bcode (type_info[result1->type].b_cint, &b->b);
      result1->type = TYPE_INT;
      break;

    case TOKEN_CLNG:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            result1->value.l = (T_LONG)result1->value.i;
            break;
          case TYPE_LONG:
            break;
          case TYPE_SINGLE:
            result1->value.l = (T_LONG)floor((double)(result1->value.s + 0.5f));
            break;
          case TYPE_DOUBLE:
            result1->value.l = (T_LONG)floor((double)(result1->value.d + 0.5));
            break;
          default:
            break;
        }
      else
        if (result1->type != TYPE_LONG)
          gen_bcode (type_info[result1->type].b_clng, &b->b);
      result1->type = TYPE_LONG;
      break;

    case TOKEN_CSNG:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            result1->value.s = (T_SINGLE)result1->value.i;
            break;
          case TYPE_LONG:
            result1->value.s = (T_SINGLE)result1->value.l;
            break;
          case TYPE_SINGLE:
            break;
          case TYPE_DOUBLE:
            result1->value.s = (T_SINGLE)result1->value.d;
            break;
          default:
            break;
        }
      else
        if (result1->type != TYPE_SINGLE)
          gen_bcode (type_info[result1->type].b_csng, &b->b);
      result1->type = TYPE_SINGLE;
      break;

    case TOKEN_CDBL:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            result1->value.d = (T_DOUBLE)result1->value.i;
            break;
          case TYPE_LONG:
            result1->value.d = (T_DOUBLE)result1->value.l;
            break;
          case TYPE_SINGLE:
            result1->value.d = (T_DOUBLE)result1->value.s;
            break;
          case TYPE_DOUBLE:
            break;
          default:
            break;
        }
      else
        if (result1->type != TYPE_DOUBLE)
          gen_bcode (type_info[result1->type].b_cdbl, &b->b);
      result1->type = TYPE_DOUBLE;
      break;

    case TOKEN_FIX:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            break;
          case TYPE_LONG:
            result1->value.i = (T_INT)result1->value.l;
            break;
          case TYPE_SINGLE:
            result1->value.i = (T_INT)result1->value.s;
            break;
          case TYPE_DOUBLE:
            result1->value.i = (T_INT)result1->value.d;
            break;
          default:
            break;
        }
      else
        if (result1->type != TYPE_INT)
          gen_bcode (type_info[result1->type].b_fix, &b->b);
      result1->type = TYPE_INT;
      break;

    case TOKEN_SGN:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      if (!result1->on_stack)
        switch (result1->type) {
          case TYPE_INT:
            if (result1->value.i > 0)
              result1->value.i = 1;
            else if (result1->value.i < 0)
              result1->value.i = -1;
            else
              result1->value.i = 0;
            break;
          case TYPE_LONG:
            if (result1->value.l > 0)
              result1->value.i = 1;
            else if (result1->value.l < 0)
              result1->value.i = -1;
            else
              result1->value.i = 0;
            break;
          case TYPE_SINGLE:
            if (result1->value.s > (T_SINGLE)0.0)
              result1->value.i = 1;
            else if (result1->value.s < (T_SINGLE)0.0)
              result1->value.i = -1;
            else
              result1->value.i = 0;
            break;
          case TYPE_DOUBLE:
            if (result1->value.d > 0.0)
              result1->value.i = 1;
            else if (result1->value.d < 0.0)
              result1->value.i = -1;
            else
              result1->value.i = 0;
            break;
          default:
            break;
        }
      else
        gen_bcode (type_info[result1->type].b_sgn, &b->b);
      result1->type = TYPE_INT;
      break;

    case TOKEN_LEN:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      if (!result1->on_stack) {
        str = result1->value.str;
        result1->value.l = str->length;
        str = free_string (str);
      } else
        gen_bcode (B_STRING_LEN, &b->b);
      result1->type = TYPE_LONG;
      break;

    case TOKEN_LEFT:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_expression (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_COMMA, NULL,
                       "Comma expected");
      p = compile_expression (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_RBRACKET, NULL,
                       "Right bracket expected");
      gen_bcode (B_STRING_LEFT, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_MID:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_expression (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_COMMA, NULL,
                       "Comma expected");
      p = compile_expression (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      p = get_token (p, &token, NULL);
      if (token == TOKEN_COMMA) {
        p = compile_expression (p, b, result1);
        compile_convert_type (result1, TYPE_LONG, b);
        compile_push_result (result1, &b->b);
        p = get_token (p, &token, NULL);
        bcode = B_STRING_MID3;
      } else
        bcode = B_STRING_MID2;
      if (token != TOKEN_RBRACKET)
        compile_error ("Right bracket expected");
      gen_bcode (bcode, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_RIGHT:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_expression (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_COMMA, NULL,
                       "Comma expected");
      p = compile_expression (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_RBRACKET, NULL,
                     "Right bracket expected");
      gen_bcode (B_STRING_RIGHT, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_INSTR:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_expression (p, b, result1);
      if (type_info[result1->type].type_numeric) {
        compile_convert_type (result1, TYPE_LONG, b);
        compile_push_result (result1, &b->b);
        p = check_token (p, TOKEN_COMMA, NULL,
                         "Comma expected");
        p = compile_expression (p, b, result1);
        bcode = B_STRING_INSTR3;
      } else
        bcode = B_STRING_INSTR2;
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_COMMA, NULL,
                       "Comma expected");
      p = compile_expression (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_RBRACKET, NULL,
                       "Right bracket expected");
      gen_bcode (bcode, &b->b);
      result1->type = TYPE_LONG;
      break;

    case TOKEN_RINSTR:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_expression (p, b, result1);
      if (type_info[result1->type].type_numeric) {
        compile_convert_type (result1, TYPE_LONG, b);
        compile_push_result (result1, &b->b);
        p = check_token (p, TOKEN_COMMA, NULL,
                         "Comma expected");
        p = compile_expression (p, b, result1);
        bcode = B_STRING_RINSTR3;
      } else
        bcode = B_STRING_RINSTR2;
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_COMMA, NULL,
                       "Comma expected");
      p = compile_expression (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_RBRACKET, NULL,
                       "Right bracket expected");
      gen_bcode (bcode, &b->b);
      result1->type = TYPE_LONG;
      break;

    case TOKEN_CHR:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_INT, b);
      if (!result1->on_stack) {
        if ((str = alloc_string (1, NULL)) == NULL)
          compile_error ("Out of memory for string!");
        str->body[0] = (char)result1->value.i;
        result1->value.str = str;
      } else
        gen_bcode (B_STRING_CHR, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_ASC:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      if (!result1->on_stack) {
        str = result1->value.str;
        if (str->length > 0)
          result1->value.i = (T_INT)str->body[0];
        else
          result1->value.i = 0;
        str = free_string (str);
      } else
        gen_bcode (B_STRING_ASC, &b->b);
      result1->type = TYPE_INT;
      break;

    case TOKEN_VAL:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_VAL, &b->b);
      result1->type = TYPE_DOUBLE;
      break;

    case TOKEN_STR:
      p = compile_single_argument (p, b, result1);
      compile_check_numeric (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (type_info[result1->type].b_str, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_MKI:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_INT, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_MKI, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_MKL:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_MKL, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_MKS:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_SINGLE, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_MKS, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_MKD:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_DOUBLE, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_MKD, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_CVI:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_INT_CVI, &b->b);
      result1->type = TYPE_INT;
      break;

    case TOKEN_CVL:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_LONG_CVL, &b->b);
      result1->type = TYPE_LONG;
      break;

    case TOKEN_CVS:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_SINGLE_CVS, &b->b);
      result1->type = TYPE_SINGLE;
      break;

    case TOKEN_CVD:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_DOUBLE_CVD, &b->b);
      result1->type = TYPE_DOUBLE;
      break;

    case TOKEN_BIN:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_BIN, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_OCT:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_OCT, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_HEX:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_HEX, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_LCASE:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_LCASE, &b->b);
      break;

    case TOKEN_UCASE:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_UCASE, &b->b);
      break;

    case TOKEN_TRIM:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_TRIM, &b->b);
      break;

    case TOKEN_LTRIM:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_LTRIM, &b->b);
      break;

    case TOKEN_RTRIM:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_RTRIM, &b->b);
      break;

    case TOKEN_STRING_DOLLAR:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_expression (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      p = check_token (p, TOKEN_COMMA, NULL,
                       "Comma expected");
      p = compile_expression (p, b, result1);
      if (result1->type == TYPE_STRING) {
        compile_push_result (result1, &b->b);
        gen_bcode (B_STRING_STRING, &b->b);
      } else {
        compile_convert_type (result1, TYPE_INT, b);
        compile_push_result (result1, &b->b);
        gen_bcode (B_STRING_STRING2, &b->b);
      }
      p = check_token (p, TOKEN_RBRACKET, NULL,
                       "Right bracket expected");
      result1->type = TYPE_STRING;
      break;

    case TOKEN_SPACE:
      p = compile_single_argument (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_SPACE, &b->b);
      result1->type = TYPE_STRING;
      break;

    case TOKEN_GETENV:
      p = compile_single_argument (p, b, result1);
      compile_check_string (result1->type);
      compile_push_result (result1, &b->b);
      gen_bcode (B_STRING_GETENV, &b->b);
      break;

    case TOKEN_LBOUND:
      compile_error ("Sorry, LBOUND is not implemented yet!");
      break;

    case TOKEN_UBOUND:
      compile_error ("Sorry, UBOUND is not implemented yet!");
      break;

    case TOKEN_DATE:
      gen_bcode (B_STRING_DATE, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_STRING;
      break;

    case TOKEN_TIME:
      gen_bcode (B_STRING_TIME, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_STRING;
      break;

    case TOKEN_COMMAND:
      p2 = get_token (p, &token, NULL);
      if (token == TOKEN_LBRACKET) {
        p = compile_single_argument (p, b, result1);
        compile_convert_type (result1, TYPE_INT, b);
        compile_push_result (result1, &b->b);
        gen_bcode (B_STRING_COMMAND1, &b->b);
      } else {
        gen_bcode (B_STRING_COMMAND, &b->b);
      }
      result1->on_stack = TRUE;
      result1->type = TYPE_STRING;
      break;

    case TOKEN_COMMANDCOUNT:
      gen_bcode (B_INT_COMMANDCOUNT, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_INT;
      break;

    case TOKEN_TIMER:
      gen_bcode (B_DOUBLE_TIMER, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_DOUBLE;
      break;

    case TOKEN_ERR:
      gen_bcode (B_ERR, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_INT;
      break;

    case TOKEN_ERL:
      gen_bcode (B_ERL, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_INT;
      break;

    case TOKEN_EOF:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_file_number (p, b, &token_info, "EOF", &var);
      gen_bcode_size_t (B_SET_SECONDARY_FILE, B_SET_SECONDARY_FILE_16,
                        var->offset, &b->b);
      p = check_token (p, TOKEN_RBRACKET, NULL,
                       "Right bracket expected");
      gen_bcode (B_EOF, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_INT;
      break;

    case TOKEN_LOF:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_file_number (p, b, &token_info, "LOF", &var);
      gen_bcode_size_t (B_SET_SECONDARY_FILE, B_SET_SECONDARY_FILE_16,
                        var->offset, &b->b);
      p = check_token (p, TOKEN_RBRACKET, NULL,
                       "Right bracket expected");
      gen_bcode (B_LOF, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_LONG;
      break;

    case TOKEN_POS:
      p2 = get_token (p, &token, NULL);
      if (token == TOKEN_LBRACKET) {
        p = compile_file_number (p2, b, &token_info, "POS", &var);
        gen_bcode_size_t (B_SET_SECONDARY_FILE, B_SET_SECONDARY_FILE_16,
                          var->offset, &b->b);
        p = check_token (p, TOKEN_RBRACKET, NULL,
                         "Right bracket expected");
      } else
        gen_bcode (B_SET_SECONDARY_OUTPUT_FILE, &b->b);
      gen_bcode (B_POS, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_LONG;
      break;

    case TOKEN_INPUT_DOLLAR:
      p = check_token (p, TOKEN_LBRACKET, NULL,
                       "Left bracket expected");
      p = compile_expression (p, b, result1);
      compile_convert_type (result1, TYPE_LONG, b);
      compile_push_result (result1, &b->b);
      p = get_token (p, &token, NULL);
      if (token == TOKEN_COMMA) {
        p = compile_file_number (p, b, &token_info, "INPUT$", &var);
        gen_bcode_size_t (B_SET_SECONDARY_FILE, B_SET_SECONDARY_FILE_16,
                          var->offset, &b->b);
        p = get_token (p, &token, NULL);
      } else
        gen_bcode (B_SET_SECONDARY_INPUT_FILE, &b->b);
      if (token != TOKEN_RBRACKET)
        compile_error ("Right bracket or comma expected in INPUT$");
      gen_bcode (B_INPUT_DOLLAR, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_STRING;
      break;

    case TOKEN_INKEY_DOLLAR:
      gen_bcode (B_INKEY_DOLLAR, &b->b);
      result1->on_stack = TRUE;
      result1->type = TYPE_STRING;
      break;

    default:
      compile_error ("Variable or constant expected in expression at %s",
                     token_info.token_string);

  }

  deffn_string = my_free (deffn_string);

  return p;
} /* compile_power_operand */

/****************************************************************************/

static char *compile_single_argument (char *p, struct b *b,
                                      struct result_type *result1)
{
  /* debug ((stderr, "      compile_single_argument %s", p)); */
  p = check_token (p, TOKEN_LBRACKET, NULL, "Left bracket expected");
  p = compile_expression (p, b, result1);
  p = check_token (p, TOKEN_RBRACKET, NULL, "Right bracket expected");
  return p;
} /* compile_single_argument */

/****************************************************************************/

static char *compile_subscripts (char *p, struct token_info_type *token_info,
                                 struct var_node **var,
                                 struct result_type *result, struct b *b)
{
  int dim;
  struct result_type result2;
  enum token_type token;
  T_SIZE_T index;

  token_info->dim = 0;
  *var = lookup_var (&array_root[token_info->token_type], token_info, FALSE, b);
  if (*var == NULL) {
    /*compile_error ("Array %s used before dimensioned!", (*var)->var_name);*/
    if (b->option_base == -1)
      b->option_base = 0;
    token_info->dim_size[token_info->dim++] = 10; /* implicit DIM var(10) */
    *var = lookup_var (&array_root[token_info->token_type], token_info, TRUE,
                      b);
    (*var)->count = 1;
  }
  result->value.index = (T_SIZE_T)0;
  result->on_stack = FALSE;
  dim = 0;
  do { /* loop over dimensions */
    if (dim > 0) { /* if not first dimension */
      if (!result->on_stack)
        result->value.index *= (*var)->factor[dim];
      else
        gen_bcode_size_t (B_INDEX_TIMES_CONST, B_INDEX_TIMES_CONST_16,
                          (*var)->factor[dim], &b->b);
    }
    if (dim >= (*var)->dim)
      compile_error ("Wrong number of dimensions for array %s",
                     (*var)->var_name);
    p = compile_expression (p, b, &result2);
    compile_check_numeric (result2.type);
    if (!result2.on_stack && !result->on_stack) {
      switch (result2.type) {
        case TYPE_INT:
          index = (T_SIZE_T)result2.value.i;
          break;
        case TYPE_LONG:
          index = (T_SIZE_T)result2.value.l;
          break;
        case TYPE_SINGLE:
          index = (T_SIZE_T)result2.value.s;
          break;
        case TYPE_DOUBLE:
          index = (T_SIZE_T)result2.value.d;
          break;
        default:
          index = -1;
          break;
      }
      index -= b->option_base;
      if ((result->value.index += index) < 0 || index > (*var)->dim_size[dim++])
        compile_error ("Array index out of bounds error");
    } else {
      compile_push_result (&result2, &b->b);
      if (dim == 0) {
        if (b->option_base == 0) {
          gen_bcode_size_t (type_info[result2.type].b_convert_to_index,
                            type_info[result2.type].b_convert_to_index_16,
                            (*var)->dim_size[dim++], &b->b);
        } else {
          gen_bcode_size_t (type_info[result2.type].b_convert_to_index_minus_1,
                            type_info[result2.type].b_convert_to_index_minus_1_16,
                            (*var)->dim_size[dim++], &b->b);
        }
      } else {
        if (!result->on_stack)
          gen_bcode_size_t (B_PUSH_CONST_INDEX, B_PUSH_CONST_INDEX_16,
                            result->value.index - b->option_base, &b->b);
        if (b->option_base == 0) {
          gen_bcode_size_t (type_info[result2.type].b_convert_to_index_plus,
                            type_info[result2.type].b_convert_to_index_plus_16,
                            (*var)->dim_size[dim++], &b->b);
        } else {
          gen_bcode_size_t (type_info[result2.type].b_convert_to_index_plus_minus_1,
                            type_info[result2.type].b_convert_to_index_plus_minus_1_16,
                            (*var)->dim_size[dim++], &b->b);
        }
      }
      result->on_stack = TRUE;
    }
    p = get_token (p, &token, NULL);
  } while (token == TOKEN_COMMA);
  if (dim != (*var)->dim)
    compile_error ("Wrong number of dimensions for array %s",
                   (*var)->var_name);
  if (token != TOKEN_RBRACKET)
    compile_error ("Comma or close bracket expected at %s",
                   token_info->token_string);
  return p;
} /* compile_subscripts */

/****************************************************************************/

static char *compile_file_number (char *p, struct b *b,
                                  struct token_info_type *token_info,
                                  char *where, struct var_node **var)
{
  enum token_type token;
  struct result_type result;
  char *p2 = get_token (p, &token, token_info);
  if (token == TOKEN_FILE)
    p = p2;
  else {
    p = compile_expression (p, b, &result);
    compile_convert_type (&result, TYPE_LONG, b);
    if (result.on_stack)
      compile_error ("'#filenum' expected in %s", where);
    token_info->token_type = TYPE_FILE;
    sprintf (token_info->token_string, "#%ld", result.value.l);
  }
  *var = lookup_var (&var_root[TYPE_FILE], token_info, TRUE, b);
  return p;
} /* compile_file_number */

/****************************************************************************/

static void compile_match_type (struct result_type *result1,
                                struct result_type *result2,
                                BOOL commutative, struct b *b)
{
  if (result1->type < result2->type) {
    compile_convert_type (result1, result2->type, b);
    if (!commutative && result1->on_stack && result2->on_stack)
      gen_bcode (type_info[result2->type].b_swap, &b->b);
  } else if (result2->type < result1->type)
    compile_convert_type (result2, result1->type, b);
} /* compile_match_type */

/****************************************************************************/

static void compile_push_and_operate (struct result_type *result1,
                                      struct result_type *result2,
                                      BOOL commutative, BCODE bcode,
                                      BCODE bcode_const, BCODE bcode_const_16,
                                      BCODE swap_bcode, BCODE swap_bcode_const,
                                      BCODE swap_bcode_const_16, struct b *b)
{
  BOOL need_to_swap;
  enum var_type_type type;
  union number_union *value;

#ifdef DEBUG
  if (result1->type != result2->type)
    compile_error ("Internal error, type mismatch in compile_push_and_operate");
#endif
  type = result1->type;
  need_to_swap = !commutative && !result1->on_stack && result2->on_stack;
  if (type == TYPE_STRING) {
    compile_push_result (result1, &b->b);
    compile_push_result (result2, &b->b);
    if (need_to_swap)
      gen_bcode (B_STRING_SWAP, &b->b);
    gen_bcode (bcode, &b->b);
    result1->on_stack = TRUE;
    return;
  }
  if (bcode == type_info[type].b_plus) {
    if ((result1->on_stack && !result2->on_stack) ||
        (!result1->on_stack && result2->on_stack)) {
      if (result1->on_stack && !result2->on_stack)
        value = &result2->value;
      else
        value = &result1->value;
      if (value_is_equal (type, *value, 1)) {
        gen_bcode (type_info[type].b_increment, &b->b);
        result1->on_stack = TRUE;
        return;
      }
      if (value_is_equal (type, *value, -1)) {
        gen_bcode (type_info[type].b_decrement, &b->b);
        result1->on_stack = TRUE;
        return;
      }
    } else if (bcode == type_info[type].b_minus) {
      if (result1->on_stack && !result2->on_stack) {
        value = &result2->value;
        if (value_is_equal (type, *value, 1)) {
          gen_bcode (type_info[type].b_decrement, &b->b);
          return;
        }
        if (value_is_equal (type, *value, -1)) {
          gen_bcode (type_info[type].b_increment, &b->b);
          return;
        }
      }
    }
  }
  if (result1->on_stack && !result2->on_stack) {
    gen_bcode_with_const (bcode_const, bcode_const_16, type, &result2->value,
                          &b->b);
    return;
  }
  if (!result1->on_stack && result2->on_stack) {
    gen_bcode_with_const (swap_bcode_const, swap_bcode_const_16, type,
                          &result1->value, &b->b);
    result1->on_stack = TRUE;
    return;
  }
  compile_push_result (result1, &b->b);
  compile_push_result (result2, &b->b);

  {
    int history_count = b->b.history_count;
    struct history *history = b->b.history;
    if (b->b.is_keeping_history &&
        history_count >= 1 && history_count < MAX_HISTORY &&
        (bcode == type_info[type].b_plus ||
         (bcode == type_info[type].b_minus && !need_to_swap) ||
         bcode == type_info[type].b_times) &&
        (history[history_count - 1].bcode == type_info[type].b_push_var ||
         history[history_count - 1].bcode == type_info[type].b_push_var_16)) {
      T_SIZE_T index = 0;
      if (history[history_count - 1].type == TYPE_SHORT)
        index = (T_SIZE_T)history[history_count - 1].value.sh;
      else if (history[history_count - 1].type == TYPE_SIZE_T)
        index = history[history_count - 1].value.index;
      else
        compile_error ("Internal error in compile_push_and_operate()");
      debug ((stderr, "\n      |      Deleting the last one BCODE"));
      debug ((stderr, "\n      |      Seeking to offset: %zx",
              history[history_count - 1].offset));
      b->b.offset = history[history_count - 1].offset;
      if (mseek (b->b.f, b->b.offset, SEEK_SET))
        compile_error ("Error seeking in BCODE file");
      --b->b.history_count;
      if (bcode == type_info[type].b_plus)
        gen_bcode_size_t (type_info[type].b_plus_var,
                          type_info[type].b_plus_var_16, index, &b->b);
      else if (bcode == type_info[type].b_minus)
        gen_bcode_size_t (type_info[type].b_minus_var,
                          type_info[type].b_minus_var_16, index, &b->b);
      else if (bcode == type_info[type].b_times)
        gen_bcode_size_t (type_info[type].b_times_var,
                          type_info[type].b_times_var_16, index, &b->b);
      else
        compile_error ("Internal error in compile_push_and_operate()");
      result1->on_stack = TRUE;
      return;
    }
  }

  if (need_to_swap)
    gen_bcode (swap_bcode, &b->b);
  else
    gen_bcode (bcode, &b->b);
  result1->on_stack = TRUE;
} /* compile_push_and_operate */

/****************************************************************************/

static void compile_push_const (enum var_type_type type,
                                union number_union *value, struct file *b)
{
  if (value_is_equal (type, *value, 0)) {
    gen_bcode (type_info[type].b_push_const_zero, b);
  } else if (value_is_equal (type, *value, 1)) {
    gen_bcode (type_info[type].b_push_const_one, b);
  } else {
    gen_bcode_with_const (type_info[type].b_push_const,
                          type_info[type].b_push_const_16, type, value, b);
  }
} /* compile_push_const */

/****************************************************************************/

static void gen_bcode_with_const (BCODE bcode, BCODE bcode_16,
                                  enum var_type_type type,
                                  union number_union *value, struct file *b)
{
  switch (type) {
    case TYPE_INT:
      if (value->i >= -32768 && value->i <= 32767) {
        gen_bcode (bcode_16, b);
        gen_short ((T_SHORT)value->i, b);
        return;
      }
      break;
    case TYPE_LONG:
      if (value->l >= -32768 && value->l <= 32767) {
        gen_bcode (bcode_16, b);
        gen_short ((T_SHORT)value->l, b);
        return;
      }
      break;
    case TYPE_SINGLE:
      if (value->s == (T_SINGLE)floor((double)value->s) &&
	  value->s >= -32768.0f && value->s < 32768.0f) {
        gen_bcode (bcode_16, b);
        gen_short ((T_SHORT)value->s, b);
        return;
      }
      break;
    case TYPE_DOUBLE:
      if (value->d == (T_DOUBLE)floor((double)value->d) &&
          value->d >= -32768.0 && value->d < 32768.0) {
        gen_bcode (bcode_16, b);
        gen_short ((T_SHORT)value->d, b);
        return;
      }
      break;
    default:
      break;
  }
  gen_bcode (bcode, b);
  switch (type) {
    case TYPE_INT:
      gen_int (value->i, b);
      break;
    case TYPE_LONG:
      gen_long (value->l, b);
      break;
    case TYPE_SINGLE:
      gen_single (value->s, b);
      break;
    case TYPE_DOUBLE:
      gen_double (value->d, b);
      break;
    case TYPE_STRING:
      gen_string (value->str, b);
      break;
    default:
      compile_error ("Expecting numeric or string type, found %s instead",
                     type_info[type].type_name);
      break;
  }
} /* gen_bcode_with_const */

/****************************************************************************/

static void compile_push_result (struct result_type *result, struct file *b)
{
  if (!result->on_stack) {
    compile_push_const (result->type, &result->value, b);
    result->on_stack = TRUE;
  }
} /* compile_push_result */

/****************************************************************************/

static BOOL value_is_equal (enum var_type_type type, union number_union value,
                            int number)
{
  return (type == TYPE_INT && value.i == number) ||
    (type == TYPE_LONG && value.l == number) ||
    (type == TYPE_SINGLE && value.s == (T_SINGLE)number) ||
    (type == TYPE_DOUBLE && value.d == (T_DOUBLE)number);
} /* value_is_equal */

/****************************************************************************/

static void compile_check_numeric (enum var_type_type type)
{
  if (!type_info[type].type_numeric)
    compile_error ("Expecting numeric type, found %s instead",
                   type_info[type].type_name);
} /* compile_check_numeric */

/****************************************************************************/

static void compile_check_numeric_or_string (enum var_type_type type)
{
  if (!type_info[type].type_numeric && type != TYPE_STRING)
    compile_error ("Expecting numeric or string type, found %s instead",
                   type_info[type].type_name);
}

/****************************************************************************/

static void compile_check_string (enum var_type_type type)
{
  if (type != TYPE_STRING)
    compile_error ("Expecting string type, found %s instead",
                   type_info[type].type_name);
} /* compile_check_numeric_or_string */

/****************************************************************************/

static void compile_convert_to_int_or_long (struct result_type *result,
                                            struct b *b)
{
  compile_check_numeric (result->type);
  if (!type_info[result->type].type_integer)
    compile_convert_type (result, TYPE_LONG, b);
} /* compile_convert_to_int_or_long */

/****************************************************************************/

static void compile_convert_to_single_or_double (struct result_type *result,
                                                 struct b *b)
{
  compile_check_numeric (result->type);
  if (!type_info[result->type].type_float)
    compile_convert_type (result, TYPE_SINGLE, b);
} /* compile_convert_to_single_or_double */

/****************************************************************************/

static void compile_convert_type (struct result_type *src,
                                  enum var_type_type dst_type, struct b *b)
{
  if (!src->on_stack) {
    switch (dst_type) {
      case TYPE_INT:
        switch (src->type) {
          case TYPE_INT:
            break;
          case TYPE_LONG:
            src->value.i = (T_INT)src->value.l;
            break;
          case TYPE_SINGLE:
            src->value.i = (T_INT)floor((double)(src->value.s + 0.5f));
            break;
          case TYPE_DOUBLE:
            src->value.i = (T_INT)floor((double)(src->value.d + 0.5));
            break;
          default:
            compile_error ("Unable to convert type %s to %s",
                           type_info[src->type].type_name,
                           type_info[dst_type].type_name);
        }
        break;
      case TYPE_LONG:
        switch (src->type) {
          case TYPE_INT:
            src->value.l = (T_LONG)src->value.i;
            break;
          case TYPE_LONG:
            break;
          case TYPE_SINGLE:
            src->value.l = (T_LONG)floor((double)(src->value.s + 0.5f));
            break;
          case TYPE_DOUBLE:
            src->value.l = (T_LONG)floor((double)(src->value.d + 0.5));
            break;
          default:
            compile_error ("Unable to convert type %s to %s",
                           type_info[src->type].type_name,
                           type_info[dst_type].type_name);
        }
        break;
      case TYPE_SINGLE:
        switch (src->type) {
          case TYPE_INT:
            src->value.s = (T_SINGLE)src->value.i;
            break;
          case TYPE_LONG:
            src->value.s = (T_SINGLE)src->value.l;
            break;
          case TYPE_SINGLE:
            break;
          case TYPE_DOUBLE:
            src->value.s = (T_SINGLE)src->value.d;
            break;
          default:
            compile_error ("Unable to convert type %s to %s",
                           type_info[src->type].type_name,
                           type_info[dst_type].type_name);
        }
        break;
      case TYPE_DOUBLE:
        switch (src->type) {
          case TYPE_INT:
            src->value.d = (T_DOUBLE)src->value.i;
            break;
          case TYPE_LONG:
            src->value.d = (T_DOUBLE)src->value.l;
            break;
          case TYPE_SINGLE:
            src->value.d = (T_DOUBLE)src->value.s;
            break;
          case TYPE_DOUBLE:
            break;
          default:
            compile_error ("Unable to convert type %s to %s",
                           type_info[src->type].type_name,
                           type_info[dst_type].type_name);
        }
        break;
      default:
        if (src->type != dst_type)
          compile_error ("Unable to convert type %s to %s",
                         type_info[src->type].type_name,
                         type_info[dst_type].type_name);
    }
  } else {
    if (type_info[src->type].b_convert[dst_type] == B_ILLEGAL)
      compile_error ("Unable to convert type %s to %s",
                     type_info[src->type].type_name,
                     type_info[dst_type].type_name);
    if (type_info[src->type].b_convert[dst_type] != B_NOP)
      gen_bcode (type_info[src->type].b_convert[dst_type], &b->b);
  }
  src->type = dst_type;
} /* compile_convert_type */

/****************************************************************************/

static char *check_token (char *line, enum token_type token_to_check_for,
                          struct token_info_type *token_info,
                          char *errmsg, ...)
{
  enum token_type token;
  va_list arglist;

  line = get_token (line, &token, token_info);
  if (token != token_to_check_for) {
    va_start (arglist, errmsg);
    vcompile_error (errmsg, arglist);
    va_end (arglist);
  }
  return line;
} /* check_token */

/****************************************************************************/
/*   T O K E N   P A R S E R   */

#define COPY_P_TO_T {                                     \
    if (++token_length >= MAX_TOKEN_LENGTH)               \
      compile_error ("Identifier or number too long");    \
    *t++ = toupper(*p);                                   \
    p++; }

char *get_token (char *line, enum token_type *token,
                 struct token_info_type *token_info)
{
  char *p, *p2, *t, *t2 = NULL;
  int token_length;
  enum token_type ctoken;
  enum var_type_type token_type;
  int ndigits;
  long int value;
  char token_string[MAX_TOKEN_LENGTH];

  p = line;
  t = token_string;
  token_length = 0;
  token_type = (enum var_type_type)(-1);
  while (*p == ' ' || *p == '\t')
    p++;
  if (*p == '\0' || *p == '\n' || *p == '\r') {
    *token = TOKEN_NONE;
  } else if ((*p >= 'A' && *p <= 'Z') || (*p >= 'a' && *p <= 'z') || *p == '_') {
    *token = TOKEN_ID;
    COPY_P_TO_T;
    while ((*p >= 'A' && *p <= 'Z') || (*p >= 'a' && *p <= 'z') ||
           (*p >= '0' && *p <= '9') || *p == '.' || *p == '_') {
      COPY_P_TO_T;
    }
    if (*p == '%')
      token_type = TYPE_INT;
    else if (*p == '&')
      token_type = TYPE_LONG;
    else if (*p == '!')
      token_type = TYPE_SINGLE;
    else if (*p == '#')
      token_type = TYPE_DOUBLE;
    else if (*p == '$')
      token_type = TYPE_STRING;
    else {
      token_type = token_type_array[token_string[0] - 'A'];
      p--;
    }
    p++;
    *t = '\0';
    if (token_string[0] == 'F' &&
        token_string[1] == 'N' &&
        token_string[2] != '\0') {
      *token = TOKEN_FUNCTION;
    } else {
      for (ctoken = FIRST_TOKEN; ctoken <= LAST_TOKEN; ctoken++)
        if (strcmp (token_string, token_table[ctoken].token_string) == 0 &&
            ((token_table[ctoken].dollar && token_type == TYPE_STRING) ||
             (!token_table[ctoken].dollar && *(p-1) != '$'))) {
          *token = ctoken;
          break;
        }
    }
  } else if ((*p >= '0' && *p <= '9') || *p == '.') {
    ndigits = 0;
    *token = TOKEN_NUMBER;
    while (*p >= '0' && *p <= '9') {
      COPY_P_TO_T;
      ndigits++;
    }
    *t = '\0';
    if (ndigits < 6 &&
        atol (token_string) >= 0L &&
        atol (token_string) <= 32767L)
      token_type = TYPE_INT;
    else
      token_type = TYPE_LONG;
    if (*p == '.') {
      COPY_P_TO_T;
      while (*p >= '0' && *p <= '9') {
        COPY_P_TO_T;
        ndigits++;
      }
      if (ndigits > 6)
        token_type = TYPE_DOUBLE;
      else
        token_type = TYPE_SINGLE;
    }
    if (*p == 'E' || *p == 'e' || *p == 'D' || *p == 'd') {
      if (*p == 'D' || *p == 'd' || ndigits > 6)
        token_type = TYPE_DOUBLE;
      else
        token_type = TYPE_SINGLE;
      COPY_P_TO_T;
      if (*p == '-' || *p == '+') {
        COPY_P_TO_T;
      }
      while (*p >= '0' && *p <= '9') {
        COPY_P_TO_T;
      }
    }
    if (*p == '%')
      token_type = TYPE_INT;
    else if (*p == '&')
      token_type = TYPE_LONG;
    else if (*p == '!')
      token_type = TYPE_SINGLE;
    else if (*p == '#')
      token_type = TYPE_DOUBLE;
    else
      p--;
    p++;
    if (token_info != NULL) {
      *t = '\0';
      switch (token_type) {
        case TYPE_INT:
          token_info->value.i = (T_INT)atoi (token_string);
          break;
        case TYPE_LONG:
          token_info->value.l = (T_LONG)atol (token_string);
          break;
        case TYPE_SINGLE:
          token_info->value.s = (T_SINGLE)atof (token_string);
          break;
        case TYPE_DOUBLE:
          token_info->value.d = (T_DOUBLE)atof (token_string);
          break;
        default:
          break;
      }
    }
  } else if (*p == '&') {
    *token = TOKEN_NUMBER;
    COPY_P_TO_T;
    if (*p == 'H' || *p == 'h') {
      COPY_P_TO_T;
    }
    ndigits = 0;
    if (token_info != NULL)
      t2 = t;
    while ((*p >= '0' && *p <= '9') ||
           (*p >= 'A' && *p <= 'F') ||
           (*p >= 'a' && *p <= 'f')) {
      COPY_P_TO_T;
      ndigits++;
    }
    if (ndigits <= 4)
      token_type = TYPE_INT;
    else
      token_type = TYPE_LONG;
    if (token_info != NULL) {
      *t = '\0';
      sscanf (t2, "%lx", &value);
      if (token_type == TYPE_INT)
        token_info->value.i = (T_INT)value;
      else
        token_info->value.l = (T_LONG)value;
    }
  } else if (*p == '"') {
    *token = TOKEN_STRING;
    token_type = TYPE_STRING;
    if (token_info != NULL)
      if (++token_length < MAX_TOKEN_LENGTH)
        *t++ = *p;
    p++;
    p2 = p;
    while (*p != '"' && *p != '\0' && *p != '\n' && *p != '\r') {
      if (token_info != NULL)
        if (++token_length < MAX_TOKEN_LENGTH)
          *t++ = *p;
      p++;
    }
    if (*p != '"')
      compile_error ("Unterminated string: %s!", p2 - 1);
    if (token_info != NULL) {
      if (++token_length < MAX_TOKEN_LENGTH)
        *t++ = *p;
      if ((token_info->value.str = alloc_string (p - p2, p2)) == NULL)
        compile_error ("Out of memory for string!");
    }
    p++;
  } else if (*p == '#') {
    *token = TOKEN_FILE;
    token_type = TYPE_FILE;
    COPY_P_TO_T;
    while (*p >= '0' && *p <= '9') {
      COPY_P_TO_T;
    }
    if (token_info != NULL)
      token_info->dim = 0;
  } else {
    switch (*p++) {
      case ':':
        *token = TOKEN_COLON;
        break;
      case ';':
        *token = TOKEN_SEMICOLON;
        break;
      case ',':
        *token = TOKEN_COMMA;
        break;
      case '\'':
        *token = TOKEN_QUOTE;
        break;
      case '=':
        *token = TOKEN_EQUAL;
        break;
      case '+':
        *token = TOKEN_PLUS;
        break;
      case '-':
        *token = TOKEN_MINUS;
        break;
      case '*':
        if (*p == '*') {
          *token = TOKEN_POWER;
          p++;
        } else
          *token = TOKEN_TIMES;
        break;
      case '/':
        *token = TOKEN_DIVIDE;
        break;
      case '\\':
        *token = TOKEN_IDIVIDE;
        break;
      case '^':
        *token = TOKEN_POWER;
        break;
      case '<':
        if (*p == '>') {
          *token = TOKEN_NOTEQUAL;
          p++;
        } else if (*p == '=') {
          *token = TOKEN_LESSTHANOREQUAL;
          p++;
        } else
          *token = TOKEN_LESSTHAN;
        break;
      case '>':
        if (*p == '=') {
          *token = TOKEN_GREATERTHANOREQUAL;
          p++;
        } else
          *token = TOKEN_GREATERTHAN;
        break;
      case '(':
        *token = TOKEN_LBRACKET;
        break;
      case ')':
        *token = TOKEN_RBRACKET;
        break;
      case '?':
        *token = TOKEN_QUESTIONMARK;
        break;
      default:
        *token = TOKEN_OTHER;
        break;
    }
  }
  if (token_info != NULL) {
    token_info->token_type = token_type;
    *t = '\0';
    strcpy (token_info->token_string, token_string);
  }
  return p;
} /* get_token */

/****************************************************************************/
/*   V A R I A B L E S   T A B L E   L O O K U P   ( A N D   U P D A T E )   */

static struct var_node *lookup_var (struct var_node **p,
                                    struct token_info_type *token_info,
                                    BOOL create_new_entry,
                                    struct b *b)
{
  int cmp, dim;
  T_SIZE_T size;

  if (*p == NULL) {
    if (create_new_entry) {
      if ((*p = (struct var_node *)my_malloc (sizeof(struct var_node))) == NULL)
        compile_error ("Out of memory for %s!", token_info->token_string);
      (*p)->left = NULL;
      (*p)->right = NULL;
      if (((*p)->var_name = (char *)my_malloc(strlen(token_info->token_string) + 1))
          == NULL)
        compile_error ("Out of memory for %s!", token_info->token_string);
      strcpy ((*p)->var_name, token_info->token_string);
      (*p)->var_type = token_info->token_type;
      if ((*p)->var_type == TYPE_LABEL) {
        /*debug ((stderr, "    Adding %s to label table, code offset = %zu\n",
                token_info->token_string, b->b.offset));*/
        (*p)->offset = b->b.offset;
        (*p)->data_offset = b->data.offset;
      } else {
        /*debug ((stderr, "    Adding %s to %s table at %zu\n",
                token_info->token_string, type_info[(*p)->var_type].type_name,
                vars_size[(*p)->var_type]));*/
        (*p)->dim = token_info->dim;
        size = 1;
        for (dim = token_info->dim - 1; dim >= 0; --dim) {
          (*p)->dim_size[dim] = token_info->dim_size[dim];
          size *= (token_info->dim_size[dim] + 1);
          /*(*p)->factor[dim] = size;*/
          (*p)->factor[dim] = token_info->dim_size[dim] + 1;
        }
        (*p)->offset = vars_size[(*p)->var_type];
        vars_size[(*p)->var_type] += size;
      }
      (*p)->count = 0;                /* count of references */
      (*p)->nargs = 0;
      (*p)->arg = NULL;
    }
    return *p;
  } else {
    cmp = strcmp (token_info->token_string, (*p)->var_name);
    if (cmp < 0)
      return lookup_var (&(*p)->left, token_info, create_new_entry, b);
    else if (cmp > 0)
      return lookup_var (&(*p)->right, token_info, create_new_entry, b);
    else
      return *p;
  }
} /* lookup_var */

/****************************************************************************/

static void *free_tree (struct var_node *p)
{
  if (p != NULL) {
    p->left = free_tree (p->left);
    p->right = free_tree (p->right);
    if (p->var_name != NULL)
      p->var_name = my_free (p->var_name);
    if (p->arg != NULL)
      p->arg = my_free ((char *)p->arg);
    p = my_free ((char *)p);
  }
  return NULL;
} /* free_tree */

/****************************************************************************/

static void free_const_strings (void)
{
  size_t result;
  T_STRING str = NULL;

  mrewind (mtrack);
  do {
    result = mread ((char *)&str, sizeof(str), 1, mtrack);
#ifdef DEBUG
    if (str == NULL)
      printf ("free_static_string: %d %p\n", (int)result, str);
    else if (str->body == NULL)
      printf ("free_static_string: %d %p %p\n", (int)result, str, str->body);
    else
      printf ("free_static_string: %d %p %p \"%.*s\"\n", (int)result, str,
              str->body, (int)str->length, str->body);
#endif
    if (result != 0 && str != NULL)
      str = free_string (str);
  } while (result != 0);
} /* free_const_strings */

/****************************************************************************/

static void gen_bcode_size_t (BCODE bcode, BCODE bcode_16, T_SIZE_T s,
                              struct file *b)
{
  if (bcode_16 != B_ILLEGAL && (s & ~0x7fff) == 0) {
    gen_bcode (bcode_16, b);
    gen_short ((T_SHORT)(s & 0x7fff), b);
  } else {
    gen_bcode (bcode, b);
    gen_size_t (s, b);
  }
} /* gen_bcode_size_t */

/****************************************************************************/

static void gen_bcode (BCODE bcode, struct file *b)
{
  T_SHORT ccode = (T_SHORT)bcode;
  debug ((stderr, "\n  %4zx|  %-36.36s ", b->offset, bcode_table[bcode]));
  if (b->is_keeping_history) {
    if (b->history_count < MAX_HISTORY) {
      b->history[b->history_count].type = TYPE_NONE;
      b->history[b->history_count].bcode = bcode;
      b->history[b->history_count++].offset = b->offset;
    }
  }
#ifdef THREADED
  if (b->is_keeping_history) {
    /* convert raw to threaded bcode */
    threaded_bcode_type threaded_bcode =
      (threaded_bcode_type)static_bcode_labels[ccode];
    write_b ((char *)&threaded_bcode, SIZE_THREADED_BCODE, ALIGN_THREADED_BCODE,
             b);
  } else
    write_b ((char *)&ccode, SIZE_BCODE, ALIGN_BCODE, b);
#else
  write_b ((char *)&ccode, SIZE_BCODE, ALIGN_BCODE, b);
#endif
} /* gen_bcode */

/****************************************************************************/

static void gen_size_t (T_SIZE_T s, struct file *b)
{
  if (b->is_keeping_history) {
    b->history[b->history_count - 1].type = TYPE_SIZE_T;
    b->history[b->history_count - 1].value.index = s;
  }
  write_b ((char *)&s, SIZE_SIZE_T, ALIGN_SIZE_T, b);
} /* gen_size_t */

/****************************************************************************/

static void gen_short (T_SHORT sh, struct file *b)
{
  if (b->is_keeping_history) {
    b->history[b->history_count - 1].type = TYPE_SHORT;
    b->history[b->history_count - 1].value.sh = sh;
  }
  write_b ((char *)&sh, SIZE_SHORT, ALIGN_SHORT, b);
} /* gen_short */

/****************************************************************************/

static void gen_int (T_INT i, struct file *b)
{
  if (b->is_keeping_history) {
    b->history[b->history_count - 1].type = TYPE_INT;
    b->history[b->history_count - 1].value.i = i;
  }
  write_b ((char *)&i, SIZE_INT, ALIGN_INT, b);
} /* gen_int */

/****************************************************************************/

static void gen_long (T_LONG l, struct file *b)
{
  if (b->is_keeping_history) {
    b->history[b->history_count - 1].type = TYPE_LONG;
    b->history[b->history_count - 1].value.l = l;
  }
  write_b ((char *)&l, SIZE_LONG, ALIGN_LONG, b);
} /* gen_long */

/****************************************************************************/

static void gen_single (T_SINGLE s, struct file *b)
{
  if (b->is_keeping_history) {
    b->history[b->history_count - 1].type = TYPE_SINGLE;
    b->history[b->history_count - 1].value.s = s;
  }
  write_b ((char *)&s, SIZE_SINGLE, ALIGN_SINGLE, b);
} /* gen_single */

/****************************************************************************/

static void gen_double (double d, struct file *b)
{
  if (b->is_keeping_history) {
    b->history[b->history_count - 1].type = TYPE_DOUBLE;
    b->history[b->history_count - 1].value.d = d;
  }
  write_b ((char *)&d, SIZE_DOUBLE, ALIGN_DOUBLE, b);
} /* gen_double */

/****************************************************************************/

static void gen_string (T_STRING str, struct file *b)
{
  if (b->is_keeping_history) {
    b->history[b->history_count - 1].type = TYPE_STRING;
    b->history[b->history_count - 1].value.str = str;
  }
  write_b ((char *)&str, SIZE_STRING, ALIGN_STRING, b);
  mwrite_check ((char *)&str, sizeof(str), mtrack);
} /* gen_string */

/****************************************************************************/

static void write_b (char *buff, size_t size, size_t align, struct file *b)
{
#ifdef DEBUG
  unsigned char *d;
  int i;

  d = (unsigned char *)buff;
  fprintf (stderr, "  ");
  for (i = 0; i < size; i++)
    fprintf (stderr, "%02x", *d++);
#endif
  pad_align (align, b);
  mwrite_check (buff, size, b->f);
  b->offset += size;
} /* write_b */

/****************************************************************************/

#ifndef NOALIGN
static void pad_align (size_t align, struct file *b)
{
  size_t pad;
  static const T_LONG zeros[4] = {0L, 0L, 0L, 0L};

  if ((pad = (align - 1) - ((b->offset + (align - 1)) & (align - 1))) != 0) {
    debug ((stderr, "  pad%zu", pad));
    mwrite_check ((char *)&zeros, pad, b->f);
    b->offset += pad;
  }
} /* pad_align */
#endif

/****************************************************************************/

static void mwrite_check (char *buff, size_t size, MFILE *f)
{
  if (mwrite (buff, 1, size, f) != size)
    compile_error ("Error writing temporary file");
} /* mwrite_check */

/****************************************************************************/
/****************************************************************************/

T_STRING alloc_string (size_t length, char *body)
{
  T_STRING str;

  if ((str = (T_STRING)my_malloc (sizeof(struct string_descr))) != NULL) {
    str->length = length;
    str->ref_count = 1;
    str->allocated_length = str->length > 0 ? str->length : 1;
    if ((str->body = (char *)my_malloc (str->allocated_length)) == NULL) {
      str = my_free ((char *)str);
      return NULL;
    }
    if (body != NULL)
      memcpy (str->body, body, length);
  }
  return str;
} /* alloc_string */

/****************************************************************************/

T_STRING free_string (T_STRING str)
{
  if (str != NULL)
    if (--str->ref_count == 0) {
      if (str->body != NULL)
        str->body = my_free (str->body);
      str = my_free ((char *)str);
    }
  return NULL;
} /* free_string */

/****************************************************************************/
/****************************************************************************/

#ifndef NOALIGN
char *malloc_align (size_t size, size_t align)
{
  char *p, *p2;

  if ((p = my_malloc(SIZE_SIZE_T + size + align - 1)) != NULL) {
    p2 = (char *)(((T_SIZE_T)(p + SIZE_SIZE_T + (align - 1))) & ~(align - 1));
    *(char **)(p2 - SIZE_SIZE_T) = p;
    return p2;
  }  else
    return p;
} /* malloc_align */
#endif

/****************************************************************************/

#ifndef NOALIGN
void *free_align (char *p)
{
  if (p == NULL)
    return NULL;
  p = my_free (*(char **)(p - SIZE_SIZE_T));
  return p;
} /* free_align */
#endif

/****************************************************************************/
/****************************************************************************/
